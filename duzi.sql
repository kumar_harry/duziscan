-- phpMyAdmin SQL Dump
-- version 4.6.5.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Nov 13, 2017 at 01:36 PM
-- Server version: 10.1.21-MariaDB
-- PHP Version: 5.6.30

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `duzi`
--

-- --------------------------------------------------------

--
-- Table structure for table `duziscan_cart`
--

CREATE TABLE `duziscan_cart` (
  `cart_id` int(100) NOT NULL,
  `product_type` varchar(100) NOT NULL,
  `product_name` varchar(100) NOT NULL,
  `product_price` varchar(100) NOT NULL,
  `user_id` varchar(100) NOT NULL,
  `quantity` varchar(100) NOT NULL,
  `total_amount` varchar(100) NOT NULL,
  `product_image` varchar(100) NOT NULL,
  `style` varchar(200) NOT NULL,
  `fabric` varchar(200) NOT NULL,
  `manq` varchar(200) NOT NULL,
  `manneColor` varchar(200) NOT NULL,
  `isRead` enum('0','1') NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

--
-- Dumping data for table `duziscan_cart`
--

INSERT INTO `duziscan_cart` (`cart_id`, `product_type`, `product_name`, `product_price`, `user_id`, `quantity`, `total_amount`, `product_image`, `style`, `fabric`, `manq`, `manneColor`, `isRead`) VALUES
(15, 'Swimsuit', 'Swimsuit847100', '100', '81', '1', '100', 'http://www.scan2fit.com/duziscan/images/lady.png', 'StyleOne', 'fabric0', 'Yes', '2d2d2d', '0');

-- --------------------------------------------------------

--
-- Table structure for table `duziscan_orders`
--

CREATE TABLE `duziscan_orders` (
  `order_id` int(11) NOT NULL,
  `order_date` text NOT NULL,
  `order_state` varchar(100) NOT NULL,
  `order_payment_status` varchar(100) NOT NULL,
  `order_number` varchar(100) NOT NULL,
  `order_total` varchar(100) NOT NULL,
  `order_user_id` int(50) NOT NULL,
  `paid_amount` varchar(100) NOT NULL,
  `product_type` varchar(100) NOT NULL,
  `order_txn_id` text NOT NULL,
  `order_currency_code` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

--
-- Dumping data for table `duziscan_orders`
--

INSERT INTO `duziscan_orders` (`order_id`, `order_date`, `order_state`, `order_payment_status`, `order_number`, `order_total`, `order_user_id`, `paid_amount`, `product_type`, `order_txn_id`, `order_currency_code`) VALUES
(7, '1504700861', 'open', 'Success', '397009', '107.25', 83, '107.25', '', '', ''),
(8, '1504772915', 'open', 'Success', '332575', '107.25', 81, '107.25', '', '', ''),
(9, '1504850408', 'open', 'Pending', '312201', '107.25', 81, '0.00', '', '', '');

-- --------------------------------------------------------

--
-- Table structure for table `duziscan_orders_detail`
--

CREATE TABLE `duziscan_orders_detail` (
  `det_id` int(100) NOT NULL,
  `det_order_id` varchar(100) NOT NULL,
  `det_price` varchar(100) NOT NULL,
  `det_quantity` varchar(100) NOT NULL,
  `det_user_id` varchar(100) NOT NULL,
  `det_product_type` varchar(100) NOT NULL,
  `det_total_amount` varchar(100) NOT NULL,
  `det_product_name` varchar(100) NOT NULL,
  `det_style` varchar(100) NOT NULL,
  `det_fabric` varchar(100) NOT NULL,
  `det_manq` varchar(100) NOT NULL,
  `det_manneColor` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

--
-- Dumping data for table `duziscan_orders_detail`
--

INSERT INTO `duziscan_orders_detail` (`det_id`, `det_order_id`, `det_price`, `det_quantity`, `det_user_id`, `det_product_type`, `det_total_amount`, `det_product_name`, `det_style`, `det_fabric`, `det_manq`, `det_manneColor`) VALUES
(1, '9', '100', '1', '81', 'Swimsuit', '100', 'Swimsuit49100', 'StyleOne', 'fabric0', 'Yes', '2d2d2d');

-- --------------------------------------------------------

--
-- Table structure for table `duziscan_users`
--

CREATE TABLE `duziscan_users` (
  `user_id` int(100) NOT NULL,
  `email` varchar(100) NOT NULL,
  `password` varchar(100) NOT NULL,
  `fname` varchar(100) NOT NULL,
  `lname` varchar(100) NOT NULL,
  `gender` varchar(100) NOT NULL,
  `dob` varchar(100) NOT NULL,
  `height` varchar(100) NOT NULL,
  `weight` varchar(100) NOT NULL,
  `user_profile` varchar(1000) NOT NULL,
  `stylist` varchar(100) NOT NULL,
  `address` varchar(100) NOT NULL,
  `pincode` varchar(100) NOT NULL,
  `city` varchar(100) NOT NULL,
  `state` varchar(100) NOT NULL,
  `country` text NOT NULL,
  `contact` varchar(100) NOT NULL,
  `status` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

--
-- Dumping data for table `duziscan_users`
--

INSERT INTO `duziscan_users` (`user_id`, `email`, `password`, `fname`, `lname`, `gender`, `dob`, `height`, `weight`, `user_profile`, `stylist`, `address`, `pincode`, `city`, `state`, `country`, `contact`, `status`) VALUES
(81, 'sbsunilbhatia9@gmail.com', '12345678', 'Sunil', 'Bhatia', 'Male', '14/07/1994', '381', '70.00', 'http://scan2fit.com/size/api/Files/users/File285266.png', 'kapil', 'address', '144514', 'Jujuy', 'Select State', 'Guam', '977', '1'),
(83, 'harry@gmail.com', '12345678', 'harry', 'k', 'Female', '12/12/1993', 'NaN', '23716', 'http://www.scan2fit.com/duziscan//api/Files/users/File231414.jpg', '45', 'nawan shahr', '11', 'aa', 'Saint George', 'Bermuda', '9779784701', '1'),
(85, 'abc@gmail.com', '123456', 'aa', 'bb', 'Male', '11/01/2017', '64', '12', '', 'styl', 'adreess', '144', 'undefined', 'Saint George', 'Antigua And Barbuda', '977', '1');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `duziscan_cart`
--
ALTER TABLE `duziscan_cart`
  ADD PRIMARY KEY (`cart_id`);

--
-- Indexes for table `duziscan_orders`
--
ALTER TABLE `duziscan_orders`
  ADD PRIMARY KEY (`order_id`);

--
-- Indexes for table `duziscan_orders_detail`
--
ALTER TABLE `duziscan_orders_detail`
  ADD PRIMARY KEY (`det_id`);

--
-- Indexes for table `duziscan_users`
--
ALTER TABLE `duziscan_users`
  ADD PRIMARY KEY (`user_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `duziscan_cart`
--
ALTER TABLE `duziscan_cart`
  MODIFY `cart_id` int(100) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;
--
-- AUTO_INCREMENT for table `duziscan_orders`
--
ALTER TABLE `duziscan_orders`
  MODIFY `order_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;
--
-- AUTO_INCREMENT for table `duziscan_orders_detail`
--
ALTER TABLE `duziscan_orders_detail`
  MODIFY `det_id` int(100) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `duziscan_users`
--
ALTER TABLE `duziscan_users`
  MODIFY `user_id` int(100) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=86;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
