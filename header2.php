<?php
session_start();
?>
<!DOCTYPE html>
<html lang="en">
<head>
<title>DUZI SCAN</title>
<!-- for-mobile-apps -->
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="keywords" content="Sauna Responsive web template, Bootstrap Web Templates, Flat Web Templates, Android Compatible web template, 
Smartphone Compatible web template, free webdesigns for Nokia, Samsung, LG, SonyEricsson, Motorola web design" />
<script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false);
		function hideURLbar(){ window.scrollTo(0,1); } </script>
<!-- //for-mobile-apps -->
<link href="css/bootstrap.css" rel="stylesheet" type="text/css" media="all" />
<!-- pop-up -->
<link href="css/popuo-box.css" rel="stylesheet" type="text/css" media="all" />
<!-- //pop-up -->
<link rel="stylesheet" type="text/css" href="css/zoomslider.css" />
<link rel="stylesheet" type="text/css" href="css/style.css" />
<link rel="stylesheet" type="text/css" href="css/duzi-scan.css" />
<link href="css/font-awesome.css" rel="stylesheet"> 
<script type="text/javascript" src="js/modernizr-2.6.2.min.js"></script>
<!--/web-fonts-->
<link href='//fonts.googleapis.com/css?family=Tangerine:400,700' rel='stylesheet' type='text/css'>
<link href="//fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i" rel="stylesheet">
<link href='//fonts.googleapis.com/css?family=Montserrat:400,700' rel='stylesheet' type='text/css'>
<!--//web-fonts-->
<style>
    #demo-1{
        min-height: 100px!important;
        height: 100px!important;
    }
    .navbar-default .navbar-nav > li > a
    {
        color: #777;
    }
</style>
</head>
<body>
<div id="loadd" class="loader hideloader">
    <img class="loaderimg" src="images/762.gif">
    <p style="margin-left:49%;font-size:18px">Loading....</p>
</div>
<!--/main-header-->
  <!--/banner-section-->
	<div id="demo-1" class="banner-inner">
		<!--/header-w3l-->
			   <div class="header-w3-agileits" id="home">
			     <div class="inner-header-agile">	
								<nav class="navbar navbar-default">
									<div class="navbar-header">
										<button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
											<span class="sr-only">Toggle navigation</span>
											<span class="icon-bar"></span>
											<span class="icon-bar"></span>
											<span class="icon-bar"></span>
										</button>

										<img src="images/logo_duziscan.png" class="img-responsive header2-logo" >
									</div>
									<!-- navbar-header -->
									<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
				           				<ul class="nav navbar-nav">
										    <li class="home"><a href="index">Home</a></li>
											<li class="about" class="active"><a href="about">About US</a></li>
                                            <?php
                                            if(isset($_SESSION['duzi_user_id'])) {
                                                ?>
                                                <li class="order"><a href="orders">Order Now</a></li>
                                                <?php
                                            }
                                            else{
                                                ?>
                                                <li class="order" >
                                                    <a href="#" data-toggle="modal" data-target="#loginModal">Order Now</a>
                                                </li>
                                                <?php
                                            }
                                            ?>
                                            <li class="contact"><a href="contact">Contact Us</a></li>
                                            <?php
                                            if(isset($_SESSION['duzi_user_id'])) {
                                                ?>
                                                <li class="user_profile_li">
                                                    <img class="user_profile_img" src="" alt="p">
                                                </li>
                                                <li class="dropdown " style="margin-left: 0px">
                                                    <a href="#" class="dropdown-toggle hvr-bounce-to-bottom user_profile_ancor"
                                                       data-toggle="dropdown" aria-expanded="false" >
                                                        <span class="top_user_name">Sign In</span>
                                                        <b class="caret"></b>
                                                    </a>
                                                    <ul class="dropdown-menu">
                                                        <li><a href="myaccount">My Account</a></li>
                                                        <li><a href="myorders">My Orders</a></li>
                                                        <li><a href="signout">Sign Out</a></li>
                                                    </ul>
                                                </li>
                                                <li>
                                                    <div id="sb-search" onclick="rediredTo('cart')">
                                                        <span class="sb-icon-search">
                                                            <span class="bagde">0</span>
                                                        </span>
                                                    </div>
                                                </li>
                                                <?php
                                            }
                                            else{
                                                ?>
                                                <li class="signin">
                                                    <a href="#" class="header-li-a" data-toggle="modal" data-target="#loginModal">
                                                        Sign In
                                                    </a>
                                                </li>
                                                <?php
                                            }
                                            ?>
                                        </ul>
									</div>
									<div class="clearfix"> </div>	
								</nav>					
							</div> 			
		<!--//header-w3l-->
			<!--/banner-info-->
			<!--/banner-ingo-->
			
		</div>
		 </div>
  <!--/banner-section-->
 <!--//main-header-->