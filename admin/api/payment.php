<?php
include("Constants/configuration.php");
include("Constants/dbConfig.php");
include("Constants/functions.php");
require_once "Classes/ORDERS.php";
$orderClass = new \Modals\ORDERS();
$userClass = new \Modals\USERS();
$requiredFeilds = array("payment_status");
$response = RequiredFields($_REQUEST, $requiredFeilds);
if ($response[Status] != Success) {
    $orderClass->apiResponse($response);
    return false;
}
$payment_status = $_REQUEST['payment_status'];
if ($payment_status == "Success") {

    $order_id = trim($_REQUEST['order_id']);
    $paid_amount = trim($_REQUEST['paid_amount']);
    $pay_type = "Web";
    /*pay type = android for response*/
    if(isset($_REQUEST['pay_type']))
    {
        $pay_type = $_REQUEST['pay_type'];
    }
    ($response = $orderClass->paymentSuccess($order_id,$paid_amount,$pay_type));
    if ($response[Status] != Success) {
        $orderClass->apiResponse($response);
        return false;
    }
    $orderClass->apiResponse($response);
}
else if($payment_status == "Failed"){
    if(isset($_REQUEST['pay_type']))
    {
        if($_REQUEST['pay_type'] == "android" )
        {
            $response[Status] = Error;
            $response[Message] = "Payment update failed";
            $orderClass->apiResponse($response);
        }
        else
        {
            header("Location:".MainServer."payment?pmt=Failure");
        }

    }
    else
    {
        header("Location:".MainServer."payment?pmt=Failure");
    }
}

else
{
    $response[Status] = Error;
    $response[Message] = "502 UnAuthorized Access";
    $orderClass->apiResponse($response);
}
?>