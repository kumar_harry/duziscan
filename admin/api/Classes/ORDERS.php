<?php
/**
 * Created by PhpStorm.
 * User: sunil
 * Date: 3/22/2017
 * Time: 9:18 PM
 */
namespace Modals;
require_once('CONNECT.php');
require_once('USERS.php');
class ORDERS
{
    public $link = null;
    public $bookClass = null;
    public $userClass = null;
    public $response = array();
    function __construct()
    {
        $this->link = new CONNECT();
        $this->link2 = new CONNECT();
        $this->userClass = new USERS();
        $this->currentDateTime = date('d M Y h:i:s A');
        $this->currentDateTimeStamp = strtotime($this->currentDateTime);
    }
    /*public function addCoupon($coupon_code, $coupon_value, $expiry, $coupon_status)
    {
        $link = $this->link->connect();
        if ($link) {
            $query = "insert into discount_coupon (coupon_code,coupon_value,generated_on,expired_on,coupon_status) 
            VALUES ('$coupon_code','$coupon_value','$this->currentDateTimeStamp','$expiry','$coupon_status')";
            $result = mysqli_query($link, $query);
            if ($result) {
                $this->response[STATUS] = Success;
                $this->response[MESSAGE] = "New Coupon Code Added SuccessFully";
                $this->response['catId'] = $this->link->getLastId();
            } else {
                $this->response[STATUS] = Error;
                $this->response[MESSAGE] = $this->link->sqlError();
            }
        } else {
            $this->response[STATUS] = Error;
            $this->response[MESSAGE] = $this->link->sqlError();
        }
        return $this->response;
    }
    public function editCoupon($coupon_id,$coupon_code,$coupon_value,$expired_on,$coupon_status)
    {
        $link = $this->link->connect();
        if ($link) {
            $query = "update discount_coupon set coupon_code='$coupon_code',coupon_value='$coupon_value',expired_on='$expired_on',
            coupon_status='$coupon_status' where coupon_id = '$coupon_id'";
            $result = mysqli_query($link, $query);
            if ($result) {
                $this->response[STATUS] = Success;
                $this->response[MESSAGE] = "Coupon Updated SuccessFully";
                $this->response['planId'] = $coupon_id;
            } else {
                $this->response[STATUS] = Error;
                $this->response[MESSAGE] = $this->link->sqlError();
            }
        } else {
            $this->response[STATUS] = Error;
            $this->response[MESSAGE] = $this->link->sqlError();
        }
        return $this->response;
    }
    public function checkCouponExistence($coupon_code)
    {
        $link = $this->link->connect();
        if ($link) {
            $query = "select * from discount_coupon where coupon_code = '$coupon_code'";
            $result = mysqli_query($link, $query);
            if ($result) {
                $num = mysqli_num_rows($result);
                if ($num > 0) {
                    $this->response[STATUS] = Error;
                    $this->response[MESSAGE] = "Coupon Code Already Registered Please Enter Different Code";
                    $row = mysqli_fetch_array($result);
                    $this->response['couponId'] = $row['coupon_id'];
                } else {
                    $this->response[STATUS] = Success;
                    $this->response[MESSAGE] = "Valid Name";
                }
            } else {
                $this->response[STATUS] = Error;
                $this->response[MESSAGE] = $this->link->sqlError();
            }
        } else {
            $this->response[STATUS] = Error;
            $this->response[MESSAGE] = $this->link->sqlError();
        }
        return $this->response;
    }*/
    public function addToCart($user_id,$selectedstyle,$selectedfabric,$selectedmanq,$selectedmanneColor,
                              $product_name,$product_price,$quantity,$total_amount,$product_type)
    {
        $link2 = $this->link2->connect2();
        if($link2) {
            $query = "select * from duziscan_cart where user_id = '$user_id' and product_type='$product_type' and style='$selectedstyle'
            and fabric='$selectedfabric' and manq='$selectedmanq' and manneColor ='$selectedmanneColor'";
            $result = mysqli_query($this->link2->connect2(),$query);
            if ($result) {
                $num = mysqli_num_rows($result);
                if($num > 0)
                {
                    $row = mysqli_fetch_assoc($result);
                    $cart_id= $row['cart_id'];
                    $query2 = "update duziscan_cart set isRead='0',quantity = quantity+1, total_amount= total_amount+'$product_price' where 
                     cart_id = '$cart_id' and user_id = '$user_id'";
                    $result2 = mysqli_query($this->link2->connect2(),$query2);
                    if ($result2) {
                        $this->link2->response[Status] = Success;
                        $this->link2->response[Message] = "Product added successfully.";
                    }
                    else {
                        $this->link2->response[Status] = Error;
                        $this->link2->response[Message] = $this->link2->sqlError2();
                    }

                }
                else
                {
                    $query2 = "insert into duziscan_cart(product_type,product_name,product_price,user_id,quantity,total_amount,style,
                    fabric,manq,manneColor,product_image,isRead) values ('$product_type','$product_name','$product_price','$user_id','$quantity',
                    '$total_amount','$selectedstyle','$selectedfabric','$selectedmanq','$selectedmanneColor','http://www.scan2fit.com/duziscan/images/lady.png','0')";
                    $result2 = mysqli_query($this->link2->connect2(),$query2);
                    if ($result2) {
                        $this->link2->response[Status] = Success;
                        $this->link2->response[Message] = "Product added successfully.";
                    }
                    else {
                        $this->link2->response[Status] = Error;
                        $this->link2->response[Message] = $this->link2->sqlError2();
                    }

                }

            }
            else{
                $this->link2->response[Status] = Error;
                $this->link2->response[Message] = $this->link2->sqlError2();
            }
        }
        else
        {
            $this->response[STATUS] = Error;
            $this->response[MESSAGE] = $this->link2->sqlError2();
        }
        return $this->link2->response;
    }
    public function getCart($user_id,$serviceType)
    {
        $cartData = array();
        $link2 = $this->link2->connect2();
        if($link2) {
            if($serviceType == '1')
            {
                $query2 = "update duziscan_cart set isRead= '$serviceType' where user_id = '$user_id'";
                $result2 = mysqli_query($this->link2->connect2(),$query2);
            }

            $query="select * from duziscan_cart where user_id = '$user_id'";
            $result = mysqli_query($this->link2->connect2(),$query);
            if ($result) {
                $num = mysqli_num_rows($result);
                if($num>0){

                    while($row = mysqli_fetch_assoc($result))
                    {
                        $cartData[] = $row;
                    }
                    $this->link2->response[Status] = Success;
                    $this->link2->response[Message] = "Data Found";
                    $this->link2->response['cartData'] = $cartData;
                }
                else{
                    $this->link2->response[Status] = Error;
                    $this->link2->response[Message] = "Data no found";
                }
            }
            else {
                $this->link2->response[Status] = Error;
                $this->link2->response[Message] = $this->link2->sqlError2();
            }
        }
        else
        {
            $this->response[STATUS] = Error;
            $this->response[MESSAGE] = $this->link2->sqlError2();
        }
        return $this->link2->response;
    }
    public function updateCart($cart_id,$quantity,$user_id,$total)
    {
        $link2 = $this->link2->connect2();
        if($link2) {
            $query = "update duziscan_cart set quantity = '$quantity',total_amount= '$total' where cart_id = '$cart_id'";
            $result = mysqli_query($this->link2->connect2(),$query);
            if ($result) {
                $query2="select * from duziscan_cart where user_id ='$user_id'";
                $result2 = mysqli_query($link2,$query2);
                if($result2) {
                    $num = mysqli_num_rows($result2);
                    if ($num > 0) {
                        $cart_totalPrice = 0;
                        while($rows = mysqli_fetch_assoc($result2))
                        {
                            $product_price = $rows['product_price'];
                            $quantity = $rows['quantity'];
                            $productTotal = $product_price * $quantity;
                            $cart_totalPrice = $cart_totalPrice + $productTotal;
                        }
                        $this->link2->response[Status] = Success;
                        $this->link2->response[Message] = "Quantity updated successfully.";
                        $this->link2->response['cart_totalPrice'] = $cart_totalPrice;
                    }
                    else {
                        $this->link2->response[Status] = Error;
                        $this->link2->response[Message] = "No data found";
                    }
                }
                else {
                    $this->link2->response[Status] = Error;
                    $this->link2->response[Message] = $this->link2->sqlError2();
                }
            }
            else {
                $this->link2->response[Status] = Error;
                $this->link2->response[Message] = $this->link2->sqlError2();
            }
        }
        else
        {
            $this->response[STATUS] = Error;
            $this->response[MESSAGE] = $this->link2->sqlError2();
        }
        return $this->link2->response;
    }
    public function removeItem($cart_id)
    {
        $link2 = $this->link2->connect2();
        if($link2) {
            $query="select * from duziscan_cart where cart_id ='$cart_id'";
            $result = mysqli_query($link2,$query);
            if($result) {
                $num = mysqli_num_rows($result);
                if ($num > 0) {

                   $query2 = "delete from duziscan_cart where cart_id ='$cart_id'";
                    $result2 = mysqli_query($link2,$query2);
                    if($result2) {
                        $this->link2->response[Status] = Success;
                        $this->link2->response[Message] = "Item deleted successfully.";
                    }
                    else
                    {
                        $this->link2->response[Status] = Error;
                        $this->link2->response[Message] = $this->link2->sqlError2();
                    }

                }
                else {
                    $this->link2->response[Status] = Error;
                    $this->link2->response[Message] = "Invalid cart id";
                }
            }
            else {
                $this->link2->response[Status] = Error;
                $this->link2->response[Message] = $this->link2->sqlError2();
            }
        }
        else
        {
            $this->response[STATUS] = Error;
            $this->response[MESSAGE] = $this->link2->sqlError2();
        }
        return $this->link2->response;
    }
    public function clearCart($user_id)
    {
        $link2 = $this->link2->connect2();
        if($link2) {
            $query="select * from duziscan_cart where user_id='$user_id'";
            $result = mysqli_query($link2,$query);
            if($result)
            {
                $num = mysqli_num_rows($result);
                if($num>0) {
                    $query2 = "delete from duziscan_cart where user_id ='$user_id'";
                    $result2 = mysqli_query($link2,$query2);
                    if($result2) {
                        $this->link2->response[Status] = Success;
                        $this->link2->response[Message] = "Cart clear successfully.";
                    }
                    else
                    {
                        $this->link2->response[Status] = Error;
                        $this->link2->response[Message] = $this->link2->sqlError2();
                    }
                }
                else
                {
                    $this->link2->response[Status] = Error;
                    $this->link2->response[Message] = "Cart already empty";
                }

            }
            else
            {
                $this->link2->response[Status] = Error;
                $this->link2->response[Message] = $this->link2->sqlError2();
            }

        }
        else
        {
            $this->response[STATUS] = Error;
            $this->response[MESSAGE] = $this->link2->sqlError2();
        }
        return $this->link2->response;
    }
    public function orderCreate($json){
        $link2 = $this->link2->connect2();
        if($link2) {
            $json = json_decode($json);
            $userid = $json->user_id;
            $total = $json->total;
            $items = $json->items;
            $order_number = mt_rand(000000, 999999);
            $order_date = $this->currentDateTimeStamp = strtotime($this->currentDateTime);
            $query="insert into duziscan_orders(order_state,order_payment_status,order_number,order_total,
		    order_user_id,order_date,paid_amount)values('open','Pending','$order_number','$total','$userid','$order_date','0.00')";
            $result = mysqli_query($link2,$query);
            if($result)
            {
                $order_id = $this->link2->lastId2();
                for($b=0; $b< sizeof($items); $b++)
                {
                    $det_product_type = $items[$b]->product_type;
                    $det_product_name = $items[$b]->product_name;
                    $det_product_price = $items[$b]->product_price;
                    $det_quantity = $items[$b]->product_quantity;
                    $det_total_amount = $items[$b]->total_amount;
                    $det_style = $items[$b]->style;
                    $det_fabric = $items[$b]->fabric;
                    $det_manq = $items[$b]->manq;
                    $det_manneColor = $items[$b]->manneColor;
                    $query2 = "insert into duziscan_orders_detail(det_order_id,det_price,det_quantity,det_user_id,
                    det_product_type,det_total_amount,det_product_name,det_style,det_fabric,det_manq,
                    det_manneColor) values ('$order_id','$det_product_price','$det_quantity','$userid','$det_product_type',
                    '$det_total_amount','$det_product_name','$det_style','$det_fabric','$det_manq','$det_manneColor')";
                    $result2 = mysqli_query($this->link2->connect2(),$query2);
                    if ($result2) {
                        $this->link2->response[Status] = Success;
                        $this->link2->response[Message] = "Order added successfully.";
                        $this->link2->response['order_id'] = strval($order_id);;

                    }
                    else {
                        $this->link2->response[Status] = Error;
                        $this->link2->response[Message] = $this->link2->sqlError2();
                    }
                }
            }
            else
            {
                $this->link2->response[Status] = Error;
                $this->link2->response[Message] = $this->link2->sqlError2();
            }

        }
        else
        {
            $this->response[STATUS] = Error;
            $this->response[MESSAGE] = $this->link2->sqlError2();
        }
        return $this->link2->response;
    }
    public function getParticularOrderData($order_id)
    {
        $link = $this->link->connect();
        $order_detail=array();
        if($link) {
            $query="select * from orders where order_id='$order_id'";
            $result = mysqli_query($link,$query);
            if($result)
            {
                $num = mysqli_num_rows($result);
                if($num>0) {
                    $row = mysqli_fetch_assoc($result);
                    $order_id = $row['order_id'];
                    $query2 = "select * from orderdetail where order_id = '$order_id'";
                    $result2 = mysqli_query($link,$query2);
                    if($result2){
                        $num = mysqli_num_rows($result2);
                        if($num>0){
                            while($detail = mysqli_fetch_array($result2)){
                                $detail_id = $detail['item_id'];
                                $temp = $this->bookClass->getParticularBookData($detail_id);
                                $temp = $temp['bookData'];
                                $order_detail[]=array(
                                    "detail_id"=>$detail['detail_id'],
                                    "book_name"=>$temp['book_name'],
                                    "cat_id"=>$temp['cat_id'],
                                    "book_desc"=>$temp['book_desc'],
                                    "book_author"=>$temp['book_author'],
                                    "book_narrator"=>$temp['book_narrator'],
                                    "play_time"=>$temp['play_time'],
                                    "front_look"=>$temp['front_look'],
                                    "audio_file"=>$temp['audio_file'],
                                    "item_price"=>$detail['item_price']
                                );
                            }
                            $user_id = $row['order_userId'];
                            $userData = $this->userClass->getParticularUserData($user_id);
                            $userData = $userData['UserData'];
                            $order = array(
                                "order_id"=>$row['order_id'],
                                "order_number"=>$row['order_number'],
                                "transaction_id"=>$row['transaction_id'],
                                "amount"=>$row['amount'],
                                "payment_status"=>$row['payment_status'],
                                "order_dated"=>$row['order_dated'],
                                "order_userId"=>$row['order_userId'],
                                "order_user_name" => $userData['user_name'],
                                "order_user_profile" => $userData['user_profile'],
                                ImagesBaseURLKey=>ImagesBaseURL,
                                AudiosBaseURLKey=>AudiosBaseURL,
                                "order_detail"=>$order_detail
                            );
                            $this->response[STATUS] = Success;
                            $this->response[MESSAGE] = "Order Data Exist";
                        }
                        else{
                            $this->response[STATUS] = Success;
                            $this->response[MESSAGE] = "Empty Order Details";
                        }
                    }
                    else{
                        $this->response[STATUS] = Error;
                        $this->response[MESSAGE] = $this->link->sqlError();
                    }

                    $this->response[STATUS] = Success;
                    $this->response[MESSAGE] = "Order Found";
                    $this->response['order'] = $order;
                }
                else{
                    $this->response[STATUS] = Error;
                    $this->response[MESSAGE] = "Invalid Order Identification";
                }
            }
            else
            {
                $this->response[STATUS] = Error;
                $this->response[MESSAGE] = $this->link->sqlError();
            }
        }
        else
        {
            $this->response[STATUS] = Error;
            $this->response[MESSAGE] = $this->link->sqlError();
        }
        return $this->response;
    }
    public function paymentSuccess($order_id,$paid_amount,$pay_type)
    {
        $link2 = $this->link2->connect2();
        if($link2) {
            $query = "select * from  duziscan_orders where order_id = '$order_id'";
            $result = mysqli_query($this->link2->connect2(),$query);
            if($result)
            {
                $num = mysqli_num_rows($result);
                if($num > 0){
                    $query2 = "update duziscan_orders set order_payment_status = 'Success',paid_amount= '$paid_amount' where order_id = '$order_id'";
                    $result2 = mysqli_query($this->link2->connect2(),$query2);
                    if ($result2) {
                        if($pay_type == "android")
                        {
                            $this->link2->response[Status] = Success;
                            $this->link2->response[Message] = "Payment update successfull";
                        }
                        else
                        {
                            header("Location:".MainServer."payment?pmt=Success");
                        }
                    }
                    else {
                        $this->link2->response[Status] = Error;
                        $this->link2->response[Message] = $this->link2->sqlError2();
                    }
                }
                else
                {
                    $this->link2->response[Status] = Error;
                    $this->link2->response[Message] = "Order not found";
                }
            }
            else
            {
                $this->link2->response[Status] = Error;
                $this->link2->response[Message] = $this->link2->sqlError2();
            }
        }
        else
        {
            $this->response[STATUS] = Error;
            $this->response[MESSAGE] = $this->link2->sqlError2();
        }
        return $this->link2->response;
    }
    public function getCartCount($user_id){
        $link2 = $this->link2->connect2();
        if($link2) {
            $query2 = "select * from duziscan_cart where user_id = '$user_id'";
            $result2 = mysqli_query($link2, $query2);
            if ($result2) {
                $num = mysqli_num_rows($result2);
                if($num > 0)
                {
                    $this->response[STATUS] = Success;
                    $this->response[MESSAGE] = "Data found";
                    $this->response['count'] = $num;
                }
                else
                {
                    $this->response[STATUS] = Error;
                    $this->response[MESSAGE] = "No data found";
                    $this->response['count'] = 0;
                }
            }
            else
            {
                $this->response[STATUS] = Error;
                $this->response[MESSAGE] = $this->link2->sqlError2();
            }
        }
        else{
            $this->response[STATUS] = Error;
            $this->response[MESSAGE] = $this->link2->sqlError2();
        }
        return $this->response;
    }
    public function getAllOrders($userId)
    {
        $link2 = $this->link2->connect2();
        $order_detail=array();
        $order=array();
        if($link2) {
            $query="select * from duziscan_orders where order_user_id = '$userId' order by order_id desc";
            $result = mysqli_query($link2,$query);
            if($result)
            {
                $num = mysqli_num_rows($result);
                if($num>0) {
                    while($row = mysqli_fetch_assoc($result)) {
                        $order_id = $row['order_id'];
                        $query2 = "select * from duziscan_orders_detail where det_order_id = '$order_id'";
                        $result2 = mysqli_query($link2, $query2);
                        if ($result2) {

                            $num = mysqli_num_rows($result2);
                            if ($num > 0) {
                                unset($order_detail);
                                while ($detail = mysqli_fetch_assoc($result2)) {
                                    $order_detail[] = $detail;
                                }
                                $order[] = array("order_id" => $row['order_id'],
                                    "order_date" => $row['order_date'],
                                    "order_state" => $row['order_state'],
                                    "order_number" => $row['order_number'],
                                    "order_total" => $row['order_total'],
                                    "order_payment_status" => $row['order_payment_status'],
                                    "order_user_id" => $row['order_user_id'],
                                    "order_detail" => $order_detail);
                                $this->response[STATUS] = Success;
                                $this->response[MESSAGE] = "Order Details Found";
                            }
                            else{
                                $this->response[STATUS] = Success;
                                $this->response[MESSAGE] = "Empty Order Details";
                            }
                        }else {
                            $this->response[STATUS] = Error;
                            $this->response[MESSAGE] = $this->link2->sqlError2();
                        }
                    }
                    $this->response[STATUS] = Success;
                    $this->response[MESSAGE] = "Order Found";
                    $this->response['order'] = $order;
                }
                else{
                    $this->response[STATUS] = Error;
                    $this->response[MESSAGE] = "Invalid Order Identification";
                }
            }
            else
            {
                $this->response[STATUS] = Error;
                $this->response[MESSAGE] = $this->link2->sqlError2();
            }
        }
        else
        {
            $this->response[STATUS] = Error;
            $this->response[MESSAGE] = $this->link2->sqlError2();
        }
        return $this->response;
    }
    public function refundStatus($order_id, $value){
        $link = $this->link->connect();
        if($link) {
            $query = "select * from orders where order_id='$order_id'";
            $result = mysqli_query($link, $query);
            if ($result) {
                $num = mysqli_num_rows($result);
                if ($num > 0) {
                    $update = mysqli_query($link, "UPDATE orders SET refund_generated='$value' WHERE order_id='$order_id'");
                    if ($update) {
                        $this->response[STATUS] = Success;
                        $this->response[MESSAGE] = "Status Has Been Changed Successfully";
                    } else {
                        $this->response[STATUS] = Error;
                        $this->response[MESSAGE] = $this->link->sqlError();
                    }
                } else {
                    $this->response[STATUS] = Error;
                    $this->response[MESSAGE] = "UnAuthorized Access";
                }
            } else {
                $this->response[STATUS] = Error;
                $this->response[MESSAGE] = $this->link->sqlError();
            }
        }
        else{
            $this->response[STATUS] = Error;
            $this->response[MESSAGE] = "Connection Error";
        }
        return $this->response;
    }
    public function cancelOrder($order_id)
    {
        $link = $this->link->connect();
        if($link) {
            $query = "select * from orders where order_id='$order_id'";
            $result = mysqli_query($link, $query);
            if ($result) {
                $num = mysqli_num_rows($result);
                if ($num > 0) {
                    $update = mysqli_query($link, "UPDATE orders SET order_status='Cancelled' WHERE order_id='$order_id'");
                    if ($update) {
                        $this->response[STATUS] = Success;
                        $this->response[MESSAGE] = "Order Has Been Cancelled Successfully";
                    } else {
                        $this->response[STATUS] = Error;
                        $this->response[MESSAGE] = $this->link->sqlError();
                    }
                } else {
                    $this->response[STATUS] = Error;
                    $this->response[MESSAGE] = "UnAuthorized Access";
                }
            } else {
                $this->response[STATUS] = Error;
                $this->response[MESSAGE] = $this->link->sqlError();
            }
        }
        else{
            $this->response[STATUS] = Error;
            $this->response[MESSAGE] = "Connection Error";
        }
        return $this->response;
    }
    public function apiResponse($response)
    {
        header("Content-Type: application/json");
        echo json_encode($response);
    }
}