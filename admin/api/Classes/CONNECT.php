<?php
/**
 * Created by PhpStorm.
 * User: Sourav
 * Date: 06-Apr-17
 * Time: 12:56 PM
 */
namespace Modals;
class CONNECT{
    public $link = null;
    public $link2 = null;
    public $response = array();
    function __construct()
    {

        $this->link = mysqli_connect(DBHOST,DBUSER,DBPASS,DBNAME);
        $this->link2 = mysqli_connect(DBHOST,DBUSER2,DBPASS2,DBNAME2);
    }
    public function connect(){
        return $this->link;
    }
    public function connect2(){
        return $this->link2;
    }
    public function lastId(){
        return mysqli_insert_id($this->link);
    }
    public function sqlError(){
        return mysqli_error($this->link);
    }
    public function lastId2(){
        return mysqli_insert_id($this->link2);
    }
    public function sqlError2(){
        return mysqli_error($this->link2);
    }
}
?>