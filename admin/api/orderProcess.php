<?php
include("Constants/configuration.php");
include("Constants/dbConfig.php");
include("Constants/functions.php");
require_once "Classes/ORDERS.php";
$orderClass = new \Modals\ORDERS();
$userClass = new \Modals\USERS();
$requiredFeilds = array("type");
$response = RequiredFields($_POST, $requiredFeilds);
if ($response[Status] != Success) {
    $orderClass->apiResponse($response);
    return false;
}
$type = $_POST['type'];
if ($type == "addToCart") {
    $requiredFeilds = array('user_id','selectedstyle','selectedfabric','selectedmanq','selectedmanneColor','product_name',
        'product_type','product_price','quantity','total_amount');
    $response = RequiredFields($_POST, $requiredFeilds);
    if ($response[Status] != Success) {
        $orderClass->apiResponse($response);
        return false;
    }

    $user_id = trim($_POST['user_id']);
    $selectedstyle = trim($_POST['selectedstyle']);
    $selectedfabric = trim($_POST['selectedfabric']);
    $selectedmanq = trim($_POST['selectedmanq']);
    $selectedmanneColor = trim($_POST['selectedmanneColor']);
    $product_name   = trim($_POST['product_name']);
    $product_price  = trim($_POST['product_price']);
    $quantity = trim($_POST['quantity']);
    $total_amount = trim($_POST['total_amount']);
    $product_type = trim($_POST['product_type']);

    ($response = $orderClass->addToCart($user_id,$selectedstyle,$selectedfabric,$selectedmanq,$selectedmanneColor,
        $product_name,$product_price,$quantity,$total_amount,$product_type));
    if ($response[Status] != Success) {
        $orderClass->apiResponse($response);
        return false;
    }
    $orderClass->apiResponse($response);
}
else if($type == "getCart"){
    $requiredFeilds = array('user_id');
    $response = RequiredFields($_POST, $requiredFeilds);
    if ($response[Status] != Success) {
        $orderClass->apiResponse($response);
        return false;
    }
    $user_id = trim($_POST['user_id']);
    $serviceType = 0;
    if(isset($_REQUEST['serviceType']))
    {
        if($_REQUEST['serviceType'] == "android")
        {
            $serviceType = 1;
        }
    }
    ($response = $orderClass->getCart($user_id,$serviceType));
    if ($response[Status] != Success) {
        $orderClass->apiResponse($response);
        return false;
    }
    $orderClass->apiResponse($response);
}
else if($type == "updateCart"){
    $requiredFeilds = array('cart_id','quantity','user_id','price');
    $response = RequiredFields($_POST, $requiredFeilds);
    if ($response[Status] != Success) {
        $orderClass->apiResponse($response);
        return false;
    }
    $cart_id = trim($_POST['cart_id']);
    $quantity = trim($_POST['quantity']);
    $user_id = trim($_POST['user_id']);
    $price = trim($_POST['price']);
    $total = $price * $quantity;
    ($response = $orderClass->updateCart($cart_id,$quantity,$user_id,$total));
    if ($response[Status] != Success) {
        $orderClass->apiResponse($response);
        return false;
    }
    $orderClass->apiResponse($response);
}

else if($type == "removeItem"){
    $requiredFeilds = array('cart_id');
    $response = RequiredFields($_POST, $requiredFeilds);
    if ($response[Status] != Success) {
        $orderClass->apiResponse($response);
        return false;
    }
    $cart_id = trim($_POST['cart_id']);
    ($response = $orderClass->removeItem($cart_id));
    if ($response[Status] != Success) {
        $orderClass->apiResponse($response);
        return false;
    }
    $orderClass->apiResponse($response);
}
else if($type == "clearCart"){
    $requiredFeilds = array('user_id');
    $response = RequiredFields($_POST, $requiredFeilds);
    if ($response[Status] != Success) {
        $orderClass->apiResponse($response);
        return false;
    }
    $user_id = trim($_POST['user_id']);
    ($response = $orderClass->clearCart($user_id));
    if ($response[Status] != Success) {
        $orderClass->apiResponse($response);
        return false;
    }
    $orderClass->apiResponse($response);
}
else if($type == "billing"){
    $requiredFeilds = array('user_id','billing_fname','billing_lname','billing_address','country','state','city','pincode','phone');
    $response = RequiredFields($_POST, $requiredFeilds);
    if ($response[Status] != Success) {
        $orderClass->apiResponse($response);
        return false;
    }
    $user_id = trim($_POST['user_id']);
    $billing_fname = trim($_POST['billing_fname']);
    $billing_lname = trim($_POST['billing_lname']);
    $billing_address = trim($_POST['billing_address']);
    $country = trim($_POST['country']);
    $state = trim($_POST['state']);
    $city = trim($_POST['city']);
    $pincode = trim($_POST['pincode']);
    $phone = trim($_POST['phone']);
    ($response = $userClass->updateBillingAddress($user_id,$billing_fname,$billing_lname,$billing_address,$country,$state,$city,$pincode,$phone));
    if ($response[Status] != Success) {
        $userClass->apiResponse($response);
        return false;
    }
    $serviceType = 0;
    ($response = $orderClass->getCart($user_id,$serviceType));
    if ($response[Status] != Success) {
        $orderClass->apiResponse($response);
        return false;
    }
    $userClass->apiResponse($response);
}
else if($type == "getCartCount"){
    $requiredFeilds = array('user_id');
    $response = RequiredFields($_POST, $requiredFeilds);
    if ($response[Status] != Success) {
        $orderClass->apiResponse($response);
        return false;
    }
    $user_id = trim($_POST['user_id']);
    ($response = $orderClass->getCartCount($user_id));
    if ($response[Status] != Success) {
        $orderClass->apiResponse($response);
        return false;
    }
    $orderClass->apiResponse($response);
}
else if($type == "orderDetail"){
    $requiredFeilds = array('json','user_id');
    $response = RequiredFields($_POST, $requiredFeilds);
    if ($response[Status] != Success) {
        $orderClass->apiResponse($response);
        return false;
    }
    $json = trim($_POST['json']);
    $user_id = trim($_POST['user_id']);
    ($response = $orderClass->clearCart($user_id));
    if ($response[Status] != Success) {
        $orderClass->apiResponse($response);
        return false;
    }
    ($response = $orderClass->orderCreate($json));
    if ($response[Status] != Success) {
        $orderClass->apiResponse($response);
        return false;
    }
    $orderClass->apiResponse($response);
}
/*according to user get all orders*/
else if($type == "getAllOrders"){
    $requiredFeilds = array('user_id');
    $response = RequiredFields($_POST, $requiredFeilds);
    if ($response[Status] != Success) {
        $orderClass->apiResponse($response);
        return false;
    }
    $user_id = trim($_POST['user_id']);
    $response = $userClass->getParticularUserData($user_id);
    if($response[Status] == Error){
        $userClass->apiResponse($response);
        return false;
    }
    $userData = $response['userData'];
    ($response = $orderClass->getAllOrders($user_id));
    if ($response[Status] != Success) {
        $orderClass->apiResponse($response);
        return false;
    }
    $response['userData'] = $userData;
    $orderClass->apiResponse($response);
}
else
{
    $response[Status] = Error;
    $response[Message] = "502 UnAuthorized Access";
    $orderClass->apiResponse($response);
}
?>