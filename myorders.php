<?php
include("header2.php");
$paypal_url = 'https://www.paypal.com/cgi-bin/webscr'; //Test PayPal API URL
$paypal_id = 'kapillikes@gmail.com'; //Business Email
//$paypal_id = 'sksh3527@gmail.com';
 ?>
<style>
    .order_inner_box{
        -moz-border-bottom-colors: none;
        -moz-border-left-colors: none;
        -moz-border-right-colors: none;
        -moz-border-top-colors: none;
        align-items: flex-start;
        border-image: none;
        border-top: 3px solid #E00681;
        box-shadow: 1px 6px 8px lightgray;
        display: flex;
        flex-direction: column;
        margin: 5px 5px 26px;
        padding: 5px;
        position: relative;
        width: auto;
    }
    .inner_heading {
        color: green;
        font-size: 18px;
        font-weight: 700;
        margin: 8px 15px 0;
        padding: 0;
    }
    .order_outer_box{
        margin-top: 40px;
    }
    .order_payment{
        background: white none repeat scroll 0 0;
        border: 1px solid #ccc;
        border-radius: 5px;
        color: #000;
        cursor: pointer;
        font-size: 13px;
        font-weight: 600;
        margin-left: 14px;
        padding: 5px 10px;
        vertical-align: middle;
    }
    .order_payment:hover{
        background: #E00681;
        border: 1px solid #E00681;
        color: #fff;
    }
    .order_total
    {
        background: #f6921e none repeat scroll 0 0;
        border-radius: 4px;
        color: white;
        font-size: 14px;
        font-weight: 600;
        margin-bottom: -8px;
        margin-right: 20px;
        margin-top: 8px;
        padding: 3px 7px;
    }
    .order_prod_img{
        width: 30px;
    }

    .my_order_table
    {
        text-align: center;
    }
    .my_order_table tr {
        border-bottom: 1px solid #eee;
    }
    .my_order_table tr:nth-last-child(1) {
        border-bottom: none;
    }
    .table.my_order_table th{
        text-align: center !important;
    }
    .my_order_table>td{
        text-align: center;
    }
    .payment_status_th{
        position: relative;
        top: 8px;
    }
    .order_noData{
        color: #adaeaf;
        font-size: 17px;
        font-weight: bold;
        padding-bottom: 0;
        text-align: center;
    }
    .order_bag_box
    {
        text-align: center;
    }

    .order-bag {
        filter: grayscale(1);
        -webkit-filter: grayscale(1);
        padding: 19px 0 0;
    }
    .start_shopping {
        border: 1px solid #000;
        border-radius: 4px;
        color: #555;
        display: table;
        margin: 26px auto;
        padding: 8px 19px;
        text-align: center;
        background: none;
        outline: 0;
    }
</style>
<div class="w3_breadcrumb">
    <div class="breadcrumb-inner">
        <ul>
            <li><a href="index">Home</a> <i> /</i></li>
            <li>My Orders</li>
        </ul>
    </div>
</div>
<div class="container order_outer_box">
    <div class="row">
        <div class="col-md-12">
            <div class="order_top_header">
                <h3 class="w3l-inner-h-title">
                    My Orders
                </h3>
                <!--for paypal redirect form satrt here-->
                <form action="<?php echo $paypal_url; ?>" method="post">
                    <!-- Identify your business so that you can collect the payments. -->
                    <input name="business" value="<?php echo $paypal_id; ?>" type="hidden">
                    <!-- Specify a Buy Now button. -->
                    <input name="cmd" value="_xclick" type="hidden">
                    <!--    <input type="hidden" name="cmd" value="_cart">-->
                    <!-- Specify details about the item that buyers will purchase. -->
                    <input name="item_name" value="Orders" type="hidden">
                    <input name="item_number" id="pp_order_id" value="" type="hidden">
                    <input id="pp_order_amount" name="amount" value="" type="hidden">
                    <input name="currency_code" value="USD" type="hidden">
                    <!-- Specify URLs -->
                    <input type='hidden' name='cancel_return' value="" id="cancelReturnUrl"/>
                    <input name="return" value="" id="returnUrl" type="hidden"/>
                    <!-- Display the payment button. -->
                    <input style="display:none" name="submit" value="PayNow" id="paybtn" type="submit">
                </form>
                <!--for paypal redirect form end here-->
            </div>
            <div class="order_main_box">
                <!--<div class="order_inner_box"> <h1 class="inner_heading">Open</h1><table class="table"><thead><tr><th>Jul 04, 2017 18:53</th><th>Product Price</th> <th>Order number: 820991</th><th>Payment Status: Pending <input type="button" value="Make Payment" class="order_payment" /></th><th>Total :<label class="order_total">$203.20</label></th></tr></thead><tbody><tr><td><img src="images/lady.png" class="order_prod_img img-responsive"></td><td>Product Type  : Swimsuit</td><td>Product Name  : Swimsuit12514</td><td>Product Price : $203.20</td><td>Product Qunatity : 2</td></tr></tbody></table></div>-->
            </div>
        </div>
    </div>
</div>
<?php include("footer.php");?>
<script>
function my_order(){
    $(".loader").fadeIn("slow");
    var user_id = $("#user_id").val();
    var url = "admin/api/orderProcess.php";
    $.post(url,{"type":"getAllOrders","user_id":user_id},function(data){
        var status = data.Status;
        var order = data.order;
        var userData = data.userData;
        if(status == "Success")
        {
            $(".loader").fadeOut("slow");

            var orderData = "";
            for(var i = 0; i < order.length; i++){
                var theadData = "";
                var tbodyData = "";
                var order_id = order[i].order_id;
                var order_state = order[i].order_state;
                var order_date = order[i].order_date;
                var date = toHumanFormat(order_date);

                var order_number = order[i].order_number;
                var order_total = order[i].order_total;
                var order_payment_status = order[i].order_payment_status;
                var order_detail = order[i].order_detail;

                if(order_payment_status == "Success"){
                    theadData = theadData+'<div class="order_inner_box"> <h1 class="inner_heading">'+order_state+'</h1>' +
                        '<table class="table my_order_table"><thead><tr><th>'+date+'</th><th>'+userData.name+'</th><th>Order number: '+order_number+'</th>' +
                        '<th>Payment Status: '+order_payment_status+' </th>' +
                        '<th>Total : <label class="order_total">$'+order_total+'</label></th></tr></thead><tbody>';
                }
                else
                {
                    theadData = theadData+'<div class="order_inner_box"> <h1 class="inner_heading">Open</h1>' +
                        '<table class="table my_order_table"><thead><tr><th>'+date+'</th><th>'+userData.name+'</th><th>Order number: '+order_number+'</th>' +
                        '<th class="payment_status_th">Payment Status: '+order_payment_status+' <input type="button"  id="order_make_payment'+i+'" value="Make Payment" class="order_payment" onclick=makePayment("'+order_id+'","'+order_total+'") /></th>' +
                        '<th>Total : <label class="order_total">$'+order_total+'</label></th></tr></thead><tbody>';
                }



                for(var a = 0; a < order_detail.length; a++){
                    tbodyData = tbodyData +'<tr><td align="center"><img src="images/lady.png" class="order_prod_img img-responsive">' +
                        '</td><td>Product Type  : '+order_detail[a].det_product_type+'</td><td>Product Name  : '+order_detail[a].det_product_name+'</td><td>Product Price : $'+order_detail[a].det_price+'</td>' +
                        '<td>Product Qunatity : '+order_detail[a].det_quantity+'</td></tr>';
                }

                var table = theadData+tbodyData+"</tbody></table></div>"
                orderData = orderData + table;
            }
            $(".order_main_box").html(orderData);

        }
        else
        {
            $(".loader").fadeOut("slow");
            $(".order_main_box").html("<div class='order_bag_box'><h1 class='order_noData'>No Orders available Yet...</h1>" +
                "<img src='images/bage.png' class='order-bag'>  </div><a href='orders'> <button class='btn start_shopping' >START SHOPPING" +
                "</button></a>");
        }

    });
}
my_order();

function makePayment(order_id,total){
    $(".loader").fadeIn("slow");
    $("#returnUrl").val("http://scan2fit.com/duziscan/admin/api/payment.php?payment_status=Success&order_id=" +
        order_id + "&paid_amount=" + total);
    $("#cancelReturnUrl").val("http://scan2fit.com/duziscan/admin/api/payment.php?payment_status=Failed&order_id=" +
        order_id + "&paid_amount=" + total);
    $("#pp_order_amount").val(total);
    $("#paybtn").click();
}
</script>