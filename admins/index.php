<?php
error_reporting(0);
include('header.php');
require_once('api/Classes/CATEGORY.php');
$conn = new \Classes\CONNECT();
$category = new \Classes\CATEGORY();
?>
<style>
    .cop-price {
        float: left;
        height: 28px;
        width: 55px;
    }
</style>
<!-- page content -->
<div class="right_col" role="main">
    <!-- top tiles -->
    <div class="row tile_count">

    </div>
    <div class="row" role="main">
        <div class="">
            <!--<div class="page-title">
                <div class="title_left"></div>@f1*2C3AmY7+
            </div>-->
            <div class="clearfix"></div>
            <div class="row">
                <div class="col-md-12 col-sm-12 col-xs-12">
                    <div class="x_panel">
                        <?php if ($_REQUEST['type'] == 'price_list') { ?>
                            <div class="x_title" style="border-bottom: 1px solid #e6e9ed;">
                                <h2><span style="color:#1ABB9C;"></span>Select Categories
                                    <small></small>
                                </h2>

                                <div class="clearfix"></div>
                            </div>
                            <div class="col-md-12 col-sm-12 col-xs-12 catagrories-suit">
                                <ul>
                                    <a href="index.php?type=Suits2pc">
                                        <li><img src='images/2pcsuit.jpg' style='height:35px' class='img-thumbnail'>Suit
                                            2 Pc
                                        </li>
                                    </a>
                                    <a href="index.php?type=Suits3pc">
                                        <li><img src='images/3pcsuit.png' style='height:35px' class='img-thumbnail'>Suit
                                            3 Pc
                                        </li>
                                    </a>
                                    <a href="index.php?type=Jacket">
                                        <li><img src='images/jacket.jpg' style='height:35px' class='img-thumbnail'>Jacket
                                        </li>
                                    </a>
                                    <a href="index.php?type=Pants">
                                        <li><img src='images/pants.jpg' style='height:35px' class='img-thumbnail'>Pants
                                        </li>
                                    </a>
                                    <a href="index.php?type=Vest">
                                        <li><img src='images/vest.jpg' style='height:35px' class='img-thumbnail'>Vest
                                        </li>
                                    </a>
                                    <a href="index.php?type=Shirts">
                                        <li><img src='images/shirts.jpg' style='height:35px' class='img-thumbnail'>Shirts
                                        </li>
                                    </a>
                                    <a href="index.php?type=Accessories">
                                        <li><img src='images/suit-accessories.jpg' style='height:35px'
                                                 class='img-thumbnail'>Accessories
                                        </li>
                                    </a>
                                    <a href="index.php?type=Overcoats">
                                        <li><img src='images/overcoat.jpg' style='height:35px' class='img-thumbnail'>Overcoats
                                        </li>
                                    </a>
                                    <a href="index.php?type=LuxeriesSuits">
                                        <li><img src='images/luxerysuits.jpg' style='height:35px' class='img-thumbnail'>Luxeries
                                            Suits
                                        </li>
                                    </a>
                                </ul>
                            </div>
                        <?php } else { ?>

                            <div class="x_title">
                                <h2><span style="color:#1ABB9C;"></span><?php echo $_REQUEST['type']; ?> Price List
                                    <small></small>
                                </h2>
                                <?php if ($_REQUEST['type'] == 'Coupons') { ?>
                                    <ul class="nav navbar-right panel_toolbox">
                                        <li><a class="btn btn-info"
                                               onclick="addNewSuitPrice('<?php echo $_REQUEST['type']; ?>')"
                                               style="color: white"><i class="fa fa-plus"></i> Add Coupon</a></li>
                                    </ul>
                                <?php } ?>
                                <div class="clearfix"></div>
                            </div>
                            <div class="x_content">
                                <p class="text-muted font-13 m-b-30">
                                    From here admin can manage/modify the content of the catalogs
                                </p>
                                <table id="catTable" class="table table-striped table-bordered display">
                                    <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>Image</th>
                                        <?php if ($_REQUEST['type'] == 'Coupons') { ?>
                                            <th>Coupon Name</th>
                                            <th>Discount %</th>
                                            <th>Coupon Uses</th>
                                        <?php } else { ?>
                                            <th>Product Name</th>
                                            <th>Price</th>
                                        <?php } ?>
                                        <th>Type</th>
                                        <th>Added On</th>
                                        <!--<th>Action</th>-->
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <?php
                                    $link = $conn->connect();
                                    if ($link) {
                                        $query = "select * from categories where cat_type= '" . $_REQUEST['type'] . "' 
                                        order by cat_id asc";
                                        $result = mysqli_query($link, $query);
                                        if ($result) {
                                            $num = mysqli_num_rows($result);
                                            if ($num > 0) {
                                                $j = 0;
                                                while ($catData = mysqli_fetch_array($result)) {
                                                    $j++;
                                                    ?>
                                                    <tr>
                                                        <td data-title='#'><?php echo $j ?></td>
                                                        <td data-title='Image'>
                                                            <img src='images/twopicesuit.jpg' style='height:35px'
                                                                 class='img-thumbnail'>
                                                        </td>
                                                        <td data-title='Category'>
                                                            <a href='#'><?php echo $catData['cat_name'] ?></a>
                                                        </td>
                                                        <td data-title='Price' style="font-weight: 600;">
                                                            <?php if ($_REQUEST['type'] == 'Coupons') {
                                                                echo "";
                                                            } else {
                                                                echo "$";
                                                            } ?>
                                                            <input id="cat_price_<?php echo $catData['cat_id'] ?>"
                                                                   class="form-control cop-price" maxlength="50"
                                                                   pattern="\d*" maxlength="4"
                                                                   value="<?php echo $catData['cat_price'] ?>"
                                                                   required="" type="text" style="display: none;">
                                                            <span id="staticprice_<?php echo $catData['cat_id'] ?>"><?php echo $catData['cat_price'] ?></span><input
                                                                    type='hidden' value='editCategory' id='type'/>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                                            <span id='editprice_<?php echo $catData['cat_id'] ?>'
                                                                  onclick=editprice('<?php echo $catData['cat_id'] ?>')
                                                            <i class="fa fa-pencil-square-o" aria-hidden="true"
                                                               style="color: rgb(208, 94, 97);cursor: pointer; font-size: 19px; float: right; margin-top: 6px; font-weight: bold; margin-right: 14px;"></i>
                                                            </span><span id="editsave_<?php echo $catData['cat_id'] ?>"
                                                                         style="display:none;"
                                                                         onclick=addContent('<?php echo $catData['cat_id'] ?>')><i
                                                                        class="fa fa-floppy-o" aria-hidden="true"
                                                                        style="color: green; margin-top: 3px; margin-left: -10px; font-size: 23px;cursor: pointer;"></i>
                                                        </span> <span id="canclesave_<?php echo $catData['cat_id'] ?>"
                                                                      style="display:none;"
                                                                      onclick="canclesave('<?php echo $catData['cat_id'] ?>')"><i
                                                                        class="fa fa-times" aria-hidden="true"
                                                                        style="color: rgb(208, 94, 97); float: right; font-weight: bold; font-size: 24px; margin-right: -4px; margin-top: 1px; cursor: pointer;"></i>
                                                        </span></td>

                                                        <?php if ($_REQUEST['type'] == 'Coupons') { ?>
                                                            <td data-title='uses'><select
                                                                        onchange=couponInfinity('<?php echo $catData['cat_id'] ?>')
                                                                        id='infinty'
                                                                        style='width: 108px; height: 28px; margin-top: 3px;'>
                                                                    <option value='once'>Once</option>
                                                                    <option value='infinity'>Infinity</option>
                                                                </select></td>
                                                        <?php } else { ?>
                                                            <div id='infinty'></div>
                                                        <?php } ?>
                                                        <td data-title='Type'><?php echo $catData['cat_type'] ?></td>
                                                        <td data-title='Added On'><?php echo $catData['added_on'] ?></td>
                                                    </tr>
                                                    <?php
                                                }
                                            }
                                        }
                                    }
                                    ?>
                                    </tbody>
                                </table>
                            </div>
                        <?php } ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- /page content -->
<?php
include('footer.php');
?>