<?php
/**
 * Created by PhpStorm.
 * User: sunil
 * Date: 3/20/2017
 * Time: 11:23 AM
 */

namespace Classes;
require_once('CONNECT.php');
class SUPPLIER
{
    public $link = null;
    public $response = array();

    function __construct()
    {
        $this->link = new CONNECT();
        $this->currentDateTime = date('d M Y h:i:s A');
        $this->currentDateTimeStamp = strtotime($this->currentDateTime);
    }
    public function addSupplier($company_name,$company_address1,$company_address2,$company_work_phone,$company_mobile_no,$company_fax,$contact_first_name,$family_name,$contact_email)
    {
        $link = $this->link->connect();
        if ($link) {
            $query = "insert into wp_supplier (company_name,company_address1,company_address2,company_work_phone,company_mobile_no,company_fax,contact_first_name,family_name,contact_email) VALUES ('$company_name','$company_address1','$company_address2','$company_work_phone','$company_mobile_no','$company_fax','$contact_first_name','$family_name','$contact_email')";
            $result = mysqli_query($link, $query);
            if ($result) {
                $this->response[STATUS] = Success;
                $this->response[MESSAGE] = "Supplier Added SuccessFully";
                $this->response['catId'] = $this->link->getLastId();
            } else {
                $this->response[STATUS] = Error;
                $this->response[MESSAGE] = $this->link->sqlError();
            }
        } else {
            $this->response[STATUS] = Error;
            $this->response[MESSAGE] = $this->link->sqlError();
        }
        return $this->response;
    }
    public function updateSupplier($company_name,$company_address1,$company_address2,$company_work_phone,$company_mobile_no,$company_fax,$contact_first_name,$family_name,$contact_email,$supplierId)
    {
        $link = $this->link->connect();
        if ($link) {
            $query = "update wp_supplier set company_name='$company_name',company_address1='$company_address1',company_address2='$company_address2',company_work_phone='$company_work_phone',company_mobile_no='$company_mobile_no',company_fax='$company_fax',contact_first_name='$contact_first_name',family_name='$family_name',contact_email='$contact_email' where sup_id = '$supplierId'";
            $result = mysqli_query($link, $query);
            if ($result) {
                $this->response[STATUS] = Success;
                $this->response[MESSAGE] = "Supplier Details Updated SuccessFully";
                $this->response['catId'] = $cat_id;
            } else {
                $this->response[STATUS] = Error;
                $this->response[MESSAGE] = $this->link->sqlError();
            }
        } else {
            $this->response[STATUS] = Error;
            $this->response[MESSAGE] = $this->link->sqlError();
        }
        return $this->response;
    }

    public function getSupplierDetails()
    {
        $catArray = array();
        $link = $this->link->connect();
        if($link) {
            $query="select * from wp_supplier order by sup_id DESC";
            $result = mysqli_query($link,$query);
            if($result)
            {
                $num = mysqli_num_rows($result);
                if($num>0) {
                    while($supData = mysqli_fetch_array($result)) {
                        $supArray[]=array(
                            "sup_id"=>$supData['sup_id'],
                            "company_name"=>$supData['company_name'],
                            "company_address1"=>$supData['company_address1'],
                            "company_address2"=>$supData['company_address2'],
                            "company_work_phone"=>$supData['company_work_phone'],
                            "company_mobile_no"=>$supData['company_mobile_no'],
                            "company_fax"=>$supData['company_fax'],
                            "contact_first_name"=>$supData['contact_first_name'],
                            "family_name"=>$supData['family_name'],
                            "contact_email"=>$supData['contact_email']
                        );
                    }
                    $this->response[STATUS] = Success;
                    $this->response[MESSAGE] = "Data Found";
                    $this->response['supplierData'] = $supArray;
                }
                else{
                    $this->response[STATUS] = Error;
                    $this->response[MESSAGE] = "No Supplier Found";
                }
            }
            else
            {
                $this->response[STATUS] = Error;
                $this->response[MESSAGE] = $this->link->sqlError();
            }
        }
        else
        {
            $this->response[STATUS] = Error;
            $this->response[MESSAGE] = $this->link->sqlError();
        }
        return $this->response;
    }

    public function deleteSupplier($sup_id){
        $link = $this->link->connect();
        if($link) {
				$update = mysqli_query($link, "delete from wp_supplier WHERE sup_id='$sup_id'");
				if ($update) {
					$row = mysqli_fetch_array($result);
					$this->response[STATUS] = Success;
					$this->response[MESSAGE] = "Supplier Has Been Deleted Successfully";
				} else {
					$this->response[STATUS] = Error;
					$this->response[MESSAGE] = $this->link->sqlError();
				}
			} else {
				$this->response[STATUS] = Error;
				$this->response[MESSAGE] = "UnAuthorized Access";
			}
        return $this->response;
    }
	
	public function sendMaillSupplier($supplierEmail,$div_Email)
    {
        require 'SMTP/PHPMailerAutoload.php';
        $mail = new \PHPMailer();
        $mail->isSMTP();
        $mail->Host = 'md-in-68.webhostbox.net';
        $mail->SMTPAuth = true;
        $mail->Username = 'noreply@stsmentor.com';
        $mail->Password = 'noreply@sts';
        $mail->SMTPSecure = 'TLS';
        $mail->Port = 587;
        $mail->setFrom('noreply@stsmentor.com', 'SFTailor');   // sender
        $mail->addAddress($supplierEmail);
        $mail->addAttachment('');         // Add attachments
        $mail->addAttachment('');    // Optional name
        $mail->isHTML(true);                                  // Set email format to HTML
        $mail->Subject = 'noreply@SFTailor';
        $mail->Body= $div_Email;
        $mail->AltBody = '';
        if(!$mail->send())
        {
            return $mail->ErrorInfo;
        }
        else {
			
            return array("Status"=>"Success","Message"=>"Email has been sent Successfully to your selected supplier Email address");
			
        }
    }
    public function apiResponse($response)
    {
        header("Content-Type: application/json");
        echo json_encode($response);
    }
}