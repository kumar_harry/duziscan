<?php
/**
 * Created by PhpStorm.
 * User: sunil
 * Date: 1/5/17
 * Time: 1:27 PM
 */
namespace Classes;
require_once('CONNECT.php');
class USERCLASS
{
    public $link = null;
    public $response = array();
    public $currentDate=null;
    public $currentDateStamp=null;
    public $currentDateTime=null;
    public $currentDateTimeStamp=null;
    function __construct()
    {
        $this->link = new CONNECT();
        $this->currentDate = date('d M Y');
        $this->currentDateTime = date('d M Y h:i:s A');
        $this->currentDateStamp = strtotime($this->currentDate);
        $this->currentDateTimeStamp = strtotime($this->currentDateTime);
    }
    public function registerUser($username,$email,$contactNumber,$address,$registersource,$file_name)
    {
        $link = $this->link->connect();
        if($link) {
            $emailResponse = $this->checkEmailExistence($email);
            if($emailResponse[STATUS] == Error){
                $this->response[STATUS] = Error;
                $this->response[MESSAGE] = $emailResponse[MESSAGE];
                $this->response['UserData'] = $emailResponse['UserData'];
            }else{
                $token = $this->generateToken();
                /*$expiry_date = "";
                if ($renewal_type == "Monthly") {
                    $days = $this->link->currentMonthDays();
                    $expiry_date = ($this->currentDateTimeStamp + (86400 * $days)) - 86400;
                } elseif ($renewal_type == "Yearly") {
                    $days = $this->link->YearDays();
                    $expiry_date = ($this->currentDateTimeStamp + (86400 * $days)) - 86400;
                }*/
                $query = "insert into users (user_name,user_email,user_contact,user_status,user_token,user_address,
                register_source,email_verified,user_profile) VALUES ('$username','$email','$contactNumber','1','$token',
                '$address','$registersource','No','$file_name')";
                $result = mysqli_query($link, $query);
                if ($result) {
                    $last_id = mysqli_insert_id($link);
                    $userResponse = $this->getParticularUserData($last_id);
                    $UserData = $userResponse['UserData'];
                    $this->response[STATUS] = Success;
                    $this->response[MESSAGE] = "Verification Link has been Sent to your Registered E-Mail Address ";
                    $this->response['UserData'] = $UserData;
                } else {
                    $this->response[STATUS] = Error;
                    $this->response[MESSAGE] = $this->link->sqlError();
                }
            }
        }else{
            $this->response[STATUS] = Error;
            $this->response[MESSAGE] = $this->link->sqlError();
        }
        return $this->response;
    }
    public function generateToken()
    {
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $charactersLength; $i++) {
            $randomString .= $characters[rand(0,15)];
        }
        return $randomString;
    }
    public function checkEmailExistence($email)
    {
        $link = $this->link->connect();
        if($link) {
            $query="select * from users where user_email = '$email'";
            $result = mysqli_query($link,$query);
            if($result)
            {
                $num = mysqli_num_rows($result);
                if($num>0) {
                    $this->response[STATUS] = Error;
                    $this->response[MESSAGE] = "E-Mail Address Already Registered";
                    $row = mysqli_fetch_assoc($result);
                    $this->response['UserData'] = $row;
                }
                else{
                    $this->response[STATUS] = Success;
                    $this->response[MESSAGE] = "Now Register";
                }
            }
            else
            {
                $this->response[STATUS] = Error;
                $this->response[MESSAGE] = $this->link->sqlError();
            }
        }
        else
        {
            $this->response[STATUS] = Error;
            $this->response[MESSAGE] = $this->link->sqlError();
        }
        return $this->response;
    }
    public function getParticularUserData($userId)
    {
        $link = $this->link->connect();
        if($link) {
            $query="select * from duziscan_users where user_id='$userId'";
            $result = mysqli_query($link,$query);
            if($result)
            {
                $num = mysqli_num_rows($result);
                if($num>0) {
                    $userData = mysqli_fetch_assoc($result);
                    $this->response[STATUS] = Success;
                    $this->response[MESSAGE] = "Valid User";
                    $this->response['UserData'] = $userData;
                }
                else{
                    $this->response[STATUS] = Error;
                    $this->response[MESSAGE] = "Invalid User";
                }
            }
            else
            {
                $this->response[STATUS] = Error;
                $this->response[MESSAGE] = $this->link->sqlError();
            }
        }
        else
        {
            $this->response[STATUS] = Error;
            $this->response[MESSAGE] = $this->link->sqlError();
        }
        return $this->response;
    }
    public function updateUserData($userId,$fname,$lname,$email,$password,$contact)
    {
        $link = $this->link->connect();
        if($link) {
            $query = "update duziscan_users set fname='$fname',lname='$lname',
            email='$email',password = '$password',contact = '$contact' where user_id='$userId' ";
            $result = mysqli_query($link,$query);
            if($result)
            {
                $this->response[STATUS] = Success;
                $this->response[MESSAGE] = "Success";
                $this->response['userId'] = $userId;
            }
            else
            {
                $this->response[STATUS] = Error;
                $this->response[MESSAGE] = $this->link->sqlError();
            }
        }
        else
        {
            $this->response[STATUS] = Error;
            $this->response[MESSAGE] = $this->link->sqlError();
        }
        return $this->response;
    }

    ///////////////////////////////////////////////////////

    public function statusChange($user_id,$value){
        $link = $this->link->connect();
        if($link) {
            $query = "select * from duziscan_users where user_id='$user_id'";
            $result = mysqli_query($link, $query);
            if ($result) {
                $num = mysqli_num_rows($result);
                if ($num > 0) {
                    $update = mysqli_query($link, "UPDATE duziscan_users SET status='$value' WHERE user_id='$user_id'");
                    if ($update) {
                        $this->response[STATUS] = Success;
                        $this->response[MESSAGE] = "Status Has Been Changed Successfully";
                    } else {
                        $this->response[STATUS] = Error;
                        $this->response[MESSAGE] = $this->link->sqlError();
                    }
                } else {
                    $this->response[STATUS] = Error;
                    $this->response[MESSAGE] = "UnAuthorized Access";
                }
            } else {
                $this->response[STATUS] = Error;
                $this->response[MESSAGE] = $this->link->sqlError();
            }
        }
        else{
            $this->response[STATUS] = Error;
            $this->response[MESSAGE] = "Connection Error";
        }
        return $this->response;
    }

    public function deleteUser($user_id){
         $link = $this->link->connect();
        if($link) {
				$update = mysqli_query($link, "delete from duziscan_users WHERE user_id='$user_id'");
				if ($update) {
					$row = mysqli_fetch_array($result);
					$this->response[STATUS] = Success;
					$this->response[MESSAGE] = "user Has Been Deleted Successfully";
				} else {
					$this->response[STATUS] = Error;
					$this->response[MESSAGE] = $this->link->sqlError();
				}
			} else {
				$this->response[STATUS] = Error;
				$this->response[MESSAGE] = "UnAuthorized Access";
			}
        return $this->response;
    }
    public function getAllUsers(){
        $link = $this->link->connect();
        if($link) {
            $query = "select * from duziscan_users";
            $result = mysqli_query($link, $query);
            if ($result) {
                $num = mysqli_num_rows($result);
                if($num>0){
                    while($rows = mysqli_fetch_array($result)){
                        $usersData[]=array(
                          "user_id"=>$rows["user_id"],
                          "fname"=>$rows["fname"],
                          "lname"=>$rows["lname"],
                          "email"=>$rows["email"],
                          "password"=>$rows["password"],
                          "gender"=>$rows["gender"],
                          "dob"=>$rows["dob"],
                          "height"=>$rows["height"],
                          "weight"=>$rows["weight"],
                          "user_profile"=>$rows["user_profile"],
                          "stylist"=>$rows["stylist"],
                          "address"=>$rows["address"],
                          "pincode"=>$rows["pincode"],
                          "city"=>$rows["city"],
                          "state"=>$rows["state"],
                          "country"=>$rows["country"],
                          "contact"=>$rows["contact"],
                          "status"=>$rows["status"]
                        );
                    }
                    $this->response[STATUS] = Success;
                    $this->response[MESSAGE] = "Data Found";
                    $this->response['Count'] = $num;
                    $this->response['usersData'] = $usersData;
                }
                else{
                    $this->response[STATUS] = Error;
                    $this->response[MESSAGE] = "No Users Found";
                }

            } else {
                $this->response[STATUS] = Error;
                $this->response[MESSAGE] = $this->link->sqlError();
            }
        }
        else{
            $this->response[STATUS] = Error;
            $this->response[MESSAGE] = "Connection Error";
        }
        return $this->response;
    }
    public function getAllUsersData(){
        $link = $this->link->connect();
        if($link) {
            $query = "select * from duziscan_users order by user_id DESC";
            $result = mysqli_query($link, $query);
            if ($result) {
                $num = mysqli_num_rows($result);
                if($num>0){
                    while($rows = mysqli_fetch_array($result)){
                        $usersData[]=$rows;
                    }
                    $this->response[STATUS] = Success;
                    $this->response[MESSAGE] = "Data Found";
                    $this->response['Count'] = $num;
                    $this->response['usersData'] = $usersData;
                }
                else{
                    $this->response[STATUS] = Error;
                    $this->response[MESSAGE] = "No Users Found";
                }

            } else {
                $this->response[STATUS] = Error;
                $this->response[MESSAGE] = $this->link->sqlError();
            }
        }
        else{
            $this->response[STATUS] = Error;
            $this->response[MESSAGE] = "Connection Error";
        }
        return $this->response;
    }
    public function getAllQuesData($userId){
        $link = $this->link->connect();
        if($link) {
            $query = "select * from style_genie where user_id='$userId'";
            $result = mysqli_query($link, $query);
            if ($result) {
                $num = mysqli_num_rows($result);
                if($num>0){
                    while($rows = mysqli_fetch_array($result)){
                        $usersData[]=array(
                            "user_id"=>$rows["user_id"],
                            "shirt_que_1"=>$rows["shirt_que_1"],
                            "suit_que_1"=>$rows["suit_que_1"],
                        );
                    }
                    $this->response[STATUS] = Success;
                    $this->response[MESSAGE] = "Data Found";
                    $this->response['Count'] = $num;
                    $this->response['quesData'] = $usersData;
                }
                else{
                    $this->response[STATUS] = Error;
                    $this->response[MESSAGE] = "No Users Found";
                }

            } else {
                $this->response[STATUS] = Error;
                $this->response[MESSAGE] = $this->link->sqlError();
            }
        }
        else{
            $this->response[STATUS] = Error;
            $this->response[MESSAGE] = "Connection Error";
        }
        return $this->response;
    }
    public function apiResponse($response)
    {
        header("Content-Type: application/json");
        echo json_encode($response);
    }
}