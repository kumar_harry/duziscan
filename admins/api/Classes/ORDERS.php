<?php
/**
 * Created by PhpStorm.
 * User: sunil
 * Date: 3/22/2017
 * Time: 9:18 PM
 */

namespace Classes;
require_once('CONNECT.php');
require_once('USERCLASS.php');
class ORDERS
{
    public $link = null;
    public $bookClass = null;
    public $userClass = null;
    public $response = array();
    function __construct()
    {
        $this->link = new CONNECT();
        $this->userClass = new USERCLASS();
        $this->currentDateTime = date('d M Y h:i:s A');
        $this->currentDateTimeStamp = strtotime($this->currentDateTime);
    }
    public function getParticularOrderData($order_id)
    {
        $link = $this->link->connect();
        $order_detail=array();
        if($link) {
            $query="select * from duziscan_orders where order_id='$order_id'";
            $result = mysqli_query($link,$query);
            if($result){
                $num = mysqli_num_rows($result);
                if($num>0) {
                    $orderData = mysqli_fetch_assoc($result);
                    $order_id = $orderData['order_id'];
                    $user_id = $orderData['order_user_id'];
                    $userData = $this->getParticularUserData($user_id);
                    $query2 = "select * from duziscan_orders_detail where det_order_id = '$order_id'";
                    $result2 = mysqli_query($link,$query2);
                    if($result2){
                        $num = mysqli_num_rows($result2);
                        if($num>0){
                            while($detail = mysqli_fetch_assoc($result2)){
                                $order_detail[] = $detail;
                            }
                            $this->response[STATUS] = Success;
                            $this->response[MESSAGE] = "Order Data Exist";
                        }
                        else{
                            $this->response[STATUS] = Success;
                            $this->response[MESSAGE] = "Empty Order Details";
                        }
                    }
                    else{
                        $this->response[STATUS] = Error;
                        $this->response[MESSAGE] = $this->link->sqlError();
                    }
                    $orderData['orderDetail'] = $order_detail;
                    $this->response[STATUS] = Success;
                    $this->response[MESSAGE] = "Order Found";
                    $this->response['order'] = $orderData;
                }
                else{
                    $this->response[STATUS] = Error;
                    $this->response[MESSAGE] = "Invalid Order Identification";
                }
            }
            else{
                $this->response[STATUS] = Error;
                $this->response[MESSAGE] = $this->link->sqlError();
            }
        }
        else{
            $this->response[STATUS] = Error;
            $this->response[MESSAGE] = $this->link->sqlError();
        }
        return $this->response;
    }
    public function getParticularUserData($userId)
    {
        $link = $this->link->connect();
        if($link) {
            $query="select * from duziscan_users where user_id='$userId'";
            $result = mysqli_query($link,$query);
            if($result)
            {
                $num = mysqli_num_rows($result);
                if($num>0) {
                    $userData = mysqli_fetch_assoc($result);
                    $this->response[STATUS] = Success;
                    $this->response[MESSAGE] = "Valid User";
                    $this->response['UserData'] = $userData;
                }
                else{
                    $this->response[STATUS] = Error;
                    $this->response[MESSAGE] = "Invalid User";
                }
            }
            else
            {
                $this->response[STATUS] = Error;
                $this->response[MESSAGE] = $this->link->sqlError();
            }
        }
        else
        {
            $this->response[STATUS] = Error;
            $this->response[MESSAGE] = $this->link->sqlError();
        }
        return $this->response;
    }
    public function getFinalMeasurementData($meas_id){
        $link = $this->link->connect();
        $finalMeasurement   =   array();
        if($link) {
            $query="select * from final_measurements where measurement_id='$meas_id'";
            $result = mysqli_query($link,$query);
            if($result){
                $num = mysqli_num_rows($result);
                if($num>0) {
                    $detail = mysqli_fetch_assoc($result);
                    $finalMeasurement[]=$detail;
                    $this->response[STATUS] = Success;
                    $this->response[MESSAGE] = "Measurement Data Find";
                    $this->response['finalMeasurement'] = $finalMeasurement;
                }
                else{
                    $this->response[STATUS] = Error;
                    $this->response[MESSAGE] = "Measurement Data not Found";
                }
            }
            else{
                $this->response[STATUS] = Error;
                $this->response[MESSAGE] = $this->link->sqlError();
            }
        }
        else{
            $this->response[STATUS] = Error;
            $this->response[MESSAGE] = $this->link->sqlError();
        }
        return $this->response;
    }
    public function getParticularOrderDetailData($orderRowId)
    {
        $link = $this->link->connect();
        $orderDetRowData   =   array();
        if($link) {
            $query="select * from duziscan_orders_detail where det_id='$orderRowId'";
            $result = mysqli_query($link,$query);
            if($result){
                $num = mysqli_num_rows($result);
                if($num>0) {
                        $detail = mysqli_fetch_assoc($result);
                        $orderDetRowData[]=$detail;

                    $this->response[STATUS] = Success;
                    $this->response[MESSAGE] = "Order Detail Data Found";
                    $this->response['orderDetRowData'] = $orderDetRowData;
                }
                else{
                    $this->response[STATUS] = Error;
                    $this->response[MESSAGE] = "Order Detail Data not Found";
                }
            }
            else{
                $this->response[STATUS] = Error;
                $this->response[MESSAGE] = $this->link->sqlError();
            }
        }
        else{
            $this->response[STATUS] = Error;
            $this->response[MESSAGE] = $this->link->sqlError();
        }
        return $this->response;
    }
 public function getAllOrders()
    {
        $link2 = $this->link2->connect2();
        $order_detail=array();
        $order=array();
        if($link2) {
            $query="select * from duziscan_orders order by order_id desc";
            $result = mysqli_query($link2,$query);
            if($result)
            {
                $num = mysqli_num_rows($result);
                if($num>0) {
                    while($row = mysqli_fetch_assoc($result)) {
                        $order_id = $row['order_id'];
                        $query2 = "select * from duziscan_orders_detail where det_order_id = '$order_id'";
                        $result2 = mysqli_query($link2, $query2);
                        if ($result2) {

                            $num = mysqli_num_rows($result2);
                            if ($num > 0) {
                                unset($order_detail);
                                while ($detail = mysqli_fetch_assoc($result2)) {
                                    $order_detail[] = $detail;
                                }
                                $order[] = array("order_id" => $row['order_id'],
                                    "order_date" => $row['order_date'],
                                    "order_state" => $row['order_state'],
                                    "order_number" => $row['order_number'],
                                    "order_total" => $row['order_total'],
                                    "order_payment_status" => $row['order_payment_status'],
                                    "order_user_id" => $row['order_user_id'],
                                    "order_detail" => $order_detail);
                                $this->response[STATUS] = Success;
                                $this->response[MESSAGE] = "Order Details Found";
                            }
                            else{
                                $this->response[STATUS] = Success;
                                $this->response[MESSAGE] = "Empty Order Details";
                            }
                        }else {
                            $this->response[STATUS] = Error;
                            $this->response[MESSAGE] = $this->link2->sqlError2();
                        }
                    }
                    $this->response[STATUS] = Success;
                    $this->response[MESSAGE] = "Order Found";
                    $this->response['order'] = $order;
                }
                else{
                    $this->response[STATUS] = Error;
                    $this->response[MESSAGE] = "Invalid Order Identification";
                }
            }
            else
            {
                $this->response[STATUS] = Error;
                $this->response[MESSAGE] = $this->link2->sqlError2();
            }
        }
        else
        {
            $this->response[STATUS] = Error;
            $this->response[MESSAGE] = $this->link2->sqlError2();
        }
        return $this->response;
    }
    public function apiResponse($response)
    {
        header("Content-Type: application/json");
        echo json_encode($response);
    }
    public function cancelOrder($order_id)
    {
        $link = $this->link->connect();
        if($link) {
            $query = "select * from duziscan_orders where order_id='$order_id'";
            $result = mysqli_query($link, $query);
            if ($result) {
                $num = mysqli_num_rows($result);
                if ($num > 0) {
                    $update = mysqli_query($link, "UPDATE duziscan_orders SET order_payment_status='Cancelled' WHERE order_id='$order_id'");
                    if ($update) {
                        $this->response[STATUS] = Success;
                        $this->response[MESSAGE] = "Order Has Been Cancelled Successfully";
                    } else {
                        $this->response[STATUS] = Error;
                        $this->response[MESSAGE] = $this->link->sqlError();
                    }
                } else {
                    $this->response[STATUS] = Error;
                    $this->response[MESSAGE] = "UnAuthorized Access";
                }
            } else {
                $this->response[STATUS] = Error;
                $this->response[MESSAGE] = $this->link->sqlError();
            }
        }
        else{
            $this->response[STATUS] = Error;
            $this->response[MESSAGE] = "Connection Error";
        }
        return $this->response;
    }

    public function updateOrderData($order_id,$all_fields,$user_id){
        $array = explode(',', $all_fields);
        $OrderDate = $array[1].','.$array[2];
        $Status = $array[3];
        $Type = $array[4];
        $name = $array[5];
        $phone = $array[6];
        $weight = $array[7];
        $height = $array[8];
        $link = $this->link->connect();
        if($link) {
                $update = mysqli_query($link, "UPDATE duziscan_orders_detail SET det_product_name='$Type', WHERE det_id='$order_id'");
                $update = mysqli_query($link, "UPDATE duziscan_orders SET order_date='$OrderDate', order_payment_status='$Status' WHERE order_id='$order_id'");
                $update = mysqli_query($link, "UPDATE duziscan_users SET fname='$name', contact='$phone',weight='$weight',height='$height' WHERE user_id='$user_id'");
            if ($update) {
                $this->response[STATUS] = Success;
                $this->response[MESSAGE] = "Order Has Been update Successfully";
            } else {
                $this->response[STATUS] = Error;
                $this->response[MESSAGE] = $this->link->sqlError();
            }

        }
        else{
            $this->response[STATUS] = Error;
            $this->response[MESSAGE] = "Connection Error";
        }
        return $this->response;
    }
    public function updateEditDetail($order_detail_id,$column,$value,$table){
        $link = $this->link->connect();
        if($link) {
            $update = mysqli_query($link, "UPDATE $table SET $column = '$value' WHERE det_id='$order_detail_id'");
            if ($update) {
                $this->response[STATUS] = Success;
                $this->response[MESSAGE] = "Order Has Been update Successfully";
            } else {
                $this->response[STATUS] = Error;
                $this->response[MESSAGE] = $this->link->sqlError();
            }
        }
        else{
            $this->response[STATUS] = Error;
            $this->response[MESSAGE] = "Connection Error";
        }
        return $this->response;
    }
	
	   public function deleteOrder($order_id){
         $link = $this->link->connect();
        if($link) {
				$update = mysqli_query($link, "delete from duziscan_orders WHERE order_id='$order_id'");
				$update = mysqli_query($link, "delete from duziscan_orders_detail WHERE det_order_id='$order_id'");
				if ($update) {
					$row = mysqli_fetch_array($result);
					$this->response[STATUS] = Success;
					$this->response[MESSAGE] = "order Has Been Deleted Successfully";
				} else {
					$this->response[STATUS] = Error;
					$this->response[MESSAGE] = $this->link->sqlError();
				}
			} else {
				$this->response[STATUS] = Error;
				$this->response[MESSAGE] = "UnAuthorized Access";
			}
        return $this->response;
    }
}
?>