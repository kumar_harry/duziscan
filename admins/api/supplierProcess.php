<?php
require_once ('Classes/SUPPLIER.php');
require_once ('Constants/functions.php');
require_once('Constants/configuration.php');
require_once('Constants/DbConfig.php');
$supClass = new \Classes\SUPPLIER();
$requiredfields = array('type');

($response = RequiredFields($_POST, $requiredfields));
if($response['Status'] == 'Failure'){
    $supClass->apiResponse($response);
    return false;
}
error_reporting(0);
$type = $_POST['type'];
if($type == "addSupplier")
{
    $requiredfields = array('company_name','company_address1','contact_first_name');
    $response = RequiredFields($_POST, $requiredfields);
    if($response['Status'] == 'Failure'){
        $supClass->apiResponse($response);
        return false;
    }
    $company_name = trim($_POST['company_name']);
    $company_address1 = trim($_POST['company_address1']);
    $company_address2 = trim($_POST['company_address2']);
    $company_work_phone = trim($_POST['company_work_phone']);
    $company_mobile_no = trim($_POST['company_mobile_no']);
    $company_fax = trim($_POST['company_fax']);
    $contact_first_name = trim($_POST['contact_first_name']);
    $family_name = trim($_POST['family_name']);
    $contact_email = trim($_POST['contact_email']);
    $response = $supClass->addSupplier($company_name,$company_address1,$company_address2,$company_work_phone,$company_mobile_no,$company_fax,$contact_first_name,$family_name,$contact_email);
   
    if($response[STATUS] == Error){
        $supClass->apiResponse($response);
        return false;
    }
    $supClass->apiResponse($response);
}
else if($type == "updateSupplier"){
    $requiredfields = array('supplier_id','company_name','company_address1','contact_first_name');
    $response = RequiredFields($_POST, $requiredfields);
    if($response['Status'] == 'Failure'){
        $supClass->apiResponse($response);
        return false;
    }
    $company_name = trim($_POST['company_name']);
    $company_address1 = trim($_POST['company_address1']);
    $company_address2 = trim($_POST['company_address2']);
    $company_work_phone = trim($_POST['company_work_phone']);
    $company_mobile_no = trim($_POST['company_mobile_no']);
    $company_fax = trim($_POST['company_fax']);
    $contact_first_name = trim($_POST['contact_first_name']);
    $family_name = trim($_POST['family_name']);
    $contact_email = trim($_POST['contact_email']);
    $supplier_id = trim($_POST['supplier_id']);

    $response = $supClass->updateSupplier($company_name,$company_address1,$company_address2,$company_work_phone,$company_mobile_no,$company_fax,$contact_first_name,$family_name,$contact_email,$supplier_id);
    if($response[STATUS] == Error){
        $supClass->apiResponse($response);
        return false;
    }
    $supClass->apiResponse($response);
}

else if($type == "deleteSupplier")
{
    $requiredfields = array('supplier_id');
    $response = RequiredFields($_POST, $requiredfields);
    if($response['Status'] == 'Failure'){
        $supClass->apiResponse($response);
        return false;
    }
    $sup_id = $_REQUEST['supplier_id'];
    $response = $supClass->deleteSupplier($sup_id);
    if($response[STATUS] == Error) {
        $supClass->apiResponse($response);
        return false;
    }
    $supClass->apiResponse($response);
}
 else if ($type == "supplierEmail") {
    $requiredfields = array('supplier_email');
    $response = RequiredFields($_POST, $requiredfields);
    if($response['Status'] == 'Failure'){
        $supClass->apiResponse($response);
        return false;
    }
    $sup_Email = $_REQUEST['supplier_email'];
    $div_Email = $_REQUEST['div_email'];
    $response = $supClass->sendMaillSupplier($sup_Email,$div_Email);
    if($response[STATUS] == Error) {
        $supClass->apiResponse($response);
        return false;
    }
    $supClass->apiResponse($response);
}
else{
    $response[STATUS] = Error;
    $response[MESSAGE] = "502 UnAuthorised Request";
    $supClass->apiResponse($response);
}
?>