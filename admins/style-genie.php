<?php
include('header.php');
require_once('api/Classes/USERCLASS.php');
$conn = new \Classes\CONNECT();
$userClass = new \Classes\USERCLASS();
?>
<!-- page content -->
<div class="right_col" role="main">
    <div class="row tile_count"></div>
    <div class="">
        <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                    <div class="x_title">
                        <h2>All Users <small></small></h2>
                        <ul class="nav navbar-right panel_toolbox" style="display: none;">
                            <li>
                                <button style="margin-top:5px" onclick="window.location='api/excelProcess.php?dataType=allOrders'" class="btn btn-info btn-sm">Download Excel File</button>
                            </li>
                        </ul>
                        <div class="clearfix"></div>
                    </div>
                    <div class="x_content">
                        <p class="text-muted font-13 m-b-30">
                            List of Users of sftailor
                        </p>
                        <table id="orderTable" class="table table-striped table-bordered">
                            <thead>
                            <tr>
                                <th>S.No</th>
                                <th>Profile Pic</th>
								<th>User Name</th>
                                <th>User Email</th>
                                <th>Contact Number</th>
                                <th>Gender</th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php
                            $link = $conn->connect();//for scan2tailor
                            if ($link) {
                                $getAllUsers = $userClass->getAllUsersData();
                                $userData = $getAllUsers['usersData'];
                                for($i=0;$i<count($userData);$i++){
                                    ?>
                                    <tr>
                                        <td data-title='#'><?php echo $i+1; ?></td>
                                        <td data-title='User profile'><img class='img-thumbnail' src="<?php echo $userData[$i]['user_profile'];?>"
                                            style="width:40px;height:40px;"></td>
                                        <td data-title='User Name'><a href="sdet.php?user_id=<?php echo $userData[$i]['user_id'];?>">
                                            <?php echo $userData[$i]['user_fname']." ".$userData[$i]['user_lname'];?></a>
                                        </td>
                                        <td data-title='User Email'><?php echo $userData[$i]['user_email'];?></td>
                                        <td data-title='User Contact'><?php echo $userData[$i]['user_contact'];?></td>
                                        <td data-title='User gender'><?php echo $userData[$i]['user_gender'];?></td>
                                    </tr>
                                    <?php
                                }
                            }
                            ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- /page content -->
<?php
include('footer.php');
?>
<script>
    $(document).ready(function () {
        $('#orderTable').DataTable({});
    });
</script>
