<?php
include('header.php');
if(isset($_REQUEST['_']) && isset($_REQUEST['_'])){
    echo "<input type='hidden' value='".$_REQUEST['_']."' id='user_id' />";
    echo "<input type='hidden' value='".$_REQUEST['m']."' id='meas_id' />";
}else{
    header('Location:stylegenie.php');
}
?>
<style>
    .boxes {
        background: white;
        min-height: 100px;
        border: 1px solid #ddd;
        margin-bottom: 10px;
    }

    .boxes .tabdiv {
        background: #eee none repeat scroll 0 0;
        border: 1px solid;
        font-weight: bold;
        margin-top: 22px;
        padding: 8px;
        text-align: center;
    }

    .loaderdiv {
        background: rgba(0, 0, 0, 0.5) none repeat scroll 0 0;
        height: 700px;
        position: fixed;
        width: 100%;
        z-index: 99999;
        display: block;
    }

    .block {
        display: block !important;
    }

    .loaderhidden {
        display: none !important;
    }

    .loaderdiv > img {
        height: 50px;
        margin: 300px 50%;
        width: 50px;
    }

    .savebtn {
        letter-spacing: 1px;
        margin-bottom: 18px;
        text-transform: uppercase;
    }

    .err {
        text-align: center;
        color: red;
    }
</style>
<!-- page content -->
<div class="loaderdiv">
    <img src="images/preloader.gif"/>
</div>
<div class="right_col" role="main">
    <div class="row">
        <div class="col-md-12 boxes">
                <hr>
                <div class="col-md-12">
                    <div class="col-md-12" id="dimes_ul">
                        <div class="form-inline pull-right" style="margin-bottom:10px">
                            <label>Unit Parameter's : </label>
                            <select class="form-control paramselect" onchange="convertData()" style="width:200px"
                                    id="params">
                                <option value="Inches" selected="selected">Inches</option>
                                <option value="CM">CM</option>
                            </select>
                        </div>
                        <table class="table table-bordered">
                            <thead>
                            <th colspan="3" style="text-align: center">Body Measurement</th>
                            <th colspan="4" style="text-align: center">Extra Allowance</th>
                            <th colspan="4" style="text-align: center">Final Measurement</th>
                            </thead>
                            <tbody>
                            <tr>
                                <th>S.No.</th>
                                <th>Parameters</th>
                                <th>Values</th>
                                <th>Shirt</th>
                                <th>Jacket</th>
                                <th>Vest</th>
                                <th>Pant</th>
                                <th>Shirt</th>
                                <th>Jacket</th>
                                <th>Vest</th>
                                <th>Pant</th>
                            </tr>
                            <tr>
                                <td>1.</td>
                                <td><span>Collar</span></td>
                                <td id="collar"></td>
                                <td id="collar_shirt_allow">0</td>
                                <td id="collar_jacket_allow">0</td>
                                <td id="collar_vest_allow">0</td>
                                <td id="collar_pant_allow">0</td>
                                <td>
                                    <i class="fa fa-minus" style="margin-right:5px;cursor:pointer"
                                       onclick="incdec('minus','final_shirt_collar')"></i>
                                    <span id="final_shirt_collar">0</span>
                                    <i class="fa fa-plus" style="margin-left:5px;cursor:pointer"
                                       onclick="incdec('plus','final_shirt_collar')"></i>
                                </td>
                                <td>
                                    <!--<i class="fa fa-minus" style="margin-right:5px;cursor:pointer"
                                       onclick="incdec('minus','final_jacket_collar')"></i>
                                    <span id="final_jacket_collar">0</span>
                                    <i class="fa fa-plus" style="margin-left:5px;cursor:pointer"
                                       onclick="incdec('plus','final_jacket_collar')"></i>-->
                                </td>
                                <td>
                                    <!--<i class="fa fa-minus" style="margin-right:5px;cursor:pointer"
                                       onclick="incdec('minus','final_vest_collar')"></i>
                                    <span id="final_vest_collar">0</span>
                                    <i class="fa fa-plus" style="margin-left:5px;cursor:pointer"
                                       onclick="incdec('plus','final_vest_collar')"></i>-->
                                </td>
                                <td>
                                    <!--<i class="fa fa-minus" style="margin-right:5px;cursor:pointer"
                                       onclick="incdec('minus','final_pant_collar')"></i>
                                    <span id="final_pant_collar">0</span>
                                    <i class="fa fa-plus" style="margin-left:5px;cursor:pointer"
                                       onclick="incdec('plus','final_pant_collar')"></i>-->
                                </td>
                            </tr>
                            <tr>
                                <td>2.</td>
                                <td><span>Chest</span></td>
                                <td id="chest_front"></td>
                                <td id="chest_shirt_allow">0</td>
                                <td id="chest_jacket_allow">0</td>
                                <td id="chest_vest_allow">0</td>
                                <td id="chest_pant_allow">0</td>
                                <td>
                                    <i class="fa fa-minus" style="margin-right:5px;cursor:pointer"
                                       onclick="incdec('minus','final_shirt_chest')"></i>
                                    <span id="final_shirt_chest">0</span>
                                    <i class="fa fa-plus" style="margin-left:5px;cursor:pointer"
                                       onclick="incdec('plus','final_shirt_chest')"></i>
                                </td>
                                <td>
                                    <i class="fa fa-minus" style="margin-right:5px;cursor:pointer"
                                       onclick="incdec('minus','final_jacket_chest')"></i>
                                    <span id="final_jacket_chest">0</span>
                                    <i class="fa fa-plus" style="margin-left:5px;cursor:pointer"
                                       onclick="incdec('plus','final_jacket_chest')"></i>
                                </td>
                                <td>
                                    <i class="fa fa-minus" style="margin-right:5px;cursor:pointer"
                                       onclick="incdec('minus','final_vest_chest')"></i>
                                    <span id="final_vest_chest">0</span>
                                    <i class="fa fa-plus" style="margin-left:5px;cursor:pointer"
                                       onclick="incdec('plus','final_vest_chest')"></i>
                                </td>
                                <td>
                                    <!--<i class="fa fa-minus" style="margin-right:5px;cursor:pointer"
                                       onclick="incdec('minus','final_pant_chest')"></i>
                                    <span id="final_pant_chest">0</span>
                                    <i class="fa fa-plus" style="margin-left:5px;cursor:pointer"
                                       onclick="incdec('plus','final_pant_chest')"></i>-->
                                </td>
                            </tr>
                            <tr>
                                <td>3.</td>
                                <td><span>Waist Horizontal Line (stomach)</span></td>
                                <td id="stomach"></td>
                                <td id="stomach_shirt_allow">0</td>
                                <td id="stomach_jacket_allow">0</td>
                                <td id="stomach_vest_allow">0</td>
                                <td id="stomach_pant_allow">0</td>
                                <td>
                                    <i class="fa fa-minus" style="margin-right:5px;cursor:pointer"
                                       onclick="incdec('minus','final_shirt_stomach')"></i>
                                    <span id="final_shirt_stomach">0</span>
                                    <i class="fa fa-plus" style="margin-left:5px;cursor:pointer"
                                       onclick="incdec('plus','final_shirt_stomach')"></i>
                                </td>
                                <td>
                                    <i class="fa fa-minus" style="margin-right:5px;cursor:pointer"
                                       onclick="incdec('minus','final_jacket_stomach')"></i>
                                    <span id="final_jacket_stomach">0</span>
                                    <i class="fa fa-plus" style="margin-left:5px;cursor:pointer"
                                       onclick="incdec('plus','final_jacket_stomach')"></i>
                                </td>
                                <td>
                                    <i class="fa fa-minus" style="margin-right:5px;cursor:pointer"
                                       onclick="incdec('minus','final_vest_stomach')"></i>
                                    <span id="final_vest_stomach">0</span>
                                    <i class="fa fa-plus" style="margin-left:5px;cursor:pointer"
                                       onclick="incdec('plus','final_vest_stomach')"></i>
                                </td>
                                <td>
                                    <!--<i class="fa fa-minus" style="margin-right:5px;cursor:pointer"
                                       onclick="incdec('minus','final_pant_stomach')"></i>
                                    <span id="final_pant_stomach">0</span>
                                    <i class="fa fa-plus" style="margin-left:5px;cursor:pointer"
                                       onclick="incdec('plus','final_pant_stomach')"></i>-->
                                </td>
                            </tr>
                            <tr>
                                <td>4.</td>
                                <td><span>Seat</span></td>
                                <td id="seat_front"></td>
                                <td id="seat_shirt_allow">0</td>
                                <td id="seat_jacket_allow">0</td>
                                <td id="seat_vest_allow">0</td>
                                <td id="seat_pant_allow">0</td>
                                <td>
                                    <i class="fa fa-minus" style="margin-right:5px;cursor:pointer"
                                       onclick="incdec('minus','final_shirt_seat')"></i>
                                    <span id="final_shirt_seat">0</span>
                                    <i class="fa fa-plus" style="margin-left:5px;cursor:pointer"
                                       onclick="incdec('plus','final_shirt_seat')"></i>
                                </td>
                                <td>
                                    <i class="fa fa-minus" style="margin-right:5px;cursor:pointer"
                                       onclick="incdec('minus','final_jacket_seat')"></i>
                                    <span id="final_jacket_seat">0</span>
                                    <i class="fa fa-plus" style="margin-left:5px;cursor:pointer"
                                       onclick="incdec('plus','final_jacket_seat')"></i>
                                </td>
                                <td>
                                    <!--<i class="fa fa-minus" style="margin-right:5px;cursor:pointer"
                                       onclick="incdec('minus','final_vest_seat')"></i>
                                    <span id="final_vest_seat">0</span>
                                    <i class="fa fa-plus" style="margin-left:5px;cursor:pointer"
                                       onclick="incdec('plus','final_vest_seat')"></i>-->
                                </td>
                                <td>
                                    <i class="fa fa-minus" style="margin-right:5px;cursor:pointer"
                                       onclick="incdec('minus','final_pant_seat')"></i>
                                    <span id="final_pant_seat">0</span>
                                    <i class="fa fa-plus" style="margin-left:5px;cursor:pointer"
                                       onclick="incdec('plus','final_pant_seat')"></i>
                                </td>
                            </tr>
                            <tr>
                                <td>5.</td>
                                <td><span>Bicep</span></td>
                                <td id="bicep_front"></td>
                                <td id="bicep_shirt_allow">0</td>
                                <td id="bicep_jacket_allow">0</td>
                                <td id="bicep_vest_allow">0</td>
                                <td id="bicep_pant_allow">0</td>
                                <td>
                                    <i class="fa fa-minus" style="margin-right:5px;cursor:pointer"
                                       onclick="incdec('minus','final_shirt_bicep')"></i>
                                    <span id="final_shirt_bicep">0</span>
                                    <i class="fa fa-plus" style="margin-left:5px;cursor:pointer"
                                       onclick="incdec('plus','final_shirt_bicep')"></i>
                                </td>
                                <td>
                                    <i class="fa fa-minus" style="margin-right:5px;cursor:pointer"
                                       onclick="incdec('minus','final_jacket_bicep')"></i>
                                    <span id="final_jacket_bicep">0</span>
                                    <i class="fa fa-plus" style="margin-left:5px;cursor:pointer"
                                       onclick="incdec('plus','final_jacket_bicep')"></i>
                                </td>
                                <td>
                                    <!--<i class="fa fa-minus" style="margin-right:5px;cursor:pointer"
                                       onclick="incdec('minus','final_vest_bicep')"></i>
                                    <span id="final_vest_bicep">0</span>
                                    <i class="fa fa-plus" style="margin-left:5px;cursor:pointer"
                                       onclick="incdec('plus','final_vest_bicep')"></i>-->
                                </td>
                                <td>
                                    <!--<i class="fa fa-minus" style="margin-right:5px;cursor:pointer"
                                       onclick="incdec('minus','final_pant_bicep')"></i>
                                    <span id="final_pant_bicep">0</span>
                                    <i class="fa fa-plus" style="margin-left:5px;cursor:pointer"
                                       onclick="incdec('plus','final_pant_bicep')"></i>-->
                                </td>
                            </tr>
                            <tr>
                                <td>6.</td>
                                <td><span>Back Shoulder</span></td>
                                <td id="shoulder_back"></td>
                                <td id="back_shoulder_shirt_allow">0</td>
                                <td id="back_shoulder_jacket_allow">0</td>
                                <td id="back_shoulder_vest_allow">0</td>
                                <td id="back_shoulder_pant_allow">0</td>
                                <td>
                                    <i class="fa fa-minus" style="margin-right:5px;cursor:pointer"
                                       onclick="incdec('minus','final_shirt_back_shoulder')"></i>
                                    <span id="final_shirt_back_shoulder">0</span>
                                    <i class="fa fa-plus" style="margin-left:5px;cursor:pointer"
                                       onclick="incdec('plus','final_shirt_back_shoulder')"></i>
                                </td>
                                <td>
                                    <i class="fa fa-minus" style="margin-right:5px;cursor:pointer"
                                       onclick="incdec('minus','final_jacket_back_shoulder')"></i>
                                    <span id="final_jacket_back_shoulder">0</span>
                                    <i class="fa fa-plus" style="margin-left:5px;cursor:pointer"
                                       onclick="incdec('plus','final_jacket_back_shoulder')"></i>
                                </td>
                                <td>
                                    <!--<i class="fa fa-minus" style="margin-right:5px;cursor:pointer"
                                       onclick="incdec('minus','final_vest_back_shoulder')"></i>
                                    <span id="final_vest_back_shoulder">0</span>
                                    <i class="fa fa-plus" style="margin-left:5px;cursor:pointer"
                                       onclick="incdec('plus','final_vest_back_shoulder')"></i>-->
                                </td>
                                <td>
                                    <!--<i class="fa fa-minus" style="margin-right:5px;cursor:pointer"
                                       onclick="incdec('minus','final_pant_back_shoulder')"></i>
                                    <span id="final_pant_back_shoulder">0</span>
                                    <i class="fa fa-plus" style="margin-left:5px;cursor:pointer"
                                       onclick="incdec('plus','final_pant_back_shoulder')"></i>-->
                                </td>
                            </tr>
                            <tr>
                                <td>7.</td>
                                <td><span>Front Shoulder</span></td>
                                <td id="shoulder_front"></td>
                                <td id="front_shoulder_shirt_allow">0</td>
                                <td id="front_shoulder_jacket_allow">0</td>
                                <td id="front_shoulder_vest_allow">0</td>
                                <td id="front_shoulder_pant_allow">0</td>
                                <td>
                                    <i class="fa fa-minus" style="margin-right:5px;cursor:pointer"
                                       onclick="incdec('minus','final_shirt_front_shoulder')"></i>
                                    <span id="final_shirt_front_shoulder">0</span>
                                    <i class="fa fa-plus" style="margin-left:5px;cursor:pointer"
                                       onclick="incdec('plus','final_shirt_front_shoulder')"></i>
                                </td>
                                <td>
                                    <i class="fa fa-minus" style="margin-right:5px;cursor:pointer"
                                       onclick="incdec('minus','final_jacket_front_shoulder')"></i>
                                    <span id="final_jacket_front_shoulder">0</span>
                                    <i class="fa fa-plus" style="margin-left:5px;cursor:pointer"
                                       onclick="incdec('plus','final_jacket_front_shoulder')"></i>
                                </td>
                                <td>
                                    <!--<i class="fa fa-minus" style="margin-right:5px;cursor:pointer"
                                       onclick="incdec('minus','final_vest_front_shoulder')"></i>
                                    <span id="final_vest_front_shoulder">0</span>
                                    <i class="fa fa-plus" style="margin-left:5px;cursor:pointer"
                                       onclick="incdec('plus','final_vest_front_shoulder')"></i>-->
                                </td>
                                <td>
                                    <!--<i class="fa fa-minus" style="margin-right:5px;cursor:pointer"
                                       onclick="incdec('minus','final_pant_front_shoulder')"></i>
                                    <span id="final_pant_front_shoulder">0</span>
                                    <i class="fa fa-plus" style="margin-left:5px;cursor:pointer"
                                       onclick="incdec('plus','final_pant_front_shoulder')"></i>-->
                                </td>
                            </tr>
                            <tr>
                                <td>8.</td>
                                <td><span>Sleeve length (right)</span></td>
                                <td id="shoulder_sleeve_right"></td>
                                <td id="sleeve_right_shirt_allow">0</td>
                                <td id="sleeve_right_jacket_allow">0</td>
                                <td id="sleeve_right_vest_allow">0</td>
                                <td id="sleeve_right_pant_allow">0</td>
                                <td>
                                    <i class="fa fa-minus" style="margin-right:5px;cursor:pointer"
                                       onclick="incdec('minus','final_shirt_sleeve_right')"></i>
                                    <span id="final_shirt_sleeve_right">0</span>
                                    <i class="fa fa-plus" style="margin-left:5px;cursor:pointer"
                                       onclick="incdec('plus','final_shirt_sleeve_right')"></i>
                                </td>
                                <td>
                                    <i class="fa fa-minus" style="margin-right:5px;cursor:pointer"
                                       onclick="incdec('minus','final_jacket_sleeve_right')"></i>
                                    <span id="final_jacket_sleeve_right">0</span>
                                    <i class="fa fa-plus" style="margin-left:5px;cursor:pointer"
                                       onclick="incdec('plus','final_jacket_sleeve_right')"></i>
                                </td>
                                <td>
                                    <!--<i class="fa fa-minus" style="margin-right:5px;cursor:pointer"
                                       onclick="incdec('minus','final_vest_sleeve_right')"></i>
                                    <span id="final_vest_sleeve_right">0</span>
                                    <i class="fa fa-plus" style="margin-left:5px;cursor:pointer"
                                       onclick="incdec('plus','final_vest_sleeve_right')"></i>-->
                                </td>
                                <td>
                                    <!--<i class="fa fa-minus" style="margin-right:5px;cursor:pointer"
                                       onclick="incdec('minus','final_pant_sleeve_right')"></i>
                                    <span id="final_pant_sleeve_right">0</span>
                                    <i class="fa fa-plus" style="margin-left:5px;cursor:pointer"
                                       onclick="incdec('plus','final_pant_sleeve_right')"></i>-->
                                </td>
                            </tr>
                            <tr>
                                <td>9.</td>
                                <td><span>Sleeve length (left)</span></td>
                                <td id="shoulder_sleeve_left"></td>
                                <td id="sleeve_left_shirt_allow">0</td>
                                <td id="sleeve_left_jacket_allow">0</td>
                                <td id="sleeve_left_vest_allow">0</td>
                                <td id="sleeve_left_pant_allow">0</td>
                                <td>
                                    <i class="fa fa-minus" style="margin-right:5px;cursor:pointer"
                                       onclick="incdec('minus','final_shirt_sleeve_left')"></i>
                                    <span id="final_shirt_sleeve_left">0</span>
                                    <i class="fa fa-plus" style="margin-left:5px;cursor:pointer"
                                       onclick="incdec('plus','final_shirt_sleeve_left')"></i>
                                </td>
                                <td>
                                    <i class="fa fa-minus" style="margin-right:5px;cursor:pointer"
                                       onclick="incdec('minus','final_jacket_sleeve_left')"></i>
                                    <span id="final_jacket_sleeve_left">0</span>
                                    <i class="fa fa-plus" style="margin-left:5px;cursor:pointer"
                                       onclick="incdec('plus','final_jacket_sleeve_left')"></i>
                                </td>
                                <td>
                                    <!--<i class="fa fa-minus" style="margin-right:5px;cursor:pointer"
                                       onclick="incdec('minus','final_vest_sleeve_left')"></i>
                                    <span id="final_vest_sleeve_left">0</span>
                                    <i class="fa fa-plus" style="margin-left:5px;cursor:pointer"
                                       onclick="incdec('plus','final_vest_sleeve_left')"></i>-->
                                </td>
                                <td>
                                    <!--<i class="fa fa-minus" style="margin-right:5px;cursor:pointer"
                                       onclick="incdec('minus','final_pant_sleeve_left')"></i>
                                    <span id="final_pant_sleeve_left">0</span>
                                    <i class="fa fa-plus" style="margin-left:5px;cursor:pointer"
                                       onclick="incdec('plus','final_pant_sleeve_left')"></i>-->
                                </td>
                            </tr>
                            <tr>
                                <td>10.</td>
                                <td><span>Thigh</span></td>
                                <td id="thigh"></td>
                                <td id="thigh_shirt_allow">0</td>
                                <td id="thigh_jacket_allow">0</td>
                                <td id="thigh_vest_allow">0</td>
                                <td id="thigh_pant_allow">0</td>
                                <td>
                                    <!--<i class="fa fa-minus" style="margin-right:5px;cursor:pointer"
                                       onclick="incdec('minus','final_shirt_thigh')"></i>
                                    <span id="final_shirt_thigh">0</span>
                                    <i class="fa fa-plus" style="margin-left:5px;cursor:pointer"
                                       onclick="incdec('plus','final_shirt_thigh')"></i>-->
                                </td>
                                <td>
                                    <!--<i class="fa fa-minus" style="margin-right:5px;cursor:pointer"
                                       onclick="incdec('minus','final_jacket_thigh')"></i>
                                    <span id="final_jacket_thigh">0</span>
                                    <i class="fa fa-plus" style="margin-left:5px;cursor:pointer"
                                       onclick="incdec('plus','final_jacket_thigh')"></i>-->
                                </td>
                                <td>
                                    <!--<i class="fa fa-minus" style="margin-right:5px;cursor:pointer"
                                       onclick="incdec('minus','final_vest_thigh')"></i>
                                    <span id="final_vest_thigh">0</span>
                                    <i class="fa fa-plus" style="margin-left:5px;cursor:pointer"
                                       onclick="incdec('plus','final_vest_thigh')"></i>-->
                                </td>
                                <td>
                                    <i class="fa fa-minus" style="margin-right:5px;cursor:pointer"
                                       onclick="incdec('minus','final_pant_thigh')"></i>
                                    <span id="final_pant_thigh">0</span>
                                    <i class="fa fa-plus" style="margin-left:5px;cursor:pointer"
                                       onclick="incdec('plus','final_pant_thigh')"></i>
                                </td>
                            </tr>
                            <tr>
                                <td>11.</td>
                                <td><span>Nape to waist (back)</span></td>
                                <td id="napetowaist_back"></td>
                                <td id="nape_shirt_allow">0</td>
                                <td id="nape_jacket_allow">0</td>
                                <td id="nape_vest_allow">0</td>
                                <td id="nape_pant_allow">0</td>
                                <td>
                                    <!--<i class="fa fa-minus" style="margin-right:5px;cursor:pointer"
                                       onclick="incdec('minus','final_shirt_nape')"></i>
                                    <span id="final_shirt_nape">0</span>
                                    <i class="fa fa-plus" style="margin-left:5px;cursor:pointer"
                                       onclick="incdec('plus','final_shirt_nape')"></i>-->
                                </td>
                                <td>
                                    <!--<i class="fa fa-minus" style="margin-right:5px;cursor:pointer"
                                       onclick="incdec('minus','final_jacket_nape')"></i>
                                    <span id="final_jacket_nape">0</span>
                                    <i class="fa fa-plus" style="margin-left:5px;cursor:pointer"
                                       onclick="incdec('plus','final_jacket_nape')"></i>-->
                                </td>
                                <td>
                                    <!--<i class="fa fa-minus" style="margin-right:5px;cursor:pointer"
                                       onclick="incdec('minus','final_vest_nape')"></i>
                                    <span id="final_vest_nape">0</span>
                                    <i class="fa fa-plus" style="margin-left:5px;cursor:pointer"
                                       onclick="incdec('plus','final_vest_nape')"></i>-->
                                </td>
                                <td>
                                    <!--<i class="fa fa-minus" style="margin-right:5px;cursor:pointer"
                                       onclick="incdec('minus','final_pant_nape')"></i>
                                    <span id="final_pant_nape">0</span>
                                    <i class="fa fa-plus" style="margin-left:5px;cursor:pointer"
                                       onclick="incdec('plus','final_pant_nape')"></i>-->
                                </td>
                            </tr>
                            <tr>
                                <td>12.</td>
                                <td><span>Front waist length</span></td>
                                <td id="front_waist_length"></td>
                                <td id="front_waist_shirt_allow">0</td>
                                <td id="front_waist_jacket_allow">0</td>
                                <td id="front_waist_vest_allow">0</td>
                                <td id="front_waist_pant_allow">0</td>
                                <td>
                                    <!--<i class="fa fa-minus" style="margin-right:5px;cursor:pointer"
                                       onclick="incdec('minus','final_shirt_front_waist')"></i>
                                    <span id="final_shirt_front_waist">0</span>
                                    <i class="fa fa-plus" style="margin-left:5px;cursor:pointer"
                                       onclick="incdec('plus','final_shirt_front_waist')"></i>-->
                                </td>
                                <td>
                                    <!--<i class="fa fa-minus" style="margin-right:5px;cursor:pointer"
                                       onclick="incdec('minus','final_jacket_front_waist')"></i>
                                    <span id="final_jacket_front_waist">0</span>
                                    <i class="fa fa-plus" style="margin-left:5px;cursor:pointer"
                                       onclick="incdec('plus','final_jacket_front_waist')"></i>-->
                                </td>
                                <td>
                                    <!--<i class="fa fa-minus" style="margin-right:5px;cursor:pointer"
                                       onclick="incdec('minus','final_vest_front_waist')"></i>
                                    <span id="final_vest_front_waist">0</span>
                                    <i class="fa fa-plus" style="margin-left:5px;cursor:pointer"
                                       onclick="incdec('plus','final_vest_front_waist')"></i>-->
                                </td>
                                <td>
                                    <!--<i class="fa fa-minus" style="margin-right:5px;cursor:pointer"
                                       onclick="incdec('minus','final_pant_front_waist')"></i>
                                    <span id="final_pant_front_waist">0</span>
                                    <i class="fa fa-plus" style="margin-left:5px;cursor:pointer"
                                       onclick="incdec('plus','final_pant_front_waist')"></i>-->
                                </td>
                            </tr>
                            <tr>
                                <td>13.</td>
                                <td><span>Wrist</span></td>
                                <td id="wrist"></td>
                                <td id="wrist_shirt_allow">0</td>
                                <td id="wrist_jacket_allow">0</td>
                                <td id="wrist_vest_allow">0</td>
                                <td id="wrist_pant_allow">0</td>
                                <td>
                                    <!--<i class="fa fa-minus" style="margin-right:5px;cursor:pointer"
                                       onclick="incdec('minus','final_shirt_wrist')"></i>
                                    <span id="final_shirt_wrist">0</span>
                                    <i class="fa fa-plus" style="margin-left:5px;cursor:pointer"
                                       onclick="incdec('plus','final_shirt_wrist')"></i>-->
                                </td>
                                <td>
                                    <!--<i class="fa fa-minus" style="margin-right:5px;cursor:pointer"
                                       onclick="incdec('minus','final_jacket_wrist')"></i>
                                    <span id="final_jacket_wrist">0</span>
                                    <i class="fa fa-plus" style="margin-left:5px;cursor:pointer"
                                       onclick="incdec('plus','final_jacket_wrist')"></i>-->
                                </td>
                                <td>
                                    <!--<i class="fa fa-minus" style="margin-right:5px;cursor:pointer"
                                       onclick="incdec('minus','final_vest_wrist')"></i>
                                    <span id="final_vest_wrist">0</span>
                                    <i class="fa fa-plus" style="margin-left:5px;cursor:pointer"
                                       onclick="incdec('plus','final_vest_wrist')"></i>-->
                                </td>
                                <td>
                                    <!-- <i class="fa fa-minus" style="margin-right:5px;cursor:pointer"
                                        onclick="incdec('minus','final_pant_wrist')"></i>
                                     <span id="final_pant_wrist">0</span>
                                     <i class="fa fa-plus" style="margin-left:5px;cursor:pointer"
                                        onclick="incdec('plus','final_pant_wrist')"></i>-->
                                </td>
                            </tr>
                            <tr>
                                <td>14.</td>
                                <td><span>Waist</span></td>
                                <td id="waist"></td>
                                <td id="waist_shirt_allow">0</td>
                                <td id="waist_jacket_allow">0</td>
                                <td id="waist_vest_allow">0</td>
                                <td id="waist_pant_allow">0</td>
                                <td>
                                    <!--<i class="fa fa-minus" style="margin-right:5px;cursor:pointer" onclick="incdec('minus','final_shirt_waist')"></i>
                                    <span id="final_shirt_waist">0</span>
                                    <i class="fa fa-plus" style="margin-left:5px;cursor:pointer" onclick="incdec('plus','final_shirt_waist')"></i>-->
                                </td>
                                <td>
                                    <!--<i class="fa fa-minus" style="margin-right:5px;cursor:pointer" onclick="incdec('minus','final_jacket_waist')"></i>
                                    <span id="final_jacket_waist">0</span>
                                    <i class="fa fa-plus" style="margin-left:5px;cursor:pointer" onclick="incdec('plus','final_jacket_waist')"></i>-->
                                </td>
                                <td>
                                    <!--<i class="fa fa-minus" style="margin-right:5px;cursor:pointer" onclick="incdec('minus','final_vest_waist')"></i>
                                    <span id="final_vest_waist">0</span>
                                    <i class="fa fa-plus" style="margin-left:5px;cursor:pointer" onclick="incdec('plus','final_vest_waist')"></i>-->
                                </td>
                                <td>
                                    <i class="fa fa-minus" style="margin-right:5px;cursor:pointer" onclick="incdec('minus','final_pant_waist')"></i>
                                    <span id="final_pant_waist">0</span>
                                    <i class="fa fa-plus" style="margin-left:5px;cursor:pointer" onclick="incdec('plus','final_pant_waist')"></i>
                                </td>
                            </tr>
                            <tr>
                                <td>15.</td>
                                <td><span>Calf</span></td>
                                <td id="calf"></td>
                                <td id="calf_shirt_allow">0</td>
                                <td id="calf_jacket_allow">0</td>
                                <td id="calf_vest_allow">0</td>
                                <td id="calf_pant_allow">0</td>
                                <td>
                                    <!--<i class="fa fa-minus" style="margin-right:5px;cursor:pointer"
                                       onclick="incdec('minus','final_shirt_calf')"></i>
                                    <span id="final_shirt_calf">0</span>
                                    <i class="fa fa-plus" style="margin-left:5px;cursor:pointer"
                                       onclick="incdec('plus','final_shirt_calf')"></i>-->
                                </td>
                                <td>
                                    <!--<i class="fa fa-minus" style="margin-right:5px;cursor:pointer"
                                       onclick="incdec('minus','final_jacket_calf')"></i>
                                    <span id="final_jacket_calf">0</span>
                                    <i class="fa fa-plus" style="margin-left:5px;cursor:pointer"
                                       onclick="incdec('plus','final_jacket_calf')"></i>-->
                                </td>
                                <td>
                                    <!--<i class="fa fa-minus" style="margin-right:5px;cursor:pointer"
                                       onclick="incdec('minus','final_vest_calf')"></i>
                                    <span id="final_vest_calf">0</span>
                                    <i class="fa fa-plus" style="margin-left:5px;cursor:pointer"
                                       onclick="incdec('plus','final_vest_calf')"></i>-->
                                </td>
                                <td>
                                    <i class="fa fa-minus" style="margin-right:5px;cursor:pointer"
                                       onclick="incdec('minus','final_pant_calf')"></i>
                                    <span id="final_pant_calf">0</span>
                                    <i class="fa fa-plus" style="margin-left:5px;cursor:pointer"
                                       onclick="incdec('plus','final_pant_calf')"></i>
                                </td>
                            </tr>
                            <tr>
                                <td>16.</td>
                                <td><span>Back Waist Height</span></td>
                                <td id="back_waist_height"></td>
                                <td id="b_waist_shirt_allow">0</td>
                                <td id="b_waist_jacket_allow">0</td>
                                <td id="b_waist_vest_allow">0</td>
                                <td id="b_waist_pant_allow">0</td>
                                <td>
                                    <!--<i class="fa fa-minus" style="margin-right:5px;cursor:pointer"
                                       onclick="incdec('minus','final_shirt_b_waist')"></i>
                                    <span id="final_shirt_b_waist">0</span>
                                    <i class="fa fa-plus" style="margin-left:5px;cursor:pointer"
                                       onclick="incdec('plus','final_shirt_b_waist')"></i>-->
                                </td>
                                <td>
                                    <!--<i class="fa fa-minus" style="margin-right:5px;cursor:pointer"
                                       onclick="incdec('minus','final_jacket_b_waist')"></i>
                                    <span id="final_jacket_b_waist">0</span>
                                    <i class="fa fa-plus" style="margin-left:5px;cursor:pointer"
                                       onclick="incdec('plus','final_jacket_b_waist')"></i>-->
                                </td>
                                <td>
                                    <!--<i class="fa fa-minus" style="margin-right:5px;cursor:pointer"
                                       onclick="incdec('minus','final_vest_b_waist')"></i>
                                    <span id="final_vest_b_waist">0</span>
                                    <i class="fa fa-plus" style="margin-left:5px;cursor:pointer"
                                       onclick="incdec('plus','final_vest_b_waist')"></i>-->
                                </td>
                                <td>
                                    <i class="fa fa-minus" style="margin-right:5px;cursor:pointer"
                                       onclick="incdec('minus','final_pant_b_waist')"></i>
                                    <span id="final_pant_b_waist">0</span>
                                    <i class="fa fa-plus" style="margin-left:5px;cursor:pointer"
                                       onclick="incdec('plus','final_pant_b_waist')"></i>
                                </td>
                            </tr>
                            <tr>
                                <td>17.</td>
                                <td><span>Front Waist Height</span></td>
                                <td id="front_waist_height"></td>
                                <td id="f_waist_shirt_allow">0</td>
                                <td id="f_waist_jacket_allow">0</td>
                                <td id="f_waist_vest_allow">0</td>
                                <td id="f_waist_pant_allow">0</td>
                                <td>
                                    <!--<i class="fa fa-minus" style="margin-right:5px;cursor:pointer"
                                       onclick="incdec('minus','final_shirt_f_waist')"></i>
                                    <span id="final_shirt_f_waist">0</span>
                                    <i class="fa fa-plus" style="margin-left:5px;cursor:pointer"
                                       onclick="incdec('plus','final_shirt_f_waist')"></i>-->
                                </td>
                                <td>
                                    <!--<i class="fa fa-minus" style="margin-right:5px;cursor:pointer"
                                       onclick="incdec('minus','final_jacket_f_waist')"></i>
                                    <span id="final_jacket_f_waist">0</span>
                                    <i class="fa fa-plus" style="margin-left:5px;cursor:pointer"
                                       onclick="incdec('plus','final_jacket_f_waist')"></i>-->
                                </td>
                                <td>
                                    <!--<i class="fa fa-minus" style="margin-right:5px;cursor:pointer"
                                       onclick="incdec('minus','final_vest_f_waist')"></i>
                                    <span id="final_vest_f_waist">0</span>
                                    <i class="fa fa-plus" style="margin-left:5px;cursor:pointer"
                                       onclick="incdec('plus','final_vest_f_waist')"></i>-->
                                </td>
                                <td>
                                    <i class="fa fa-minus" style="margin-right:5px;cursor:pointer"
                                       onclick="incdec('minus','final_pant_f_waist')"></i>
                                    <span id="final_pant_f_waist">0</span>
                                    <i class="fa fa-plus" style="margin-left:5px;cursor:pointer"
                                       onclick="incdec('plus','final_pant_f_waist')"></i>
                                </td>
                            </tr>
                            <tr>
                                <td>18.</td>
                                <td><span>Knee</span></td>
                                <td id="knee_right"></td>
                                <td id="knee_shirt_allow">0</td>
                                <td id="knee_jacket_allow">0</td>
                                <td id="knee_vest_allow">0</td>
                                <td id="knee_pant_allow">0</td>
                                <td>
                                    <!--<i class="fa fa-minus" style="margin-right:5px;cursor:pointer"
                                       onclick="incdec('minus','final_shirt_knee')"></i>
                                    <span id="final_shirt_knee">0</span>
                                    <i class="fa fa-plus" style="margin-left:5px;cursor:pointer"
                                       onclick="incdec('plus','final_shirt_knee')"></i>-->
                                </td>
                                <td>
                                    <!--<i class="fa fa-minus" style="margin-right:5px;cursor:pointer"
                                       onclick="incdec('minus','final_jacket_knee')"></i>
                                    <span id="final_jacket_knee">0</span>
                                    <i class="fa fa-plus" style="margin-left:5px;cursor:pointer"
                                       onclick="incdec('plus','final_jacket_knee')"></i>-->
                                </td>
                                <td>
                                    <!--<i class="fa fa-minus" style="margin-right:5px;cursor:pointer"
                                       onclick="incdec('minus','final_vest_knee')"></i>
                                    <span id="final_vest_knee">0</span>
                                    <i class="fa fa-plus" style="margin-left:5px;cursor:pointer"
                                       onclick="incdec('plus','final_vest_knee')"></i>-->
                                </td>
                                <td>
                                    <i class="fa fa-minus" style="margin-right:5px;cursor:pointer"
                                       onclick="incdec('minus','final_pant_knee')"></i>
                                    <span id="final_pant_knee">0</span>
                                    <i class="fa fa-plus" style="margin-left:5px;cursor:pointer"
                                       onclick="incdec('plus','final_pant_knee')"></i>
                                </td>
                            </tr>
                            <tr>
                                <td>19.</td>
                                <td><span>Back_Jacket_Length</span></td>
                                <td id="back_jacket_length"></td>
                                <td id="jacket_shirt_allow">0</td>
                                <td id="jacket_jacket_allow">0</td>
                                <td id="jacket_vest_allow">0</td>
                                <td id="jacket_pant_allow">0</td>
                                <td>
                                    <i class="fa fa-minus" style="margin-right:5px;cursor:pointer"
                                       onclick="incdec('minus','final_shirt_jacket')"></i>
                                    <span id="final_shirt_jacket">0</span>
                                    <i class="fa fa-plus" style="margin-left:5px;cursor:pointer"
                                       onclick="incdec('plus','final_shirt_jacket')"></i>
                                </td>
                                <td>
                                    <i class="fa fa-minus" style="margin-right:5px;cursor:pointer"
                                       onclick="incdec('minus','final_jacket_jacket')"></i>
                                    <span id="final_jacket_jacket">0</span>
                                    <i class="fa fa-plus" style="margin-left:5px;cursor:pointer"
                                       onclick="incdec('plus','final_jacket_jacket')"></i>
                                </td>
                                <td>
                                    <i class="fa fa-minus" style="margin-right:5px;cursor:pointer"
                                       onclick="incdec('minus','final_vest_jacket')"></i>
                                    <span id="final_vest_jacket">0</span>
                                    <i class="fa fa-plus" style="margin-left:5px;cursor:pointer"
                                       onclick="incdec('plus','final_vest_jacket')"></i>
                                </td>
                                <td>
                                    <!--<i class="fa fa-minus" style="margin-right:5px;cursor:pointer"
                                       onclick="incdec('minus','final_pant_jacket')"></i>
                                    <span id="final_pant_jacket">0</span>
                                    <i class="fa fa-plus" style="margin-left:5px;cursor:pointer"
                                       onclick="incdec('plus','final_pant_jacket')"></i>-->
                                </td>
                            </tr>
                            <tr>
                                <td>20.</td>
                                <td><span>U-rise</span></td>
                                <td id="u_rise"></td>
                                <td id="rise_shirt_allow">0</td>
                                <td id="rise_jacket_allow">0</td>
                                <td id="rise_vest_allow">0</td>
                                <td id="rise_pant_allow">0</td>
                                <td>
                                    <!--<i class="fa fa-minus" style="margin-right:5px;cursor:pointer"
                                       onclick="incdec('minus','final_shirt_rise')"></i>
                                    <span id="final_shirt_rise">0</span>
                                    <i class="fa fa-plus" style="margin-left:5px;cursor:pointer"
                                       onclick="incdec('plus','final_shirt_rise')"></i>-->
                                </td>
                                <td>
                                    <!--<i class="fa fa-minus" style="margin-right:5px;cursor:pointer"
                                       onclick="incdec('minus','final_jacket_rise')"></i>
                                    <span id="final_jacket_rise">0</span>
                                    <i class="fa fa-plus" style="margin-left:5px;cursor:pointer"
                                       onclick="incdec('plus','final_jacket_rise')"></i>-->
                                </td>
                                <td>
                                    <!--<i class="fa fa-minus" style="margin-right:5px;cursor:pointer"
                                       onclick="incdec('minus','final_vest_rise')"></i>
                                    <span id="final_vest_rise">0</span>
                                    <i class="fa fa-plus" style="margin-left:5px;cursor:pointer"
                                       onclick="incdec('plus','final_vest_rise')"></i>-->
                                </td>
                                <td>
                                    <i class="fa fa-minus" style="margin-right:5px;cursor:pointer"
                                       onclick="incdec('minus','final_pant_rise')"></i>
                                    <span id="final_pant_rise">0</span>
                                    <i class="fa fa-plus" style="margin-left:5px;cursor:pointer"
                                       onclick="incdec('plus','final_pant_rise')"></i>
                                </td>
                            </tr>
                            <tr>
                                <td>21.</td>
                                <td><span>Pant Left Outseam</span></td>
                                <td id="outseam_l_pants"></td>
                                <td id="p_left_shirt_allow">0</td>
                                <td id="p_left_jacket_allow">0</td>
                                <td id="p_left_vest_allow">0</td>
                                <td id="p_left_pant_allow">0</td>
                                <td>
                                    <!--<i class="fa fa-minus" style="margin-right:5px;cursor:pointer"
                                       onclick="incdec('minus','final_shirt_p_left')"></i>
                                    <span id="final_shirt_p_left">0</span>
                                    <i class="fa fa-plus" style="margin-left:5px;cursor:pointer"
                                       onclick="incdec('plus','final_shirt_p_left')"></i>-->
                                </td>
                                <td>
                                    <!--<i class="fa fa-minus" style="margin-right:5px;cursor:pointer"
                                       onclick="incdec('minus','final_jacket_p_left')"></i>
                                    <span id="final_jacket_p_left">0</span>
                                    <i class="fa fa-plus" style="margin-left:5px;cursor:pointer"
                                       onclick="incdec('plus','final_jacket_p_left')"></i>-->
                                </td>
                                <td>
                                    <!-- <i class="fa fa-minus" style="margin-right:5px;cursor:pointer"
                                        onclick="incdec('minus','final_vest_p_left')"></i>
                                     <span id="final_vest_p_left">0</span>
                                     <i class="fa fa-plus" style="margin-left:5px;cursor:pointer"
                                        onclick="incdec('plus','final_vest_p_left')"></i>-->
                                </td>
                                <td>
                                    <i class="fa fa-minus" style="margin-right:5px;cursor:pointer"
                                       onclick="incdec('minus','final_pant_p_left')"></i>
                                    <span id="final_pant_p_left">0</span>
                                    <i class="fa fa-plus" style="margin-left:5px;cursor:pointer"
                                       onclick="incdec('plus','final_pant_p_left')"></i>
                                </td>
                            </tr>
                            <tr>
                                <td>22.</td>
                                <td><span>Pant Right Outseam</span></td>
                                <td id="outseam_r_pants"></td>
                                <td id="p_right_shirt_allow">0</td>
                                <td id="p_right_jacket_allow">0</td>
                                <td id="p_right_vest_allow">0</td>
                                <td id="p_right_pant_allow">0</td>
                                <td>
                                    <!--<i class="fa fa-minus" style="margin-right:5px;cursor:pointer"
                                       onclick="incdec('minus','final_shirt_p_right')"></i>
                                    <span id="final_shirt_p_right">0</span>
                                    <i class="fa fa-plus" style="margin-left:5px;cursor:pointer"
                                       onclick="incdec('plus','final_shirt_p_right')"></i>-->
                                </td>
                                <td>
                                    <!--<i class="fa fa-minus" style="margin-right:5px;cursor:pointer"
                                       onclick="incdec('minus','final_jacket_p_right')"></i>
                                    <span id="final_jacket_p_right">0</span>
                                    <i class="fa fa-plus" style="margin-left:5px;cursor:pointer"
                                       onclick="incdec('plus','final_jacket_p_right')"></i>-->
                                </td>
                                <td>
                                    <!--<i class="fa fa-minus" style="margin-right:5px;cursor:pointer"
                                       onclick="incdec('minus','final_vest_p_right')"></i>
                                    <span id="final_vest_p_right">0</span>
                                    <i class="fa fa-plus" style="margin-left:5px;cursor:pointer"
                                       onclick="incdec('plus','final_vest_p_right')"></i>-->
                                </td>
                                <td>
                                    <i class="fa fa-minus" style="margin-right:5px;cursor:pointer"
                                       onclick="incdec('minus','final_pant_p_right')"></i>
                                    <span id="final_pant_p_right">0</span>
                                    <i class="fa fa-plus" style="margin-left:5px;cursor:pointer"
                                       onclick="incdec('plus','final_pant_p_right')"></i>
                                </td>
                            </tr>
                            <tr>
                                <td>23.</td>
                                <td><span>Bottom</span></td>
                                <td id="bottom"></td>
                                <td id="bottom_shirt_allow">0</td>
                                <td id="bottom_jacket_allow">0</td>
                                <td id="bottom_vest_allow">0</td>
                                <td id="bottom_pant_allow">0</td>
                                <td>
                                    <!--<i class="fa fa-minus" style="margin-right:5px;cursor:pointer"
                                       onclick="incdec('minus','final_shirt_bottom')"></i>
                                    <span id="final_shirt_bottom">0</span>
                                    <i class="fa fa-plus" style="margin-left:5px;cursor:pointer"
                                       onclick="incdec('plus','final_shirt_bottom')"></i>-->
                                </td>
                                <td>
                                    <!--<i class="fa fa-minus" style="margin-right:5px;cursor:pointer"
                                       onclick="incdec('minus','final_jacket_bottom')"></i>
                                    <span id="final_jacket_bottom">0</span>
                                    <i class="fa fa-plus" style="margin-left:5px;cursor:pointer"
                                       onclick="incdec('plus','final_jacket_bottom')"></i>-->
                                </td>
                                <td>
                                    <!--<i class="fa fa-minus" style="margin-right:5px;cursor:pointer"
                                       onclick="incdec('minus','final_vest_bottom')"></i>
                                    <span id="final_vest_bottom">0</span>
                                    <i class="fa fa-plus" style="margin-left:5px;cursor:pointer"
                                       onclick="incdec('plus','final_vest_bottom')"></i>-->
                                </td>
                                <td>
                                    <i class="fa fa-minus" style="margin-right:5px;cursor:pointer"
                                       onclick="incdec('minus','final_pant_bottom')"></i>
                                    <span id="final_pant_bottom">0</span>
                                    <i class="fa fa-plus" style="margin-left:5px;cursor:pointer"
                                       onclick="incdec('plus','final_pant_bottom')"></i>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                    <div class="col-md-12" style="padding:0">
                        <input type="button" class="btn btn-info pull-right savebtn" value="Save Changes" onclick="confirmSave()"/>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- /page content -->
<?php
include('footer.php');
?>
<script>
    var answer = "";
    var ques1, ques2, ques3, ques4, ques5, ques6, svalue, jvalue, pvalue, vvalue, value, answer1, answer2, answer3, answer4, answer5, answer6,
        janswer1, janswer2, vanswer1, panswer1,collarshirt,collarjacket,collarvest,collarpant,chestshirt,chestjacket,chestvest,chestpant,
        stomachshirt,stomachjacket,stomachpant,stomachvest,seatshirt,seatjacket,seatpant,seatvest,bicepshirt,bicepjacket,biceppant,bicepvest,
        backshouldershirt,backshoulderjacket,backshoulderpant,backshouldervest,frontshouldershirt,frontshoulderjacket,frontshoulderpant,
        frontshouldervest,sleeveleftshirt,sleeveleftjacket,sleeveleftpant,sleeveleftvest,sleeverightshirt,sleeverightjacket,sleeverightpant,
        sleeverightvest,thighshirt,thighjacket,thighpant,thighvest,napetowaistshirt,napetowaistjacket,napetowaistpant,napetowaistvest,
        frontwaistlengthshirt,frontwaistlengthjacket,frontwaistlengthpant,frontwaistlengthvest,wristshirt,wristjacket,wristpant,wristvest,
        calfshirt,calfjacket,calfpant,calfvest,backwaistheightshirt,backwaistheightjacket,backwaistheightpant,backwaistheightvest,
        frontwaistheightshirt,frontwaistheightjacket,frontwaistheightpant,frontwaistheightvest,kneeshirt,kneejacket,kneepant,kneevest,
        backjacketlengthshirt,backjacketlengthjacket,backjacketlengthpant,backjacketlengthvest,riseshirt,risejacket,risepant,risevest,
        pantleftshirt,pantleftjacket,pantleftpant,pantleftvest,pantrightshirt,pantrightjacket,pantrightpant,pantrightvest,bottomshirt,
        bottomjacket,bottompant,bottomvest;
    function getMeasureData(sizeData,measurement_id) {
        if (sizeData.unit_type == "Inches") {
            var chest = parseFloat(sizeData.chest)*2.54;///cm conversion
            var stomach = parseFloat(sizeData.stomach)*2.54;///cm conversion
            ////collar shirt
            answer1 = sizeData.shirt_que_1;
            answer2 = sizeData.shirt_que_2;
            answer3 = sizeData.shirt_que_3;
            answer4 = sizeData.shirt_que_4;
            answer5 = sizeData.shirt_que_5;
            answer6 = sizeData.shirt_que_6;
            janswer1 = sizeData.suit_que_1;
            janswer2 = sizeData.suit_que_2;
            vanswer1 = sizeData.vest_que_1;
            panswer1 = sizeData.pant_que_1;

            if (chest-stomach < 7) {
                value = getResponse2("collar", "ques1:" + answer1 + "@ques2:" + answer2 + "@ques3:" + answer3 + "@ques4:" + answer4 + "@ques5:" + answer5 + "@ques6:" + answer6 + "@jques1lessthan7:" + janswer1 + "@jques2lessthan7:" + janswer2 + "@vques1:" + vanswer1 + "@pques1:" + panswer1);
            } else {
                value = getResponse2("collar", "ques1:" + answer1 + "@ques2:" + answer2 + "@ques3:" + answer3 + "@ques4:" + answer4 + "@ques5:" + answer5 + "@ques6:" + answer6 + "@jques1:" + janswer1 + "@jques2:" + janswer2 + "@vques1:" + vanswer1 + "@pques1:" + panswer1);
            }
            svalue = value.split(";")[0];
            collarshirt = svalue;
            jvalue = value.split(";")[1];
            collarjacket = jvalue;
            vvalue = value.split(";")[2];
            collarvest = vvalue;
            pvalue = value.split(";")[3];
            collarpant = pvalue;
            var collar_shirt = parseFloat(svalue);
            $("#collar_shirt_allow").html((collar_shirt / 2.54).toFixed(1) + " Inch");
            //$("#final_shirt_collar").html((parseFloat(sizeData.collar) + parseFloat(collar_shirt / 2.54)).toFixed(1) + " Inch");
            // for collar jacket
            var collar_jacket = (parseFloat(jvalue));
            $("#collar_jacket_allow").html((collar_jacket / 2.54).toFixed(1) + " Inch");
            //$("#final_jacket_collar").html((parseFloat(sizeData.collar) + parseFloat(collar_jacket / 2.54)).toFixed(1) + " Inch");
            // for collar vest
            var collar_vest = (parseFloat(vvalue));
            $("#collar_vest_allow").html((collar_vest / 2.54).toFixed(1) + " Inch");
            //$("#final_vest_collar").html((parseFloat(sizeData.collar) + parseFloat(collar_vest / 2.54)).toFixed(1) + " Inch");
            // for collar pant
            var collar_pant = (parseFloat(pvalue));
            $("#collar_pant_allow").html((collar_pant / 2.54).toFixed(1) + " Inch");
            //$("#final_pant_collar").html((parseFloat(sizeData.collar) + parseFloat(collar_pant / 2.54)).toFixed(1) + " Inch");

            //for chest shirt
            answer1 = sizeData.shirt_que_1;
            answer2 = sizeData.shirt_que_2;
            answer3 = sizeData.shirt_que_3;
            answer4 = sizeData.shirt_que_4;
            answer5 = sizeData.shirt_que_5;
            answer6 = sizeData.shirt_que_6;
            janswer1 = sizeData.suit_que_1;
            janswer2 = sizeData.suit_que_2;
            vanswer1 = sizeData.vest_que_1;
            panswer1 = sizeData.pant_que_1;
            if (chest-stomach < 7) {
                value = getResponse2("chest", "ques1:" + answer1 + "@ques2:" + answer2 + "@ques3:" + answer3 + "@ques4:" + answer4 + "@ques5:" + answer5 + "@ques6:" + answer6 + "@jques1lessthan7:" + janswer1 + "@jques2lessthan7:" + janswer2 + "@vques1:" + vanswer1 + "@pques1:" + panswer1);
            } else {
                value = getResponse2("chest", "ques1:" + answer1 + "@ques2:" + answer2 + "@ques3:" + answer3 + "@ques4:" + answer4 + "@ques5:" + answer5 + "@ques6:" + answer6 + "@jques1:" + janswer1 + "@jques2:" + janswer2 + "@vques1:" + vanswer1 + "@pques1:" + panswer1);
            }
            svalue = value.split(";")[0];
            chestshirt = svalue;
            jvalue = value.split(";")[1];
            chestjacket = jvalue;
            vvalue = value.split(";")[2];
            chestvest = vvalue;
            pvalue = value.split(";")[3];
            chestpant = pvalue;
            // for chest shirt
            var chest_shirt = parseFloat(svalue);
            $("#chest_shirt_allow").html((chest_shirt / 2.54).toFixed(1) + " Inch");
            //$("#final_shirt_chest").html((parseFloat(sizeData.chest) + parseFloat(chest_shirt / 2.54)).toFixed(1) + " Inch");
            // for chest jacket
            var chest_jacket = (parseFloat(jvalue));
            $("#chest_jacket_allow").html((chest_jacket / 2.54).toFixed(1) + " Inch");
            //$("#final_jacket_chest").html((parseFloat(sizeData.chest) + parseFloat(chest_jacket / 2.54)).toFixed(1) + " Inch");
            // for chest vest
            var chest_vest = parseFloat(vvalue);
            $("#chest_vest_allow").html((chest_vest / 2.54).toFixed(1) + " Inch");
            //$("#final_vest_chest").html((parseFloat(sizeData.chest) + parseFloat(chest_vest / 2.54)).toFixed(1) + " Inch");
            // for chest pant
            var chest_pant = (parseFloat(pvalue));
            $("#chest_pant_allow").html((chest_pant / 2.54).toFixed(1) + " Inch");
            //$("#final_pant_chest").html((parseFloat(sizeData.chest) + parseFloat(chest_pant / 2.54)).toFixed(1) + " Inch");

            ////////for Stomach shirt
            answer1 = sizeData.shirt_que_1;
            answer2 = sizeData.shirt_que_2;
            answer3 = sizeData.shirt_que_3;
            answer4 = sizeData.shirt_que_4;
            answer5 = sizeData.shirt_que_5;
            answer6 = sizeData.shirt_que_6;
            janswer1 = sizeData.suit_que_1;
            janswer2 = sizeData.suit_que_2;
            vanswer1 = sizeData.vest_que_1;
            panswer1 = sizeData.pant_que_1;
            if (chest-stomach < 7) {
                value = getResponse2("stomach", "ques1:" + answer1 + "@ques2:" + answer2 + "@ques3:" + answer3 + "@ques4:" + answer4 + "@ques5:" + answer5 + "@ques6:" + answer6 + "@jques1lessthan7:" + janswer1 + "@jques2lessthan7:" + janswer2 + "@vques1:" + vanswer1 + "@pques1:" + panswer1);
            } else {
                value = getResponse2("stomach", "ques1:" + answer1 + "@ques2:" + answer2 + "@ques3:" + answer3 + "@ques4:" + answer4 + "@ques5:" + answer5 + "@ques6:" + answer6 + "@jques1:" + janswer1 + "@jques2:" + janswer2 + "@vques1:" + vanswer1 + "@pques1:" + panswer1);
            }
            svalue = value.split(";")[0];
            stomachshirt = svalue;
            jvalue = value.split(";")[1];
            stomachjacket = jvalue;
            vvalue = value.split(";")[2];
            stomachvest = vvalue;
            pvalue = value.split(";")[3];
            stomachpant = pvalue;
            var stomach_shirt = parseFloat(svalue);
            $("#stomach_shirt_allow").html((stomach_shirt / 2.54).toFixed(1) + " Inch");
            //$("#final_shirt_stomach").html((parseFloat(sizeData.stomach) + parseFloat(stomach_shirt / 2.54)).toFixed(1) + " Inch");
            // for stomach jacket
            var stomach_jacket = (parseFloat(jvalue));
            $("#stomach_jacket_allow").html((stomach_jacket / 2.54).toFixed(1) + " Inch");
            //$("#final_jacket_stomach").html((parseFloat(sizeData.stomach) + parseFloat(stomach_jacket / 2.54)).toFixed(1) + " Inch");
            // for stomach vest
            var stomach_vest = (parseFloat(vvalue));
            $("#stomach_vest_allow").html((stomach_vest / 2.54).toFixed(1) + " Inch");
            //$("#final_vest_stomach").html((parseFloat(sizeData.stomach) + parseFloat(stomach_vest / 2.54)).toFixed(1) + " Inch");
            // for stomach pant
            var stomach_pant = (parseFloat(pvalue));
            $("#stomach_pant_allow").html((stomach_pant / 2.54).toFixed(1) + " Inch");
            //$("#final_pant_stomach").html((parseFloat(sizeData.stomach) + parseFloat(stomach_pant / 2.54)).toFixed(1) + " Inch");

            //for seat shirt
            answer1 = sizeData.shirt_que_1;
            answer2 = sizeData.shirt_que_2;
            answer3 = sizeData.shirt_que_3;
            answer4 = sizeData.shirt_que_4;
            answer5 = sizeData.shirt_que_5;
            answer6 = sizeData.shirt_que_6;
            janswer1 = sizeData.suit_que_1;
            janswer2 = sizeData.suit_que_2;
            vanswer1 = sizeData.vest_que_1;
            panswer1 = sizeData.pant_que_1;
            if (chest-stomach < 7) {
                value = getResponse2("seat", "ques1:" + answer1 + "@ques2:" + answer2 + "@ques3:" + answer3 + "@ques4:" + answer4 + "@ques5:" + answer5 + "@ques6:" + answer6 + "@jques1lessthan7:" + janswer1 + "@jques2lessthan7:" + janswer2 + "@vques1:" + vanswer1 + "@pques1:" + panswer1);
            } else {
                value = getResponse2("seat", "ques1:" + answer1 + "@ques2:" + answer2 + "@ques3:" + answer3 + "@ques4:" + answer4 + "@ques5:" + answer5 + "@ques6:" + answer6 + "@jques1:" + janswer1 + "@jques2:" + janswer2 + "@vques1:" + vanswer1 + "@pques1:" + panswer1);
            }
            svalue = value.split(";")[0];
            seatshirt = svalue;
            jvalue = value.split(";")[1];
            seatjacket = jvalue;
            vvalue = value.split(";")[2];
            seatvest = vvalue;
            pvalue = value.split(";")[3];
            seatpant = pvalue;
            // for seat shirt
            var seat_shirt = parseFloat(svalue);
            $("#seat_shirt_allow").html((seat_shirt / 2.54).toFixed(1) + " Inch");
            //$("#final_shirt_seat").html((parseFloat(sizeData.seat) + parseFloat(seat_shirt / 2.54)).toFixed(1) + " Inch");
            // for seat jacket
            var seat_jacket = (parseFloat(jvalue));
            $("#seat_jacket_allow").html((seat_jacket / 2.54).toFixed(1) + " Inch");
            //$("#final_jacket_seat").html((parseFloat(sizeData.seat) + parseFloat(seat_jacket / 2.54)).toFixed(1) + " Inch");
            // for seat vest
            var seat_vest = parseFloat(vvalue);
            $("#seat_vest_allow").html((seat_vest / 2.54).toFixed(1) + " Inch");
            //$("#final_vest_seat").html((parseFloat(sizeData.seat) + parseFloat(seat_vest / 2.54)).toFixed(1) + " Inch");
            // for seat_pant
            var seat_pant = (parseFloat(pvalue));
            $("#seat_pant_allow").html((seat_pant / 2.54).toFixed(1) + " Inch");
            //$("#final_pant_seat").html((parseFloat(sizeData.seat) + parseFloat(seat_pant / 2.54)).toFixed(1) + " Inch");

            ///////for bicep shirt
            answer1 = sizeData.shirt_que_1;
            answer2 = sizeData.shirt_que_2;
            answer3 = sizeData.shirt_que_3;
            answer4 = sizeData.shirt_que_4;
            answer5 = sizeData.shirt_que_5;
            answer6 = sizeData.shirt_que_6;
            janswer1 = sizeData.suit_que_1;
            janswer2 = sizeData.suit_que_2;
            vanswer1 = sizeData.vest_que_1;
            panswer1 = sizeData.pant_que_1;
            if (chest-stomach < 7) {
                value = getResponse2("bicep", "ques1:" + answer1 + "@ques2:" + answer2 + "@ques3:" + answer3 + "@ques4:" + answer4 + "@ques5:" + answer5 + "@ques6:" + answer6 + "@jques1lessthan7:" + janswer1 + "@jques2lessthan7:" + janswer2 + "@vques1:" + vanswer1 + "@pques1:" + panswer1);
            } else {
                value = getResponse2("bicep", "ques1:" + answer1 + "@ques2:" + answer2 + "@ques3:" + answer3 + "@ques4:" + answer4 + "@ques5:" + answer5 + "@ques6:" + answer6 + "@jques1:" + janswer1 + "@jques2:" + janswer2 + "@vques1:" + vanswer1 + "@pques1:" + panswer1);
            }
            svalue = value.split(";")[0];
            bicepshirt = svalue;
            jvalue = value.split(";")[1];
            bicepjacket = jvalue;
            vvalue = value.split(";")[2];
            bicepvest = vvalue;
            pvalue = value.split(";")[3];
            biceppant = pvalue;
            var bicep_shirt = parseFloat(svalue);
            $("#bicep_shirt_allow").html((bicep_shirt / 2.54).toFixed(1) + " Inch");
            //$("#final_shirt_bicep").html((parseFloat(sizeData.bicep) + parseFloat(bicep_shirt / 2.54)).toFixed(1) + " Inch");
            //for bicep jacket
            var bicep_jacket = (parseFloat(jvalue));
            $("#bicep_jacket_allow").html((bicep_jacket / 2.54).toFixed(1) + " Inch");
            //$("#final_jacket_bicep").html((parseFloat(sizeData.bicep) + parseFloat(bicep_jacket / 2.54)).toFixed(1) + " Inch");
            // for bicep vest
            var bicep_vest = parseFloat(vvalue);
            $("#bicep_vest_allow").html((bicep_vest / 2.54).toFixed(1) + " Inch");
            //$("#final_vest_bicep").html((parseFloat(sizeData.bicep) + parseFloat(bicep_vest / 2.54)).toFixed(1) + " Inch");
            //for bicep pant
            var bicep_pant = (parseFloat(pvalue));
            $("#bicep_pant_allow").html((bicep_pant / 2.54).toFixed(1) + " Inch");
            //$("#final_pant_bicep").html((parseFloat(sizeData.bicep) + parseFloat(bicep_pant / 2.54)).toFixed(1) + " Inch");

            //for back shoulder shirt
            answer1 = sizeData.shirt_que_1;
            answer2 = sizeData.shirt_que_2;
            answer3 = sizeData.shirt_que_3;
            answer4 = sizeData.shirt_que_4;
            answer5 = sizeData.shirt_que_5;
            answer6 = sizeData.shirt_que_6;
            janswer1 = sizeData.suit_que_1;
            janswer2 = sizeData.suit_que_2;
            vanswer1 = sizeData.vest_que_1;
            panswer1 = sizeData.pant_que_1;
            if (chest-stomach < 7) {
                value = getResponse2("back_shoulder_width", "ques1:" + answer1 + "@ques2:" + answer2 + "@ques3:" + answer3 + "@ques4:" + answer4 + "@ques5:" + answer5 + "@ques6:" + answer6 + "@jques1lessthan7:" + janswer1 + "@jques2lessthan7:" + janswer2 + "@vques1:" + vanswer1 + "@pques1:" + panswer1);
            } else {
                value = getResponse2("back_shoulder_width", "ques1:" + answer1 + "@ques2:" + answer2 + "@ques3:" + answer3 + "@ques4:" + answer4 + "@ques5:" + answer5 + "@ques6:" + answer6 + "@jques1:" + janswer1 + "@jques2:" + janswer2 + "@vques1:" + vanswer1 + "@pques1:" + panswer1);
            }
            svalue = value.split(";")[0];
            backshouldershirt = svalue;
            jvalue = value.split(";")[1];
            backshoulderjacket = jvalue;
            vvalue = value.split(";")[2];
            backshouldervest = vvalue;
            pvalue = value.split(";")[3];
            backshoulderpant = pvalue;
            //for back shoulder shirt
            var back_shoulder_shirt = parseFloat(svalue);
            $("#back_shoulder_shirt_allow").html((back_shoulder_shirt / 2.54).toFixed(1) + " Inch");
            //$("#final_shirt_back_shoulder").html((parseFloat(sizeData.back_sholuder) + parseFloat(back_shoulder_shirt / 2.54)).toFixed(1) + " Inch");
            //for back shoulder jacket
            var back_shoulder_jacket = (parseFloat(jvalue));
            $("#back_shoulder_jacket_allow").html((back_shoulder_jacket / 2.54).toFixed(1) + " Inch");
            //$("#final_jacket_back_shoulder").html((parseFloat(sizeData.back_sholuder) + parseFloat(back_shoulder_jacket / 2.54)).toFixed(1) + " Inch");
            // for back shoulder vest
            var back_shoulder_vest = parseFloat(vvalue);
            $("#back_shoulder_vest_allow").html((back_shoulder_vest / 2.54).toFixed(1) + " Inch");
            //$("#final_vest_back_shoulder").html((parseFloat(sizeData.back_sholuder) + parseFloat(back_shoulder_vest / 2.54)).toFixed(1) + " Inch");
            //for back shoulder pant
            var back_shoulder_pant = (parseFloat(pvalue));
            $("#back_shoulder_pant_allow").html((back_shoulder_pant / 2.54).toFixed(1) + " Inch");
            //$("#final_pant_back_shoulder").html((parseFloat(sizeData.back_sholuder) + parseFloat(back_shoulder_pant / 2.54)).toFixed(1) + " Inch");

            ////////for front shoulder shirt
            answer1 = sizeData.shirt_que_1;
            answer2 = sizeData.shirt_que_2;
            answer3 = sizeData.shirt_que_3;
            answer4 = sizeData.shirt_que_4;
            answer5 = sizeData.shirt_que_5;
            answer6 = sizeData.shirt_que_6;
            janswer1 = sizeData.suit_que_1;
            janswer2 = sizeData.suit_que_2;
            vanswer1 = sizeData.vest_que_1;
            panswer1 = sizeData.pant_que_1;
            if (chest-stomach < 7) {
                value = getResponse2("front_shoulder_width", "ques1:" + answer1 + "@ques2:" + answer2 + "@ques3:" + answer3 + "@ques4:" + answer4 + "@ques5:" + answer5 + "@ques6:" + answer6 + "@jques1lessthan7:" + janswer1 + "@jques2lessthan7:" + janswer2 + "@vques1:" + vanswer1 + "@pques1:" + panswer1);
            } else {
                value = getResponse2("front_shoulder_width", "ques1:" + answer1 + "@ques2:" + answer2 + "@ques3:" + answer3 + "@ques4:" + answer4 + "@ques5:" + answer5 + "@ques6:" + answer6 + "@jques1:" + janswer1 + "@jques2:" + janswer2 + "@vques1:" + vanswer1 + "@pques1:" + panswer1);
            }
            svalue = value.split(";")[0];
            frontshouldershirt = svalue;
            jvalue = value.split(";")[1];
            frontshoulderjacket = jvalue;
            vvalue = value.split(";")[2];
            frontshouldervest = vvalue;
            pvalue = value.split(";")[3];
            frontshoulderpant = pvalue;
            // for front shoulder shirt
            var front_shoulder_shirt = parseFloat(svalue);
            $("#front_shoulder_shirt_allow").html((front_shoulder_shirt / 2.54).toFixed(1) + " Inch");
            //$("#final_shirt_front_shoulder").html((parseFloat(sizeData.front_shoulder) + parseFloat(front_shoulder_shirt / 2.54)).toFixed(1) + " Inch");
            //for front shoulder jacket
            var front_shoulder_jacket = (parseFloat(jvalue));
            $("#front_shoulder_jacket_allow").html((front_shoulder_jacket / 2.54).toFixed(1) + " Inch");
            //$("#final_jacket_front_shoulder").html((parseFloat(sizeData.front_shoulder) + parseFloat(front_shoulder_jacket / 2.54)).toFixed(1) + " Inch");
            // for front shoulder vest
            var front_shoulder_vest = parseFloat(vvalue);
            $("#front_shoulder_vest_allow").html((front_shoulder_vest / 2.54).toFixed(1) + " Inch");
            //$("#final_vest_front_shoulder").html((parseFloat(sizeData.front_shoulder) + parseFloat(front_shoulder_vest / 2.54)).toFixed(1) + " Inch");
            // for front shoulder pant
            var front_shoulder_pant = (parseFloat(pvalue));
            $("#front_shoulder_pant_allow").html((front_shoulder_pant / 2.54).toFixed(1) + " Inch");
            //$("#final_pant_front_shoulder").html((parseFloat(sizeData.front_shoulder) + parseFloat(front_shoulder_pant / 2.54)).toFixed(1) + " Inch");

            //for left_sleeve_length shirt
            answer1 = sizeData.shirt_que_1;
            answer2 = sizeData.shirt_que_2;
            answer3 = sizeData.shirt_que_3;
            answer4 = sizeData.shirt_que_4;
            answer5 = sizeData.shirt_que_5;
            answer6 = sizeData.shirt_que_6;
            janswer1 = sizeData.suit_que_1;
            janswer2 = sizeData.suit_que_2;
            vanswer1 = sizeData.vest_que_1;
            panswer1 = sizeData.pant_que_1;
            if (chest-stomach < 7) {
                value = getResponse2("left_sleeve_length", "ques1:" + answer1 + "@ques2:" + answer2 + "@ques3:" + answer3 + "@ques4:" + answer4 + "@ques5:" + answer5 + "@ques6:" + answer6 + "@jques1lessthan7:" + janswer1 + "@jques2lessthan7:" + janswer2 + "@vques1:" + vanswer1 + "@pques1:" + panswer1);
            } else {
                value = getResponse2("left_sleeve_length", "ques1:" + answer1 + "@ques2:" + answer2 + "@ques3:" + answer3 + "@ques4:" + answer4 + "@ques5:" + answer5 + "@ques6:" + answer6 + "@jques1:" + janswer1 + "@jques2:" + janswer2 + "@vques1:" + vanswer1 + "@pques1:" + panswer1);
            }
            svalue = value.split(";")[0];
            sleeveleftshirt = svalue;
            jvalue = value.split(";")[1];
            sleeveleftjacket = jvalue;
            vvalue = value.split(";")[2];
            sleeveleftvest = vvalue;
            pvalue = value.split(";")[3];
            sleeveleftpant = pvalue;
            var left_sleeve_length_shirt = parseFloat(svalue);
            $("#sleeve_left_shirt_allow").html((left_sleeve_length_shirt / 2.54).toFixed(1) + " Inch");
            //$("#final_shirt_sleeve_left").html((parseFloat(sizeData.sleeve_length_left) + parseFloat(left_sleeve_length_shirt / 2.54)).toFixed(1) + " Inch");
            //for front shoulder jacket
            var left_sleeve_length_jacket = (parseFloat(jvalue));
            $("#sleeve_left_jacket_allow").html((left_sleeve_length_jacket / 2.54).toFixed(1) + " Inch");
            //$("#final_jacket_sleeve_left").html((parseFloat(sizeData.sleeve_length_left) + parseFloat(left_sleeve_length_jacket / 2.54)).toFixed(1) + " Inch");
            //for front shoulder vest
            var left_sleeve_length_vest = parseFloat(vvalue);
            $("#sleeve_left_vest_allow").html((left_sleeve_length_vest / 2.54).toFixed(1) + " Inch");
            //$("#final_vest_sleeve_left").html((parseFloat(sizeData.sleeve_length_left) + parseFloat(left_sleeve_length_vest / 2.54)).toFixed(1) + " Inch");
            //for front shoulder pant
            var left_sleeve_length_pant = (parseFloat(pvalue));
            $("#sleeve_left_pant_allow").html((left_sleeve_length_pant / 2.54).toFixed(1) + " Inch");
            //$("#final_pant_sleeve_left").html((parseFloat(sizeData.sleeve_length_left) + parseFloat(left_sleeve_length_pant / 2.54)).toFixed(1) + " Inch");

            /////for right_sleeve_length shirt
            answer1 = sizeData.shirt_que_1;
            answer2 = sizeData.shirt_que_2;
            answer3 = sizeData.shirt_que_3;
            answer4 = sizeData.shirt_que_4;
            answer5 = sizeData.shirt_que_5;
            answer6 = sizeData.shirt_que_6;
            janswer1 = sizeData.suit_que_1;
            janswer2 = sizeData.suit_que_2;
            vanswer1 = sizeData.vest_que_1;
            panswer1 = sizeData.pant_que_1;
            if (chest-stomach < 7) {
                value = getResponse2("right_sleeve_length", "ques1:" + answer1 + "@ques2:" + answer2 + "@ques3:" + answer3 + "@ques4:" + answer4 + "@ques5:" + answer5 + "@ques6:" + answer6 + "@jques1lessthan7:" + janswer1 + "@jques2lessthan7:" + janswer2 + "@vques1:" + vanswer1 + "@pques1:" + panswer1);
            } else {
                value = getResponse2("right_sleeve_length", "ques1:" + answer1 + "@ques2:" + answer2 + "@ques3:" + answer3 + "@ques4:" + answer4 + "@ques5:" + answer5 + "@ques6:" + answer6 + "@jques1:" + janswer1 + "@jques2:" + janswer2 + "@vques1:" + vanswer1 + "@pques1:" + panswer1);
            }
            svalue = value.split(";")[0];
            sleeverightshirt = svalue;
            jvalue = value.split(";")[1];
            sleeverightjacket = jvalue;
            vvalue = value.split(";")[2];
            sleeverightvest = vvalue;
            pvalue = value.split(";")[3];
            sleeverightpant = pvalue;
            //for right_sleeve_length shirt
            var right_sleeve_length_shirt = parseFloat(svalue);
            $("#sleeve_right_shirt_allow").html((right_sleeve_length_shirt / 2.54).toFixed(1) + " Inch");
            //$("#final_shirt_sleeve_right").html((parseFloat(sizeData.sleeve_length_right) + parseFloat(right_sleeve_length_shirt / 2.54)).toFixed(1) + " Inch");
            //for right_sleeve_length jacket
            var right_sleeve_length_jacket = (parseFloat(jvalue));
            $("#sleeve_right_jacket_allow").html((right_sleeve_length_jacket / 2.54).toFixed(1) + " Inch");
            //$("#final_jacket_sleeve_right").html((parseFloat(sizeData.sleeve_length_right) + parseFloat(right_sleeve_length_jacket / 2.54)).toFixed(1) + " Inch");
            //for right_sleeve_length vest
            var right_sleeve_length_vest = parseFloat(vvalue);
            $("#sleeve_right_vest_allow").html((right_sleeve_length_vest / 2.54).toFixed(1) + " Inch");
            //$("#final_vest_sleeve_right").html((parseFloat(sizeData.sleeve_length_right) + parseFloat(right_sleeve_length_vest / 2.54)).toFixed(1) + " Inch");
            //for right_sleeve_length pant
            var right_sleeve_length_pant = (parseFloat(pvalue));
            $("#sleeve_right_pant_allow").html((right_sleeve_length_pant / 2.54).toFixed(1) + " Inch");
            //$("#final_pant_sleeve_right").html((parseFloat(sizeData.sleeve_length_right) + parseFloat(right_sleeve_length_pant / 2.54)).toFixed(1) + " Inch");

            //// for thigh shirt
            answer1 = sizeData.shirt_que_1;
            answer2 = sizeData.shirt_que_2;
            answer3 = sizeData.shirt_que_3;
            answer4 = sizeData.shirt_que_4;
            answer5 = sizeData.shirt_que_5;
            answer6 = sizeData.shirt_que_6;
            janswer1 = sizeData.suit_que_1;
            janswer2 = sizeData.suit_que_2;
            vanswer1 = sizeData.vest_que_1;
            panswer1 = sizeData.pant_que_1;
            if (chest-stomach < 7) {
                value = getResponse2("thigh", "ques1:" + answer1 + "@ques2:" + answer2 + "@ques3:" + answer3 + "@ques4:" + answer4 + "@ques5:" + answer5 + "@ques6:" + answer6 + "@jques1lessthan7:" + janswer1 + "@jques2lessthan7:" + janswer2 + "@vques1:" + vanswer1 + "@pques1:" + panswer1);
            } else {
                value = getResponse2("thigh", "ques1:" + answer1 + "@ques2:" + answer2 + "@ques3:" + answer3 + "@ques4:" + answer4 + "@ques5:" + answer5 + "@ques6:" + answer6 + "@jques1:" + janswer1 + "@jques2:" + janswer2 + "@vques1:" + vanswer1 + "@pques1:" + panswer1);
            }
            svalue = value.split(";")[0];
            thighshirt = svalue;
            jvalue = value.split(";")[1];
            thighjacket = jvalue;
            vvalue = value.split(";")[2];
            thighvest = vvalue;
            pvalue = value.split(";")[3];
            thighpant = pvalue;
            //for thigh shirt
            var thigh_shirt = parseFloat(svalue);
            $("#thigh_shirt_allow").html((thigh_shirt / 2.54).toFixed(1) + " Inch");
            //$("#final_shirt_thigh").html((parseFloat(sizeData.thigh) + parseFloat(thigh_shirt / 2.54)).toFixed(1) + " Inch");
            //for thigh jacket
            var thigh_jacket = (parseFloat(jvalue));
            $("#thigh_jacket_allow").html((thigh_jacket / 2.54).toFixed(1) + " Inch");
            //$("#final_jacket_thigh").html((parseFloat(sizeData.thigh) + parseFloat(thigh_jacket / 2.54)).toFixed(1) + " Inch");
            // for thigh vest
            var thigh_vest = parseFloat(vvalue);
            $("#thigh_vest_allow").html((thigh_vest / 2.54).toFixed(1) + " Inch");
            //$("#final_vest_thigh").html((parseFloat(sizeData.thigh) + parseFloat(thigh_vest / 2.54)).toFixed(1) + " Inch");
            //for thigh pant
            var thigh_pant = (parseFloat(pvalue));
            $("#thigh_pant_allow").html((thigh_pant / 2.54).toFixed(1) + " Inch");
            //$("#final_pant_thigh").html((parseFloat(sizeData.thigh) + parseFloat(thigh_pant / 2.54)).toFixed(1) + " Inch");

            //for nape shirt
            answer1 = sizeData.shirt_que_1;
            answer2 = sizeData.shirt_que_2;
            answer3 = sizeData.shirt_que_3;
            answer4 = sizeData.shirt_que_4;
            answer5 = sizeData.shirt_que_5;
            answer6 = sizeData.shirt_que_6;
            janswer1 = sizeData.suit_que_1;
            janswer2 = sizeData.suit_que_2;
            vanswer1 = sizeData.vest_que_1;
            panswer1 = sizeData.pant_que_1;
            if (chest-stomach < 7) {
                value = getResponse2("nape_to_waist", "ques1:" + answer1 + "@ques2:" + answer2 + "@ques3:" + answer3 + "@ques4:" + answer4 + "@ques5:" + answer5 + "@ques6:" + answer6 + "@jques1lessthan7:" + janswer1 + "@jques2lessthan7:" + janswer2 + "@vques1:" + vanswer1 + "@pques1:" + panswer1);
            } else {
                value = getResponse2("nape_to_waist", "ques1:" + answer1 + "@ques2:" + answer2 + "@ques3:" + answer3 + "@ques4:" + answer4 + "@ques5:" + answer5 + "@ques6:" + answer6 + "@jques1:" + janswer1 + "@jques2:" + janswer2 + "@vques1:" + vanswer1 + "@pques1:" + panswer1);
            }
            svalue = value.split(";")[0];
            napetowaistshirt = svalue;
            jvalue = value.split(";")[1];
            napetowaistjacket = jvalue;
            vvalue = value.split(";")[2];
            napetowaistvest = vvalue;
            pvalue = value.split(";")[3];
            napetowaistpant = pvalue;
            var nape_to_waist_shirt = parseFloat(svalue);
            $("#nape_shirt_allow").html((nape_to_waist_shirt / 2.54).toFixed(1) + " Inch");
            //$("#final_shirt_nape").html((parseFloat(sizeData.nape_to_waist) + parseFloat(nape_to_waist_shirt / 2.54)).toFixed(1) + " Inch");
            //for front shoulder jacket
            var nape_to_waist_jacket = (parseFloat(jvalue));
            $("#nape_jacket_allow").html((nape_to_waist_jacket / 2.54).toFixed(1) + " Inch");
            //$("#final_jacket_nape").html((parseFloat(sizeData.nape_to_waist) + parseFloat(nape_to_waist_jacket / 2.54)).toFixed(1) + " Inch");
            // for nape_to_waist vest
            var nape_to_waist_vest = parseFloat(vvalue);
            $("#nape_vest_allow").html((nape_to_waist_vest / 2.54).toFixed(1) + " Inch");
            //$("#final_vest_nape").html((parseFloat(sizeData.nape_to_waist) + parseFloat(nape_to_waist_vest / 2.54)).toFixed(1) + " Inch");
            //for nape_to_waist pant
            var nape_to_waist_pant = (parseFloat(pvalue));
            $("#nape_pant_allow").html((nape_to_waist_pant / 2.54).toFixed(1) + " Inch");
            //$("#final_pant_nape").html((parseFloat(sizeData.nape_to_waist) + parseFloat(nape_to_waist_pant / 2.54)).toFixed(1) + " Inch");

            ////////for front_waist shirt
            answer1 = sizeData.shirt_que_1;
            answer2 = sizeData.shirt_que_2;
            answer3 = sizeData.shirt_que_3;
            answer4 = sizeData.shirt_que_4;
            answer5 = sizeData.shirt_que_5;
            answer6 = sizeData.shirt_que_6;
            janswer1 = sizeData.suit_que_1;
            janswer2 = sizeData.suit_que_2;
            vanswer1 = sizeData.vest_que_1;
            panswer1 = sizeData.pant_que_1;
            if (chest-stomach < 7) {
                value = getResponse2("front_waist_length", "ques1:" + answer1 + "@ques2:" + answer2 + "@ques3:" + answer3 + "@ques4:" + answer4 + "@ques5:" + answer5 + "@ques6:" + answer6 + "@jques1lessthan7:" + janswer1 + "@jques2lessthan7:" + janswer2 + "@vques1:" + vanswer1 + "@pques1:" + panswer1);
            } else {
                value = getResponse2("front_waist_length", "ques1:" + answer1 + "@ques2:" + answer2 + "@ques3:" + answer3 + "@ques4:" + answer4 + "@ques5:" + answer5 + "@ques6:" + answer6 + "@jques1:" + janswer1 + "@jques2:" + janswer2 + "@vques1:" + vanswer1 + "@pques1:" + panswer1);
            }
            svalue = value.split(";")[0];
            frontwaistlengthshirt = svalue;
            jvalue = value.split(";")[1];
            frontwaistlengthjacket = jvalue;
            vvalue = value.split(";")[2];
            frontwaistlengthvest = vvalue;
            pvalue = value.split(";")[3];
            frontwaistlengthpant = pvalue;
            //for front shoulder shirt
            var front_waist_shirt = parseFloat(svalue);
            $("#front_waist_shirt_allow").html((front_waist_shirt / 2.54).toFixed(1) + " Inch");
            //$("#final_shirt_front_waist").html((parseFloat(sizeData.front_waist_length) + parseFloat(front_waist_shirt / 2.54)).toFixed(1) + " Inch");
            //for front shoulder jacket
            var front_waist_jacket = (parseFloat(jvalue));
            $("#front_waist_jacket_allow").html((front_waist_jacket / 2.54).toFixed(1) + " Inch");
            //$("#final_jacket_front_waist").html((parseFloat(sizeData.front_waist_length) + parseFloat(front_waist_jacket / 2.54)).toFixed(1) + " Inch");
            // for front shoulder vest
            var front_waist_vest = parseFloat(vvalue);
            $("#front_waist_vest_allow").html((front_waist_vest / 2.54).toFixed(1) + " Inch");
            //$("#final_vest_front_waist").html((parseFloat(sizeData.front_waist_length) + parseFloat(front_waist_vest / 2.54)).toFixed(1) + " Inch");
            // for front shoulder pant
            var front_waist_pant = (parseFloat(jvalue));
            $("#front_waist_pant_allow").html((front_waist_pant / 2.54).toFixed(1) + " Inch");
            //$("#final_pant_front_waist").html((parseFloat(sizeData.front_waist_length) + parseFloat(front_waist_pant / 2.54)).toFixed(1) + " Inch");

            //for wrist shirt
            answer1 = sizeData.shirt_que_1;
            answer2 = sizeData.shirt_que_2;
            answer3 = sizeData.shirt_que_3;
            answer4 = sizeData.shirt_que_4;
            answer5 = sizeData.shirt_que_5;
            answer6 = sizeData.shirt_que_6;
            janswer1 = sizeData.suit_que_1;
            janswer2 = sizeData.suit_que_2;
            vanswer1 = sizeData.vest_que_1;
            panswer1 = sizeData.pant_que_1;
            if (chest-stomach < 7) {
                value = getResponse2("wrist", "ques1:" + answer1 + "@ques2:" + answer2 + "@ques3:" + answer3 + "@ques4:" + answer4 + "@ques5:" + answer5 + "@ques6:" + answer6 + "@jques1lessthan7:" + janswer1 + "@jques2lessthan7:" + janswer2 + "@vques1:" + vanswer1 + "@pques1:" + panswer1);
            } else {
                value = getResponse2("wrist", "ques1:" + answer1 + "@ques2:" + answer2 + "@ques3:" + answer3 + "@ques4:" + answer4 + "@ques5:" + answer5 + "@ques6:" + answer6 + "@jques1:" + janswer1 + "@jques2:" + janswer2 + "@vques1:" + vanswer1 + "@pques1:" + panswer1);
            }
            svalue = value.split(";")[0];
            wristshirt = svalue;
            jvalue = value.split(";")[1];
            wristjacket = jvalue;
            vvalue = value.split(";")[2];
            wristvest = vvalue;
            pvalue = value.split(";")[3];
            wristpant = pvalue;
            var wrist_shirt = parseFloat(svalue);
            $("#wrist_shirt_allow").html((wrist_shirt / 2.54).toFixed(1) + " Inch");
            //$("#final_shirt_wrist").html((parseFloat(sizeData.wrist) + parseFloat(wrist_shirt / 2.54)).toFixed(1) + " Inch");
            //for front shoulder jacket
            var wrist_jacket = (parseFloat(jvalue));
            $("#wrist_jacket_allow").html((wrist_jacket / 2.54).toFixed(1) + " Inch");
            //$("#final_jacket_wrist").html((parseFloat(sizeData.wrist) + parseFloat(wrist_jacket / 2.54)).toFixed(1) + " Inch");
            // for wrist vest
            var wrist_vest = parseFloat(vvalue);
            $("#wrist_vest_allow").html((wrist_vest / 2.54).toFixed(1) + " Inch");
            //$("#final_vest_wrist").html((parseFloat(sizeData.wrist) + parseFloat(wrist_vest / 2.54)).toFixed(1) + " Inch");
            //for wrist pant
            var wrist_pant = (parseFloat(pvalue));
            $("#wrist_pant_allow").html((wrist_pant / 2.54).toFixed(1) + " Inch");
            //$("#final_pant_wrist").html((parseFloat(sizeData.wrist) + parseFloat(wrist_pant / 2.54)).toFixed(1) + " Inch");

            ///////for waist shirt
            answer1 = sizeData.shirt_que_1;
            answer2 = sizeData.shirt_que_2;
            answer3 = sizeData.shirt_que_3;
            answer4 = sizeData.shirt_que_4;
            answer5 = sizeData.shirt_que_5;
            answer6 = sizeData.shirt_que_6;
            janswer1 = sizeData.suit_que_1;
            janswer2 = sizeData.suit_que_2;
            vanswer1 = sizeData.vest_que_1;
            panswer1 = sizeData.pant_que_1;
            if (chest-stomach < 7) {
                value = getResponse2("waist", "ques1:" + answer1 + "@ques2:" + answer2 + "@ques3:" + answer3 + "@ques4:" + answer4 + "@ques5:" + answer5 + "@ques6:" + answer6 + "@jques1lessthan7:" + janswer1 + "@jques2lessthan7:" + janswer2 + "@vques1:" + vanswer1 + "@pques1:" + panswer1);
            } else {
                value = getResponse2("waist", "ques1:" + answer1 + "@ques2:" + answer2 + "@ques3:" + answer3 + "@ques4:" + answer4 + "@ques5:" + answer5 + "@ques6:" + answer6 + "@jques1:" + janswer1 + "@jques2:" + janswer2 + "@vques1:" + vanswer1 + "@pques1:" + panswer1);
            }
            svalue = value.split(";")[0];
            waistshirt = svalue;
            jvalue = value.split(";")[1];
            waistjacket = jvalue;
            vvalue = value.split(";")[2];
            waistvest = vvalue;
            pvalue = value.split(";")[3];
            waistpant = pvalue;
            var waist_shirt = parseFloat(svalue);
            $("#waist_shirt_allow").html((waist_shirt / 2.54).toFixed(1) + " Inch");
            //$("#final_shirt_waist").html((parseFloat(sizeData.waist) + parseFloat(waist_shirt / 2.54)).toFixed(1) + " Inch");
            //for front shoulder jacket
            var waist_jacket = (parseFloat(jvalue));
            $("#waist_jacket_allow").html((waist_jacket / 2.54).toFixed(1) + " Inch");
            //$("#final_jacket_waist").html((parseFloat(sizeData.waist) + parseFloat(waist_jacket / 2.54)).toFixed(1) + " Inch");
            // for waist vest
            var waist_vest = parseFloat(vvalue);
            $("#waist_vest_allow").html((waist_vest / 2.54).toFixed(1) + " Inch");
            //$("#final_vest_waist").html((parseFloat(sizeData.waist) + parseFloat(waist_vest / 2.54)).toFixed(1) + " Inch");
            //for front shoulder jacket
            var waist_pant = (parseFloat(pvalue));
            $("#waist_pant_allow").html((waist_pant / 2.54).toFixed(1) + " Inch");
            //$("#final_pant_waist").html((parseFloat(sizeData.waist) + parseFloat(waist_pant / 2.54)).toFixed(1) + " Inch");

            //for calf shirt
            answer1 = sizeData.shirt_que_1;
            answer2 = sizeData.shirt_que_2;
            answer3 = sizeData.shirt_que_3;
            answer4 = sizeData.shirt_que_4;
            answer5 = sizeData.shirt_que_5;
            answer6 = sizeData.shirt_que_6;
            janswer1 = sizeData.suit_que_1;
            janswer2 = sizeData.suit_que_2;
            vanswer1 = sizeData.vest_que_1;
            panswer1 = sizeData.pant_que_1;
            if (chest-stomach < 7) {
                value = getResponse2("calf", "ques1:" + answer1 + "@ques2:" + answer2 + "@ques3:" + answer3 + "@ques4:" + answer4 + "@ques5:" + answer5 + "@ques6:" + answer6 + "@jques1lessthan7:" + janswer1 + "@jques2lessthan7:" + janswer2 + "@vques1:" + vanswer1 + "@pques1:" + panswer1);
            } else {
                value = getResponse2("calf", "ques1:" + answer1 + "@ques2:" + answer2 + "@ques3:" + answer3 + "@ques4:" + answer4 + "@ques5:" + answer5 + "@ques6:" + answer6 + "@jques1:" + janswer1 + "@jques2:" + janswer2 + "@vques1:" + vanswer1 + "@pques1:" + panswer1);
            }
            svalue = value.split(";")[0];
            calfshirt = svalue;
            jvalue = value.split(";")[1];
            calfjacket = jvalue;
            vvalue = value.split(";")[2];
            calfvest = vvalue;
            pvalue = value.split(";")[3];
            calfpant = pvalue;
            var calf_shirt = parseFloat(svalue);
            $("#calf_shirt_allow").html((calf_shirt / 2.54).toFixed(1) + " Inch");
            //$("#final_shirt_calf").html((parseFloat(sizeData.calf) + parseFloat(calf_shirt / 2.54)).toFixed(1) + " Inch");
            //for front shoulder jacket
            var calf_jacket = (parseFloat(jvalue));
            $("#calf_jacket_allow").html((calf_jacket / 2.54).toFixed(1) + " Inch");
            //$("#final_jacket_calf").html((parseFloat(sizeData.calf) + parseFloat(calf_jacket / 2.54)).toFixed(1) + " Inch");
            // for calf vest
            var calf_vest = parseFloat(vvalue);
            $("#calf_vest_allow").html((calf_vest / 2.54).toFixed(1) + " Inch");
            //$("#final_vest_calf").html((parseFloat(sizeData.calf) + parseFloat(calf_vest / 2.54)).toFixed(1) + " Inch");
            //for calf pant
            var calf_pant = (parseFloat(pvalue));
            $("#calf_pant_allow").html((calf_pant / 2.54).toFixed(1) + " Inch");
            //$("#final_pant_calf").html((parseFloat(sizeData.calf) + parseFloat(calf_pant / 2.54)).toFixed(1) + " Inch");

            ////////for b_waist_shirt
            answer1 = sizeData.shirt_que_1;
            answer2 = sizeData.shirt_que_2;
            answer3 = sizeData.shirt_que_3;
            answer4 = sizeData.shirt_que_4;
            answer5 = sizeData.shirt_que_5;
            answer6 = sizeData.shirt_que_6;
            janswer1 = sizeData.suit_que_1;
            janswer2 = sizeData.suit_que_2;
            vanswer1 = sizeData.vest_que_1;
            panswer1 = sizeData.pant_que_1;
            if (chest-stomach < 7) {
                value = getResponse2("back_waist_height", "ques1:" + answer1 + "@ques2:" + answer2 + "@ques3:" + answer3 + "@ques4:" + answer4 + "@ques5:" + answer5 + "@ques6:" + answer6 + "@jques1lessthan7:" + janswer1 + "@jques2lessthan7:" + janswer2 + "@vques1:" + vanswer1 + "@pques1:" + panswer1);
            } else {
                value = getResponse2("back_waist_height", "ques1:" + answer1 + "@ques2:" + answer2 + "@ques3:" + answer3 + "@ques4:" + answer4 + "@ques5:" + answer5 + "@ques6:" + answer6 + "@jques1:" + janswer1 + "@jques2:" + janswer2 + "@vques1:" + vanswer1 + "@pques1:" + panswer1);
            }
            svalue = value.split(";")[0];
            backwaistheightshirt = svalue;
            jvalue = value.split(";")[1];
            backwaistheightjacket = jvalue;
            vvalue = value.split(";")[2];
            backwaistheightvest = vvalue;
            pvalue = value.split(";")[3];
            backwaistheightpant = pvalue;
            //for b_waist shirt
            var b_waist_shirt = parseFloat(svalue);
            $("#b_waist_shirt_allow").html((b_waist_shirt / 2.54).toFixed(1) + " Inch");
            //$("#final_shirt_b_waist").html((parseFloat(sizeData.back_waist_height) + parseFloat(b_waist_shirt / 2.54)).toFixed(1) + " Inch");
            //for b_waist jacket
            var b_waist_jacket = (parseFloat(jvalue));
            $("#b_waist_jacket_allow").html((b_waist_jacket / 2.54).toFixed(1) + " Inch");
            //$("#final_jacket_b_waist").html((parseFloat(sizeData.back_waist_height) + parseFloat(b_waist_jacket / 2.54)).toFixed(1) + " Inch");
            //for b_waist vest
            var b_waist_vest = parseFloat(vvalue);
            $("#b_waist_vest_allow").html((b_waist_vest / 2.54).toFixed(1) + " Inch");
            //$("#final_vest_b_waist").html((parseFloat(sizeData.back_waist_height) + parseFloat(b_waist_vest / 2.54)).toFixed(1) + " Inch");
            //for b_waist_pant
            var b_waist_pant = (parseFloat(pvalue));
            $("#b_waist_pant_allow").html((b_waist_pant / 2.54).toFixed(1) + " Inch");
            //$("#final_pant_b_waist").html((parseFloat(sizeData.back_waist_height) + parseFloat(b_waist_pant / 2.54)).toFixed(1) + " Inch");

            //for f_waist_shirt
            answer1 = sizeData.shirt_que_1;
            answer2 = sizeData.shirt_que_2;
            answer3 = sizeData.shirt_que_3;
            answer4 = sizeData.shirt_que_4;
            answer5 = sizeData.shirt_que_5;
            answer6 = sizeData.shirt_que_6;
            janswer1 = sizeData.suit_que_1;
            janswer2 = sizeData.suit_que_2;
            vanswer1 = sizeData.vest_que_1;
            panswer1 = sizeData.pant_que_1;
            if (chest-stomach < 7) {
                value = getResponse2("front_waist_height", "ques1:" + answer1 + "@ques2:" + answer2 + "@ques3:" + answer3 + "@ques4:" + answer4 + "@ques5:" + answer5 + "@ques6:" + answer6 + "@jques1lessthan7:" + janswer1 + "@jques2lessthan7:" + janswer2 + "@vques1:" + vanswer1 + "@pques1:" + panswer1);
            } else {
                value = getResponse2("front_waist_height", "ques1:" + answer1 + "@ques2:" + answer2 + "@ques3:" + answer3 + "@ques4:" + answer4 + "@ques5:" + answer5 + "@ques6:" + answer6 + "@jques1:" + janswer1 + "@jques2:" + janswer2 + "@vques1:" + vanswer1 + "@pques1:" + panswer1);
            }
            svalue = value.split(";")[0];
            frontwaistheightshirt = svalue;
            jvalue = value.split(";")[1];
            frontwaistheightjacket = jvalue;
            vvalue = value.split(";")[2];
            frontwaistheightvest = vvalue;
            pvalue = value.split(";")[3];
            frontwaistheightpant = pvalue;
            //for f_waist shirt
            var f_waist_shirt = parseFloat(svalue);
            $("#f_waist_shirt_allow").html((f_waist_shirt / 2.54).toFixed(1) + " Inch");
            //$("#final_shirt_f_waist").html((parseFloat(sizeData.front_waist_height) + parseFloat(f_waist_shirt / 2.54)).toFixed(1) + " Inch");
            //for f_waist jacket
            var f_waist_jacket = (parseFloat(jvalue));
            $("#f_waist_jacket_allow").html((f_waist_jacket / 2.54).toFixed(1) + " Inch");
            //$("#final_jacket_f_waist").html((parseFloat(sizeData.front_waist_height) + parseFloat(f_waist_jacket / 2.54)).toFixed(1) + " Inch");
            //for f_waist vest
            var f_waist_vest = parseFloat(vvalue);
            $("#f_waist_vest_allow").html((f_waist_vest / 2.54).toFixed(1) + " Inch");
            //$("#final_vest_f_waist").html((parseFloat(sizeData.front_waist_height) + parseFloat(f_waist_vest / 2.54)).toFixed(1) + " Inch");
            //for f_waist pant
            var f_waist_pant = (parseFloat(pvalue));
            $("#f_waist_pant_allow").html((f_waist_pant / 2.54).toFixed(1) + " Inch");
            //$("#final_pant_f_waist").html((parseFloat(sizeData.front_waist_height) + parseFloat(f_waist_pant / 2.54)).toFixed(1) + " Inch");

            /////for knee shirt
            answer1 = sizeData.shirt_que_1;
            answer2 = sizeData.shirt_que_2;
            answer3 = sizeData.shirt_que_3;
            answer4 = sizeData.shirt_que_4;
            answer5 = sizeData.shirt_que_5;
            answer6 = sizeData.shirt_que_6;
            janswer1 = sizeData.suit_que_1;
            janswer2 = sizeData.suit_que_2;
            vanswer1 = sizeData.vest_que_1;
            panswer1 = sizeData.pant_que_1;
            if (chest-stomach < 7) {
                value = getResponse2("knee", "ques1:" + answer1 + "@ques2:" + answer2 + "@ques3:" + answer3 + "@ques4:" + answer4 + "@ques5:" + answer5 + "@ques6:" + answer6 + "@jques1lessthan7:" + janswer1 + "@jques2lessthan7:" + janswer2 + "@vques1:" + vanswer1 + "@pques1:" + panswer1);
            } else {
                value = getResponse2("knee", "ques1:" + answer1 + "@ques2:" + answer2 + "@ques3:" + answer3 + "@ques4:" + answer4 + "@ques5:" + answer5 + "@ques6:" + answer6 + "@jques1:" + janswer1 + "@jques2:" + janswer2 + "@vques1:" + vanswer1 + "@pques1:" + panswer1);
            }
            svalue = value.split(";")[0];
            kneeshirt = svalue;
            jvalue = value.split(";")[1];
            kneejacket = jvalue;
            vvalue = value.split(";")[2];
            kneevest = vvalue;
            pvalue = value.split(";")[3];
            kneepant = pvalue;
            //for knee shirt
            var knee_shirt = parseFloat(svalue);
            $("#knee_shirt_allow").html((knee_shirt / 2.54).toFixed(1) + " Inch");
            //$("#final_shirt_knee").html((parseFloat(sizeData.knee) + parseFloat(knee_shirt / 2.54)).toFixed(1) + " Inch");
            //for knee jacket
            var knee_jacket = (parseFloat(jvalue));
            $("#knee_jacket_allow").html((knee_jacket / 2.54).toFixed(1) + " Inch");
            //$("#final_jacket_knee").html((parseFloat(sizeData.knee) + parseFloat(knee_jacket / 2.54)).toFixed(1) + " Inch");
            //for knee vest
            var knee_vest = parseFloat(vvalue);
            $("#knee_vest_allow").html((knee_vest / 2.54).toFixed(1) + " Inch");
            //$("#final_vest_knee").html((parseFloat(sizeData.knee) + parseFloat(knee_vest / 2.54)).toFixed(1) + " Inch");
            //for knee pant
            var knee_pant = (parseFloat(pvalue));
            $("#knee_pant_allow").html((knee_pant / 2.54).toFixed(1) + " Inch");
            //$("#final_pant_knee").html((parseFloat(sizeData.knee) + parseFloat(knee_pant / 2.54)).toFixed(1) + " Inch");

            //for back_jacket_length shirt
            answer1 = sizeData.shirt_que_1;
            answer2 = sizeData.shirt_que_2;
            answer3 = sizeData.shirt_que_3;
            answer4 = sizeData.shirt_que_4;
            answer5 = sizeData.shirt_que_5;
            answer6 = sizeData.shirt_que_6;
            janswer1 = sizeData.suit_que_1;
            janswer2 = sizeData.suit_que_2;
            vanswer1 = sizeData.vest_que_1;
            panswer1 = sizeData.pant_que_1;
            if (chest-stomach < 7) {
                value = getResponse2("back_jacket_length", "ques1:" + answer1 + "@ques2:" + answer2 + "@ques3:" + answer3 + "@ques4:" + answer4 + "@ques5:" + answer5 + "@ques6:" + answer6 + "@jques1lessthan7:" + janswer1 + "@jques2lessthan7:" + janswer2 + "@vques1:" + vanswer1 + "@pques1:" + panswer1);
            } else {
                value = getResponse2("back_jacket_length", "ques1:" + answer1 + "@ques2:" + answer2 + "@ques3:" + answer3 + "@ques4:" + answer4 + "@ques5:" + answer5 + "@ques6:" + answer6 + "@jques1:" + janswer1 + "@jques2:" + janswer2 + "@vques1:" + vanswer1 + "@pques1:" + panswer1);
            }
            svalue = value.split(";")[0];
            backjacketlengthshirt = svalue;
            jvalue = value.split(";")[1];
            backjacketlengthjacket = jvalue;
            vvalue = value.split(";")[2];
            backjacketlengthvest = vvalue;
            pvalue = value.split(";")[3];
            backjacketlengthpant = pvalue;
            //for back_jacket_length shirt
            var jacket_shirt = parseFloat(svalue);
            $("#jacket_shirt_allow").html((jacket_shirt / 2.54).toFixed(1) + " Inch");
            //$("#final_shirt_jacket").html((parseFloat(sizeData.back_jacket_length) + parseFloat(jacket_shirt / 2.54)).toFixed(1) + " Inch");
            //for back_jacket_length jacket
            var jacket_jacket = (parseFloat(jvalue));
            $("#jacket_jacket_allow").html((jacket_jacket / 2.54).toFixed(1) + " Inch");
            //$("#final_jacket_jacket").html((parseFloat(sizeData.back_jacket_length) + parseFloat(jacket_jacket / 2.54)).toFixed(1) + " Inch");
            //for back_jacket_length vest
            var jacket_vest = parseFloat(vvalue);
            $("#jacket_vest_allow").html((jacket_vest / 2.54).toFixed(1) + " Inch");
            //$("#final_vest_jacket").html((parseFloat(sizeData.back_jacket_length) + parseFloat(jacket_vest / 2.54)).toFixed(1) + " Inch");
            //for back_jacket_length pant
            var jacket_pant = (parseFloat(pvalue));
            $("#jacket_pant_allow").html((jacket_pant / 2.54).toFixed(1) + " Inch");
            //$("#final_pant_jacket").html((parseFloat(sizeData.back_jacket_length) + parseFloat(jacket_pant / 2.54)).toFixed(1) + " Inch");

            //for u_rise shirt
            answer1 = sizeData.shirt_que_1;
            answer2 = sizeData.shirt_que_2;
            answer3 = sizeData.shirt_que_3;
            answer4 = sizeData.shirt_que_4;
            answer5 = sizeData.shirt_que_5;
            answer6 = sizeData.shirt_que_6;
            janswer1 = sizeData.suit_que_1;
            janswer2 = sizeData.suit_que_2;
            vanswer1 = sizeData.vest_que_1;
            panswer1 = sizeData.pant_que_1;
            if (chest-stomach < 7) {
                value = getResponse2("u_rise", "ques1:" + answer1 + "@ques2:" + answer2 + "@ques3:" + answer3 + "@ques4:" + answer4 + "@ques5:" + answer5 + "@ques6:" + answer6 + "@jques1lessthan7:" + janswer1 + "@jques2lessthan7:" + janswer2 + "@vques1:" + vanswer1 + "@pques1:" + panswer1);
            } else {
                value = getResponse2("u_rise", "ques1:" + answer1 + "@ques2:" + answer2 + "@ques3:" + answer3 + "@ques4:" + answer4 + "@ques5:" + answer5 + "@ques6:" + answer6 + "@jques1:" + janswer1 + "@jques2:" + janswer2 + "@vques1:" + vanswer1 + "@pques1:" + panswer1);
            }
            svalue = value.split(";")[0];
            riseshirt = svalue;
            jvalue = value.split(";")[1];
            risejacket = jvalue;
            vvalue = value.split(";")[2];
            risevest = vvalue;
            pvalue = value.split(";")[3];
            risepant = pvalue;
            //for rise shirt
            var rise_shirt = parseFloat(svalue);
            $("#rise_shirt_allow").html((rise_shirt / 2.54).toFixed(1) + " Inch");
            //$("#final_shirt_rise").html((parseFloat(sizeData.u_rise) + parseFloat(rise_shirt / 2.54)).toFixed(1) + " Inch");
            //for rise jacket
            var rise_jacket = (parseFloat(jvalue));
            $("#rise_jacket_allow").html((rise_jacket / 2.54).toFixed(1) + " Inch");
            //$("#final_jacket_rise").html((parseFloat(sizeData.u_rise) + parseFloat(rise_jacket / 2.54)).toFixed(1) + " Inch");
            // for rise vest
            var rise_vest = parseFloat(vvalue);
            $("#rise_vest_allow").html((rise_vest / 2.54).toFixed(1) + " Inch");
            //$("#final_vest_rise").html((parseFloat(sizeData.u_rise) + parseFloat(rise_vest / 2.54)).toFixed(1) + " Inch");
            //for rise pant
            var rise_pant = (parseFloat(pvalue));
            $("#rise_pant_allow").html((rise_pant / 2.54).toFixed(1) + " Inch");
            //$("#final_pant_rise").html((parseFloat(sizeData.u_rise) + parseFloat(rise_pant / 2.54)).toFixed(1) + " Inch");

            ////////for pant_left_shirt
            answer1 = sizeData.shirt_que_1;
            answer2 = sizeData.shirt_que_2;
            answer3 = sizeData.shirt_que_3;
            answer4 = sizeData.shirt_que_4;
            answer5 = sizeData.shirt_que_5;
            answer6 = sizeData.shirt_que_6;
            janswer1 = sizeData.suit_que_1;
            janswer2 = sizeData.suit_que_2;
            vanswer1 = sizeData.vest_que_1;
            panswer1 = sizeData.pant_que_1;
            if (chest-stomach < 7) {
                value = getResponse2("pants_left_outseam", "ques1:" + answer1 + "@ques2:" + answer2 + "@ques3:" + answer3 + "@ques4:" + answer4 + "@ques5:" + answer5 + "@ques6:" + answer6 + "@jques1lessthan7:" + janswer1 + "@jques2lessthan7:" + janswer2 + "@vques1:" + vanswer1 + "@pques1:" + panswer1);
            } else {
                value = getResponse2("pants_left_outseam", "ques1:" + answer1 + "@ques2:" + answer2 + "@ques3:" + answer3 + "@ques4:" + answer4 + "@ques5:" + answer5 + "@ques6:" + answer6 + "@jques1:" + janswer1 + "@jques2:" + janswer2 + "@vques1:" + vanswer1 + "@pques1:" + panswer1);
            }
            svalue = value.split(";")[0];
            pantleftshirt = svalue;
            jvalue = value.split(";")[1];
            pantleftjacket = jvalue;
            vvalue = value.split(";")[2];
            pantleftvest = vvalue;
            pvalue = value.split(";")[3];
            pantleftpant = pvalue;
            var p_left_shirt = parseFloat(svalue);
            $("#p_left_shirt_allow").html((p_left_shirt / 2.54).toFixed(1) + " Inch");
            //$("#final_shirt_p_left").html((parseFloat(sizeData.pant_left_outseam) + parseFloat(p_left_shirt / 2.54)).toFixed(1) + " Inch");
            //for back_jacket_length jacket
            var p_left_jacket = (parseFloat(jvalue));
            $("#p_left_jacket_allow").html((p_left_jacket / 2.54).toFixed(1) + " Inch");
            //$("#final_jacket_p_left").html((parseFloat(sizeData.pant_left_outseam) + parseFloat(p_left_jacket / 2.54)).toFixed(1) + " Inch");
            //for back_jacket_length vest
            var p_left_vest = parseFloat(vvalue);
            $("#p_left_vest_allow").html((p_left_vest / 2.54).toFixed(1) + " Inch");
            //$("#final_vest_p_left").html((parseFloat(sizeData.pant_left_outseam) + parseFloat(p_left_vest / 2.54)).toFixed(1) + " Inch");
            //for back_jacket_length pant
            var p_left_pant = (parseFloat(pvalue));
            $("#p_left_pant_allow").html((p_left_pant / 2.54).toFixed(1) + " Inch");
            //$("#final_pant_p_left").html((parseFloat(sizeData.pant_left_outseam) + parseFloat(p_left_pant / 2.54)).toFixed(1) + " Inch");

            //for p_right shirt
            answer1 = sizeData.shirt_que_1;
            answer2 = sizeData.shirt_que_2;
            answer3 = sizeData.shirt_que_3;
            answer4 = sizeData.shirt_que_4;
            answer5 = sizeData.shirt_que_5;
            answer6 = sizeData.shirt_que_6;
            janswer1 = sizeData.suit_que_1;
            janswer2 = sizeData.suit_que_2;
            vanswer1 = sizeData.vest_que_1;
            panswer1 = sizeData.pant_que_1;
            if (chest-stomach < 7) {
                value = getResponse2("pants_right_outseam", "ques1:" + answer1 + "@ques2:" + answer2 + "@ques3:" + answer3 + "@ques4:" + answer4 + "@ques5:" + answer5 + "@ques6:" + answer6 + "@jques1lessthan7:" + janswer1 + "@jques2lessthan7:" + janswer2 + "@vques1:" + vanswer1 + "@pques1:" + panswer1);
            } else {
                value = getResponse2("pants_right_outseam", "ques1:" + answer1 + "@ques2:" + answer2 + "@ques3:" + answer3 + "@ques4:" + answer4 + "@ques5:" + answer5 + "@ques6:" + answer6 + "@jques1:" + janswer1 + "@jques2:" + janswer2 + "@vques1:" + vanswer1 + "@pques1:" + panswer1);
            }
            svalue = value.split(";")[0];
            pantrightshirt = svalue;
            jvalue = value.split(";")[1];
            pantrightjacket = jvalue;
            vvalue = value.split(";")[2];
            pantrightvest = vvalue;
            pvalue = value.split(";")[3];
            pantrightpant = pvalue;
            var p_right_shirt = parseFloat(svalue);
            $("#p_right_shirt_allow").html((p_right_shirt / 2.54).toFixed(1) + " Inch");
            //$("#final_shirt_p_right").html((parseFloat(sizeData.pant_right_outseam) + parseFloat(p_right_shirt / 2.54)).toFixed(1) + " Inch");
            //for back_jacket_length jacket
            var p_right_jacket = (parseFloat(jvalue));
            $("#p_right_jacket_allow").html((p_right_jacket / 2.54).toFixed(1) + " Inch");
            //$("#final_jacket_p_right").html((parseFloat(sizeData.pant_right_outseam) + parseFloat(p_right_jacket / 2.54)).toFixed(1) + " Inch");
            // for p_right vest
            var p_right_vest = parseFloat(vvalue);
            $("#p_right_vest_allow").html((p_right_vest / 2.54).toFixed(1) + " Inch");
            //$("#final_vest_p_right").html((parseFloat(sizeData.pant_right_outseam) + parseFloat(p_right_vest / 2.54)).toFixed(1) + " Inch");
            // for p_right pant
            var p_right_pant = (parseFloat(pvalue));
            $("#p_right_pant_allow").html((p_right_pant / 2.54).toFixed(1) + " Inch");
            //$("#final_pant_p_right").html((parseFloat(sizeData.pant_right_outseam) + parseFloat(p_right_pant / 2.54)).toFixed(1) + " Inch");

            /////for bottom shirt
            answer1 = sizeData.shirt_que_1;
            answer2 = sizeData.shirt_que_2;
            answer3 = sizeData.shirt_que_3;
            answer4 = sizeData.shirt_que_4;
            answer5 = sizeData.shirt_que_5;
            answer6 = sizeData.shirt_que_6;
            janswer1 = sizeData.suit_que_1;
            janswer2 = sizeData.suit_que_2;
            vanswer1 = sizeData.vest_que_1;
            panswer1 = sizeData.pant_que_1;
            if (chest-stomach < 7) {
                value = getResponse2("bottom", "ques1:" + answer1 + "@ques2:" + answer2 + "@ques3:" + answer3 + "@ques4:" + answer4 + "@ques5:" + answer5 + "@ques6:" + answer6 + "@jques1lessthan7:" + janswer1 + "@jques2lessthan7:" + janswer2 + "@vques1:" + vanswer1 + "@pques1:" + panswer1);
            } else {
                value = getResponse2("bottom", "ques1:" + answer1 + "@ques2:" + answer2 + "@ques3:" + answer3 + "@ques4:" + answer4 + "@ques5:" + answer5 + "@ques6:" + answer6 + "@jques1:" + janswer1 + "@jques2:" + janswer2 + "@vques1:" + vanswer1 + "@pques1:" + panswer1);
            }
            svalue = value.split(";")[0];
            bottomshirt = svalue;
            jvalue = value.split(";")[1];
            bottomjacket = jvalue;
            vvalue = value.split(";")[2];
            bottomvest = vvalue;
            pvalue = value.split(";")[3];
            bottompant = pvalue;
            var bottom_shirt = parseFloat(svalue);
            $("#bottom_shirt_allow").html((bottom_shirt / 2.54).toFixed(1) + " Inch");
            //$("#final_shirt_bottom").html((parseFloat(sizeData.bottom) + parseFloat(bottom_shirt / 2.54)).toFixed(1) + " Inch");
            //for bottom jacket
            var bottom_jacket = (parseFloat(jvalue));
            $("#bottom_jacket_allow").html((bottom_jacket / 2.54).toFixed(1) + " Inch");
            //$("#final_jacket_bottom").html((parseFloat(sizeData.bottom) + parseFloat(bottom_jacket / 2.54)).toFixed(1) + " Inch");
            // for bottom vest
            var bottom_vest = parseFloat(vvalue);
            $("#bottom_vest_allow").html((bottom_vest / 2.54).toFixed(1) + " Inch");
            //$("#final_vest_bottom").html((parseFloat(sizeData.bottom) + parseFloat(bottom_vest / 2.54)).toFixed(1) + " Inch");
            // for bottom pant
            var bottom_pant = (parseFloat(pvalue));
            $("#bottom_pant_allow").html((bottom_pant / 2.54).toFixed(1) + " Inch");
            //$("#final_pant_bottom").html((parseFloat(sizeData.bottom) + parseFloat(bottom_pant / 2.54)).toFixed(1) + " Inch");
            $(".loaderdiv").removeClass("block");
            $(".loaderdiv").addClass("loaderhidden");
        } else {
            ///cm from backend need to conversion into inches
            var chest = parseFloat(sizeData.chest);///cm conversion
            var stomach = parseFloat(sizeData.stomach);///cm conversion
            ////collar shirt
            answer1 = sizeData.shirt_que_1;
            answer2 = sizeData.shirt_que_2;
            answer3 = sizeData.shirt_que_3;
            answer4 = sizeData.shirt_que_4;
            answer5 = sizeData.shirt_que_5;
            answer6 = sizeData.shirt_que_6;
            janswer1 = sizeData.suit_que_1;
            janswer2 = sizeData.suit_que_2;
            vanswer1 = sizeData.vest_que_1;
            panswer1 = sizeData.pant_que_1;
            if (chest-stomach < 7) {
                value = getResponse2("collar", "ques1:" + answer1 + "@ques2:" + answer2 + "@ques3:" + answer3 + "@ques4:" + answer4 + "@ques5:" + answer5 + "@ques6:" + answer6 + "@jques1lessthan7:" + janswer1 + "@jques2lessthan7:" + janswer2 + "@vques1:" + vanswer1 + "@pques1:" + panswer1);
            } else {
                value = getResponse2("collar", "ques1:" + answer1 + "@ques2:" + answer2 + "@ques3:" + answer3 + "@ques4:" + answer4 + "@ques5:" + answer5 + "@ques6:" + answer6 + "@jques1:" + janswer1 + "@jques2:" + janswer2 + "@vques1:" + vanswer1 + "@pques1:" + panswer1);
            }
            svalue = value.split(";")[0];
            collarshirt = svalue;
            jvalue = value.split(";")[1];
            collarjacket = jvalue;
            vvalue = value.split(";")[2];
            collarvest = vvalue;
            pvalue = value.split(";")[3];
            collarpant = pvalue;
            //for collar shirt
            var collar_shirt = parseFloat(svalue);
            $("#collar_shirt_allow").html(collar_shirt+ " Cm");
            //$("#final_shirt_collar").html((parseFloat(sizeData.collar) + parseFloat(collar_shirt)).toFixed(1) + " Cm");
            // for collar jacket
            var collar_jacket = (parseFloat(jvalue));
            $("#collar_jacket_allow").html(collar_jacket+ " Cm");
            //$("#final_jacket_collar").html((parseFloat(sizeData.collar) + parseFloat(collar_jacket)).toFixed(1) + " Cm");
            // for collar vest
            var collar_vest = (parseFloat(vvalue));
            $("#collar_vest_allow").html(collar_vest+ " Cm");
            //$("#final_vest_collar").html((parseFloat(sizeData.collar) + parseFloat(collar_vest)).toFixed(1) + " Cm");
            // for collar pant
            var collar_pant = (parseFloat(pvalue));
            $("#collar_pant_allow").html(collar_pant+ " Cm");
            //$("#final_pant_collar").html((parseFloat(sizeData.collar) + parseFloat(collar_pant)).toFixed(1) + " Cm");

            //for chest shirt
            answer1 = sizeData.shirt_que_1;
            answer2 = sizeData.shirt_que_2;
            answer3 = sizeData.shirt_que_3;
            answer4 = sizeData.shirt_que_4;
            answer5 = sizeData.shirt_que_5;
            answer6 = sizeData.shirt_que_6;
            janswer1 = sizeData.suit_que_1;
            janswer2 = sizeData.suit_que_2;
            vanswer1 = sizeData.vest_que_1;
            panswer1 = sizeData.pant_que_1;
            if (chest-stomach < 7) {
                value = getResponse2("chest", "ques1:" + answer1 + "@ques2:" + answer2 + "@ques3:" + answer3 + "@ques4:" + answer4 + "@ques5:" + answer5 + "@ques6:" + answer6 + "@jques1lessthan7:" + janswer1 + "@jques2lessthan7:" + janswer2 + "@vques1:" + vanswer1 + "@pques1:" + panswer1);
            } else {
                value = getResponse2("chest", "ques1:" + answer1 + "@ques2:" + answer2 + "@ques3:" + answer3 + "@ques4:" + answer4 + "@ques5:" + answer5 + "@ques6:" + answer6 + "@jques1:" + janswer1 + "@jques2:" + janswer2 + "@vques1:" + vanswer1 + "@pques1:" + panswer1);
            }
            svalue = value.split(";")[0];
            chestshirt = svalue;
            jvalue = value.split(";")[1];
            chestjacket = jvalue;
            vvalue = value.split(";")[2];
            chestvest = vvalue;
            pvalue = value.split(";")[3];
            chestpant = pvalue;
            //for chest shirt
            var chest_shirt = parseFloat(svalue);
            $("#chest_shirt_allow").html(chest_shirt+ " Cm");
            //$("#final_shirt_chest").html((parseFloat(sizeData.chest) + parseFloat(chest_shirt)).toFixed(1) + " Cm");
            // for chest jacket
            var chest_jacket = (parseFloat(jvalue));
            $("#chest_jacket_allow").html(chest_jacket+ " Cm");
            //$("#final_jacket_chest").html((parseFloat(sizeData.chest) + parseFloat(chest_jacket)).toFixed(1) + " Cm");
            //for chest vest
            var chest_vest = parseFloat(vvalue);
            $("#chest_vest_allow").html(chest_vest+ " Cm");
            //$("#final_vest_chest").html((parseFloat(sizeData.chest) + parseFloat(chest_vest)).toFixed(1) + " Cm");
            // for chest pant
            var chest_pant = (parseFloat(pvalue));
            $("#chest_pant_allow").html(chest_pant+ " Cm");
            //$("#final_pant_chest").html((parseFloat(sizeData.chest) + parseFloat(chest_pant)).toFixed(1) + " Cm");

            ////////for Stomach shirt
            answer1 = sizeData.shirt_que_1;
            answer2 = sizeData.shirt_que_2;
            answer3 = sizeData.shirt_que_3;
            answer4 = sizeData.shirt_que_4;
            answer5 = sizeData.shirt_que_5;
            answer6 = sizeData.shirt_que_6;
            janswer1 = sizeData.suit_que_1;
            janswer2 = sizeData.suit_que_2;
            vanswer1 = sizeData.vest_que_1;
            panswer1 = sizeData.pant_que_1;
            if (chest-stomach < 7) {
                value = getResponse2("stomach", "ques1:" + answer1 + "@ques2:" + answer2 + "@ques3:" + answer3 + "@ques4:" + answer4 + "@ques5:" + answer5 + "@ques6:" + answer6 + "@jques1lessthan7:" + janswer1 + "@jques2lessthan7:" + janswer2 + "@vques1:" + vanswer1 + "@pques1:" + panswer1);
            } else {
                value = getResponse2("stomach", "ques1:" + answer1 + "@ques2:" + answer2 + "@ques3:" + answer3 + "@ques4:" + answer4 + "@ques5:" + answer5 + "@ques6:" + answer6 + "@jques1:" + janswer1 + "@jques2:" + janswer2 + "@vques1:" + vanswer1 + "@pques1:" + panswer1);
            }
            svalue = value.split(";")[0];
            stomachshirt = svalue;
            jvalue = value.split(";")[1];
            stomachjacket = jvalue;
            vvalue = value.split(";")[2];
            stomachvest = vvalue;
            pvalue = value.split(";")[3];
            stomachpant = pvalue;
            // for stomach shirt
            var stomach_shirt = parseFloat(svalue);
            $("#stomach_shirt_allow").html(stomach_shirt+ " Cm");
            //$("#final_shirt_stomach").html((parseFloat(sizeData.stomach) + parseFloat(stomach_shirt)).toFixed(1) + " Cm");
            // for stomach jacket
            var stomach_jacket = (parseFloat(jvalue));
            $("#stomach_jacket_allow").html(stomach_jacket+ " Cm");
            //$("#final_jacket_stomach").html((parseFloat(sizeData.stomach) + parseFloat(stomach_jacket)).toFixed(1) + " Cm");
            // for stomach vest
            var stomach_vest = parseFloat(vvalue);
            $("#stomach_vest_allow").html(stomach_vest+ " Cm");
            //$("#final_vest_stomach").html((parseFloat(sizeData.stomach) + parseFloat(stomach_vest)).toFixed(1) + " Cm");
            // for stomach pant
            var stomach_pant = (parseFloat(pvalue));
            $("#stomach_pant_allow").html(stomach_pant+ " Cm");
            //$("#final_pant_stomach").html((parseFloat(sizeData.stomach) + parseFloat(stomach_pant)).toFixed(1) + " Cm");

            //for seat shirt
            answer1 = sizeData.shirt_que_1;
            answer2 = sizeData.shirt_que_2;
            answer3 = sizeData.shirt_que_3;
            answer4 = sizeData.shirt_que_4;
            answer5 = sizeData.shirt_que_5;
            answer6 = sizeData.shirt_que_6;
            janswer1 = sizeData.suit_que_1;
            janswer2 = sizeData.suit_que_2;
            vanswer1 = sizeData.vest_que_1;
            panswer1 = sizeData.pant_que_1;
            if (chest-stomach < 7) {
                value = getResponse2("seat", "ques1:" + answer1 + "@ques2:" + answer2 + "@ques3:" + answer3 + "@ques4:" + answer4 + "@ques5:" + answer5 + "@ques6:" + answer6 + "@jques1lessthan7:" + janswer1 + "@jques2lessthan7:" + janswer2 + "@vques1:" + vanswer1 + "@pques1:" + panswer1);
            } else {
                value = getResponse2("seat", "ques1:" + answer1 + "@ques2:" + answer2 + "@ques3:" + answer3 + "@ques4:" + answer4 + "@ques5:" + answer5 + "@ques6:" + answer6 + "@jques1:" + janswer1 + "@jques2:" + janswer2 + "@vques1:" + vanswer1 + "@pques1:" + panswer1);
            }
            svalue = value.split(";")[0];
            seatshirt = svalue;
            jvalue = value.split(";")[1];
            seatjacket = jvalue;
            vvalue = value.split(";")[2];
            seatvest = vvalue;
            pvalue = value.split(";")[3];
            seatpant = pvalue;
            var seat_shirt = parseFloat(svalue);
            $("#seat_shirt_allow").html(seat_shirt+ " Cm");
            //$("#final_shirt_seat").html((parseFloat(sizeData.seat) + parseFloat(seat_shirt)).toFixed(1) + " Cm");
            // for stomach jacket
            var seat_jacket = (parseFloat(jvalue));
            $("#seat_jacket_allow").html(seat_jacket+ " Cm");
            //$("#final_jacket_seat").html((parseFloat(sizeData.seat) + parseFloat(seat_jacket)).toFixed(1) + " Cm");
            var seat_vest = parseFloat(vvalue);
            $("#seat_vest_allow").html(seat_vest+ " Cm");
            //$("#final_vest_seat").html((parseFloat(sizeData.seat) + parseFloat(seat_vest)).toFixed(1) + " Cm");
            // for stomach pant
            var seat_pant = (parseFloat(pvalue));
            $("#seat_pant_allow").html(seat_pant+ " Cm");
            //$("#final_pant_seat").html((parseFloat(sizeData.seat) + parseFloat(seat_pant)).toFixed(1) + " Cm");

            ///////for bicep shirt
            answer1 = sizeData.shirt_que_1;
            answer2 = sizeData.shirt_que_2;
            answer3 = sizeData.shirt_que_3;
            answer4 = sizeData.shirt_que_4;
            answer5 = sizeData.shirt_que_5;
            answer6 = sizeData.shirt_que_6;
            janswer1 = sizeData.suit_que_1;
            janswer2 = sizeData.suit_que_2;
            vanswer1 = sizeData.vest_que_1;
            panswer1 = sizeData.pant_que_1;
            if (chest-stomach < 7) {
                value = getResponse2("bicep", "ques1:" + answer1 + "@ques2:" + answer2 + "@ques3:" + answer3 + "@ques4:" + answer4 + "@ques5:" + answer5 + "@ques6:" + answer6 + "@jques1lessthan7:" + janswer1 + "@jques2lessthan7:" + janswer2 + "@vques1:" + vanswer1 + "@pques1:" + panswer1);
            } else {
                value = getResponse2("bicep", "ques1:" + answer1 + "@ques2:" + answer2 + "@ques3:" + answer3 + "@ques4:" + answer4 + "@ques5:" + answer5 + "@ques6:" + answer6 + "@jques1:" + janswer1 + "@jques2:" + janswer2 + "@vques1:" + vanswer1 + "@pques1:" + panswer1);
            }
            svalue = value.split(";")[0];
            bicepshirt = svalue;
            jvalue = value.split(";")[1];
            bicepjacket = jvalue;
            vvalue = value.split(";")[2];
            bicepvest = vvalue;
            pvalue = value.split(";")[3];
            biceppant = pvalue;
            var bicep_shirt = parseFloat(svalue);
            $("#bicep_shirt_allow").html(bicep_shirt+ " Cm");
            //$("#final_shirt_bicep").html((parseFloat(sizeData.bicep) + parseFloat(bicep_shirt)).toFixed(1) + " Cm");
            //for bicep jacket
            var bicep_jacket = (parseFloat(jvalue));
            $("#bicep_jacket_allow").html(bicep_jacket+ " Cm");
            //$("#final_jacket_bicep").html((parseFloat(sizeData.bicep) + parseFloat(bicep_jacket)).toFixed(1) + " Cm");

            var bicep_vest = parseFloat(vvalue);
            $("#bicep_vest_allow").html(bicep_vest+ " Cm");
            //$("#final_vest_bicep").html((parseFloat(sizeData.bicep) + parseFloat(bicep_vest)).toFixed(1) + " Cm");
            //for bicep pant
            var bicep_pant = (parseFloat(pvalue));
            $("#bicep_pant_allow").html(bicep_pant+ " Cm");
            //$("#final_pant_bicep").html((parseFloat(sizeData.bicep) + parseFloat(bicep_pant)).toFixed(1) + " Cm");

            //for back shoulder shirt
            answer1 = sizeData.shirt_que_1;
            answer2 = sizeData.shirt_que_2;
            answer3 = sizeData.shirt_que_3;
            answer4 = sizeData.shirt_que_4;
            answer5 = sizeData.shirt_que_5;
            answer6 = sizeData.shirt_que_6;
            janswer1 = sizeData.suit_que_1;
            janswer2 = sizeData.suit_que_2;
            vanswer1 = sizeData.vest_que_1;
            panswer1 = sizeData.pant_que_1;
            if (chest-stomach < 7) {
                value = getResponse2("back_shoulder_width", "ques1:" + answer1 + "@ques2:" + answer2 + "@ques3:" + answer3 + "@ques4:" + answer4 + "@ques5:" + answer5 + "@ques6:" + answer6 + "@jques1lessthan7:" + janswer1 + "@jques2lessthan7:" + janswer2 + "@vques1:" + vanswer1 + "@pques1:" + panswer1);
            } else {
                value = getResponse2("back_shoulder_width", "ques1:" + answer1 + "@ques2:" + answer2 + "@ques3:" + answer3 + "@ques4:" + answer4 + "@ques5:" + answer5 + "@ques6:" + answer6 + "@jques1:" + janswer1 + "@jques2:" + janswer2 + "@vques1:" + vanswer1 + "@pques1:" + panswer1);
            }
            svalue = value.split(";")[0];
            backshouldershirt = svalue;
            jvalue = value.split(";")[1];
            backshoulderjacket = jvalue;
            vvalue = value.split(";")[2];
            backshouldervest = vvalue;
            pvalue = value.split(";")[3];
            backshoulderpant = pvalue;
            var back_shoulder_shirt = parseFloat(svalue);
            $("#back_shoulder_shirt_allow").html(back_shoulder_shirt+ " Cm");
            //$("#final_shirt_back_shoulder").html((parseFloat(sizeData.back_sholuder) + parseFloat(back_shoulder_shirt)).toFixed(1) + " Cm");
            //for back shoulder jacket
            var back_shoulder_jacket = (parseFloat(jvalue));
            $("#back_shoulder_jacket_allow").html(back_shoulder_jacket+ " Cm");
            //$("#final_jacket_back_shoulder").html((parseFloat(sizeData.back_sholuder) + parseFloat(back_shoulder_jacket)).toFixed(1) + " Cm");

            var back_shoulder_vest = parseFloat(vvalue);
            $("#back_shoulder_vest_allow").html(back_shoulder_vest+ " Cm");
            //$("#final_vest_back_shoulder").html((parseFloat(sizeData.back_sholuder) + parseFloat(back_shoulder_vest)).toFixed(1) + " Cm");
            //for back shoulder pant
            var back_shoulder_pant = (parseFloat(pvalue));
            $("#back_shoulder_pant_allow").html(back_shoulder_pant+ " Cm");
            //$("#final_pant_back_shoulder").html((parseFloat(sizeData.back_sholuder) + parseFloat(back_shoulder_pant)).toFixed(1) + " Cm");

            ////////for front shoulder shirt
            answer1 = sizeData.shirt_que_1;
            answer2 = sizeData.shirt_que_2;
            answer3 = sizeData.shirt_que_3;
            answer4 = sizeData.shirt_que_4;
            answer5 = sizeData.shirt_que_5;
            answer6 = sizeData.shirt_que_6;
            janswer1 = sizeData.suit_que_1;
            janswer2 = sizeData.suit_que_2;
            vanswer1 = sizeData.vest_que_1;
            panswer1 = sizeData.pant_que_1;
            if (chest-stomach < 7) {
                value = getResponse2("front_shoulder_width", "ques1:" + answer1 + "@ques2:" + answer2 + "@ques3:" + answer3 + "@ques4:" + answer4 + "@ques5:" + answer5 + "@ques6:" + answer6 + "@jques1lessthan7:" + janswer1 + "@jques2lessthan7:" + janswer2 + "@vques1:" + vanswer1 + "@pques1:" + panswer1);
            } else {
                value = getResponse2("front_shoulder_width", "ques1:" + answer1 + "@ques2:" + answer2 + "@ques3:" + answer3 + "@ques4:" + answer4 + "@ques5:" + answer5 + "@ques6:" + answer6 + "@jques1:" + janswer1 + "@jques2:" + janswer2 + "@vques1:" + vanswer1 + "@pques1:" + panswer1);
            }
            svalue = value.split(";")[0];
            frontshouldershirt = svalue;
            jvalue = value.split(";")[1];
            frontshoulderjacket = jvalue;
            vvalue = value.split(";")[2];
            frontshouldervest = vvalue;
            pvalue = value.split(";")[3];
            frontshoulderpant = pvalue;
            var front_shoulder_shirt = parseFloat(svalue);
            $("#front_shoulder_shirt_allow").html(front_shoulder_shirt+ " Cm");
            //$("#final_shirt_front_shoulder").html((parseFloat(sizeData.front_shoulder) + parseFloat(front_shoulder_shirt)).toFixed(1) + " Cm");
            //for front shoulder jacket
            var front_shoulder_jacket = (parseFloat(jvalue));
            $("#front_shoulder_jacket_allow").html(front_shoulder_jacket+ " Cm");
            //$("#final_jacket_front_shoulder").html((parseFloat(sizeData.front_shoulder) + parseFloat(front_shoulder_jacket)).toFixed(1) + " Cm");

            var front_shoulder_vest = parseFloat(vvalue);
            $("#front_shoulder_vest_allow").html(front_shoulder_vest+ " Cm");
            //$("#final_vest_front_shoulder").html((parseFloat(sizeData.front_shoulder) + parseFloat(front_shoulder_vest)).toFixed(1) + " Cm");
            //for front shoulder pant
            var front_shoulder_pant = (parseFloat(pvalue));
            $("#front_shoulder_pant_allow").html(front_shoulder_pant+ " Cm");
            //$("#final_pant_front_shoulder").html((parseFloat(sizeData.front_shoulder) + parseFloat(front_shoulder_pant)).toFixed(1) + " Cm");

            //for left_sleeve_length shirt
            answer1 = sizeData.shirt_que_1;
            answer2 = sizeData.shirt_que_2;
            answer3 = sizeData.shirt_que_3;
            answer4 = sizeData.shirt_que_4;
            answer5 = sizeData.shirt_que_5;
            answer6 = sizeData.shirt_que_6;
            janswer1 = sizeData.suit_que_1;
            janswer2 = sizeData.suit_que_2;
            vanswer1 = sizeData.vest_que_1;
            panswer1 = sizeData.pant_que_1;
            if (chest-stomach < 7) {
                value = getResponse2("left_sleeve_length", "ques1:" + answer1 + "@ques2:" + answer2 + "@ques3:" + answer3 + "@ques4:" + answer4 + "@ques5:" + answer5 + "@ques6:" + answer6 + "@jques1lessthan7:" + janswer1 + "@jques2lessthan7:" + janswer2 + "@vques1:" + vanswer1 + "@pques1:" + panswer1);
            } else {
                value = getResponse2("left_sleeve_length", "ques1:" + answer1 + "@ques2:" + answer2 + "@ques3:" + answer3 + "@ques4:" + answer4 + "@ques5:" + answer5 + "@ques6:" + answer6 + "@jques1:" + janswer1 + "@jques2:" + janswer2 + "@vques1:" + vanswer1 + "@pques1:" + panswer1);
            }
            svalue = value.split(";")[0];
            sleeveleftshirt = svalue;
            jvalue = value.split(";")[1];
            sleeveleftjacket = jvalue;
            vvalue = value.split(";")[2];
            sleeveleftvest = vvalue;
            pvalue = value.split(";")[3];
            sleeveleftpant = pvalue;
            var left_sleeve_length_shirt = parseFloat(svalue);
            $("#sleeve_left_shirt_allow").html(left_sleeve_length_shirt+ " Cm");
            //$("#final_shirt_sleeve_left").html((parseFloat(sizeData.sleeve_length_left) + parseFloat(left_sleeve_length_shirt)).toFixed(1) + " Cm");
            //for front shoulder jacket
            var left_sleeve_length_jacket = (parseFloat(jvalue));
            $("#sleeve_left_jacket_allow").html(left_sleeve_length_jacket+ " Cm");
            //$("#final_jacket_sleeve_left").html((parseFloat(sizeData.sleeve_length_left) + parseFloat(left_sleeve_length_jacket)).toFixed(1) + " Cm");

            var left_sleeve_length_vest = parseFloat(vvalue);
            $("#sleeve_left_vest_allow").html(left_sleeve_length_vest+ " Cm");
            //$("#final_vest_sleeve_left").html((parseFloat(sizeData.sleeve_length_left) + parseFloat(left_sleeve_length_vest)).toFixed(1) + " Cm");
            //for front shoulder pant
            var left_sleeve_length_pant = (parseFloat(pvalue));
            $("#sleeve_left_pant_allow").html(left_sleeve_length_pant+ " Cm");
            //$("#final_pant_sleeve_left").html((parseFloat(sizeData.sleeve_length_left) + parseFloat(left_sleeve_length_pant)).toFixed(1) + " Cm");

            /////for right_sleeve_length shirt
            answer1 = sizeData.shirt_que_1;
            answer2 = sizeData.shirt_que_2;
            answer3 = sizeData.shirt_que_3;
            answer4 = sizeData.shirt_que_4;
            answer5 = sizeData.shirt_que_5;
            answer6 = sizeData.shirt_que_6;
            janswer1 = sizeData.suit_que_1;
            janswer2 = sizeData.suit_que_2;
            vanswer1 = sizeData.vest_que_1;
            panswer1 = sizeData.pant_que_1;
            if (chest-stomach < 7) {
                value = getResponse2("right_sleeve_length", "ques1:" + answer1 + "@ques2:" + answer2 + "@ques3:" + answer3 + "@ques4:" + answer4 + "@ques5:" + answer5 + "@ques6:" + answer6 + "@jques1lessthan7:" + janswer1 + "@jques2lessthan7:" + janswer2 + "@vques1:" + vanswer1 + "@pques1:" + panswer1);
            } else {
                value = getResponse2("right_sleeve_length", "ques1:" + answer1 + "@ques2:" + answer2 + "@ques3:" + answer3 + "@ques4:" + answer4 + "@ques5:" + answer5 + "@ques6:" + answer6 + "@jques1:" + janswer1 + "@jques2:" + janswer2 + "@vques1:" + vanswer1 + "@pques1:" + panswer1);
            }
            svalue = value.split(";")[0];
            sleeverightshirt = svalue;
            jvalue = value.split(";")[1];
            sleeverightjacket = jvalue;
            vvalue = value.split(";")[2];
            sleeverightvest = vvalue;
            pvalue = value.split(";")[3];
            sleeverightpant = pvalue;
            var right_sleeve_length_shirt = parseFloat(svalue);
            $("#sleeve_right_shirt_allow").html(right_sleeve_length_shirt+ " Cm");
            //$("#final_shirt_sleeve_right").html((parseFloat(sizeData.sleeve_length_right) + parseFloat(right_sleeve_length_shirt)).toFixed(1) + " Cm");
            //for front shoulder jacket
            var right_sleeve_length_jacket = (parseFloat(jvalue));
            $("#sleeve_right_jacket_allow").html(right_sleeve_length_jacket+ " Cm");
            //$("#final_jacket_sleeve_right").html((parseFloat(sizeData.sleeve_length_right) + parseFloat(right_sleeve_length_jacket)).toFixed(1) + " Cm");

            var right_sleeve_length_vest = parseFloat(vvalue);
            $("#sleeve_right_vest_allow").html(right_sleeve_length_vest+ " Cm");
            //$("#final_vest_sleeve_right").html((parseFloat(sizeData.sleeve_length_right) + parseFloat(right_sleeve_length_vest)).toFixed(1) + " Cm");
            //for front shoulder pant
            var right_sleeve_length_pant = (parseFloat(pvalue));
            $("#sleeve_right_pant_allow").html(right_sleeve_length_pant+ " Cm");
            //$("#final_pant_sleeve_right").html((parseFloat(sizeData.sleeve_length_right) + parseFloat(right_sleeve_length_pant)).toFixed(1) + " Cm");

            //// for thigh shirt
            answer1 = sizeData.shirt_que_1;
            answer2 = sizeData.shirt_que_2;
            answer3 = sizeData.shirt_que_3;
            answer4 = sizeData.shirt_que_4;
            answer5 = sizeData.shirt_que_5;
            answer6 = sizeData.shirt_que_6;
            janswer1 = sizeData.suit_que_1;
            janswer2 = sizeData.suit_que_2;
            vanswer1 = sizeData.vest_que_1;
            panswer1 = sizeData.pant_que_1;
            if (chest-stomach < 7) {
                value = getResponse2("thigh", "ques1:" + answer1 + "@ques2:" + answer2 + "@ques3:" + answer3 + "@ques4:" + answer4 + "@ques5:" + answer5 + "@ques6:" + answer6 + "@jques1lessthan7:" + janswer1 + "@jques2lessthan7:" + janswer2 + "@vques1:" + vanswer1 + "@pques1:" + panswer1);
            } else {
                value = getResponse2("thigh", "ques1:" + answer1 + "@ques2:" + answer2 + "@ques3:" + answer3 + "@ques4:" + answer4 + "@ques5:" + answer5 + "@ques6:" + answer6 + "@jques1:" + janswer1 + "@jques2:" + janswer2 + "@vques1:" + vanswer1 + "@pques1:" + panswer1);
            }
            svalue = value.split(";")[0];
            thighshirt = svalue;
            jvalue = value.split(";")[1];
            thighjacket = jvalue;
            vvalue = value.split(";")[2];
            thighvest = vvalue;
            pvalue = value.split(";")[3];
            thighpant = pvalue;
            //for thigh shirt
            var thigh_shirt = parseFloat(svalue);
            $("#thigh_shirt_allow").html(thigh_shirt+ " Cm");
            //$("#final_shirt_thigh").html((parseFloat(sizeData.thigh) + parseFloat(thigh_shirt)).toFixed(1) + " Cm");
            //for thigh jacket
            var thigh_jacket = (parseFloat(jvalue));
            $("#thigh_jacket_allow").html(thigh_jacket+ " Cm");
            //$("#final_jacket_thigh").html((parseFloat(sizeData.thigh) + parseFloat(thigh_jacket)).toFixed(1) + " Cm");
            //for thigh vest
            var thigh_vest = parseFloat(vvalue);
            $("#thigh_vest_allow").html(thigh_vest+ " Cm");
            //$("#final_vest_thigh").html((parseFloat(sizeData.thigh) + parseFloat(thigh_vest)).toFixed(1) + " Cm");
            //for thigh pant
            var thigh_pant = (parseFloat(pvalue));
            $("#thigh_pant_allow").html(thigh_pant+ " Cm");
            //$("#final_pant_thigh").html((parseFloat(sizeData.thigh) + parseFloat(thigh_pant)).toFixed(1) + " Cm");

            //for nape shirt
            answer1 = sizeData.shirt_que_1;
            answer2 = sizeData.shirt_que_2;
            answer3 = sizeData.shirt_que_3;
            answer4 = sizeData.shirt_que_4;
            answer5 = sizeData.shirt_que_5;
            answer6 = sizeData.shirt_que_6;
            janswer1 = sizeData.suit_que_1;
            janswer2 = sizeData.suit_que_2;
            vanswer1 = sizeData.vest_que_1;
            panswer1 = sizeData.pant_que_1;
            if (chest-stomach < 7) {
                value = getResponse2("nape_to_waist", "ques1:" + answer1 + "@ques2:" + answer2 + "@ques3:" + answer3 + "@ques4:" + answer4 + "@ques5:" + answer5 + "@ques6:" + answer6 + "@jques1lessthan7:" + janswer1 + "@jques2lessthan7:" + janswer2 + "@vques1:" + vanswer1 + "@pques1:" + panswer1);
            } else {
                value = getResponse2("nape_to_waist", "ques1:" + answer1 + "@ques2:" + answer2 + "@ques3:" + answer3 + "@ques4:" + answer4 + "@ques5:" + answer5 + "@ques6:" + answer6 + "@jques1:" + janswer1 + "@jques2:" + janswer2 + "@vques1:" + vanswer1 + "@pques1:" + panswer1);
            }
            svalue = value.split(";")[0];
            napetowaistshirt = svalue;
            jvalue = value.split(";")[1];
            napetowaistjacket = jvalue;
            vvalue = value.split(";")[2];
            napetowaistvest = vvalue;
            pvalue = value.split(";")[3];
            napetowaistpant = pvalue;
            //for nape to waist shirt
            var nape_to_waist_shirt = parseFloat(svalue);
            $("#nape_shirt_allow").html(nape_to_waist_shirt+ " Cm");
            //$("#final_shirt_nape").html((parseFloat(sizeData.nape_to_waist) + parseFloat(nape_to_waist_shirt)).toFixed(1) + " Cm");
            //for nape to waist jacket
            var nape_to_waist_jacket = (parseFloat(jvalue));
            $("#nape_jacket_allow").html(nape_to_waist_jacket+ " Cm");
            //$("#final_jacket_nape").html((parseFloat(sizeData.nape_to_waist) + parseFloat(nape_to_waist_jacket)).toFixed(1) + " Cm");
            //for nape to waist vest
            var nape_to_waist_vest = parseFloat(vvalue);
            $("#nape_vest_allow").html(nape_to_waist_vest+ " Cm");
            //$("#final_vest_nape").html((parseFloat(sizeData.nape_to_waist) + parseFloat(nape_to_waist_vest)).toFixed(1) + " Cm");
            //for nape to waist pant
            var nape_to_waist_pant = (parseFloat(pvalue));
            $("#nape_pant_allow").html(nape_to_waist_pant+ " Cm");
            //$("#final_pant_nape").html((parseFloat(sizeData.nape_to_waist) + parseFloat(nape_to_waist_pant)).toFixed(1) + " Cm");

            ////////for front_waist shirt
            answer1 = sizeData.shirt_que_1;
            answer2 = sizeData.shirt_que_2;
            answer3 = sizeData.shirt_que_3;
            answer4 = sizeData.shirt_que_4;
            answer5 = sizeData.shirt_que_5;
            answer6 = sizeData.shirt_que_6;
            janswer1 = sizeData.suit_que_1;
            janswer2 = sizeData.suit_que_2;
            vanswer1 = sizeData.vest_que_1;
            panswer1 = sizeData.pant_que_1;
            if (chest-stomach < 7) {
                value = getResponse2("front_waist_length", "ques1:" + answer1 + "@ques2:" + answer2 + "@ques3:" + answer3 + "@ques4:" + answer4 + "@ques5:" + answer5 + "@ques6:" + answer6 + "@jques1lessthan7:" + janswer1 + "@jques2lessthan7:" + janswer2 + "@vques1:" + vanswer1 + "@pques1:" + panswer1);
            } else {
                value = getResponse2("front_waist_length", "ques1:" + answer1 + "@ques2:" + answer2 + "@ques3:" + answer3 + "@ques4:" + answer4 + "@ques5:" + answer5 + "@ques6:" + answer6 + "@jques1:" + janswer1 + "@jques2:" + janswer2 + "@vques1:" + vanswer1 + "@pques1:" + panswer1);
            }
            svalue = value.split(";")[0];
            frontwaistlengthshirt = svalue;
            jvalue = value.split(";")[1];
            frontwaistlengthjacket = jvalue;
            vvalue = value.split(";")[2];
            frontwaistlengthvest = vvalue;
            pvalue = value.split(";")[3];
            frontwaistlengthpant = pvalue;
            //for front shoulder shirt
            var front_waist_shirt = parseFloat(svalue);
            $("#front_waist_shirt_allow").html(front_waist_shirt+ " Cm");
            //$("#final_shirt_front_waist").html((parseFloat(sizeData.front_waist_length) + parseFloat(front_waist_shirt)).toFixed(1) + " Cm");
            //for front shoulder jacket
            var front_waist_jacket = (parseFloat(jvalue));
            $("#front_waist_jacket_allow").html(front_waist_jacket+ " Cm");
            //$("#final_jacket_front_waist").html((parseFloat(sizeData.front_waist_length) + parseFloat(front_waist_jacket)).toFixed(1) + " Cm");
            //for front shoulder vest
            var front_waist_vest = parseFloat(vvalue);
            $("#front_waist_vest_allow").html(front_waist_vest+ " Cm");
            //$("#final_vest_front_waist").html((parseFloat(sizeData.front_waist_length) + parseFloat(front_waist_vest)).toFixed(1) + " Cm");
            //for front shoulder pant
            var front_waist_pant = (parseFloat(pvalue));
            $("#front_waist_pant_allow").html(front_waist_pant+ " Cm");
            //$("#final_pant_front_waist").html((parseFloat(sizeData.front_waist_length) + parseFloat(front_waist_pant)).toFixed(1) + " Cm");

            //for wrist shirt
            answer1 = sizeData.shirt_que_1;
            answer2 = sizeData.shirt_que_2;
            answer3 = sizeData.shirt_que_3;
            answer4 = sizeData.shirt_que_4;
            answer5 = sizeData.shirt_que_5;
            answer6 = sizeData.shirt_que_6;
            janswer1 = sizeData.suit_que_1;
            janswer2 = sizeData.suit_que_2;
            vanswer1 = sizeData.vest_que_1;
            panswer1 = sizeData.pant_que_1;
            if (chest-stomach < 7) {
                value = getResponse2("wrist", "ques1:" + answer1 + "@ques2:" + answer2 + "@ques3:" + answer3 + "@ques4:" + answer4 + "@ques5:" + answer5 + "@ques6:" + answer6 + "@jques1lessthan7:" + janswer1 + "@jques2lessthan7:" + janswer2 + "@vques1:" + vanswer1 + "@pques1:" + panswer1);
            } else {
                value = getResponse2("wrist", "ques1:" + answer1 + "@ques2:" + answer2 + "@ques3:" + answer3 + "@ques4:" + answer4 + "@ques5:" + answer5 + "@ques6:" + answer6 + "@jques1:" + janswer1 + "@jques2:" + janswer2 + "@vques1:" + vanswer1 + "@pques1:" + panswer1);
            }
            svalue = value.split(";")[0];
            wristshirt = svalue;
            jvalue = value.split(";")[1];
            wristjacket = jvalue;
            vvalue = value.split(";")[2];
            wristvest = vvalue;
            pvalue = value.split(";")[3];
            wristpant = pvalue;
            //for front shoulder shirt
            var wrist_shirt = parseFloat(svalue);
            $("#wrist_shirt_allow").html(wrist_shirt+ " Cm");
            //$("#final_shirt_wrist").html((parseFloat(sizeData.wrist) + parseFloat(wrist_shirt)).toFixed(1) + " Cm");
            //for front shoulder jacket
            var wrist_jacket = (parseFloat(jvalue));
            $("#wrist_jacket_allow").html(wrist_jacket+ " Cm");
            //$("#final_jacket_wrist").html((parseFloat(sizeData.wrist) + parseFloat(wrist_jacket)).toFixed(1) + " Cm");
            //for front shoulder vest
            var wrist_vest = parseFloat(vvalue);
            $("#wrist_vest_allow").html(wrist_vest+ " Cm");
            //$("#final_vest_wrist").html((parseFloat(sizeData.wrist) + parseFloat(wrist_vest)).toFixed(1) + " Cm");
            //for front shoulder pant
            var wrist_pant = (parseFloat(pvalue));
            $("#wrist_pant_allow").html(wrist_pant+ " Cm");
            //$("#final_pant_wrist").html((parseFloat(sizeData.wrist) + parseFloat(wrist_pant)).toFixed(1) + " Cm");

            ///////for waist shirt
            answer1 = sizeData.shirt_que_1;
            answer2 = sizeData.shirt_que_2;
            answer3 = sizeData.shirt_que_3;
            answer4 = sizeData.shirt_que_4;
            answer5 = sizeData.shirt_que_5;
            answer6 = sizeData.shirt_que_6;
            janswer1 = sizeData.suit_que_1;
            janswer2 = sizeData.suit_que_2;
            vanswer1 = sizeData.vest_que_1;
            panswer1 = sizeData.pant_que_1;
            if (chest-stomach < 7) {
                value = getResponse2("waist", "ques1:" + answer1 + "@ques2:" + answer2 + "@ques3:" + answer3 + "@ques4:" + answer4 + "@ques5:" + answer5 + "@ques6:" + answer6 + "@jques1lessthan7:" + janswer1 + "@jques2lessthan7:" + janswer2 + "@vques1:" + vanswer1 + "@pques1:" + panswer1);
            } else {
                value = getResponse2("waist", "ques1:" + answer1 + "@ques2:" + answer2 + "@ques3:" + answer3 + "@ques4:" + answer4 + "@ques5:" + answer5 + "@ques6:" + answer6 + "@jques1:" + janswer1 + "@jques2:" + janswer2 + "@vques1:" + vanswer1 + "@pques1:" + panswer1);
            }
            svalue = value.split(";")[0];
            waistshirt = svalue;
            jvalue = value.split(";")[1];
            waistjacket = jvalue;
            vvalue = value.split(";")[2];
            waistvest = vvalue;
            pvalue = value.split(";")[3];
            waistpant = pvalue;
            //for front shoulder shirt
            var waist_shirt = parseFloat(svalue);
            $("#waist_shirt_allow").html(waist_shirt+ " Cm");
            //$("#final_shirt_waist").html((parseFloat(sizeData.waist) + parseFloat(waist_shirt)).toFixed(1) + " Cm");
            //for front shoulder jacket
            var waist_jacket = (parseFloat(jvalue));
            $("#waist_jacket_allow").html(waist_jacket+ " Cm");
            //$("#final_jacket_waist").html((parseFloat(sizeData.waist) + parseFloat(waist_jacket)).toFixed(1) + " Cm");
            //for front shoulder vest
            var waist_vest = parseFloat(vvalue);
            $("#waist_vest_allow").html(waist_vest+ " Cm");
            //$("#final_vest_waist").html((parseFloat(sizeData.waist) + parseFloat(waist_vest)).toFixed(1) + " Cm");
            //for front shoulder pant
            var waist_pant = (parseFloat(pvalue));
            $("#waist_pant_allow").html(waist_pant+ " Cm");
            //$("#final_pant_waist").html((parseFloat(sizeData.waist) + parseFloat(waist_pant)).toFixed(1) + " Cm");

            //for calf shirt
            answer1 = sizeData.shirt_que_1;
            answer2 = sizeData.shirt_que_2;
            answer3 = sizeData.shirt_que_3;
            answer4 = sizeData.shirt_que_4;
            answer5 = sizeData.shirt_que_5;
            answer6 = sizeData.shirt_que_6;
            janswer1 = sizeData.suit_que_1;
            janswer2 = sizeData.suit_que_2;
            vanswer1 = sizeData.vest_que_1;
            panswer1 = sizeData.pant_que_1;
            if (chest-stomach < 7) {
                value = getResponse2("calf", "ques1:" + answer1 + "@ques2:" + answer2 + "@ques3:" + answer3 + "@ques4:" + answer4 + "@ques5:" + answer5 + "@ques6:" + answer6 + "@jques1lessthan7:" + janswer1 + "@jques2lessthan7:" + janswer2 + "@vques1:" + vanswer1 + "@pques1:" + panswer1);
            } else {
                value = getResponse2("calf", "ques1:" + answer1 + "@ques2:" + answer2 + "@ques3:" + answer3 + "@ques4:" + answer4 + "@ques5:" + answer5 + "@ques6:" + answer6 + "@jques1:" + janswer1 + "@jques2:" + janswer2 + "@vques1:" + vanswer1 + "@pques1:" + panswer1);
            }
            svalue = value.split(";")[0];
            calfshirt = svalue;
            jvalue = value.split(";")[1];
            calfjacket = jvalue;
            vvalue = value.split(";")[2];
            calfvest = vvalue;
            pvalue = value.split(";")[3];
            calfpant = pvalue;
            //for front shoulder shirt
            var calf_shirt = parseFloat(svalue);
            $("#calf_shirt_allow").html(calf_shirt+ " Cm");
            //$("#final_shirt_calf").html((parseFloat(sizeData.calf) + parseFloat(calf_shirt)).toFixed(1) + " Cm");
            //for front shoulder jacket
            var calf_jacket = (parseFloat(jvalue));
            $("#calf_jacket_allow").html(calf_jacket+ " Cm");
            //$("#final_jacket_calf").html((parseFloat(sizeData.calf) + parseFloat(calf_jacket)).toFixed(1) + " Cm");
            //for front shoulder vest
            var calf_vest = parseFloat(vvalue);
            $("#calf_vest_allow").html(calf_vest+ " Cm");
            //$("#final_vest_calf").html((parseFloat(sizeData.calf) + parseFloat(calf_vest)).toFixed(1) + " Cm");
            //for front shoulder pant
            var calf_pant = (parseFloat(pvalue));
            $("#calf_pant_allow").html(calf_pant+ " Cm");
            //$("#final_pant_calf").html((parseFloat(sizeData.calf) + parseFloat(calf_pant)).toFixed(1) + " Cm");

            ////////for b_waist_shirt
            answer1 = sizeData.shirt_que_1;
            answer2 = sizeData.shirt_que_2;
            answer3 = sizeData.shirt_que_3;
            answer4 = sizeData.shirt_que_4;
            answer5 = sizeData.shirt_que_5;
            answer6 = sizeData.shirt_que_6;
            janswer1 = sizeData.suit_que_1;
            janswer2 = sizeData.suit_que_2;
            vanswer1 = sizeData.vest_que_1;
            panswer1 = sizeData.pant_que_1;
            if (chest-stomach < 7) {
                value = getResponse2("back_waist_height", "ques1:" + answer1 + "@ques2:" + answer2 + "@ques3:" + answer3 + "@ques4:" + answer4 + "@ques5:" + answer5 + "@ques6:" + answer6 + "@jques1lessthan7:" + janswer1 + "@jques2lessthan7:" + janswer2 + "@vques1:" + vanswer1 + "@pques1:" + panswer1);
            } else {
                value = getResponse2("back_waist_height", "ques1:" + answer1 + "@ques2:" + answer2 + "@ques3:" + answer3 + "@ques4:" + answer4 + "@ques5:" + answer5 + "@ques6:" + answer6 + "@jques1:" + janswer1 + "@jques2:" + janswer2 + "@vques1:" + vanswer1 + "@pques1:" + panswer1);
            }
            svalue = value.split(";")[0];
            backwaistheightshirt = svalue;
            jvalue = value.split(";")[1];
            backwaistheightjacket = jvalue;
            vvalue = value.split(";")[2];
            backwaistheightvest = vvalue;
            pvalue = value.split(";")[3];
            backwaistheightpant = pvalue;
            //for front shoulder shirt
            var b_waist_shirt = parseFloat(svalue);
            $("#b_waist_shirt_allow").html(b_waist_shirt+ " Cm");
            //$("#final_shirt_b_waist").html((parseFloat(sizeData.back_waist_height) + parseFloat(b_waist_shirt)).toFixed(1) + " Cm");
            //for front shoulder jacket
            var b_waist_jacket = (parseFloat(jvalue));
            $("#b_waist_jacket_allow").html(b_waist_jacket+ " Cm");
            //$("#final_jacket_b_waist").html((parseFloat(sizeData.back_waist_height) + parseFloat(b_waist_jacket)).toFixed(1) + " Cm");
            //for front shoulder vest
            var b_waist_vest = parseFloat(vvalue);
            $("#b_waist_vest_allow").html(b_waist_vest+ " Cm");
            //$("#final_vest_b_waist").html((parseFloat(sizeData.back_waist_height) + parseFloat(b_waist_vest)).toFixed(1) + " Cm");
            //for front shoulder pant
            var b_waist_pant = (parseFloat(pvalue));
            $("#b_waist_pant_allow").html(b_waist_pant+ " Cm");
            //$("#final_pant_b_waist").html((parseFloat(sizeData.back_waist_height) + parseFloat(b_waist_pant)).toFixed(1) + " Cm");

            //for f_waist_shirt
            answer1 = sizeData.shirt_que_1;
            answer2 = sizeData.shirt_que_2;
            answer3 = sizeData.shirt_que_3;
            answer4 = sizeData.shirt_que_4;
            answer5 = sizeData.shirt_que_5;
            answer6 = sizeData.shirt_que_6;
            janswer1 = sizeData.suit_que_1;
            janswer2 = sizeData.suit_que_2;
            vanswer1 = sizeData.vest_que_1;
            panswer1 = sizeData.pant_que_1;
            if (chest-stomach < 7) {
                value = getResponse2("front_waist_height", "ques1:" + answer1 + "@ques2:" + answer2 + "@ques3:" + answer3 + "@ques4:" + answer4 + "@ques5:" + answer5 + "@ques6:" + answer6 + "@jques1lessthan7:" + janswer1 + "@jques2lessthan7:" + janswer2 + "@vques1:" + vanswer1 + "@pques1:" + panswer1);
            } else {
                value = getResponse2("front_waist_height", "ques1:" + answer1 + "@ques2:" + answer2 + "@ques3:" + answer3 + "@ques4:" + answer4 + "@ques5:" + answer5 + "@ques6:" + answer6 + "@jques1:" + janswer1 + "@jques2:" + janswer2 + "@vques1:" + vanswer1 + "@pques1:" + panswer1);
            }
            svalue = value.split(";")[0];
            frontwaistheightshirt = svalue;
            jvalue = value.split(";")[1];
            frontwaistheightjacket = jvalue;
            vvalue = value.split(";")[2];
            frontwaistheightvest = vvalue;
            pvalue = value.split(";")[3];
            frontwaistheightpant = pvalue;
            //for front shoulder shirt
            var f_waist_shirt = parseFloat(svalue);
            $("#f_waist_shirt_allow").html(f_waist_shirt+ " Cm");
            //$("#final_shirt_f_waist").html((parseFloat(sizeData.front_waist_height) + parseFloat(f_waist_shirt)).toFixed(1) + " Cm");
            //for front shoulder jacket
            var f_waist_jacket = parseFloat(jvalue);
            $("#f_waist_jacket_allow").html(f_waist_jacket+ " Cm");
            //$("#final_jacket_f_waist").html((parseFloat(sizeData.front_waist_height) + parseFloat(f_waist_jacket)).toFixed(1) + " Cm");
            //for front shoulder vest
            var f_waist_vest = parseFloat(vvalue);
            $("#f_waist_vest_allow").html(f_waist_vest+ " Cm");
            //$("#final_vest_f_waist").html((parseFloat(sizeData.front_waist_height) + parseFloat(f_waist_vest)).toFixed(1) + " Cm");
            //for front shoulder pant
            var f_waist_pant = parseFloat(pvalue);
            $("#f_waist_pant_allow").html(f_waist_pant+ " Cm");
            //$("#final_pant_f_waist").html((parseFloat(sizeData.front_waist_height) + parseFloat(f_waist_pant)).toFixed(1) + " Cm");

            /////for knee shirt
            answer1 = sizeData.shirt_que_1;
            answer2 = sizeData.shirt_que_2;
            answer3 = sizeData.shirt_que_3;
            answer4 = sizeData.shirt_que_4;
            answer5 = sizeData.shirt_que_5;
            answer6 = sizeData.shirt_que_6;
            janswer1 = sizeData.suit_que_1;
            janswer2 = sizeData.suit_que_2;
            vanswer1 = sizeData.vest_que_1;
            panswer1 = sizeData.pant_que_1;
            if (chest-stomach < 7) {
                value = getResponse2("knee", "ques1:" + answer1 + "@ques2:" + answer2 + "@ques3:" + answer3 + "@ques4:" + answer4 + "@ques5:" + answer5 + "@ques6:" + answer6 + "@jques1lessthan7:" + janswer1 + "@jques2lessthan7:" + janswer2 + "@vques1:" + vanswer1 + "@pques1:" + panswer1);
            } else {
                value = getResponse2("knee", "ques1:" + answer1 + "@ques2:" + answer2 + "@ques3:" + answer3 + "@ques4:" + answer4 + "@ques5:" + answer5 + "@ques6:" + answer6 + "@jques1:" + janswer1 + "@jques2:" + janswer2 + "@vques1:" + vanswer1 + "@pques1:" + panswer1);
            }
            svalue = value.split(";")[0];
            kneeshirt = svalue;
            jvalue = value.split(";")[1];
            kneejacket = jvalue;
            vvalue = value.split(";")[2];
            kneevest = vvalue;
            pvalue = value.split(";")[3];
            kneepant = pvalue;
            //for front shoulder shirt
            var knee_shirt = parseFloat(svalue);
            $("#knee_shirt_allow").html(knee_shirt+ " Cm");
            //$("#final_shirt_knee").html((parseFloat(sizeData.knee) + parseFloat(knee_shirt)).toFixed(1) + " Cm");
            //for front shoulder jacket
            var knee_jacket = (parseFloat(jvalue));
            $("#knee_jacket_allow").html(knee_jacket+ " Cm");
            //$("#final_jacket_knee").html((parseFloat(sizeData.knee) + parseFloat(knee_jacket)).toFixed(1) + " Cm");
            //for front shoulder vest
            var knee_vest = parseFloat(vvalue);
            $("#knee_vest_allow").html(knee_vest+ " Cm");
            //$("#final_vest_knee").html((parseFloat(sizeData.knee) + parseFloat(knee_vest)).toFixed(1) + " Cm");
            //for front shoulder pant
            var knee_pant = (parseFloat(pvalue));
            $("#knee_pant_allow").html(knee_pant+ " Cm");
            //$("#final_pant_knee").html((parseFloat(sizeData.knee) + parseFloat(knee_pant)).toFixed(1) + " Cm");

            //for back_jacket_length shirt
            answer1 = sizeData.shirt_que_1;
            answer2 = sizeData.shirt_que_2;
            answer3 = sizeData.shirt_que_3;
            answer4 = sizeData.shirt_que_4;
            answer5 = sizeData.shirt_que_5;
            answer6 = sizeData.shirt_que_6;
            janswer1 = sizeData.suit_que_1;
            janswer2 = sizeData.suit_que_2;
            vanswer1 = sizeData.vest_que_1;
            panswer1 = sizeData.pant_que_1;
            if (chest-stomach < 7) {
                value = getResponse2("back_jacket_length", "ques1:" + answer1 + "@ques2:" + answer2 + "@ques3:" + answer3 + "@ques4:" + answer4 + "@ques5:" + answer5 + "@ques6:" + answer6 + "@jques1lessthan7:" + janswer1 + "@jques2lessthan7:" + janswer2 + "@vques1:" + vanswer1 + "@pques1:" + panswer1);
            } else {
                value = getResponse2("back_jacket_length", "ques1:" + answer1 + "@ques2:" + answer2 + "@ques3:" + answer3 + "@ques4:" + answer4 + "@ques5:" + answer5 + "@ques6:" + answer6 + "@jques1:" + janswer1 + "@jques2:" + janswer2 + "@vques1:" + vanswer1 + "@pques1:" + panswer1);
            }
            svalue = value.split(";")[0];
            backjacketlengthshirt = svalue;
            jvalue = value.split(";")[1];
            backjacketlengthjacket = jvalue;
            vvalue = value.split(";")[2];
            backjacketlengthvest = vvalue;
            pvalue = value.split(";")[3];
            backjacketlengthpant = pvalue;
            //for back_jacket_length shirt
            var jacket_shirt = parseFloat(svalue);
            $("#jacket_shirt_allow").html(jacket_shirt+ " Cm");
            //$("#final_shirt_jacket").html((parseFloat(sizeData.back_jacket_length) + parseFloat(jacket_shirt)).toFixed(1) + " Cm");
            //for back_jacket_length jacket
            var jacket_jacket = (parseFloat(jvalue));
            $("#jacket_jacket_allow").html(jacket_jacket+ " Cm");
            //$("#final_jacket_jacket").html((parseFloat(sizeData.back_jacket_length) + parseFloat(jacket_jacket)).toFixed(1) + " Cm");
            //for back_jacket_length vest
            var jacket_vest = parseFloat(vvalue);
            $("#jacket_vest_allow").html(jacket_vest+ " Cm");
            //$("#final_vest_jacket").html((parseFloat(sizeData.back_jacket_length) + parseFloat(jacket_vest)).toFixed(1) + " Cm");
            //for back_jacket_length pant
            var jacket_pant = (parseFloat(pvalue));
            $("#jacket_pant_allow").html(jacket_pant+ " Cm");
            //$("#final_pant_jacket").html((parseFloat(sizeData.back_jacket_length) + parseFloat(jacket_pant)).toFixed(1) + " Cm");

            //for u_rise shirt
            answer1 = sizeData.shirt_que_1;
            answer2 = sizeData.shirt_que_2;
            answer3 = sizeData.shirt_que_3;
            answer4 = sizeData.shirt_que_4;
            answer5 = sizeData.shirt_que_5;
            answer6 = sizeData.shirt_que_6;
            janswer1 = sizeData.suit_que_1;
            janswer2 = sizeData.suit_que_2;
            vanswer1 = sizeData.vest_que_1;
            panswer1 = sizeData.pant_que_1;
            if (chest-stomach < 7) {
                value = getResponse2("u_rise", "ques1:" + answer1 + "@ques2:" + answer2 + "@ques3:" + answer3 + "@ques4:" + answer4 + "@ques5:" + answer5 + "@ques6:" + answer6 + "@jques1lessthan7:" + janswer1 + "@jques2lessthan7:" + janswer2 + "@vques1:" + vanswer1 + "@pques1:" + panswer1);
            } else {
                value = getResponse2("u_rise", "ques1:" + answer1 + "@ques2:" + answer2 + "@ques3:" + answer3 + "@ques4:" + answer4 + "@ques5:" + answer5 + "@ques6:" + answer6 + "@jques1:" + janswer1 + "@jques2:" + janswer2 + "@vques1:" + vanswer1 + "@pques1:" + panswer1);
            }
            svalue = value.split(";")[0];
            riseshirt = svalue;
            jvalue = value.split(";")[1];
            risejacket = jvalue;
            vvalue = value.split(";")[2];
            risevest = vvalue;
            pvalue = value.split(";")[3];
            risepant = pvalue;
            //for back_jacket_length shirt
            var rise_shirt = parseFloat(svalue);
            $("#rise_shirt_allow").html(rise_shirt+ " Cm");
            //$("#final_shirt_rise").html((parseFloat(sizeData.u_rise) + parseFloat(rise_shirt)).toFixed(1) + " Cm");
            //for back_jacket_length jacket
            var rise_jacket = (parseFloat(jvalue));
            $("#rise_jacket_allow").html(rise_jacket+ " Cm");
            //$("#final_jacket_rise").html((parseFloat(sizeData.u_rise) + parseFloat(rise_jacket)).toFixed(1) + " Cm");
            //for back_jacket_length vest
            var rise_vest = parseFloat(vvalue);
            $("#rise_vest_allow").html(rise_vest+ " Cm");
            //$("#final_vest_rise").html((parseFloat(sizeData.u_rise) + parseFloat(rise_vest)).toFixed(1) + " Cm");
            //for back_jacket_length pant
            var rise_pant = (parseFloat(pvalue));
            $("#rise_pant_allow").html(rise_pant+ " Cm");
            //$("#final_pant_rise").html((parseFloat(sizeData.u_rise) + parseFloat(rise_pant)).toFixed(1) + " Cm");

            ////////for pant_left_shirt
            answer1 = sizeData.shirt_que_1;
            answer2 = sizeData.shirt_que_2;
            answer3 = sizeData.shirt_que_3;
            answer4 = sizeData.shirt_que_4;
            answer5 = sizeData.shirt_que_5;
            answer6 = sizeData.shirt_que_6;
            janswer1 = sizeData.suit_que_1;
            janswer2 = sizeData.suit_que_2;
            vanswer1 = sizeData.vest_que_1;
            panswer1 = sizeData.pant_que_1;
            if (chest-stomach < 7) {
                value = getResponse2("pants_left_outseam", "ques1:" + answer1 + "@ques2:" + answer2 + "@ques3:" + answer3 + "@ques4:" + answer4 + "@ques5:" + answer5 + "@ques6:" + answer6 + "@jques1lessthan7:" + janswer1 + "@jques2lessthan7:" + janswer2 + "@vques1:" + vanswer1 + "@pques1:" + panswer1);
            } else {
                value = getResponse2("pants_left_outseam", "ques1:" + answer1 + "@ques2:" + answer2 + "@ques3:" + answer3 + "@ques4:" + answer4 + "@ques5:" + answer5 + "@ques6:" + answer6 + "@jques1:" + janswer1 + "@jques2:" + janswer2 + "@vques1:" + vanswer1 + "@pques1:" + panswer1);
            }
            svalue = value.split(";")[0];
            pantleftshirt = svalue;
            jvalue = value.split(";")[1];
            pantleftjacket = jvalue;
            vvalue = value.split(";")[2];
            pantleftvest = vvalue;
            pvalue = value.split(";")[3];
            pantleftpant = pvalue;
            //for back_jacket_length shirt
            var p_left_shirt = parseFloat(svalue);
            $("#p_left_shirt_allow").html(p_left_shirt+ " Cm");
            //$("#final_shirt_p_left").html((parseFloat(sizeData.pant_left_outseam) + parseFloat(p_left_shirt)).toFixed(1) + " Cm");
            //for back_jacket_length jacket
            var p_left_jacket = (parseFloat(jvalue));
            $("#p_left_jacket_allow").html(p_left_jacket+ " Cm");
            //$("#final_jacket_p_left").html((parseFloat(sizeData.pant_left_outseam) + parseFloat(p_left_jacket)).toFixed(1) + " Cm");
            //for back_jacket_length vest
            var p_left_vest = parseFloat(vvalue);
            $("#p_left_vest_allow").html(p_left_vest+ " Cm");
            //$("#final_vest_p_left").html((parseFloat(sizeData.pant_left_outseam) + parseFloat(p_left_vest)).toFixed(1) + " Cm");
            //for back_jacket_length pant
            var p_left_pant = (parseFloat(pvalue));
            $("#p_left_pant_allow").html(p_left_pant+ " Cm");
            //$("#final_pant_p_left").html((parseFloat(sizeData.pant_left_outseam) + parseFloat(p_left_pant)).toFixed(1) + " Cm");

            //for p_right shirt
            answer1 = sizeData.shirt_que_1;
            answer2 = sizeData.shirt_que_2;
            answer3 = sizeData.shirt_que_3;
            answer4 = sizeData.shirt_que_4;
            answer5 = sizeData.shirt_que_5;
            answer6 = sizeData.shirt_que_6;
            janswer1 = sizeData.suit_que_1;
            janswer2 = sizeData.suit_que_2;
            vanswer1 = sizeData.vest_que_1;
            panswer1 = sizeData.pant_que_1;
            if (chest-stomach < 7) {
                value = getResponse2("pants_right_outseam", "ques1:" + answer1 + "@ques2:" + answer2 + "@ques3:" + answer3 + "@ques4:" + answer4 + "@ques5:" + answer5 + "@ques6:" + answer6 + "@jques1lessthan7:" + janswer1 + "@jques2lessthan7:" + janswer2 + "@vques1:" + vanswer1 + "@pques1:" + panswer1);
            } else {
                value = getResponse2("pants_right_outseam", "ques1:" + answer1 + "@ques2:" + answer2 + "@ques3:" + answer3 + "@ques4:" + answer4 + "@ques5:" + answer5 + "@ques6:" + answer6 + "@jques1:" + janswer1 + "@jques2:" + janswer2 + "@vques1:" + vanswer1 + "@pques1:" + panswer1);
            }
            svalue = value.split(";")[0];
            pantrightshirt = svalue;
            jvalue = value.split(";")[1];
            pantrightjacket = jvalue;
            vvalue = value.split(";")[2];
            pantrightvest = vvalue;
            pvalue = value.split(";")[3];
            pantrightpant = pvalue;
            //for back_jacket_length shirt
            var p_right_shirt = parseFloat(svalue);
            $("#p_right_shirt_allow").html(p_right_shirt+ " Cm");
            //$("#final_shirt_p_right").html((parseFloat(sizeData.pant_right_outseam) + parseFloat(p_right_shirt)).toFixed(1) + " Cm");
            //for back_jacket_length jacket
            var p_right_jacket = (parseFloat(jvalue));
            $("#p_right_jacket_allow").html(p_right_jacket+ " Cm");
            //$("#final_jacket_p_right").html((parseFloat(sizeData.pant_right_outseam) + parseFloat(p_right_jacket)).toFixed(1) + " Cm");
            //for back_jacket_length vest
            var p_right_vest = parseFloat(vvalue);
            $("#p_right_vest_allow").html(p_right_vest+ " Cm");
            //$("#final_vest_p_right").html((parseFloat(sizeData.pant_right_outseam) + parseFloat(p_right_vest)).toFixed(1) + " Cm");
            //for back_jacket_length pant
            var p_right_pant = (parseFloat(pvalue));
            $("#p_right_pant_allow").html(p_right_pant+ " Cm");
            //$("#final_pant_p_right").html((parseFloat(sizeData.pant_right_outseam) + parseFloat(p_right_pant)).toFixed(1) + " Cm");

            /////for bottom shirt
            answer1 = sizeData.shirt_que_1;
            answer2 = sizeData.shirt_que_2;
            answer3 = sizeData.shirt_que_3;
            answer4 = sizeData.shirt_que_4;
            answer5 = sizeData.shirt_que_5;
            answer6 = sizeData.shirt_que_6;
            janswer1 = sizeData.suit_que_1;
            janswer2 = sizeData.suit_que_2;
            vanswer1 = sizeData.vest_que_1;
            panswer1 = sizeData.pant_que_1;
            if (chest-stomach < 7) {
                value = getResponse2("bottom", "ques1:" + answer1 + "@ques2:" + answer2 + "@ques3:" + answer3 + "@ques4:" + answer4 + "@ques5:" + answer5 + "@ques6:" + answer6 + "@jques1lessthan7:" + janswer1 + "@jques2lessthan7:" + janswer2 + "@vques1:" + vanswer1 + "@pques1:" + panswer1);
            } else {
                value = getResponse2("bottom", "ques1:" + answer1 + "@ques2:" + answer2 + "@ques3:" + answer3 + "@ques4:" + answer4 + "@ques5:" + answer5 + "@ques6:" + answer6 + "@jques1:" + janswer1 + "@jques2:" + janswer2 + "@vques1:" + vanswer1 + "@pques1:" + panswer1);
            }
            svalue = value.split(";")[0];
            bottomshirt = svalue;
            jvalue = value.split(";")[1];
            bottomjacket = jvalue;
            vvalue = value.split(";")[2];
            bottomvest = vvalue;
            pvalue = value.split(";")[3];
            bottompant = pvalue;
            //for bottom shirt
            var bottom_shirt = parseFloat(svalue);
            $("#bottom_shirt_allow").html(bottom_shirt+ " Cm");
            //$("#final_shirt_bottom").html((parseFloat(sizeData.bottom) + parseFloat(bottom_shirt)).toFixed(1) + " Cm");
            //for bottom jacket
            var bottom_jacket = (parseFloat(jvalue));
            $("#bottom_jacket_allow").html(bottom_jacket+ " Cm");
            //$("#final_jacket_bottom").html((parseFloat(sizeData.bottom) + parseFloat(bottom_jacket)).toFixed(1) + " Cm");
            //for bottom vest
            var bottom_vest = parseFloat(vvalue);
            $("#bottom_vest_allow").html(bottom_vest+ " Cm");
            //$("#final_vest_bottom").html((parseFloat(sizeData.bottom) + parseFloat(bottom_vest)).toFixed(1) + " Cm");
            //for bottom pant
            var bottom_pant = (parseFloat(pvalue));
            $("#bottom_pant_allow").html(bottom_pant+ " Cm");
            //$("#final_pant_bottom").html((parseFloat(sizeData.bottom) + parseFloat(bottom_pant)).toFixed(1) + " Cm");
        }
        measurementData(measurement_id);
    }
    function getResponse2(body_param, ques_answer) {
        var url = '../api/registerUser.php';
        var jvalue = 0;
        var svalue = 0;
        var vvalue = 0;
        var pvalue = 0;
        var data = {"type": "queryProcess2", "body_param": body_param, "ques_answer": ques_answer};
        $.ajax({
            type: 'POST',
            url: url,
            data: data,
            async: false,
            success: function (data) {
                jvalue = data.jvalue;
                svalue = data.svalue;
                vvalue = data.vvalue;
                pvalue = data.pvalue;
            }
        });
        return svalue + ";" + jvalue + ";" + vvalue + ";" + pvalue;
    }
    function getData() {
        var user_id = $("#user_id").val();
        var meas_id = $("#meas_id").val();
        var params = $("#params").val();
        var url = "../api/registerUser.php";
        $.post(url, {"type": "getStyleGenieData", "user_id": user_id}, function (data) {
            var status = data.Status;
            if (status == "Success") {
                $(".loaderdiv").removeClass("loaderhidden");
                $(".loaderdiv").addClass("block");
                var sizeData = data.userData;
                if (sizeData.unit_type == "Inches") {
                    $("#params").prop('selectedIndex',0);
                    $("#collar").html((parseFloat(sizeData.collar)).toFixed(1) + " Inch");
                    $("#chest_front").html((parseFloat(sizeData.chest)) + " Inch");
                    $("#stomach").html((parseFloat(sizeData.stomach)) + " Inch");
                    $("#seat_front").html((parseFloat(sizeData.seat)) + " Inch");
                    $("#bicep_front").html((parseFloat(sizeData.bicep)) + " Inch");
                    $("#shoulder_back").html((parseFloat(sizeData.back_sholuder)) + " Inch");
                    $("#shoulder_front").html((parseFloat(sizeData.front_shoulder)) + " Inch");
                    $("#shoulder_sleeve_right").html((parseFloat(sizeData.sleeve_length_left)) + " Inch");
                    $("#shoulder_sleeve_left").html((parseFloat(sizeData.sleeve_length_right)) + " Inch");
                    $("#thigh").html((parseFloat(sizeData.thigh)) + " Inch");
                    $("#napetowaist_back").html((parseFloat(sizeData.nape_to_waist)) + " Inch");
                    $("#front_waist_length").html((parseFloat(sizeData.front_waist_length)) + " Inch");
                    $("#wrist").html((parseFloat(sizeData.wrist)) + " Inch");
                    $("#waist").html((parseFloat(sizeData.waist)) + " Inch");
                    $("#calf").html((parseFloat(sizeData.calf)) + " Inch");
                    $("#back_waist_height").html((parseFloat(sizeData.back_waist_height)) + " Inch");
                    $("#front_waist_height").html((parseFloat(sizeData.front_waist_height)) + " Inch");
                    $("#knee_right").html((parseFloat(sizeData.knee)) + " Inch");
                    $("#back_jacket_length").html((parseFloat(sizeData.back_jacket_length)) + " Inch");
                    $("#u_rise").html((parseFloat(sizeData.u_rise)) + " Inch");
                    $("#outseam_l_pants").html((parseFloat(sizeData.pant_left_outseam)) + " Inch");
                    $("#outseam_r_pants").html((parseFloat(sizeData.pant_right_outseam)) + " Inch");
                    $("#bottom").html((parseFloat(sizeData.bottom)) + " Inch");
                } else {
                    $("#params").prop('selectedIndex',1);
                    $("#collar").html(parseFloat(sizeData.collar).toFixed(1) + " Cm");
                    $("#chest_front").html(parseFloat(sizeData.chest).toFixed(1) + " Cm");
                    $("#stomach").html(parseFloat(sizeData.stomach).toFixed(1) + " Cm");
                    $("#seat_front").html(parseFloat(sizeData.seat).toFixed(1) + " Cm");
                    $("#bicep_front").html(parseFloat(sizeData.bicep).toFixed(1) + " Cm");
                    $("#shoulder_back").html(parseFloat(sizeData.back_sholuder).toFixed(1) + " Cm");
                    $("#shoulder_front").html(parseFloat(sizeData.front_shoulder).toFixed(1) + " Cm");
                    $("#shoulder_sleeve_right").html(parseFloat(sizeData.sleeve_length_left).toFixed(1) + " Cm");
                    $("#shoulder_sleeve_left").html(parseFloat(sizeData.sleeve_length_right).toFixed(1) + " Cm");
                    $("#thigh").html(parseFloat(sizeData.thigh).toFixed(1) + " Cm");
                    $("#napetowaist_back").html(parseFloat(sizeData.nape_to_waist).toFixed(1) + " Cm");
                    $("#front_waist_length").html(parseFloat(sizeData.front_waist_length).toFixed(1) + " Cm");
                    $("#wrist").html(parseFloat(sizeData.wrist).toFixed(1) + " Cm");
                    $("#waist").html(parseFloat(sizeData.waist).toFixed(1) + " Cm");
                    $("#calf").html(parseFloat(sizeData.calf).toFixed(1) + " Cm");
                    $("#back_waist_height").html(parseFloat(sizeData.back_waist_height).toFixed(1) + " Cm");
                    $("#front_waist_height").html(parseFloat(sizeData.front_waist_height).toFixed(1) + " Cm");
                    $("#knee_right").html(parseFloat(sizeData.knee).toFixed(1) + " Cm");
                    $("#back_jacket_length").html(parseFloat(sizeData.back_jacket_length).toFixed(1) + " Cm");
                    $("#u_rise").html(parseFloat(sizeData.u_rise).toFixed(1) + " Cm");
                    $("#outseam_l_pants").html(parseFloat(sizeData.pant_left_outseam).toFixed(1) + " Cm");
                    $("#outseam_r_pants").html(parseFloat(sizeData.pant_right_outseam).toFixed(1) + " Cm");
                    $("#bottom").html(parseFloat(sizeData.bottom).toFixed(1) + " Cm");
                }
                getMeasureData(sizeData,meas_id);
            } else {
                showMessage(data.Message, 'red');
            }
        }).fail(function () {
            showMessage("Server Error !!! Please Try After Some Time....", 'red');
        });
    }
    function confirmSave() {
        var meas_id = $("#meas_id").val();
        var unit_type = $("#final_shirt_collar").text().split(" ")[1];
        var shirt_collar = $("#final_shirt_collar").text().split(" ")[0];
        var jacket_collar = $("#final_jacket_collar").text().split(" ")[0];
        var vest_collar = $("#final_vest_collar").text().split(" ")[0];
        var pant_collar = $("#final_pant_collar").text().split(" ")[0];
        var shirt_chest = $("#final_shirt_chest").text().split(" ")[0];
        var jacket_chest = $("#final_jacket_chest").text().split(" ")[0];
        var vest_chest = $("#final_vest_chest").text().split(" ")[0];
        var pant_chest = $("#final_pant_chest").text().split(" ")[0];
        var shirt_stomach = $("#final_shirt_stomach").text().split(" ")[0];
        var jacket_stomach = $("#final_jacket_stomach").text().split(" ")[0];
        var vest_stomach = $("#final_vest_stomach").text().split(" ")[0];
        var pant_stomach = $("#final_pant_stomach").text().split(" ")[0];
        var shirt_seat = $("#final_shirt_seat").text().split(" ")[0];
        var jacket_seat = $("#final_jacket_seat").text().split(" ")[0];
        var vest_seat = $("#final_vest_seat").text().split(" ")[0];
        var pant_seat = $("#final_pant_seat").text().split(" ")[0];
        var shirt_bicep = $("#final_shirt_bicep").text().split(" ")[0];
        var jacket_bicep = $("#final_jacket_bicep").text().split(" ")[0];
        var vest_bicep = $("#final_vest_bicep").text().split(" ")[0];
        var pant_bicep = $("#final_pant_bicep").text().split(" ")[0];
        var shirt_back_shoulder = $("#final_shirt_back_shoulder").text().split(" ")[0];
        var jacket_back_shoulder = $("#final_jacket_back_shoulder").text().split(" ")[0];
        var vest_back_shoulder = $("#final_vest_back_shoulder").text().split(" ")[0];
        var pant_back_shoulder = $("#final_pant_back_shoulder").text().split(" ")[0];
        var shirt_front_shoulder = $("#final_shirt_front_shoulder").text().split(" ")[0];
        var jacket_front_shoulder = $("#final_jacket_front_shoulder").text().split(" ")[0];
        var vest_front_shoulder = $("#final_vest_front_shoulder").text().split(" ")[0];
        var pant_front_shoulder = $("#final_pant_front_shoulder").text().split(" ")[0];
        var shirt_sleeve_left = $("#final_shirt_sleeve_left").text().split(" ")[0];
        var jacket_sleeve_left = $("#final_jacket_sleeve_left").text().split(" ")[0];
        var vest_sleeve_left = $("#final_vest_sleeve_left").text().split(" ")[0];
        var pant_sleeve_left = $("#final_pant_sleeve_left").text().split(" ")[0];
        var shirt_sleeve_right = $("#final_shirt_sleeve_right").text().split(" ")[0];
        var jacket_sleeve_right = $("#final_jacket_sleeve_right").text().split(" ")[0];
        var vest_sleeve_right = $("#final_vest_sleeve_right").text().split(" ")[0];
        var pant_sleeve_right = $("#final_pant_sleeve_right").text().split(" ")[0];
        var shirt_thigh = $("#final_shirt_thigh").text().split(" ")[0];
        var jacket_thigh = $("#final_jacket_thigh").text().split(" ")[0];
        var vest_thigh = $("#final_vest_thigh").text().split(" ")[0];
        var pant_thigh = $("#final_pant_thigh").text().split(" ")[0];
        var shirt_nape_to_waist = $("#final_shirt_nape").text().split(" ")[0];
        var jacket_nape_to_waist = $("#final_jacket_nape").text().split(" ")[0];
        var vest_nape_to_waist = $("#final_vest_nape").text().split(" ")[0];
        var pant_nape_to_waist = $("#final_pant_nape").text().split(" ")[0];
        var shirt_front_waist_length = $("#final_shirt_front_waist").text().split(" ")[0];
        var jacket_front_waist_length = $("#final_jacket_front_waist").text().split(" ")[0];
        var vest_front_waist_length = $("#final_vest_front_waist").text().split(" ")[0];
        var pant_front_waist_length = $("#final_pant_front_waist").text().split(" ")[0];
        var shirt_wrist = $("#final_shirt_wrist").text().split(" ")[0];
        var jacket_wrist = $("#final_jacket_wrist").text().split(" ")[0];
        var vest_wrist = $("#final_vest_wrist").text().split(" ")[0];
        var pant_wrist = $("#final_pant_wrist").text().split(" ")[0];
        var shirt_waist = $("#final_shirt_waist").text().split(" ")[0];
        var jacket_waist = $("#final_jacket_waist").text().split(" ")[0];
        var vest_waist = $("#final_vest_waist").text().split(" ")[0];
        var pant_waist = $("#final_pant_waist").text().split(" ")[0];
        var shirt_calf = $("#final_shirt_calf").text().split(" ")[0];
        var jacket_calf = $("#final_jacket_calf").text().split(" ")[0];
        var vest_calf = $("#final_vest_calf").text().split(" ")[0];
        var pant_calf = $("#final_pant_calf").text().split(" ")[0];
        var shirt_back_waist_height = $("#final_shirt_b_waist").text().split(" ")[0];
        var jacket_back_waist_height = $("#final_jacket_b_waist").text().split(" ")[0];
        var vest_back_waist_height = $("#final_vest_b_waist").text().split(" ")[0];
        var pant_back_waist_height = $("#final_pant_b_waist").text().split(" ")[0];
        var shirt_front_waist_height = $("#final_shirt_f_waist").text().split(" ")[0];
        var jacket_front_waist_height = $("#final_jacket_f_waist").text().split(" ")[0];
        var vest_front_waist_height = $("#final_vest_f_waist").text().split(" ")[0];
        var pant_front_waist_height = $("#final_pant_f_waist").text().split(" ")[0];
        var shirt_knee = $("#final_shirt_knee").text().split(" ")[0];
        var jacket_knee = $("#final_jacket_knee").text().split(" ")[0];
        var vest_knee = $("#final_vest_knee").text().split(" ")[0];
        var pant_knee = $("#final_pant_knee").text().split(" ")[0];
        var shirt_back_jacket_length = $("#final_shirt_jacket").text().split(" ")[0];
        var jacket_back_jacket_length = $("#final_jacket_jacket").text().split(" ")[0];
        var vest_back_jacket_length = $("#final_vest_jacket").text().split(" ")[0];
        var pant_back_jacket_length = $("#final_pant_jacket").text().split(" ")[0];
        var shirt_u_rise = $("#final_shirt_rise").text().split(" ")[0];
        var jacket_u_rise = $("#final_jacket_rise").text().split(" ")[0];
        var vest_u_rise = $("#final_vest_rise").text().split(" ")[0];
        var pant_u_rise = $("#final_pant_rise").text().split(" ")[0];
        var shirt_pant_left_outseam = $("#final_shirt_p_left").text().split(" ")[0];
        var jacket_pant_left_outseam = $("#final_jacket_p_left").text().split(" ")[0];
        var vest_pant_left_outseam = $("#final_vest_p_left").text().split(" ")[0];
        var pant_pant_left_outseam = $("#final_pant_p_left").text().split(" ")[0];
        var shirt_pant_right_outseam = $("#final_shirt_p_right").text().split(" ")[0];
        var jacket_pant_right_outseam = $("#final_jacket_p_right").text().split(" ")[0];
        var vest_pant_right_outseam = $("#final_vest_p_right").text().split(" ")[0];
        var pant_pant_right_outseam = $("#final_pant_p_right").text().split(" ")[0];
        var shirt_bottom = $("#final_shirt_bottom").text().split(" ")[0];
        var jacket_bottom = $("#final_jacket_bottom").text().split(" ")[0];
        var vest_bottom = $("#final_vest_bottom").text().split(" ")[0];
        var pant_bottom = $("#final_pant_bottom").text().split(" ")[0];
        var url = '../api/registerUser.php';
        $.post(url,
            {
                'type': 'updateFinalMeasurement',
                'shirt_collar': shirt_collar,
                'jacket_collar': jacket_collar,
                'vest_collar': vest_collar,
                'pant_collar': pant_collar,
                'shirt_chest': shirt_chest,
                'jacket_chest': jacket_chest,
                'vest_chest': vest_chest,
                'pant_chest': pant_chest,
                'shirt_stomach': shirt_stomach,
                'jacket_stomach': jacket_stomach,
                'vest_stomach': vest_stomach,
                'pant_stomach': pant_stomach,
                'shirt_seat': shirt_seat,
                'jacket_seat': jacket_seat,
                'vest_seat': vest_seat,
                'pant_seat': pant_seat,
                'shirt_bicep': shirt_bicep,
                'jacket_bicep': jacket_bicep,
                'vest_bicep': vest_bicep,
                'pant_bicep': pant_bicep,
                'shirt_back_shoulder': shirt_back_shoulder,
                'jacket_back_shoulder': jacket_back_shoulder,
                'vest_back_shoulder': vest_back_shoulder,
                'pant_back_shoulder': pant_back_shoulder,
                'shirt_front_shoulder': shirt_front_shoulder,
                'jacket_front_shoulder': jacket_front_shoulder,
                'vest_front_shoulder': vest_front_shoulder,
                'pant_front_shoulder': pant_front_shoulder,
                'shirt_sleeve_left': shirt_sleeve_left,
                'jacket_sleeve_left': jacket_sleeve_left,
                'vest_sleeve_left': vest_sleeve_left,
                'pant_sleeve_left': pant_sleeve_left,
                'shirt_sleeve_right': shirt_sleeve_right,
                'jacket_sleeve_right': jacket_sleeve_right,
                'vest_sleeve_right': vest_sleeve_right,
                'pant_sleeve_right': pant_sleeve_right,
                'shirt_thigh': shirt_thigh,
                'jacket_thigh': jacket_thigh,
                'vest_thigh': vest_thigh,
                'pant_thigh': pant_thigh,
                'shirt_nape_to_waist': shirt_nape_to_waist,
                'jacket_nape_to_waist': jacket_nape_to_waist,
                'vest_nape_to_waist': vest_nape_to_waist,
                'pant_nape_to_waist': pant_nape_to_waist,
                'shirt_front_waist_length': shirt_front_waist_length,
                'jacket_front_waist_length': jacket_front_waist_length,
                'vest_front_waist_length': vest_front_waist_length,
                'pant_front_waist_length': pant_front_waist_length,
                'shirt_wrist': shirt_wrist,
                'jacket_wrist': jacket_wrist,
                'vest_wrist': vest_wrist,
                'pant_wrist': pant_wrist,
                'shirt_waist': shirt_waist,
                'jacket_waist': jacket_waist,
                'vest_waist': vest_waist,
                'pant_waist': pant_waist,
                'shirt_calf': shirt_calf,
                'jacket_calf': jacket_calf,
                'vest_calf': vest_calf,
                'pant_calf': pant_calf,
                'shirt_back_waist_height': shirt_back_waist_height,
                'jacket_back_waist_height': jacket_back_waist_height,
                'vest_back_waist_height': vest_back_waist_height,
                'pant_back_waist_height': pant_back_waist_height,
                'shirt_front_waist_height': shirt_front_waist_height,
                'jacket_front_waist_height': jacket_front_waist_height,
                'vest_front_waist_height': vest_front_waist_height,
                'pant_front_waist_height': pant_front_waist_height,
                'shirt_knee': shirt_knee,
                'jacket_knee': jacket_knee,
                'vest_knee': vest_knee,
                'pant_knee': pant_knee,
                'shirt_back_jacket_length': shirt_back_jacket_length,
                'jacket_back_jacket_length': jacket_back_jacket_length,
                'vest_back_jacket_length': vest_back_jacket_length,
                'pant_back_jacket_length': pant_back_jacket_length,
                'shirt_u_rise': shirt_u_rise,
                'jacket_u_rise': jacket_u_rise,
                'vest_u_rise': vest_u_rise,
                'pant_u_rise': pant_u_rise,
                'shirt_pant_left_outseam': shirt_pant_left_outseam,
                'jacket_pant_left_outseam': jacket_pant_left_outseam,
                'vest_pant_left_outseam': vest_pant_left_outseam,
                'pant_pant_left_outseam': pant_pant_left_outseam,
                'shirt_pant_right_outseam': shirt_pant_right_outseam,
                'jacket_pant_right_outseam': jacket_pant_right_outseam,
                'vest_pant_right_outseam': vest_pant_right_outseam,
                'pant_pant_right_outseam': pant_pant_right_outseam,
                'shirt_bottom': shirt_bottom,
                'jacket_bottom': jacket_bottom,
                'vest_bottom': vest_bottom,
                'pant_bottom': pant_bottom,
                'meas_id': meas_id,
                'unit_type': unit_type
            },
        function (data) {
            var Status = data.Status;
            if (Status == "Success") {
                showMessage(data.Message, 'green');
            } else {
                showMessage(data.Message, 'red');
                return false;
            }
        }).fail(function () {
            showMessage("Server Error !!! Please Try After Some Time...", 'red');
            return false;
        });
    }
    function convertData() {
        $(".loaderdiv").removeClass("loaderhidden");
        $(".loaderdiv").addClass("block");
        var unit_type = $("#final_shirt_collar").text().split(" ")[1];
        //old data of collar
        var collar = $("#collar").text().split(" ")[0];
        var collar_shirt_allow = $("#collar_shirt_allow").text().split(" ")[0];
        var collar_jacket_allow = $("#collar_jacket_allow").text().split(" ")[0];
        var collar_vest_allow = $("#collar_vest_allow").text().split(" ")[0];
        var collar_pant_allow = $("#collar_pant_allow").text().split(" ")[0];
        var final_shirt_collar = $("#final_shirt_collar").text().split(" ")[0];
        var final_jacket_collar = $("#final_jacket_collar").text().split(" ")[0];
        var final_vest_collar = $("#final_vest_collar").text().split(" ")[0];
        var final_pant_collar = $("#final_pant_collar").text().split(" ")[0];
        //old data of chest
        var chest_front = $("#chest_front").text().split(" ")[0];
        var chest_shirt_allow = $("#chest_shirt_allow").text().split(" ")[0];
        var chest_jacket_allow = $("#chest_jacket_allow").text().split(" ")[0];
        var chest_vest_allow = $("#chest_vest_allow").text().split(" ")[0];
        var chest_pant_allow = $("#chest_pant_allow").text().split(" ")[0];
        var final_shirt_chest = $("#final_shirt_chest").text().split(" ")[0];
        var final_jacket_chest = $("#final_jacket_chest").text().split(" ")[0];
        var final_vest_chest = $("#final_vest_chest").text().split(" ")[0];
        var final_pant_chest = $("#final_pant_chest").text().split(" ")[0];
        //old data of stomach
        var stomach = $("#stomach").text().split(" ")[0];
        var stomach_shirt_allow = $("#stomach_shirt_allow").text().split(" ")[0];
        var stomach_jacket_allow = $("#stomach_jacket_allow").text().split(" ")[0];
        var stomach_vest_allow = $("#stomach_vest_allow").text().split(" ")[0];
        var stomach_pant_allow = $("#stomach_pant_allow").text().split(" ")[0];
        var final_shirt_stomach = $("#final_shirt_stomach").text().split(" ")[0];
        var final_jacket_stomach = $("#final_jacket_stomach").text().split(" ")[0];
        var final_vest_stomach = $("#final_vest_stomach").text().split(" ")[0];
        var final_pant_stomach = $("#final_pant_stomach").text().split(" ")[0];
        //old data of seat
        var seat_front = $("#seat_front").text().split(" ")[0];
        var seat_shirt_allow = $("#seat_shirt_allow").text().split(" ")[0];
        var seat_jacket_allow = $("#seat_jacket_allow").text().split(" ")[0];
        var seat_vest_allow = $("#seat_vest_allow").text().split(" ")[0];
        var seat_pant_allow = $("#seat_pant_allow").text().split(" ")[0];
        var final_shirt_seat = $("#final_shirt_seat").text().split(" ")[0];
        var final_vest_seat = $("#final_jacket_seat").text().split(" ")[0];
        var final_jacket_seat = $("#final_vest_seat").text().split(" ")[0];
        var final_pant_seat = $("#final_pant_seat").text().split(" ")[0];
        //old data of bicep
        var bicep_front = $("#bicep_front").text().split(" ")[0];
        var bicep_shirt_allow = $("#bicep_shirt_allow").text().split(" ")[0];
        var bicep_jacket_allow = $("#bicep_jacket_allow").text().split(" ")[0];
        var bicep_vest_allow = $("#bicep_vest_allow").text().split(" ")[0];
        var bicep_pant_allow = $("#bicep_pant_allow").text().split(" ")[0];
        var final_shirt_bicep = $("#final_shirt_bicep").text().split(" ")[0];
        var final_jacket_bicep = $("#final_jacket_bicep").text().split(" ")[0];
        var final_vest_bicep = $("#final_vest_bicep").text().split(" ")[0];
        var final_pant_bicep = $("#final_pant_bicep").text().split(" ")[0];
        //old data of shoulder_back
        var shoulder_back = $("#shoulder_back").text().split(" ")[0];
        var back_shoulder_shirt_allow = $("#back_shoulder_shirt_allow").text().split(" ")[0];
        var back_shoulder_jacket_allow = $("#back_shoulder_jacket_allow").text().split(" ")[0];
        var back_shoulder_vest_allow = $("#back_shoulder_vest_allow").text().split(" ")[0];
        var back_shoulder_pant_allow = $("#back_shoulder_pant_allow").text().split(" ")[0];
        var final_shirt_back_shoulder = $("#final_shirt_back_shoulder").text().split(" ")[0];
        var final_jacket_back_shoulder = $("#final_jacket_back_shoulder").text().split(" ")[0];
        var final_vest_back_shoulder = $("#final_vest_back_shoulder").text().split(" ")[0];
        var final_pant_back_shoulder = $("#final_pant_back_shoulder").text().split(" ")[0];
        //old data of shoulder_front
        var shoulder_front = $("#shoulder_front").text().split(" ")[0];
        var front_shoulder_shirt_allow = $("#front_shoulder_shirt_allow").text().split(" ")[0];
        var front_shoulder_jacket_allow = $("#front_shoulder_jacket_allow").text().split(" ")[0];
        var front_shoulder_vest_allow = $("#front_shoulder_vest_allow").text().split(" ")[0];
        var front_shoulder_pant_allow = $("#front_shoulder_pant_allow").text().split(" ")[0];
        var final_shirt_front_shoulder = $("#final_shirt_front_shoulder").text().split(" ")[0];
        var final_jacket_front_shoulder = $("#final_jacket_front_shoulder").text().split(" ")[0];
        var final_vest_front_shoulder = $("#final_vest_front_shoulder").text().split(" ")[0];
        var final_pant_front_shoulder = $("#final_pant_front_shoulder").text().split(" ")[0];
        //old data of shoulder_sleeve_right
        var shoulder_sleeve_right = $("#shoulder_sleeve_right").text().split(" ")[0];
        var sleeve_right_shirt_allow = $("#sleeve_right_shirt_allow").text().split(" ")[0];
        var sleeve_right_jacket_allow = $("#sleeve_right_jacket_allow").text().split(" ")[0];
        var sleeve_right_vest_allow = $("#sleeve_right_vest_allow").text().split(" ")[0];
        var sleeve_right_pant_allow = $("#sleeve_right_pant_allow").text().split(" ")[0];
        var final_shirt_sleeve_right = $("#final_shirt_sleeve_right").text().split(" ")[0];
        var final_jacket_sleeve_right = $("#final_jacket_sleeve_right").text().split(" ")[0];
        var final_vest_sleeve_right = $("#final_vest_sleeve_right").text().split(" ")[0];
        var final_pant_sleeve_right = $("#final_pant_sleeve_right").text().split(" ")[0];
        //old data of shoulder_sleeve_left
        var shoulder_sleeve_left = $("#shoulder_sleeve_left").text().split(" ")[0];
        var sleeve_left_shirt_allow = $("#sleeve_left_shirt_allow").text().split(" ")[0];
        var sleeve_left_jacket_allow = $("#sleeve_left_jacket_allow").text().split(" ")[0];
        var sleeve_left_vest_allow = $("#sleeve_left_vest_allow").text().split(" ")[0];
        var sleeve_left_pant_allow = $("#sleeve_left_pant_allow").text().split(" ")[0];
        var final_shirt_sleeve_left = $("#final_shirt_sleeve_left").text().split(" ")[0];
        var final_jacket_sleeve_left = $("#final_jacket_sleeve_left").text().split(" ")[0];
        var final_vest_sleeve_left = $("#final_vest_sleeve_left").text().split(" ")[0];
        var final_pant_sleeve_left = $("#final_pant_sleeve_left").text().split(" ")[0];
        //old data of thigh
        var thigh = $("#thigh").text().split(" ")[0];
        var thigh_shirt_allow = $("#thigh_shirt_allow").text().split(" ")[0];
        var thigh_jacket_allow = $("#thigh_jacket_allow").text().split(" ")[0];
        var thigh_vest_allow = $("#thigh_vest_allow").text().split(" ")[0];
        var thigh_pant_allow = $("#thigh_pant_allow").text().split(" ")[0];
        var final_shirt_thigh = $("#final_shirt_thigh").text().split(" ")[0];
        var final_jacket_thigh = $("#final_jacket_thigh").text().split(" ")[0];
        var final_vest_thigh = $("#final_vest_thigh").text().split(" ")[0];
        var final_pant_thigh = $("#final_pant_thigh").text().split(" ")[0];
        //old data of napetowaist_back
        var napetowaist_back = $("#napetowaist_back").text().split(" ")[0];
        var nape_shirt_allow = $("#nape_shirt_allow").text().split(" ")[0];
        var nape_jacket_allow = $("#nape_jacket_allow").text().split(" ")[0];
        var nape_vest_allow = $("#nape_vest_allow").text().split(" ")[0];
        var nape_pant_allow = $("#nape_pant_allow").text().split(" ")[0];
        var final_shirt_nape = $("#final_shirt_nape").text().split(" ")[0];
        var final_jacket_nape = $("#final_jacket_nape").text().split(" ")[0];
        var final_vest_nape = $("#final_vest_nape").text().split(" ")[0];
        var final_pant_nape = $("#final_pant_nape").text().split(" ")[0];
        //old data of front_waist_length
        var front_waist_length = $("#front_waist_length").text().split(" ")[0];
        var front_waist_shirt_allow = $("#front_waist_shirt_allow").text().split(" ")[0];
        var front_waist_jacket_allow = $("#front_waist_jacket_allow").text().split(" ")[0];
        var front_waist_vest_allow = $("#front_waist_vest_allow").text().split(" ")[0];
        var front_waist_pant_allow = $("#front_waist_pant_allow").text().split(" ")[0];
        var final_shirt_front_waist = $("#final_shirt_front_waist").text().split(" ")[0];
        var final_jacket_front_waist = $("#final_jacket_front_waist").text().split(" ")[0];
        var final_vest_front_waist = $("#final_vest_front_waist").text().split(" ")[0];
        var final_pant_front_waist = $("#final_pant_front_waist").text().split(" ")[0];
        //old data of wrist
        var wrist = $("#wrist").text().split(" ")[0];
        var wrist_shirt_allow = $("#wrist_shirt_allow").text().split(" ")[0];
        var wrist_jacket_allow = $("#wrist_jacket_allow").text().split(" ")[0];
        var wrist_vest_allow = $("#wrist_vest_allow").text().split(" ")[0];
        var wrist_pant_allow = $("#wrist_pant_allow").text().split(" ")[0];
        var final_shirt_wrist = $("#final_shirt_wrist").text().split(" ")[0];
        var final_jacket_wrist = $("#final_jacket_wrist").text().split(" ")[0];
        var final_vest_wrist = $("#final_vest_wrist").text().split(" ")[0];
        var final_pant_wrist = $("#final_pant_wrist").text().split(" ")[0];
        //old data of waist
        var waist = $("#waist").text().split(" ")[0];
        var waist_shirt_allow = $("#waist_shirt_allow").text().split(" ")[0];
        var waist_jacket_allow = $("#waist_jacket_allow").text().split(" ")[0];
        var waist_vest_allow = $("#waist_vest_allow").text().split(" ")[0];
        var waist_pant_allow = $("#waist_pant_allow").text().split(" ")[0];
        var final_shirt_waist = $("#final_shirt_waist").text().split(" ")[0];
        var final_jacket_waist = $("#final_jacket_waist").text().split(" ")[0];
        var final_vest_waist = $("#final_vest_waist").text().split(" ")[0];
        var final_pant_waist = $("#final_pant_waist").text().split(" ")[0];
        //old data of calf
        var calf = $("#calf").text().split(" ")[0];
        var calf_shirt_allow = $("#calf_shirt_allow").text().split(" ")[0];
        var calf_jacket_allow = $("#calf_jacket_allow").text().split(" ")[0];
        var calf_vest_allow = $("#calf_vest_allow").text().split(" ")[0];
        var calf_pant_allow = $("#calf_pant_allow").text().split(" ")[0];
        var final_shirt_calf = $("#final_shirt_calf").text().split(" ")[0];
        var final_jacket_calf = $("#final_jacket_calf").text().split(" ")[0];
        var final_vest_calf = $("#final_vest_calf").text().split(" ")[0];
        var final_pant_calf = $("#final_pant_calf").text().split(" ")[0];
        //old data of back_waist_height
        var back_waist_height = $("#back_waist_height").text().split(" ")[0];
        var b_waist_shirt_allow = $("#b_waist_shirt_allow").text().split(" ")[0];
        var b_waist_jacket_allow = $("#b_waist_jacket_allow").text().split(" ")[0];
        var b_waist_vest_allow = $("#b_waist_vest_allow").text().split(" ")[0];
        var b_waist_pant_allow = $("#b_waist_pant_allow").text().split(" ")[0];
        var final_shirt_b_waist = $("#final_shirt_b_waist").text().split(" ")[0];
        var final_jacket_b_waist = $("#final_jacket_b_waist").text().split(" ")[0];
        var final_vest_b_waist = $("#final_vest_b_waist").text().split(" ")[0];
        var final_pant_b_waist = $("#final_pant_b_waist").text().split(" ")[0];
        //old data of front_waist_height
        var front_waist_height = $("#front_waist_height").text().split(" ")[0];
        var f_waist_shirt_allow = $("#f_waist_shirt_allow").text().split(" ")[0];
        var f_waist_jacket_allow = $("#f_waist_jacket_allow").text().split(" ")[0];
        var f_waist_vest_allow = $("#f_waist_vest_allow").text().split(" ")[0];
        var f_waist_pant_allow = $("#f_waist_pant_allow").text().split(" ")[0];
        var final_shirt_f_waist = $("#final_shirt_f_waist").text().split(" ")[0];
        var final_jacket_f_waist = $("#final_jacket_f_waist").text().split(" ")[0];
        var final_vest_f_waist = $("#final_vest_f_waist").text().split(" ")[0];
        var final_pant_f_waist = $("#final_pant_f_waist").text().split(" ")[0];
        //old data of knee_right
        var knee_right = $("#knee_right").text().split(" ")[0];
        var knee_shirt_allow = $("#knee_shirt_allow").text().split(" ")[0];
        var knee_jacket_allow = $("#knee_jacket_allow").text().split(" ")[0];
        var knee_vest_allow = $("#knee_vest_allow").text().split(" ")[0];
        var knee_pant_allow = $("#knee_pant_allow").text().split(" ")[0];
        var final_shirt_knee = $("#final_shirt_knee").text().split(" ")[0];
        var final_jacket_knee = $("#final_jacket_knee").text().split(" ")[0];
        var final_vest_knee = $("#final_vest_knee").text().split(" ")[0];
        var final_pant_knee = $("#final_pant_knee").text().split(" ")[0];
        //old data of back_jacket_length
        var back_jacket_length = $("#back_jacket_length").text().split(" ")[0];
        var jacket_shirt_allow = $("#jacket_shirt_allow").text().split(" ")[0];
        var jacket_jacket_allow = $("#jacket_jacket_allow").text().split(" ")[0];
        var jacket_vest_allow = $("#jacket_vest_allow").text().split(" ")[0];
        var jacket_pant_allow = $("#jacket_pant_allow").text().split(" ")[0];
        var final_shirt_jacket = $("#final_shirt_jacket").text().split(" ")[0];
        var final_jacket_jacket = $("#final_jacket_jacket").text().split(" ")[0];
        var final_vest_jacket = $("#final_vest_jacket").text().split(" ")[0];
        var final_pant_jacket = $("#final_pant_jacket").text().split(" ")[0];
        //old data of u_rise
        var u_rise = $("#u_rise").text().split(" ")[0];
        var rise_shirt_allow = $("#rise_shirt_allow").text().split(" ")[0];
        var rise_jacket_allow = $("#rise_jacket_allow").text().split(" ")[0];
        var rise_vest_allow = $("#rise_vest_allow").text().split(" ")[0];
        var rise_pant_allow = $("#rise_pant_allow").text().split(" ")[0];
        var final_shirt_rise = $("#final_shirt_rise").text().split(" ")[0];
        var final_jacket_rise = $("#final_jacket_rise").text().split(" ")[0];
        var final_vest_rise = $("#final_vest_rise").text().split(" ")[0];
        var final_pant_rise = $("#final_pant_rise").text().split(" ")[0];
        //old data of outseam_l_pants
        var outseam_l_pants = $("#outseam_l_pants").text().split(" ")[0];
        var p_left_shirt_allow = $("#p_left_shirt_allow").text().split(" ")[0];
        var p_left_jacket_allow = $("#p_left_jacket_allow").text().split(" ")[0];
        var p_left_vest_allow = $("#p_left_vest_allow").text().split(" ")[0];
        var p_left_pant_allow = $("#p_left_pant_allow").text().split(" ")[0];
        var final_shirt_p_left = $("#final_shirt_p_left").text().split(" ")[0];
        var final_jacket_p_left = $("#final_jacket_p_left").text().split(" ")[0];
        var final_vest_p_left = $("#final_vest_p_left").text().split(" ")[0];
        var final_pant_p_left = $("#final_pant_p_left").text().split(" ")[0];
        //old data of outseam_r_pants
        var outseam_r_pants = $("#outseam_r_pants").text().split(" ")[0];
        var p_right_shirt_allow = $("#p_right_shirt_allow").text().split(" ")[0];
        var p_right_jacket_allow = $("#p_right_jacket_allow").text().split(" ")[0];
        var p_right_vest_allow = $("#p_right_vest_allow").text().split(" ")[0];
        var p_right_pant_allow = $("#p_right_pant_allow").text().split(" ")[0];
        var final_shirt_p_right = $("#final_shirt_p_right").text().split(" ")[0];
        var final_jacket_p_right = $("#final_jacket_p_right").text().split(" ")[0];
        var final_vest_p_right = $("#final_vest_p_right").text().split(" ")[0];
        var final_pant_p_right = $("#final_pant_p_right").text().split(" ")[0];
        //old data of bottom
        var bottom = $("#bottom").text().split(" ")[0];
        var bottom_shirt_allow = $("#bottom_shirt_allow").text().split(" ")[0];
        var bottom_jacket_allow = $("#bottom_jacket_allow").text().split(" ")[0];
        var bottom_vest_allow = $("#bottom_vest_allow").text().split(" ")[0];
        var bottom_pant_allow = $("#bottom_pant_allow").text().split(" ")[0];
        var final_shirt_bottom = $("#final_shirt_bottom").text().split(" ")[0];
        var final_jacket_bottom = $("#final_jacket_bottom").text().split(" ")[0];
        var final_vest_bottom = $("#final_vest_bottom").text().split(" ")[0];
        var final_pant_bottom = $("#final_pant_bottom").text().split(" ")[0];
        if (unit_type == "Inch") {
            //show in centimeter means conversion required from inches to centimeter
            //for collar
            $("#collar").text(parseFloat(collar * 2.54).toFixed(1) + " Cm");
            $("#collar_shirt_allow").text(collarshirt+ " Cm");
            $("#collar_jacket_allow").text(collarjacket+ " Cm");
            $("#collar_vest_allow").text(collarvest+ " Cm");
            $("#collar_pant_allow").text(collarpant+ " Cm");
            $("#final_shirt_collar").text(parseFloat(final_shirt_collar * 2.54).toFixed(1) + " Cm");
            $("#final_jacket_collar").text(parseFloat(final_jacket_collar * 2.54).toFixed(1) + " Cm");
            $("#final_vest_collar").text(parseFloat(final_vest_collar * 2.54).toFixed(1) + " Cm");
            $("#final_pant_collar").text(parseFloat(final_pant_collar * 2.54).toFixed(1) + " Cm");
            //for chest_front
            $("#chest_front").text(parseFloat(chest_front * 2.54).toFixed(1) + " Cm");
            $("#chest_shirt_allow").text(chestshirt+ " Cm");
            $("#chest_jacket_allow").text(chestjacket+ " Cm");
            $("#chest_vest_allow").text(chestvest+ " Cm");
            $("#chest_pant_allow").text(chestpant+ " Cm");
            $("#final_shirt_chest").text(parseFloat(final_shirt_chest * 2.54).toFixed(1) + " Cm");
            $("#final_jacket_chest").text(parseFloat(final_jacket_chest * 2.54).toFixed(1) + " Cm");
            $("#final_vest_chest").text(parseFloat(final_vest_chest * 2.54).toFixed(1) + " Cm");
            $("#final_pant_chest").text(parseFloat(final_pant_chest * 2.54).toFixed(1) + " Cm");
            //for stomach
            $("#stomach").text(parseFloat(stomach * 2.54).toFixed(1) + " Cm");
            $("#stomach_shirt_allow").text(stomachshirt+ " Cm");
            $("#stomach_jacket_allow").text(stomachjacket+ " Cm");
            $("#stomach_vest_allow").text(stomachvest+ " Cm");
            $("#stomach_pant_allow").text(stomachpant+ " Cm");
            $("#final_shirt_stomach").text(parseFloat(final_shirt_stomach * 2.54).toFixed(1) + " Cm");
            $("#final_jacket_stomach").text(parseFloat(final_jacket_stomach * 2.54).toFixed(1) + " Cm");
            $("#final_vest_stomach").text(parseFloat(final_vest_stomach * 2.54).toFixed(1) + " Cm");
            $("#final_pant_stomach").text(parseFloat(final_pant_stomach * 2.54).toFixed(1) + " Cm");
            //for seat_front
            $("#seat_front").text(parseFloat(seat_front * 2.54).toFixed(1) + " Cm");
            $("#seat_shirt_allow").text(seatshirt+ " Cm");
            $("#seat_jacket_allow").text(seatjacket+ " Cm");
            $("#seat_vest_allow").text(seatvest+ " Cm");
            $("#seat_pant_allow").text(seatpant+ " Cm");
            $("#final_shirt_seat").text(parseFloat(final_shirt_seat * 2.54).toFixed(1) + " Cm");
            $("#final_jacket_seat").text(parseFloat(final_jacket_seat * 2.54).toFixed(1) + " Cm");
            $("#final_vest_seat").text(parseFloat(final_vest_seat * 2.54).toFixed(1) + " Cm");
            $("#final_pant_seat").text(parseFloat(final_pant_seat * 2.54).toFixed(1) + " Cm");
            //for bicep_front
            $("#bicep_front").text(parseFloat(bicep_front * 2.54).toFixed(1) + " Cm");
            $("#bicep_shirt_allow").text(bicepshirt+ " Cm");
            $("#bicep_jacket_allow").text(bicepjacket+ " Cm");
            $("#bicep_vest_allow").text(bicepvest+ " Cm");
            $("#bicep_pant_allow").text(biceppant+ " Cm");
            $("#final_shirt_bicep").text(parseFloat(final_shirt_bicep * 2.54).toFixed(1) + " Cm");
            $("#final_jacket_bicep").text(parseFloat(final_jacket_bicep * 2.54).toFixed(1) + " Cm");
            $("#final_vest_bicep").text(parseFloat(final_vest_bicep * 2.54).toFixed(1) + " Cm");
            $("#final_pant_bicep").text(parseFloat(final_pant_bicep * 2.54).toFixed(1) + " Cm");
            //for shoulder_back
            $("#shoulder_back").text(parseFloat(shoulder_back * 2.54).toFixed(1) + " Cm");
            $("#back_shoulder_shirt_allow").text(backshouldershirt+ " Cm");
            $("#back_shoulder_jacket_allow").text(backshoulderjacket+ " Cm");
            $("#back_shoulder_vest_allow").text(backshouldervest+ " Cm");
            $("#back_shoulder_pant_allow").text(backshoulderpant+ " Cm");
            $("#final_shirt_back_shoulder").text(parseFloat(final_shirt_back_shoulder * 2.54).toFixed(1) + " Cm");
            $("#final_jacket_back_shoulder").text(parseFloat(final_jacket_back_shoulder * 2.54).toFixed(1) + " Cm");
            $("#final_vest_back_shoulder").text(parseFloat(final_vest_back_shoulder * 2.54).toFixed(1) + " Cm");
            $("#final_pant_back_shoulder").text(parseFloat(final_pant_back_shoulder * 2.54).toFixed(1) + " Cm");
            //for shoulder_front
            $("#shoulder_front").text(parseFloat(shoulder_front * 2.54).toFixed(1) + " Cm");
            $("#front_shoulder_shirt_allow").text(frontshouldershirt+ " Cm");
            $("#front_shoulder_jacket_allow").text(frontshoulderjacket+ " Cm");
            $("#front_shoulder_vest_allow").text(frontshouldervest+ " Cm");
            $("#front_shoulder_pant_allow").text(frontshoulderpant+ " Cm");
            $("#final_shirt_front_shoulder").text(parseFloat(final_shirt_front_shoulder * 2.54).toFixed(1) + " Cm");
            $("#final_jacket_front_shoulder").text(parseFloat(final_jacket_front_shoulder * 2.54).toFixed(1) + " Cm");
            $("#final_vest_front_shoulder").text(parseFloat(final_vest_front_shoulder * 2.54).toFixed(1) + " Cm");
            $("#final_pant_front_shoulder").text(parseFloat(final_pant_front_shoulder * 2.54).toFixed(1) + " Cm");
            //for shoulder_sleeve_right
            $("#shoulder_sleeve_right").text(parseFloat(shoulder_sleeve_right * 2.54).toFixed(1) + " Cm");
            $("#sleeve_right_shirt_allow").text(sleeverightshirt+ " Cm");
            $("#sleeve_right_jacket_allow").text(sleeverightjacket+ " Cm");
            $("#sleeve_right_vest_allow").text(sleeverightvest+ " Cm");
            $("#sleeve_right_pant_allow").text(sleeverightpant+ " Cm");
            $("#final_shirt_sleeve_right").text(parseFloat(final_shirt_sleeve_right * 2.54).toFixed(1) + " Cm");
            $("#final_jacket_sleeve_right").text(parseFloat(final_jacket_sleeve_right * 2.54).toFixed(1) + " Cm");
            $("#final_vest_sleeve_right").text(parseFloat(final_vest_sleeve_right * 2.54).toFixed(1) + " Cm");
            $("#final_pant_sleeve_right").text(parseFloat(final_pant_sleeve_right * 2.54).toFixed(1) + " Cm");
            //for shoulder_sleeve_left
            $("#shoulder_sleeve_left").text(parseFloat(shoulder_sleeve_left * 2.54).toFixed(1) + " Cm");
            $("#sleeve_left_shirt_allow").text(sleeveleftshirt+ " Cm");
            $("#sleeve_left_jacket_allow").text(sleeveleftjacket+ " Cm");
            $("#sleeve_left_vest_allow").text(sleeveleftvest+ " Cm");
            $("#sleeve_left_pant_allow").text(sleeveleftpant+ " Cm");
            $("#final_shirt_sleeve_left").text(parseFloat(final_shirt_sleeve_left * 2.54).toFixed(1) + " Cm");
            $("#final_jacket_sleeve_left").text(parseFloat(final_jacket_sleeve_left * 2.54).toFixed(1) + " Cm");
            $("#final_vest_sleeve_left").text(parseFloat(final_vest_sleeve_left * 2.54).toFixed(1) + " Cm");
            $("#final_pant_sleeve_left").text(parseFloat(final_pant_sleeve_left * 2.54).toFixed(1) + " Cm");
            //for thigh
            $("#thigh").text(parseFloat(thigh * 2.54).toFixed(1) + " Cm");
            $("#thigh_shirt_allow").text(thighshirt+ " Cm");
            $("#thigh_jacket_allow").text(thighjacket+ " Cm");
            $("#thigh_vest_allow").text(thighvest+ " Cm");
            $("#thigh_pant_allow").text(thighpant+ " Cm");
            $("#final_shirt_thigh").text(parseFloat(final_shirt_thigh * 2.54).toFixed(1) + " Cm");
            $("#final_jacket_thigh").text(parseFloat(final_jacket_thigh * 2.54).toFixed(1) + " Cm");
            $("#final_vest_thigh").text(parseFloat(final_vest_thigh * 2.54).toFixed(1) + " Cm");
            $("#final_pant_thigh").text(parseFloat(final_pant_thigh * 2.54).toFixed(1) + " Cm");
            //for napetowaist_back
            $("#napetowaist_back").text(parseFloat(napetowaist_back * 2.54).toFixed(1) + " Cm");
            $("#nape_shirt_allow").text(napetowaistshirt+ " Cm");
            $("#nape_jacket_allow").text(napetowaistjacket+ " Cm");
            $("#nape_vest_allow").text(napetowaistvest+ " Cm");
            $("#nape_pant_allow").text(napetowaistpant+ " Cm");
            $("#final_shirt_nape").text(parseFloat(final_shirt_nape * 2.54).toFixed(1) + " Cm");
            $("#final_jacket_nape").text(parseFloat(final_jacket_nape * 2.54).toFixed(1) + " Cm");
            $("#final_vest_nape").text(parseFloat(final_vest_nape * 2.54).toFixed(1) + " Cm");
            $("#final_pant_nape").text(parseFloat(final_pant_nape * 2.54).toFixed(1) + " Cm");
            //for front_waist_length
            $("#front_waist_length").text(parseFloat(front_waist_length * 2.54).toFixed(1) + " Cm");
            $("#front_waist_shirt_allow").text(frontwaistlengthshirt+ " Cm");
            $("#front_waist_jacket_allow").text(frontwaistlengthjacket+ " Cm");
            $("#front_waist_vest_allow").text(frontwaistlengthvest+ " Cm");
            $("#front_waist_pant_allow").text(frontwaistlengthpant+ " Cm");
            $("#final_shirt_front_waist").text(parseFloat(final_shirt_front_waist * 2.54).toFixed(1) + " Cm");
            $("#final_jacket_front_waist").text(parseFloat(final_jacket_front_waist * 2.54).toFixed(1) + " Cm");
            $("#final_vest_front_waist").text(parseFloat(final_vest_front_waist * 2.54).toFixed(1) + " Cm");
            $("#final_pant_front_waist").text(parseFloat(final_pant_front_waist * 2.54).toFixed(1) + " Cm");
            //for wrist
            $("#wrist").text(parseFloat(wrist * 2.54).toFixed(1) + " Cm");
            $("#wrist_shirt_allow").text(wristshirt+ " Cm");
            $("#wrist_jacket_allow").text(wristjacket+ " Cm");
            $("#wrist_vest_allow").text(wristvest+ " Cm");
            $("#wrist_pant_allow").text(wristpant+ " Cm");
            $("#final_shirt_wrist").text(parseFloat(final_shirt_wrist * 2.54).toFixed(1) + " Cm");
            $("#final_jacket_wrist").text(parseFloat(final_jacket_wrist * 2.54).toFixed(1) + " Cm");
            $("#final_vest_wrist").text(parseFloat(final_vest_wrist * 2.54).toFixed(1) + " Cm");
            $("#final_pant_wrist").text(parseFloat(final_pant_wrist * 2.54).toFixed(1) + " Cm");
            //for waist
            $("#waist").text(parseFloat(waist * 2.54).toFixed(1) + " Cm");
            $("#waist_shirt_allow").text(waistshirt+ " Cm");
            $("#waist_jacket_allow").text(waistjacket+ " Cm");
            $("#waist_vest_allow").text(waistvest+ " Cm");
            $("#waist_pant_allow").text(waistpant+ " Cm");
            $("#final_shirt_waist").text(parseFloat(final_shirt_waist * 2.54).toFixed(1) + " Cm");
            $("#final_jacket_waist").text(parseFloat(final_jacket_waist * 2.54).toFixed(1) + " Cm");
            $("#final_vest_waist").text(parseFloat(final_vest_waist * 2.54).toFixed(1) + " Cm");
            $("#final_pant_waist").text(parseFloat(final_pant_waist * 2.54).toFixed(1) + " Cm");
            //for calf
            $("#calf").text(parseFloat(calf * 2.54).toFixed(1) + " Cm");
            $("#calf_shirt_allow").text(calfshirt+ " Cm");
            $("#calf_jacket_allow").text(calfjacket+ " Cm");
            $("#calf_vest_allow").text(calfvest+ " Cm");
            $("#calf_pant_allow").text(calfpant+ " Cm");
            $("#final_shirt_calf").text(parseFloat(final_shirt_calf * 2.54).toFixed(1) + " Cm");
            $("#final_jacket_calf").text(parseFloat(final_jacket_calf * 2.54).toFixed(1) + " Cm");
            $("#final_vest_calf").text(parseFloat(final_vest_calf * 2.54).toFixed(1) + " Cm");
            $("#final_pant_calf").text(parseFloat(final_pant_calf * 2.54).toFixed(1) + " Cm");
            //for back_waist_height
            $("#back_waist_height").text(parseFloat(back_waist_height * 2.54).toFixed(1) + " Cm");
            $("#b_waist_shirt_allow").text(backwaistheightshirt+ " Cm");
            $("#b_waist_jacket_allow").text(backwaistheightjacket+ " Cm");
            $("#b_waist_vest_allow").text(backwaistheightvest+ " Cm");
            $("#b_waist_pant_allow").text(backwaistheightpant+ " Cm");
            $("#final_shirt_b_waist").text(parseFloat(final_shirt_b_waist * 2.54).toFixed(1) + " Cm");
            $("#final_jacket_b_waist").text(parseFloat(final_jacket_b_waist * 2.54).toFixed(1) + " Cm");
            $("#final_vest_b_waist").text(parseFloat(final_vest_b_waist * 2.54).toFixed(1) + " Cm");
            $("#final_pant_b_waist").text(parseFloat(final_pant_b_waist * 2.54).toFixed(1) + " Cm");
            //for front_waist_height
            $("#front_waist_height").text(parseFloat(front_waist_height * 2.54).toFixed(1) + " Cm");
            $("#f_waist_shirt_allow").text(frontwaistheightshirt+ " Cm");
            $("#f_waist_jacket_allow").text(frontwaistheightjacket+ " Cm");
            $("#f_waist_vest_allow").text(frontwaistheightvest+ " Cm");
            $("#f_waist_pant_allow").text(frontwaistheightpant+ " Cm");
            $("#final_shirt_f_waist").text(parseFloat(final_shirt_f_waist * 2.54).toFixed(1) + " Cm");
            $("#final_jacket_f_waist").text(parseFloat(final_jacket_f_waist * 2.54).toFixed(1) + " Cm");
            $("#final_vest_f_waist").text(parseFloat(final_vest_f_waist * 2.54).toFixed(1) + " Cm");
            $("#final_pant_f_waist").text(parseFloat(final_pant_f_waist * 2.54).toFixed(1) + " Cm");
            //for knee_right
            $("#knee_right").text(parseFloat(knee_right * 2.54).toFixed(1) + " Cm");
            $("#knee_shirt_allow").text(kneeshirt+ " Cm");
            $("#knee_jacket_allow").text(kneejacket+ " Cm");
            $("#knee_vest_allow").text(kneevest+ " Cm");
            $("#knee_pant_allow").text(kneepant+ " Cm");
            $("#final_shirt_knee").text(parseFloat(final_shirt_knee * 2.54).toFixed(1) + " Cm");
            $("#final_jacket_knee").text(parseFloat(final_jacket_knee * 2.54).toFixed(1) + " Cm");
            $("#final_vest_knee").text(parseFloat(final_vest_knee * 2.54).toFixed(1) + " Cm");
            $("#final_pant_knee").text(parseFloat(final_pant_knee * 2.54).toFixed(1) + " Cm");
            //for back_jacket_length
            $("#back_jacket_length").text(parseFloat(back_jacket_length * 2.54).toFixed(1) + " Cm");
            $("#jacket_shirt_allow").text(backjacketlengthshirt+ " Cm");
            $("#jacket_jacket_allow").text(backjacketlengthjacket+ " Cm");
            $("#jacket_vest_allow").text(backjacketlengthvest+ " Cm");
            $("#jacket_pant_allow").text(backjacketlengthpant+ " Cm");
            $("#final_shirt_jacket").text(parseFloat(final_shirt_jacket * 2.54).toFixed(1) + " Cm");
            $("#final_jacket_jacket").text(parseFloat(final_jacket_jacket * 2.54).toFixed(1) + " Cm");
            $("#final_vest_jacket").text(parseFloat(final_vest_jacket * 2.54).toFixed(1) + " Cm");
            $("#final_pant_jacket").text(parseFloat(final_pant_jacket * 2.54).toFixed(1) + " Cm");
            //for u_rise
            $("#u_rise").text(parseFloat(u_rise * 2.54).toFixed(1) + " Cm");
            $("#rise_shirt_allow").text(riseshirt+ " Cm");
            $("#rise_jacket_allow").text(risejacket+ " Cm");
            $("#rise_vest_allow").text(risevest+ " Cm");
            $("#rise_pant_allow").text(risepant+ " Cm");
            $("#final_shirt_rise").text(parseFloat(final_shirt_rise * 2.54).toFixed(1) + " Cm");
            $("#final_jacket_rise").text(parseFloat(final_jacket_rise * 2.54).toFixed(1) + " Cm");
            $("#final_vest_rise").text(parseFloat(final_vest_rise * 2.54).toFixed(1) + " Cm");
            $("#final_pant_rise").text(parseFloat(final_pant_rise * 2.54).toFixed(1) + " Cm");
            //for outseam_l_pants
            $("#outseam_l_pants").text(parseFloat(outseam_l_pants * 2.54).toFixed(1) + " Cm");
            $("#p_left_shirt_allow").text(pantleftshirt+ " Cm");
            $("#p_left_jacket_allow").text(pantleftjacket+ " Cm");
            $("#p_left_vest_allow").text(pantleftvest+ " Cm");
            $("#p_left_pant_allow").text(pantleftpant+ " Cm");
            $("#final_shirt_p_left").text(parseFloat(final_shirt_p_left * 2.54).toFixed(1) + " Cm");
            $("#final_jacket_p_left").text(parseFloat(final_jacket_p_left * 2.54).toFixed(1) + " Cm");
            $("#final_vest_p_left").text(parseFloat(final_vest_p_left * 2.54).toFixed(1) + " Cm");
            $("#final_pant_p_left").text(parseFloat(final_pant_p_left * 2.54).toFixed(1) + " Cm");
            //for outseam_r_pants
            $("#outseam_r_pants").text(parseFloat(outseam_r_pants * 2.54).toFixed(1) + " Cm");
            $("#p_right_shirt_allow").text(pantrightshirt+ " Cm");
            $("#p_right_jacket_allow").text(pantrightjacket+ " Cm");
            $("#p_right_vest_allow").text(pantrightvest+ " Cm");
            $("#p_right_pant_allow").text(pantrightpant+ " Cm");
            $("#final_shirt_p_right").text(parseFloat(final_shirt_p_right * 2.54).toFixed(1) + " Cm");
            $("#final_jacket_p_right").text(parseFloat(final_jacket_p_right * 2.54).toFixed(1) + " Cm");
            $("#final_vest_p_right").text(parseFloat(final_vest_p_right * 2.54).toFixed(1) + " Cm");
            $("#final_pant_p_right").text(parseFloat(final_pant_p_right * 2.54).toFixed(1) + " Cm");
            //for bottom
            $("#bottom").text(parseFloat(bottom * 2.54).toFixed(1) + " Cm");
            $("#bottom_shirt_allow").text(bottomshirt+ " Cm");
            $("#bottom_jacket_allow").text(bottomjacket+ " Cm");
            $("#bottom_vest_allow").text(bottomvest+ " Cm");
            $("#bottom_pant_allow").text(bottompant+ " Cm");
            $("#final_shirt_bottom").text(parseFloat(final_shirt_bottom * 2.54).toFixed(1) + " Cm");
            $("#final_jacket_bottom").text(parseFloat(final_jacket_bottom * 2.54).toFixed(1) + " Cm");
            $("#final_vest_bottom").text(parseFloat(final_vest_bottom * 2.54).toFixed(1) + " Cm");
            $("#final_pant_bottom").text(parseFloat(final_pant_bottom * 2.54).toFixed(1) + " Cm");
            $(".loaderdiv").removeClass("block");
            $(".loaderdiv").addClass("loaderhidden");
        } else {
            //show in Inches means conversion required from centimeter to Inches
            //for collar
            $("#collar").text(parseFloat(collar / 2.54).toFixed(1) + " Inch");
            $("#collar_shirt_allow").text(parseFloat(collar_shirt_allow / 2.54).toFixed(1) + " Inch");
            $("#collar_jacket_allow").text(parseFloat(collar_jacket_allow / 2.54).toFixed(1) + " Inch");
            $("#collar_vest_allow").text(parseFloat(collar_vest_allow / 2.54).toFixed(1) + " Inch");
            $("#collar_pant_allow").text(parseFloat(collar_pant_allow / 2.54).toFixed(1) + " Inch");
            $("#final_shirt_collar").text(parseFloat(final_shirt_collar / 2.54).toFixed(1) + " Inch");
            $("#final_jacket_collar").text(parseFloat(final_jacket_collar / 2.54).toFixed(1) + " Inch");
            $("#final_vest_collar").text(parseFloat(final_vest_collar / 2.54).toFixed(1) + " Inch");
            $("#final_pant_collar").text(parseFloat(final_pant_collar / 2.54).toFixed(1) + " Inch");
            //for chest_front
            $("#chest_front").text(parseFloat(chest_front / 2.54).toFixed(1) + " Inch");
            $("#chest_shirt_allow").text(parseFloat(chest_shirt_allow / 2.54).toFixed(1) + " Inch");
            $("#chest_jacket_allow").text(parseFloat(chest_jacket_allow / 2.54).toFixed(1) + " Inch");
            $("#chest_vest_allow").text(parseFloat(chest_vest_allow / 2.54).toFixed(1) + " Inch");
            $("#chest_pant_allow").text(parseFloat(chest_pant_allow / 2.54).toFixed(1) + " Inch");
            $("#final_shirt_chest").text(parseFloat(final_shirt_chest / 2.54).toFixed(1) + " Inch");
            $("#final_jacket_chest").text(parseFloat(final_jacket_chest / 2.54).toFixed(1) + " Inch");
            $("#final_vest_chest").text(parseFloat(final_vest_chest / 2.54).toFixed(1) + " Inch");
            $("#final_pant_chest").text(parseFloat(final_pant_chest / 2.54).toFixed(1) + " Inch");
            //for stomach
            $("#stomach").text(parseFloat(stomach / 2.54).toFixed(1) + " Inch");
            $("#stomach_shirt_allow").text(parseFloat(stomach_shirt_allow / 2.54).toFixed(1) + " Inch");
            $("#stomach_jacket_allow").text(parseFloat(stomach_jacket_allow / 2.54).toFixed(1) + " Inch");
            $("#stomach_vest_allow").text(parseFloat(stomach_vest_allow / 2.54).toFixed(1) + " Inch");
            $("#stomach_pant_allow").text(parseFloat(stomach_pant_allow / 2.54).toFixed(1) + " Inch");
            $("#final_shirt_stomach").text(parseFloat(final_shirt_stomach / 2.54).toFixed(1) + " Inch");
            $("#final_jacket_stomach").text(parseFloat(final_jacket_stomach / 2.54).toFixed(1) + " Inch");
            $("#final_vest_stomach").text(parseFloat(final_vest_stomach / 2.54).toFixed(1) + " Inch");
            $("#final_pant_stomach").text(parseFloat(final_pant_stomach / 2.54).toFixed(1) + " Inch");
            //for seat_front
            $("#seat_front").text(parseFloat(seat_front / 2.54).toFixed(1) + " Inch");
            $("#seat_shirt_allow").text(parseFloat(seat_shirt_allow / 2.54).toFixed(1) + " Inch");
            $("#seat_jacket_allow").text(parseFloat(seat_jacket_allow / 2.54).toFixed(1) + " Inch");
            $("#seat_vest_allow").text(parseFloat(seat_vest_allow / 2.54).toFixed(1) + " Inch");
            $("#seat_pant_allow").text(parseFloat(seat_pant_allow / 2.54).toFixed(1) + " Inch");
            $("#final_shirt_seat").text(parseFloat(final_shirt_seat / 2.54).toFixed(1) + " Inch");
            $("#final_jacket_seat").text(parseFloat(final_jacket_seat / 2.54).toFixed(1) + " Inch");
            $("#final_vest_seat").text(parseFloat(final_vest_seat / 2.54).toFixed(1) + " Inch");
            $("#final_pant_seat").text(parseFloat(final_pant_seat / 2.54).toFixed(1) + " Inch");
            //for bicep_front
            $("#bicep_front").text(parseFloat(bicep_front / 2.54).toFixed(1) + " Inch");
            $("#bicep_shirt_allow").text(parseFloat(bicep_shirt_allow / 2.54).toFixed(1) + " Inch");
            $("#bicep_jacket_allow").text(parseFloat(bicep_jacket_allow / 2.54).toFixed(1) + " Inch");
            $("#bicep_vest_allow").text(parseFloat(bicep_vest_allow / 2.54).toFixed(1) + " Inch");
            $("#bicep_pant_allow").text(parseFloat(bicep_pant_allow / 2.54).toFixed(1) + " Inch");
            $("#final_shirt_bicep").text(parseFloat(final_shirt_bicep / 2.54).toFixed(1) + " Inch");
            $("#final_jacket_bicep").text(parseFloat(final_jacket_bicep / 2.54).toFixed(1) + " Inch");
            $("#final_vest_bicep").text(parseFloat(final_vest_bicep / 2.54).toFixed(1) + " Inch");
            $("#final_pant_bicep").text(parseFloat(final_pant_bicep / 2.54).toFixed(1) + " Inch");
            //for shoulder_back
            $("#shoulder_back").text(parseFloat(shoulder_back / 2.54).toFixed(1) + " Inch");
            $("#back_shoulder_shirt_allow").text(parseFloat(back_shoulder_shirt_allow / 2.54).toFixed(1) + " Inch");
            $("#back_shoulder_jacket_allow").text(parseFloat(back_shoulder_jacket_allow / 2.54).toFixed(1) + " Inch");
            $("#back_shoulder_vest_allow").text(parseFloat(back_shoulder_vest_allow / 2.54).toFixed(1) + " Inch");
            $("#back_shoulder_pant_allow").text(parseFloat(back_shoulder_pant_allow / 2.54).toFixed(1) + " Inch");
            $("#final_shirt_back_shoulder").text(parseFloat(final_shirt_back_shoulder / 2.54).toFixed(1) + " Inch");
            $("#final_jacket_back_shoulder").text(parseFloat(final_jacket_back_shoulder / 2.54).toFixed(1) + " Inch");
            $("#final_vest_back_shoulder").text(parseFloat(final_vest_back_shoulder / 2.54).toFixed(1) + " Inch");
            $("#final_pant_back_shoulder").text(parseFloat(final_pant_back_shoulder / 2.54).toFixed(1) + " Inch");
            //for shoulder_front
            $("#shoulder_front").text(parseFloat(shoulder_front / 2.54).toFixed(1) + " Inch");
            $("#front_shoulder_shirt_allow").text(parseFloat(front_shoulder_shirt_allow / 2.54).toFixed(1) + " Inch");
            $("#front_shoulder_jacket_allow").text(parseFloat(front_shoulder_jacket_allow / 2.54).toFixed(1) + " Inch");
            $("#front_shoulder_vest_allow").text(parseFloat(front_shoulder_vest_allow / 2.54).toFixed(1) + " Inch");
            $("#front_shoulder_pant_allow").text(parseFloat(front_shoulder_pant_allow / 2.54).toFixed(1) + " Inch");
            $("#final_shirt_front_shoulder").text(parseFloat(final_shirt_front_shoulder / 2.54).toFixed(1) + " Inch");
            $("#final_jacket_front_shoulder").text(parseFloat(final_jacket_front_shoulder / 2.54).toFixed(1) + " Inch");
            $("#final_vest_front_shoulder").text(parseFloat(final_vest_front_shoulder / 2.54).toFixed(1) + " Inch");
            $("#final_pant_front_shoulder").text(parseFloat(final_pant_front_shoulder / 2.54).toFixed(1) + " Inch");
            //for shoulder_sleeve_right
            $("#shoulder_sleeve_right").text(parseFloat(shoulder_sleeve_right / 2.54).toFixed(1) + " Inch");
            $("#sleeve_right_shirt_allow").text(parseFloat(sleeve_right_shirt_allow / 2.54).toFixed(1) + " Inch");
            $("#sleeve_right_jacket_allow").text(parseFloat(sleeve_right_jacket_allow / 2.54).toFixed(1) + " Inch");
            $("#sleeve_right_vest_allow").text(parseFloat(sleeve_right_vest_allow / 2.54).toFixed(1) + " Inch");
            $("#sleeve_right_pant_allow").text(parseFloat(sleeve_right_pant_allow / 2.54).toFixed(1) + " Inch");
            $("#final_shirt_sleeve_right").text(parseFloat(final_shirt_sleeve_right / 2.54).toFixed(1) + " Inch");
            $("#final_jacket_sleeve_right").text(parseFloat(final_jacket_sleeve_right / 2.54).toFixed(1) + " Inch");
            $("#final_vest_sleeve_right").text(parseFloat(final_vest_sleeve_right / 2.54).toFixed(1) + " Inch");
            $("#final_pant_sleeve_right").text(parseFloat(final_pant_sleeve_right / 2.54).toFixed(1) + " Inch");
            //for shoulder_sleeve_left
            $("#shoulder_sleeve_left").text(parseFloat(shoulder_sleeve_left / 2.54).toFixed(1) + " Inch");
            $("#sleeve_left_shirt_allow").text(parseFloat(sleeve_left_shirt_allow / 2.54).toFixed(1) + " Inch");
            $("#sleeve_left_jacket_allow").text(parseFloat(sleeve_left_jacket_allow / 2.54).toFixed(1) + " Inch");
            $("#sleeve_left_vest_allow").text(parseFloat(sleeve_left_vest_allow / 2.54).toFixed(1) + " Inch");
            $("#sleeve_left_pant_allow").text(parseFloat(sleeve_left_pant_allow / 2.54).toFixed(1) + " Inch");
            $("#final_shirt_sleeve_left").text(parseFloat(final_shirt_sleeve_left / 2.54).toFixed(1) + " Inch");
            $("#final_jacket_sleeve_left").text(parseFloat(final_jacket_sleeve_left / 2.54).toFixed(1) + " Inch");
            $("#final_vest_sleeve_left").text(parseFloat(final_vest_sleeve_left / 2.54).toFixed(1) + " Inch");
            $("#final_pant_sleeve_left").text(parseFloat(final_pant_sleeve_left / 2.54).toFixed(1) + " Inch");
            //for thigh
            $("#thigh").text(parseFloat(thigh / 2.54).toFixed(1) + " Inch");
            $("#thigh_shirt_allow").text(parseFloat(thigh_shirt_allow / 2.54).toFixed(1) + " Inch");
            $("#thigh_jacket_allow").text(parseFloat(thigh_jacket_allow / 2.54).toFixed(1) + " Inch");
            $("#thigh_vest_allow").text(parseFloat(thigh_vest_allow / 2.54).toFixed(1) + " Inch");
            $("#thigh_pant_allow").text(parseFloat(thigh_pant_allow / 2.54).toFixed(1) + " Inch");
            $("#final_shirt_thigh").text(parseFloat(final_shirt_thigh / 2.54).toFixed(1) + " Inch");
            $("#final_jacket_thigh").text(parseFloat(final_jacket_thigh / 2.54).toFixed(1) + " Inch");
            $("#final_vest_thigh").text(parseFloat(final_vest_thigh / 2.54).toFixed(1) + " Inch");
            $("#final_pant_thigh").text(parseFloat(final_pant_thigh / 2.54).toFixed(1) + " Inch");
            //for napetowaist_back
            $("#napetowaist_back").text(parseFloat(napetowaist_back / 2.54).toFixed(1) + " Inch");
            $("#nape_shirt_allow").text(parseFloat(nape_shirt_allow / 2.54).toFixed(1) + " Inch");
            $("#nape_jacket_allow").text(parseFloat(nape_jacket_allow / 2.54).toFixed(1) + " Inch");
            $("#nape_vest_allow").text(parseFloat(nape_vest_allow / 2.54).toFixed(1) + " Inch");
            $("#nape_pant_allow").text(parseFloat(nape_pant_allow / 2.54).toFixed(1) + " Inch");
            $("#final_shirt_nape").text(parseFloat(final_shirt_nape / 2.54).toFixed(1) + " Inch");
            $("#final_jacket_nape").text(parseFloat(final_jacket_nape / 2.54).toFixed(1) + " Inch");
            $("#final_vest_nape").text(parseFloat(final_vest_nape / 2.54).toFixed(1) + " Inch");
            $("#final_pant_nape").text(parseFloat(final_pant_nape / 2.54).toFixed(1) + " Inch");
            //for front_waist_length
            $("#front_waist_length").text(parseFloat(front_waist_length / 2.54).toFixed(1) + " Inch");
            $("#front_waist_shirt_allow").text(parseFloat(front_waist_shirt_allow / 2.54).toFixed(1) + " Inch");
            $("#front_waist_jacket_allow").text(parseFloat(front_waist_jacket_allow / 2.54).toFixed(1) + " Inch");
            $("#front_waist_vest_allow").text(parseFloat(front_waist_vest_allow / 2.54).toFixed(1) + " Inch");
            $("#front_waist_pant_allow").text(parseFloat(front_waist_pant_allow / 2.54).toFixed(1) + " Inch");
            $("#final_shirt_front_waist").text(parseFloat(final_shirt_front_waist / 2.54).toFixed(1) + " Inch");
            $("#final_jacket_front_waist").text(parseFloat(final_jacket_front_waist / 2.54).toFixed(1) + " Inch");
            $("#final_vest_front_waist").text(parseFloat(final_vest_front_waist / 2.54).toFixed(1) + " Inch");
            $("#final_pant_front_waist").text(parseFloat(final_pant_front_waist / 2.54).toFixed(1) + " Inch");
            //for wrist
            $("#wrist").text(parseFloat(wrist / 2.54).toFixed(1) + " Inch");
            $("#wrist_shirt_allow").text(parseFloat(wrist_shirt_allow / 2.54).toFixed(1) + " Inch");
            $("#wrist_jacket_allow").text(parseFloat(wrist_jacket_allow / 2.54).toFixed(1) + " Inch");
            $("#wrist_vest_allow").text(parseFloat(wrist_vest_allow / 2.54).toFixed(1) + " Inch");
            $("#wrist_pant_allow").text(parseFloat(wrist_pant_allow / 2.54).toFixed(1) + " Inch");
            $("#final_shirt_wrist").text(parseFloat(final_shirt_wrist / 2.54).toFixed(1) + " Inch");
            $("#final_jacket_wrist").text(parseFloat(final_jacket_wrist / 2.54).toFixed(1) + " Inch");
            $("#final_vest_wrist").text(parseFloat(final_vest_wrist / 2.54).toFixed(1) + " Inch");
            $("#final_pant_wrist").text(parseFloat(final_pant_wrist / 2.54).toFixed(1) + " Inch");
            //for wrist
            $("#waist").text(parseFloat(waist / 2.54).toFixed(1) + " Inch");
            $("#waist_shirt_allow").text(parseFloat(waist_shirt_allow / 2.54).toFixed(1) + " Inch");
            $("#waist_jacket_allow").text(parseFloat(waist_jacket_allow / 2.54).toFixed(1) + " Inch");
            $("#waist_vest_allow").text(parseFloat(waist_vest_allow / 2.54).toFixed(1) + " Inch");
            $("#waist_pant_allow").text(parseFloat(waist_pant_allow / 2.54).toFixed(1) + " Inch");
            $("#final_shirt_waist").text(parseFloat(final_shirt_waist / 2.54).toFixed(1) + " Inch");
            $("#final_jacket_waist").text(parseFloat(final_jacket_waist / 2.54).toFixed(1) + " Inch");
            $("#final_vest_waist").text(parseFloat(final_vest_waist / 2.54).toFixed(1) + " Inch");
            $("#final_pant_waist").text(parseFloat(final_pant_waist / 2.54).toFixed(1) + " Inch");
            //for calf
            $("#calf").text(parseFloat(calf / 2.54).toFixed(1) + " Inch");
            $("#calf_shirt_allow").text(parseFloat(calf_shirt_allow / 2.54).toFixed(1) + " Inch");
            $("#calf_jacket_allow").text(parseFloat(calf_jacket_allow / 2.54).toFixed(1) + " Inch");
            $("#calf_vest_allow").text(parseFloat(calf_vest_allow / 2.54).toFixed(1) + " Inch");
            $("#calf_pant_allow").text(parseFloat(calf_pant_allow / 2.54).toFixed(1) + " Inch");
            $("#final_shirt_calf").text(parseFloat(final_shirt_calf / 2.54).toFixed(1) + " Inch");
            $("#final_jacket_calf").text(parseFloat(final_jacket_calf / 2.54).toFixed(1) + " Inch");
            $("#final_vest_calf").text(parseFloat(final_vest_calf / 2.54).toFixed(1) + " Inch");
            $("#final_pant_calf").text(parseFloat(final_pant_calf / 2.54).toFixed(1) + " Inch");
            //for back_waist_height
            $("#back_waist_height").text(parseFloat(back_waist_height / 2.54).toFixed(1) + " Inch");
            $("#b_waist_shirt_allow").text(parseFloat(b_waist_shirt_allow / 2.54).toFixed(1) + " Inch");
            $("#b_waist_jacket_allow").text(parseFloat(b_waist_jacket_allow / 2.54).toFixed(1) + " Inch");
            $("#b_waist_vest_allow").text(parseFloat(b_waist_vest_allow / 2.54).toFixed(1) + " Inch");
            $("#b_waist_pant_allow").text(parseFloat(b_waist_pant_allow / 2.54).toFixed(1) + " Inch");
            $("#final_shirt_b_waist").text(parseFloat(final_shirt_b_waist / 2.54).toFixed(1) + " Inch");
            $("#final_jacket_b_waist").text(parseFloat(final_jacket_b_waist / 2.54).toFixed(1) + " Inch");
            $("#final_vest_b_waist").text(parseFloat(final_vest_b_waist / 2.54).toFixed(1) + " Inch");
            $("#final_pant_b_waist").text(parseFloat(final_pant_b_waist / 2.54).toFixed(1) + " Inch");
            //for front_waist_height
            $("#front_waist_height").text(parseFloat(front_waist_height / 2.54).toFixed(1) + " Inch");
            $("#f_waist_shirt_allow").text(parseFloat(f_waist_shirt_allow / 2.54).toFixed(1) + " Inch");
            $("#f_waist_jacket_allow").text(parseFloat(f_waist_jacket_allow / 2.54).toFixed(1) + " Inch");
            $("#f_waist_vest_allow").text(parseFloat(f_waist_vest_allow / 2.54).toFixed(1) + " Inch");
            $("#f_waist_pant_allow").text(parseFloat(f_waist_pant_allow / 2.54).toFixed(1) + " Inch");
            $("#final_shirt_f_waist").text(parseFloat(final_shirt_f_waist / 2.54).toFixed(1) + " Inch");
            $("#final_jacket_f_waist").text(parseFloat(final_jacket_f_waist / 2.54).toFixed(1) + " Inch");
            $("#final_vest_f_waist").text(parseFloat(final_vest_f_waist / 2.54).toFixed(1) + " Inch");
            $("#final_pant_f_waist").text(parseFloat(final_pant_f_waist / 2.54).toFixed(1) + " Inch");
            //for knee_right
            $("#knee_right").text(parseFloat(knee_right / 2.54).toFixed(1) + " Inch");
            $("#knee_shirt_allow").text(parseFloat(knee_shirt_allow / 2.54).toFixed(1) + " Inch");
            $("#knee_jacket_allow").text(parseFloat(knee_jacket_allow / 2.54).toFixed(1) + " Inch");
            $("#knee_vest_allow").text(parseFloat(knee_vest_allow / 2.54).toFixed(1) + " Inch");
            $("#knee_pant_allow").text(parseFloat(knee_pant_allow / 2.54).toFixed(1) + " Inch");
            $("#final_shirt_knee").text(parseFloat(final_shirt_knee / 2.54).toFixed(1) + " Inch");
            $("#final_jacket_knee").text(parseFloat(final_jacket_knee / 2.54).toFixed(1) + " Inch");
            $("#final_vest_knee").text(parseFloat(final_vest_knee / 2.54).toFixed(1) + " Inch");
            $("#final_pant_knee").text(parseFloat(final_pant_knee / 2.54).toFixed(1) + " Inch");
            //for back_jacket_length
            $("#back_jacket_length").text(parseFloat(back_jacket_length / 2.54).toFixed(1) + " Inch");
            $("#jacket_shirt_allow").text(parseFloat(jacket_shirt_allow / 2.54).toFixed(1) + " Inch");
            $("#jacket_jacket_allow").text(parseFloat(jacket_jacket_allow / 2.54).toFixed(1) + " Inch");
            $("#jacket_vest_allow").text(parseFloat(jacket_vest_allow / 2.54).toFixed(1) + " Inch");
            $("#jacket_pant_allow").text(parseFloat(jacket_pant_allow / 2.54).toFixed(1) + " Inch");
            $("#final_shirt_jacket").text(parseFloat(final_shirt_jacket / 2.54).toFixed(1) + " Inch");
            $("#final_jacket_jacket").text(parseFloat(final_jacket_jacket / 2.54).toFixed(1) + " Inch");
            $("#final_vest_jacket").text(parseFloat(final_vest_jacket / 2.54).toFixed(1) + " Inch");
            $("#final_pant_jacket").text(parseFloat(final_pant_jacket / 2.54).toFixed(1) + " Inch");
            //for u_rise
            $("#u_rise").text(parseFloat(u_rise / 2.54).toFixed(1) + " Inch");
            $("#rise_shirt_allow").text(parseFloat(rise_shirt_allow / 2.54).toFixed(1) + " Inch");
            $("#rise_jacket_allow").text(parseFloat(rise_jacket_allow / 2.54).toFixed(1) + " Inch");
            $("#rise_vest_allow").text(parseFloat(rise_vest_allow / 2.54).toFixed(1) + " Inch");
            $("#rise_pant_allow").text(parseFloat(rise_pant_allow / 2.54).toFixed(1) + " Inch");
            $("#final_shirt_rise").text(parseFloat(final_shirt_rise / 2.54).toFixed(1) + " Inch");
            $("#final_jacket_rise").text(parseFloat(final_jacket_rise / 2.54).toFixed(1) + " Inch");
            $("#final_vest_rise").text(parseFloat(final_vest_rise / 2.54).toFixed(1) + " Inch");
            $("#final_pant_rise").text(parseFloat(final_pant_rise / 2.54).toFixed(1) + " Inch");
            //for outseam_l_pants
            $("#outseam_l_pants").text(parseFloat(outseam_l_pants / 2.54).toFixed(1) + " Inch");
            $("#p_left_shirt_allow").text(parseFloat(p_left_shirt_allow / 2.54).toFixed(1) + " Inch");
            $("#p_left_jacket_allow").text(parseFloat(p_left_jacket_allow / 2.54).toFixed(1) + " Inch");
            $("#p_left_vest_allow").text(parseFloat(p_left_vest_allow / 2.54).toFixed(1) + " Inch");
            $("#p_left_pant_allow").text(parseFloat(p_left_pant_allow / 2.54).toFixed(1) + " Inch");
            $("#final_shirt_p_left").text(parseFloat(final_shirt_p_left / 2.54).toFixed(1) + " Inch");
            $("#final_jacket_p_left").text(parseFloat(final_jacket_p_left / 2.54).toFixed(1) + " Inch");
            $("#final_vest_p_left").text(parseFloat(final_vest_p_left / 2.54).toFixed(1) + " Inch");
            $("#final_pant_p_left").text(parseFloat(final_pant_p_left / 2.54).toFixed(1) + " Inch");
            //for outseam_r_pants
            $("#outseam_r_pants").text(parseFloat(outseam_r_pants / 2.54).toFixed(1) + " Inch");
            $("#p_right_shirt_allow").text(parseFloat(p_right_shirt_allow / 2.54).toFixed(1) + " Inch");
            $("#p_right_jacket_allow").text(parseFloat(p_right_jacket_allow / 2.54).toFixed(1) + " Inch");
            $("#p_right_vest_allow").text(parseFloat(p_right_vest_allow / 2.54).toFixed(1) + " Inch");
            $("#p_right_pant_allow").text(parseFloat(p_right_pant_allow / 2.54).toFixed(1) + " Inch");
            $("#final_shirt_p_right").text(parseFloat(final_shirt_p_right / 2.54).toFixed(1) + " Inch");
            $("#final_jacket_p_right").text(parseFloat(final_jacket_p_right / 2.54).toFixed(1) + " Inch");
            $("#final_vest_p_right").text(parseFloat(final_vest_p_right / 2.54).toFixed(1) + " Inch");
            $("#final_pant_p_right").text(parseFloat(final_pant_p_right / 2.54).toFixed(1) + " Inch");
            //for bottom
            $("#bottom").text(parseFloat(bottom / 2.54).toFixed(1) + " Inch");
            $("#bottom_shirt_allow").text(parseFloat(bottom_shirt_allow / 2.54).toFixed(1) + " Inch");
            $("#bottom_jacket_allow").text(parseFloat(bottom_jacket_allow / 2.54).toFixed(1) + " Inch");
            $("#bottom_vest_allow").text(parseFloat(bottom_vest_allow / 2.54).toFixed(1) + " Inch");
            $("#bottom_pant_allow").text(parseFloat(bottom_pant_allow / 2.54).toFixed(1) + " Inch");
            $("#final_shirt_bottom").text(parseFloat(final_shirt_bottom / 2.54).toFixed(1) + " Inch");
            $("#final_jacket_bottom").text(parseFloat(final_jacket_bottom / 2.54).toFixed(1) + " Inch");
            $("#final_vest_bottom").text(parseFloat(final_vest_bottom / 2.54).toFixed(1) + " Inch");
            $("#final_pant_bottom").text(parseFloat(final_pant_bottom / 2.54).toFixed(1) + " Inch");
            $(".loaderdiv").removeClass("block");
            $(".loaderdiv").addClass("loaderhidden");
        }
    }
    function showMessage(message, color) {
        $(".modal-header").css({"display": "none"});
        $(".modal-body").css({"display": "block"});
        $(".modal-body").html("<span style='color:" + color + "'>" + message + "</span>");
        $(".modal-footer").css({"display": "none"});
        $("#myModal").modal("show");
        setTimeout(function(){
            $("#myModal").modal("hide");
        },3000);
    }
    function incdec(op, _id) {
        if (op == "minus") {
            var val = $("#" + _id).text().split(" ")[0];
            if ($("#params").val() == "Inches") {
                val = parseFloat(val) - parseFloat(0.25);
                $("#" + _id).text(val + " Inches");
            } else {
                val = parseFloat(val) - parseFloat(0.5);
                $("#" + _id).text(val + " Cm");
            }
        } else if (op == "plus") {
            var val = $("#" + _id).text().split(" ")[0];
            if ($("#params").val() == "Inches") {
                val = parseFloat(val) + parseFloat(0.25);
                $("#" + _id).text(val + " Inches");
            } else {
                val = parseFloat(val) + parseFloat(0.5);
                $("#" + _id).text(val + " Cm");
            }
        }
    }
    function measurementData(measurement_id){
        var url = "../api/registerUser.php";
        $.post(url, {"type": "getParticularMeasurement", "meas_id": measurement_id}, function (data) {
            var status = data.Status;
            if (status == "Success") {
                var measurement = data.measurements;
                if(measurement.unit_type == "Inch" && $("#params").val() == "Inches"){
                    $("#final_shirt_collar").html(parseFloat(measurement.shirt_collar)+ " Inch");
                    $("#final_jacket_collar").html(parseFloat(measurement.jacket_collar)+ " Inch");
                    $("#final_vest_collar").html(parseFloat(measurement.vest_collar)+ " Inch");
                    $("#final_pant_collar").html(parseFloat(measurement.pant_collar)+ " Inch");
                    $("#final_shirt_chest").html(parseFloat(measurement.shirt_chest)+ " Inch");
                    $("#final_jacket_chest").html(parseFloat(measurement.jacket_chest)+ " Inch");
                    $("#final_vest_chest").html(parseFloat(measurement.vest_chest)+ " Inch");
                    $("#final_pant_chest").html(parseFloat(measurement.pant_chest)+ " Inch");
                    $("#final_shirt_stomach").html(parseFloat(measurement.shirt_stomach)+ " Inch");
                    $("#final_jacket_stomach").html(parseFloat(measurement.jacket_stomach)+ " Inch");
                    $("#final_vest_stomach").html(parseFloat(measurement.vest_stomach)+ " Inch");
                    $("#final_pant_stomach").html(parseFloat(measurement.pant_stomach)+ " Inch");
                    $("#final_shirt_seat").html(parseFloat(measurement.shirt_seat)+ " Inch");
                    $("#final_jacket_seat").html(parseFloat(measurement.jacket_seat)+ " Inch");
                    $("#final_vest_seat").html(parseFloat(measurement.vest_seat)+ " Inch");
                    $("#final_pant_seat").html(parseFloat(measurement.pant_seat)+ " Inch");
                    $("#final_shirt_bicep").html(parseFloat(measurement.shirt_bicep)+ " Inch");
                    $("#final_jacket_bicep").html(parseFloat(measurement.jacket_bicep)+ " Inch");
                    $("#final_vest_bicep").html(parseFloat(measurement.vest_bicep)+ " Inch");
                    $("#final_pant_bicep").html(parseFloat(measurement.pant_bicep)+ " Inch");
                    $("#final_shirt_back_shoulder").html(parseFloat(measurement.shirt_back_shoulder)+ " Inch");
                    $("#final_jacket_back_shoulder").html(parseFloat(measurement.jacket_back_shoulder)+ " Inch");
                    $("#final_vest_back_shoulder").html(parseFloat(measurement.vest_back_shoulder)+ " Inch");
                    $("#final_pant_back_shoulder").html(parseFloat(measurement.pant_back_shoulder)+ " Inch");
                    $("#final_shirt_front_shoulder").html(parseFloat(measurement.shirt_front_shoulder)+ " Inch");
                    $("#final_jacket_front_shoulder").html(parseFloat(measurement.jacket_front_shoulder)+ " Inch");
                    $("#final_vest_front_shoulder").html(parseFloat(measurement.vest_front_shoulder)+ " Inch");
                    $("#final_pant_front_shoulder").html(parseFloat(measurement.pant_front_shoulder)+ " Inch");
                    $("#final_shirt_sleeve_left").html(parseFloat(measurement.shirt_sleeve_left)+ " Inch");
                    $("#final_jacket_sleeve_left").html(parseFloat(measurement.jacket_sleeve_left)+ " Inch");
                    $("#final_vest_sleeve_left").html(parseFloat(measurement.vest_sleeve_left)+ " Inch");
                    $("#final_pant_sleeve_left").html(parseFloat(measurement.pant_sleeve_left)+ " Inch");
                    $("#final_shirt_sleeve_right").html(parseFloat(measurement.shirt_sleeve_right)+ " Inch");
                    $("#final_jacket_sleeve_right").html(parseFloat(measurement.jacket_sleeve_right)+ " Inch");
                    $("#final_vest_sleeve_right").html(parseFloat(measurement.vest_sleeve_right)+ " Inch");
                    $("#final_pant_sleeve_right").html(parseFloat(measurement.pant_sleeve_right)+ " Inch");
                    $("#final_shirt_thigh").html(parseFloat(measurement.shirt_thigh)+ " Inch");
                    $("#final_jacket_thigh").html(parseFloat(measurement.jacket_thigh)+ " Inch");
                    $("#final_vest_thigh").html(parseFloat(measurement.vest_thigh)+ " Inch");
                    $("#final_pant_thigh").html(parseFloat(measurement.pant_thigh)+ " Inch");
                    $("#final_shirt_nape").html(parseFloat(measurement.shirt_nape_to_waist)+ " Inch");
                    $("#final_jacket_nape").html(parseFloat(measurement.jacket_nape_to_waist)+ " Inch");
                    $("#final_vest_nape").html(parseFloat(measurement.vest_nape_to_waist)+ " Inch");
                    $("#final_pant_nape").html(parseFloat(measurement.pant_nape_to_waist)+ " Inch");
                    $("#final_shirt_front_waist").html(parseFloat(measurement.shirt_front_waist_length)+ " Inch");
                    $("#final_jacket_front_waist").html(parseFloat(measurement.jacket_front_waist_length)+ " Inch");
                    $("#final_vest_front_waist").html(parseFloat(measurement.vest_front_waist_length)+ " Inch");
                    $("#final_pant_front_waist").html(parseFloat(measurement.pant_front_waist_length)+ " Inch");
                    $("#final_shirt_wrist").html(parseFloat(measurement.shirt_wrist)+ " Inch");
                    $("#final_jacket_wrist").html(parseFloat(measurement.jacket_wrist)+ " Inch");
                    $("#final_vest_wrist").html(parseFloat(measurement.vest_wrist)+ " Inch");
                    $("#final_pant_wrist").html(parseFloat(measurement.pant_wrist)+ " Inch");
                    $("#final_shirt_waist").html(parseFloat(measurement.shirt_waist)+ " Inch");
                    $("#final_jacket_waist").html(parseFloat(measurement.jacket_waist)+ " Inch");
                    $("#final_vest_waist").html(parseFloat(measurement.vest_waist)+ " Inch");
                    $("#final_pant_waist").html(parseFloat(measurement.pant_waist)+ " Inch");
                    $("#final_shirt_calf").html(parseFloat(measurement.shirt_calf)+ " Inch");
                    $("#final_jacket_calf").html(parseFloat(measurement.jacket_calf)+ " Inch");
                    $("#final_vest_calf").html(parseFloat(measurement.vest_calf)+ " Inch");
                    $("#final_pant_calf").html(parseFloat(measurement.pant_calf)+ " Inch");
                    $("#final_shirt_b_waist").html(parseFloat(measurement.shirt_back_waist_height)+ " Inch");
                    $("#final_jacket_b_waist").html(parseFloat(measurement.jacket_back_waist_height)+ " Inch");
                    $("#final_vest_b_waist").html(parseFloat(measurement.vest_back_waist_height)+ " Inch");
                    $("#final_pant_b_waist").html(parseFloat(measurement.pant_back_waist_height)+ " Inch");
                    $("#final_shirt_f_waist").html(parseFloat(measurement.shirt_front_waist_height)+ " Inch");
                    $("#final_jacket_f_waist").html(parseFloat(measurement.jacket_front_waist_height)+ " Inch");
                    $("#final_vest_f_waist").html(parseFloat(measurement.vest_front_waist_height)+ " Inch");
                    $("#final_pant_f_waist").html(parseFloat(measurement.pant_front_waist_height)+ " Inch");
                    $("#final_shirt_knee").html(parseFloat(measurement.shirt_knee)+ " Inch");
                    $("#final_jacket_knee").html(parseFloat(measurement.jacket_knee)+ " Inch");
                    $("#final_vest_knee").html(parseFloat(measurement.vest_knee)+ " Inch");
                    $("#final_pant_knee").html(parseFloat(measurement.pant_knee)+ " Inch");
                    $("#final_shirt_jacket").html(parseFloat(measurement.shirt_back_jacket_length)+ " Inch");
                    $("#final_jacket_back_jacket_length").html(parseFloat(measurement.jacket_back_jacket_length)+ " Inch");
                    $("#final_vest_jacket").html(parseFloat(measurement.vest_back_jacket_length)+ " Inch");
                    $("#final_pant_jacket").html(parseFloat(measurement.pant_back_jacket_length)+ " Inch");
                    $("#final_shirt_rise").html(parseFloat(measurement.shirt_u_rise)+ " Inch");
                    $("#final_jacket_rise").html(parseFloat(measurement.jacket_u_rise)+ " Inch");
                    $("#final_vest_rise").html(parseFloat(measurement.vest_u_rise)+ " Inch");
                    $("#final_pant_rise").html(parseFloat(measurement.pant_u_rise)+ " Inch");
                    $("#final_shirt_p_left").html(parseFloat(measurement.shirt_pant_left_outseam)+ " Inch");
                    $("#final_jacket_p_left").html(parseFloat(measurement.jacket_pant_left_outseam)+ " Inch");
                    $("#final_vest_p_left").html(parseFloat(measurement.vest_pant_left_outseam)+ " Inch");
                    $("#final_pant_p_left").html(parseFloat(measurement.pant_pant_left_outseam)+ " Inch");
                    $("#final_shirt_p_right").html(parseFloat(measurement.shirt_pant_right_outseam)+ " Inch");
                    $("#final_jacket_p_right").html(parseFloat(measurement.jacket_pant_right_outseam)+ " Inch");
                    $("#final_vest_p_right").html(parseFloat(measurement.vest_pant_right_outseam)+ " Inch");
                    $("#final_pant_p_right").html(parseFloat(measurement.pant_pant_right_outseam)+ " Inch");
                    $("#final_shirt_bottom").html(parseFloat(measurement.shirt_bottom)+ " Inch");
                    $("#final_jacket_bottom").html(parseFloat(measurement.jacket_bottom)+ " Inch");
                    $("#final_vest_bottom").html(parseFloat(measurement.vest_bottom)+ " Inch");
                    $("#final_pant_bottom").html(parseFloat(measurement.pant_bottom)+ " Inch");
                }else if(measurement.unit_type == "Inch" && $("#params").val() == "CM"){
                    $("#final_shirt_collar").html(parseFloat(measurement.shirt_collar)*2.54+ " Cm");
                    $("#final_jacket_collar").html(parseFloat(measurement.jacket_collar)*2.54+ " Cm");
                    $("#final_vest_collar").html(parseFloat(measurement.vest_collar)*2.54+ " Cm");
                    $("#final_pant_collar").html(parseFloat(measurement.pant_collar)*2.54+ " Cm");
                    $("#final_shirt_chest").html(parseFloat(measurement.shirt_chest)*2.54+ " Cm");
                    $("#final_jacket_chest").html(parseFloat(measurement.jacket_chest)*2.54+ " Cm");
                    $("#final_vest_chest").html(parseFloat(measurement.vest_chest)*2.54+ " Cm");
                    $("#final_pant_chest").html(parseFloat(measurement.pant_chest)*2.54+ " Cm");
                    $("#final_shirt_stomach").html(parseFloat(measurement.shirt_stomach)*2.54+ " Cm");
                    $("#final_jacket_stomach").html(parseFloat(measurement.jacket_stomach)*2.54+ " Cm");
                    $("#final_vest_stomach").html(parseFloat(measurement.vest_stomach)*2.54+ " Cm");
                    $("#final_pant_stomach").html(parseFloat(measurement.pant_stomach)*2.54+ " Cm");
                    $("#final_shirt_seat").html(parseFloat(measurement.shirt_seat)*2.54+ " Cm");
                    $("#final_jacket_seat").html(parseFloat(measurement.jacket_seat)*2.54+ " Cm");
                    $("#final_vest_seat").html(parseFloat(measurement.vest_seat)*2.54+ " Cm");
                    $("#final_pant_seat").html(parseFloat(measurement.pant_seat)*2.54+ " Cm");
                    $("#final_shirt_bicep").html(parseFloat(measurement.shirt_bicep)*2.54+ " Cm");
                    $("#final_jacket_bicep").html(parseFloat(measurement.jacket_bicep)*2.54+ " Cm");
                    $("#final_vest_bicep").html(parseFloat(measurement.vest_bicep)*2.54+ " Cm");
                    $("#final_pant_bicep").html(parseFloat(measurement.pant_bicep)*2.54+ " Cm");
                    $("#final_shirt_back_shoulder").html(parseFloat(measurement.shirt_back_shoulder)*2.54+ " Cm");
                    $("#final_jacket_back_shoulder").html(parseFloat(measurement.jacket_back_shoulder)*2.54+ " Cm");
                    $("#final_vest_back_shoulder").html(parseFloat(measurement.vest_back_shoulder)*2.54+ " Cm");
                    $("#final_pant_back_shoulder").html(parseFloat(measurement.pant_back_shoulder)*2.54+ " Cm");
                    $("#final_shirt_front_shoulder").html(parseFloat(measurement.shirt_front_shoulder)*2.54+ " Cm");
                    $("#final_jacket_front_shoulder").html(parseFloat(measurement.jacket_front_shoulder)*2.54+ " Cm");
                    $("#final_vest_front_shoulder").html(parseFloat(measurement.vest_front_shoulder)*2.54+ " Cm");
                    $("#final_pant_front_shoulder").html(parseFloat(measurement.pant_front_shoulder)*2.54+ " Cm");
                    $("#final_shirt_sleeve_left").html(parseFloat(measurement.shirt_sleeve_left)*2.54+ " Cm");
                    $("#final_jacket_sleeve_left").html(parseFloat(measurement.jacket_sleeve_left)*2.54+ " Cm");
                    $("#final_vest_sleeve_left").html(parseFloat(measurement.vest_sleeve_left)*2.54+ " Cm");
                    $("#final_pant_sleeve_left").html(parseFloat(measurement.pant_sleeve_left)*2.54+ " Cm");
                    $("#final_shirt_sleeve_right").html(parseFloat(measurement.shirt_sleeve_right)*2.54+ " Cm");
                    $("#final_jacket_sleeve_right").html(parseFloat(measurement.jacket_sleeve_right)*2.54+ " Cm");
                    $("#final_vest_sleeve_right").html(parseFloat(measurement.vest_sleeve_right)*2.54+ " Cm");
                    $("#final_pant_sleeve_right").html(parseFloat(measurement.pant_sleeve_right)*2.54+ " Cm");
                    $("#final_shirt_thigh").html(parseFloat(measurement.shirt_thigh)*2.54+ " Cm");
                    $("#final_jacket_thigh").html(parseFloat(measurement.jacket_thigh)*2.54+ " Cm");
                    $("#final_vest_thigh").html(parseFloat(measurement.vest_thigh)*2.54+ " Cm");
                    $("#final_pant_thigh").html(parseFloat(measurement.pant_thigh)*2.54+ " Cm");
                    $("#final_shirt_nape").html(parseFloat(measurement.shirt_nape_to_waist)*2.54+ " Cm");
                    $("#final_jacket_nape").html(parseFloat(measurement.jacket_nape_to_waist)*2.54+ " Cm");
                    $("#final_vest_nape").html(parseFloat(measurement.vest_nape_to_waist)*2.54+ " Cm");
                    $("#final_pant_nape").html(parseFloat(measurement.pant_nape_to_waist)*2.54+ " Cm");
                    $("#final_shirt_front_waist").html(parseFloat(measurement.shirt_front_waist_length)*2.54+ " Cm");
                    $("#final_jacket_front_waist").html(parseFloat(measurement.jacket_front_waist_length)*2.54+ " Cm");
                    $("#final_vest_front_waist").html(parseFloat(measurement.vest_front_waist_length)*2.54+ " Cm");
                    $("#final_pant_front_waist").html(parseFloat(measurement.pant_front_waist_length)*2.54+ " Cm");
                    $("#final_shirt_wrist").html(parseFloat(measurement.shirt_wrist)*2.54+ " Cm");
                    $("#final_jacket_wrist").html(parseFloat(measurement.jacket_wrist)*2.54+ " Cm");
                    $("#final_vest_wrist").html(parseFloat(measurement.vest_wrist)*2.54+ " Cm");
                    $("#final_pant_wrist").html(parseFloat(measurement.pant_wrist)*2.54+ " Cm");
                    $("#final_shirt_waist").html(parseFloat(measurement.shirt_waist)*2.54+ " Cm");
                    $("#final_jacket_waist").html(parseFloat(measurement.jacket_waist)*2.54+ " Cm");
                    $("#final_vest_waist").html(parseFloat(measurement.vest_waist)*2.54+ " Cm");
                    $("#final_pant_waist").html(parseFloat(measurement.pant_waist)*2.54+ " Cm");
                    $("#final_shirt_calf").html(parseFloat(measurement.shirt_calf)*2.54+ " Cm");
                    $("#final_jacket_calf").html(parseFloat(measurement.jacket_calf)*2.54+ " Cm");
                    $("#final_vest_calf").html(parseFloat(measurement.vest_calf)*2.54+ " Cm");
                    $("#final_pant_calf").html(parseFloat(measurement.pant_calf)*2.54+ " Cm");
                    $("#final_shirt_b_waist").html(parseFloat(measurement.shirt_back_waist_height)*2.54+ " Cm");
                    $("#final_jacket_b_waist").html(parseFloat(measurement.jacket_back_waist_height)*2.54+ " Cm");
                    $("#final_vest_b_waist").html(parseFloat(measurement.vest_back_waist_height)*2.54+ " Cm");
                    $("#final_pant_b_waist").html(parseFloat(measurement.pant_back_waist_height)*2.54+ " Cm");
                    $("#final_shirt_f_waist").html(parseFloat(measurement.shirt_front_waist_height)*2.54+ " Cm");
                    $("#final_jacket_f_waist").html(parseFloat(measurement.jacket_front_waist_height)*2.54+ " Cm");
                    $("#final_vest_f_waist").html(parseFloat(measurement.vest_front_waist_height)*2.54+ " Cm");
                    $("#final_pant_f_waist").html(parseFloat(measurement.pant_front_waist_height)*2.54+ " Cm");
                    $("#final_shirt_knee").html(parseFloat(measurement.shirt_knee)*2.54+ " Cm");
                    $("#final_jacket_knee").html(parseFloat(measurement.jacket_knee)*2.54+ " Cm");
                    $("#final_vest_knee").html(parseFloat(measurement.vest_knee)*2.54+ " Cm");
                    $("#final_pant_knee").html(parseFloat(measurement.pant_knee)*2.54+ " Cm");
                    $("#final_shirt_jacket").html(parseFloat(measurement.shirt_back_jacket_length)*2.54+ " Cm");
                    $("#final_jacket_back_jacket_length").html(parseFloat(measurement.jacket_back_jacket_length)*2.54+ " Cm");
                    $("#final_vest_jacket").html(parseFloat(measurement.vest_back_jacket_length)*2.54+ " Cm");
                    $("#final_pant_jacket").html(parseFloat(measurement.pant_back_jacket_length)*2.54+ " Cm");
                    $("#final_shirt_rise").html(parseFloat(measurement.shirt_u_rise)*2.54+ " Cm");
                    $("#final_jacket_rise").html(parseFloat(measurement.jacket_u_rise)*2.54+ " Cm");
                    $("#final_vest_rise").html(parseFloat(measurement.vest_u_rise)*2.54+ " Cm");
                    $("#final_pant_rise").html(parseFloat(measurement.pant_u_rise)*2.54+ " Cm");
                    $("#final_shirt_p_left").html(parseFloat(measurement.shirt_pant_left_outseam)*2.54+ " Cm");
                    $("#final_jacket_p_left").html(parseFloat(measurement.jacket_pant_left_outseam)*2.54+ " Cm");
                    $("#final_vest_p_left").html(parseFloat(measurement.vest_pant_left_outseam)*2.54+ " Cm");
                    $("#final_pant_p_left").html(parseFloat(measurement.pant_pant_left_outseam)*2.54+ " Cm");
                    $("#final_shirt_p_right").html(parseFloat(measurement.shirt_pant_right_outseam)*2.54+ " Cm");
                    $("#final_jacket_p_right").html(parseFloat(measurement.jacket_pant_right_outseam)*2.54+ " Cm");
                    $("#final_vest_p_right").html(parseFloat(measurement.vest_pant_right_outseam)*2.54+ " Cm");
                    $("#final_pant_p_right").html(parseFloat(measurement.pant_pant_right_outseam)*2.54+ " Cm");
                    $("#final_shirt_bottom").html(parseFloat(measurement.shirt_bottom)*2.54+ " Cm");
                    $("#final_jacket_bottom").html(parseFloat(measurement.jacket_bottom)*2.54+ " Cm");
                    $("#final_vest_bottom").html(parseFloat(measurement.vest_bottom)*2.54+ " Cm");
                    $("#final_pant_bottom").html(parseFloat(measurement.pant_bottom)*2.54+ " Cm");
                }else if(measurement.unit_type == "Cm" && $("#params").val() == "Inches"){
                    $("#final_shirt_collar").html((parseFloat(measurement.shirt_collar)/2.54).toFixed(1)+ " Inch");
                    $("#final_jacket_collar").html((parseFloat(measurement.jacket_collar)/2.54).toFixed(1)+ " Inch");
                    $("#final_vest_collar").html((parseFloat(measurement.vest_collar)/2.54).toFixed(1)+ " Inch");
                    $("#final_pant_collar").html((parseFloat(measurement.pant_collar)/2.54).toFixed(1)+ " Inch");
                    $("#final_shirt_chest").html((parseFloat(measurement.shirt_chest)/2.54).toFixed(1)+ " Inch");
                    $("#final_jacket_chest").html((parseFloat(measurement.jacket_chest)/2.54).toFixed(1)+ " Inch");
                    $("#final_vest_chest").html((parseFloat(measurement.vest_chest)/2.54).toFixed(1)+ " Inch");
                    $("#final_pant_chest").html((parseFloat(measurement.pant_chest)/2.54).toFixed(1)+ " Inch");
                    $("#final_shirt_stomach").html((parseFloat(measurement.shirt_stomach)/2.54).toFixed(1)+ " Inch");
                    $("#final_jacket_stomach").html((parseFloat(measurement.jacket_stomach)/2.54).toFixed(1)+ " Inch");
                    $("#final_vest_stomach").html((parseFloat(measurement.vest_stomach)/2.54).toFixed(1)+ " Inch");
                    $("#final_pant_stomach").html((parseFloat(measurement.pant_stomach)/2.54).toFixed(1)+ " Inch");
                    $("#final_shirt_seat").html((parseFloat(measurement.shirt_seat)/2.54).toFixed(1)+ " Inch");
                    $("#final_jacket_seat").html((parseFloat(measurement.jacket_seat)/2.54).toFixed(1)+ " Inch");
                    $("#final_vest_seat").html((parseFloat(measurement.vest_seat)/2.54).toFixed(1)+ " Inch");
                    $("#final_pant_seat").html((parseFloat(measurement.pant_seat)/2.54).toFixed(1)+ " Inch");
                    $("#final_shirt_bicep").html((parseFloat(measurement.shirt_bicep)/2.54).toFixed(1)+ " Inch");
                    $("#final_jacket_bicep").html((parseFloat(measurement.jacket_bicep)/2.54).toFixed(1)+ " Inch");
                    $("#final_vest_bicep").html((parseFloat(measurement.vest_bicep)/2.54).toFixed(1)+ " Inch");
                    $("#final_pant_bicep").html((parseFloat(measurement.pant_bicep)/2.54).toFixed(1)+ " Inch");
                    $("#final_shirt_back_shoulder").html((parseFloat(measurement.shirt_back_shoulder)/2.54).toFixed(1)+ " Inch");
                    $("#final_jacket_back_shoulder").html((parseFloat(measurement.jacket_back_shoulder)/2.54).toFixed(1)+ " Inch");
                    $("#final_vest_back_shoulder").html((parseFloat(measurement.vest_back_shoulder)/2.54).toFixed(1)+ " Inch");
                    $("#final_pant_back_shoulder").html((parseFloat(measurement.pant_back_shoulder)/2.54).toFixed(1)+ " Inch");
                    $("#final_shirt_front_shoulder").html((parseFloat(measurement.shirt_front_shoulder)/2.54).toFixed(1)+ " Inch");
                    $("#final_jacket_front_shoulder").html((parseFloat(measurement.jacket_front_shoulder)/2.54).toFixed(1)+ " Inch");
                    $("#final_vest_front_shoulder").html((parseFloat(measurement.vest_front_shoulder)/2.54).toFixed(1)+ " Inch");
                    $("#final_pant_front_shoulder").html((parseFloat(measurement.pant_front_shoulder)/2.54).toFixed(1)+ " Inch");
                    $("#final_shirt_sleeve_left").html((parseFloat(measurement.shirt_sleeve_left)/2.54).toFixed(1)+ " Inch");
                    $("#final_jacket_sleeve_left").html((parseFloat(measurement.jacket_sleeve_left)/2.54).toFixed(1)+ " Inch");
                    $("#final_vest_sleeve_left").html((parseFloat(measurement.vest_sleeve_left)/2.54).toFixed(1)+ " Inch");
                    $("#final_pant_sleeve_left").html((parseFloat(measurement.pant_sleeve_left)/2.54).toFixed(1)+ " Inch");
                    $("#final_shirt_sleeve_right").html((parseFloat(measurement.shirt_sleeve_right)/2.54).toFixed(1)+ " Inch");
                    $("#final_jacket_sleeve_right").html((parseFloat(measurement.jacket_sleeve_right)/2.54).toFixed(1)+ " Inch");
                    $("#final_vest_sleeve_right").html((parseFloat(measurement.vest_sleeve_right)/2.54).toFixed(1)+ " Inch");
                    $("#final_pant_sleeve_right").html((parseFloat(measurement.pant_sleeve_right)/2.54).toFixed(1)+ " Inch");
                    $("#final_shirt_thigh").html((parseFloat(measurement.shirt_thigh)/2.54).toFixed(1)+ " Inch");
                    $("#final_jacket_thigh").html((parseFloat(measurement.jacket_thigh)/2.54).toFixed(1)+ " Inch");
                    $("#final_vest_thigh").html((parseFloat(measurement.vest_thigh)/2.54).toFixed(1)+ " Inch");
                    $("#final_pant_thigh").html((parseFloat(measurement.pant_thigh)/2.54).toFixed(1)+ " Inch");
                    $("#final_shirt_nape").html((parseFloat(measurement.shirt_nape_to_waist)/2.54).toFixed(1)+ " Inch");
                    $("#final_jacket_nape").html((parseFloat(measurement.jacket_nape_to_waist)/2.54).toFixed(1)+ " Inch");
                    $("#final_vest_nape").html((parseFloat(measurement.vest_nape_to_waist)/2.54).toFixed(1)+ " Inch");
                    $("#final_pant_nape").html((parseFloat(measurement.pant_nape_to_waist)/2.54).toFixed(1)+ " Inch");
                    $("#final_shirt_front_waist").html((parseFloat(measurement.shirt_front_waist_length)/2.54).toFixed(1)+ " Inch");
                    $("#final_jacket_front_waist").html((parseFloat(measurement.jacket_front_waist_length)/2.54).toFixed(1)+ " Inch");
                    $("#final_vest_front_waist").html((parseFloat(measurement.vest_front_waist_length)/2.54).toFixed(1)+ " Inch");
                    $("#final_pant_front_waist").html((parseFloat(measurement.pant_front_waist_length)/2.54).toFixed(1)+ " Inch");
                    $("#final_shirt_wrist").html((parseFloat(measurement.shirt_wrist)/2.54).toFixed(1)+ " Inch");
                    $("#final_jacket_wrist").html((parseFloat(measurement.jacket_wrist)/2.54).toFixed(1)+ " Inch");
                    $("#final_vest_wrist").html((parseFloat(measurement.vest_wrist)/2.54).toFixed(1)+ " Inch");
                    $("#final_pant_wrist").html((parseFloat(measurement.pant_wrist)/2.54).toFixed(1)+ " Inch");
                    $("#final_shirt_waist").html((parseFloat(measurement.shirt_waist)/2.54).toFixed(1)+ " Inch");
                    $("#final_jacket_waist").html((parseFloat(measurement.jacket_waist)/2.54).toFixed(1)+ " Inch");
                    $("#final_vest_waist").html((parseFloat(measurement.vest_waist)/2.54).toFixed(1)+ " Inch");
                    $("#final_pant_waist").html((parseFloat(measurement.pant_waist)/2.54).toFixed(1)+ " Inch");
                    $("#final_shirt_calf").html((parseFloat(measurement.shirt_calf)/2.54).toFixed(1)+ " Inch");
                    $("#final_jacket_calf").html((parseFloat(measurement.jacket_calf)/2.54).toFixed(1)+ " Inch");
                    $("#final_vest_calf").html((parseFloat(measurement.vest_calf)/2.54).toFixed(1)+ " Inch");
                    $("#final_pant_calf").html((parseFloat(measurement.pant_calf)/2.54).toFixed(1)+ " Inch");
                    $("#final_shirt_b_waist").html((parseFloat(measurement.shirt_back_waist_height)/2.54).toFixed(1)+ " Inch");
                    $("#final_jacket_b_waist").html((parseFloat(measurement.jacket_back_waist_height)/2.54).toFixed(1)+ " Inch");
                    $("#final_vest_b_waist").html((parseFloat(measurement.vest_back_waist_height)/2.54).toFixed(1)+ " Inch");
                    $("#final_pant_b_waist").html((parseFloat(measurement.pant_back_waist_height)/2.54).toFixed(1)+ " Inch");
                    $("#final_shirt_f_waist").html((parseFloat(measurement.shirt_front_waist_height)/2.54).toFixed(1)+ " Inch");
                    $("#final_jacket_f_waist").html((parseFloat(measurement.jacket_front_waist_height)/2.54).toFixed(1)+ " Inch");
                    $("#final_vest_f_waist").html((parseFloat(measurement.vest_front_waist_height)/2.54).toFixed(1)+ " Inch");
                    $("#final_pant_f_waist").html((parseFloat(measurement.pant_front_waist_height)/2.54).toFixed(1)+ " Inch");
                    $("#final_shirt_knee").html((parseFloat(measurement.shirt_knee)/2.54).toFixed(1)+ " Inch");
                    $("#final_jacket_knee").html((parseFloat(measurement.jacket_knee)/2.54).toFixed(1)+ " Inch");
                    $("#final_vest_knee").html((parseFloat(measurement.vest_knee)/2.54).toFixed(1)+ " Inch");
                    $("#final_pant_knee").html((parseFloat(measurement.pant_knee)/2.54).toFixed(1)+ " Inch");
                    $("#final_shirt_jacket").html((parseFloat(measurement.shirt_back_jacket_length)/2.54).toFixed(1)+ " Inch");
                    $("#final_jacket_back_jacket_length").html((parseFloat(measurement.jacket_back_jacket_length)/2.54).toFixed(1)+ " Inch");
                    $("#final_vest_jacket").html((parseFloat(measurement.vest_back_jacket_length)/2.54).toFixed(1)+ " Inch");
                    $("#final_pant_jacket").html((parseFloat(measurement.pant_back_jacket_length)/2.54).toFixed(1)+ " Inch");
                    $("#final_shirt_rise").html((parseFloat(measurement.shirt_u_rise)/2.54).toFixed(1)+ " Inch");
                    $("#final_jacket_rise").html((parseFloat(measurement.jacket_u_rise)/2.54).toFixed(1)+ " Inch");
                    $("#final_vest_rise").html((parseFloat(measurement.vest_u_rise)/2.54).toFixed(1)+ " Inch");
                    $("#final_pant_rise").html((parseFloat(measurement.pant_u_rise)/2.54).toFixed(1)+ " Inch");
                    $("#final_shirt_p_left").html((parseFloat(measurement.shirt_pant_left_outseam)/2.54).toFixed(1)+ " Inch");
                    $("#final_jacket_p_left").html((parseFloat(measurement.jacket_pant_left_outseam)/2.54).toFixed(1)+ " Inch");
                    $("#final_vest_p_left").html((parseFloat(measurement.vest_pant_left_outseam)/2.54).toFixed(1)+ " Inch");
                    $("#final_pant_p_left").html((parseFloat(measurement.pant_pant_left_outseam)/2.54).toFixed(1)+ " Inch");
                    $("#final_shirt_p_right").html((parseFloat(measurement.shirt_pant_right_outseam)/2.54).toFixed(1)+ " Inch");
                    $("#final_jacket_p_right").html((parseFloat(measurement.jacket_pant_right_outseam)/2.54).toFixed(1)+ " Inch");
                    $("#final_vest_p_right").html((parseFloat(measurement.vest_pant_right_outseam)/2.54).toFixed(1)+ " Inch");
                    $("#final_pant_p_right").html((parseFloat(measurement.pant_pant_right_outseam)/2.54).toFixed(1)+ " Inch");
                    $("#final_shirt_bottom").html((parseFloat(measurement.shirt_bottom)/2.54).toFixed(1)+ " Inch");
                    $("#final_jacket_bottom").html((parseFloat(measurement.jacket_bottom)/2.54).toFixed(1)+ " Inch");
                    $("#final_vest_bottom").html((parseFloat(measurement.vest_bottom)/2.54).toFixed(1)+ " Inch");
                    $("#final_pant_bottom").html((parseFloat(measurement.pant_bottom)/2.54).toFixed(1)+ " Inch");
                }else if(measurement.unit_type == "Cm" && $("#params").val() == "CM"){
                    $("#final_shirt_collar").html(parseFloat(measurement.shirt_collar)+ " Cm");
                    $("#final_jacket_collar").html(parseFloat(measurement.jacket_collar)+ " Cm");
                    $("#final_vest_collar").html(parseFloat(measurement.vest_collar)+ " Cm");
                    $("#final_pant_collar").html(parseFloat(measurement.pant_collar)+ " Cm");
                    $("#final_shirt_chest").html(parseFloat(measurement.shirt_chest)+ " Cm");
                    $("#final_jacket_chest").html(parseFloat(measurement.jacket_chest)+ " Cm");
                    $("#final_vest_chest").html(parseFloat(measurement.vest_chest)+ " Cm");
                    $("#final_pant_chest").html(parseFloat(measurement.pant_chest)+ " Cm");
                    $("#final_shirt_stomach").html(parseFloat(measurement.shirt_stomach)+ " Cm");
                    $("#final_jacket_stomach").html(parseFloat(measurement.jacket_stomach)+ " Cm");
                    $("#final_vest_stomach").html(parseFloat(measurement.vest_stomach)+ " Cm");
                    $("#final_pant_stomach").html(parseFloat(measurement.pant_stomach)+ " Cm");
                    $("#final_shirt_seat").html(parseFloat(measurement.shirt_seat)+ " Cm");
                    $("#final_jacket_seat").html(parseFloat(measurement.jacket_seat)+ " Cm");
                    $("#final_vest_seat").html(parseFloat(measurement.vest_seat)+ " Cm");
                    $("#final_pant_seat").html(parseFloat(measurement.pant_seat)+ " Cm");
                    $("#final_shirt_bicep").html(parseFloat(measurement.shirt_bicep)+ " Cm");
                    $("#final_jacket_bicep").html(parseFloat(measurement.jacket_bicep)+ " Cm");
                    $("#final_vest_bicep").html(parseFloat(measurement.vest_bicep)+ " Cm");
                    $("#final_pant_bicep").html(parseFloat(measurement.pant_bicep)+ " Cm");
                    $("#final_shirt_back_shoulder").html(parseFloat(measurement.shirt_back_shoulder)+ " Cm");
                    $("#final_jacket_back_shoulder").html(parseFloat(measurement.jacket_back_shoulder)+ " Cm");
                    $("#final_vest_back_shoulder").html(parseFloat(measurement.vest_back_shoulder)+ " Cm");
                    $("#final_pant_back_shoulder").html(parseFloat(measurement.pant_back_shoulder)+ " Cm");
                    $("#final_shirt_front_shoulder").html(parseFloat(measurement.shirt_front_shoulder)+ " Cm");
                    $("#final_jacket_front_shoulder").html(parseFloat(measurement.jacket_front_shoulder)+ " Cm");
                    $("#final_vest_front_shoulder").html(parseFloat(measurement.vest_front_shoulder)+ " Cm");
                    $("#final_pant_front_shoulder").html(parseFloat(measurement.pant_front_shoulder)+ " Cm");
                    $("#final_shirt_sleeve_left").html(parseFloat(measurement.shirt_sleeve_left)+ " Cm");
                    $("#final_jacket_sleeve_left").html(parseFloat(measurement.jacket_sleeve_left)+ " Cm");
                    $("#final_vest_sleeve_left").html(parseFloat(measurement.vest_sleeve_left)+ " Cm");
                    $("#final_pant_sleeve_left").html(parseFloat(measurement.pant_sleeve_left)+ " Cm");
                    $("#final_shirt_sleeve_right").html(parseFloat(measurement.shirt_sleeve_right)+ " Cm");
                    $("#final_jacket_sleeve_right").html(parseFloat(measurement.jacket_sleeve_right)+ " Cm");
                    $("#final_vest_sleeve_right").html(parseFloat(measurement.vest_sleeve_right)+ " Cm");
                    $("#final_pant_sleeve_right").html(parseFloat(measurement.pant_sleeve_right)+ " Cm");
                    $("#final_shirt_thigh").html(parseFloat(measurement.shirt_thigh)+ " Cm");
                    $("#final_jacket_thigh").html(parseFloat(measurement.jacket_thigh)+ " Cm");
                    $("#final_vest_thigh").html(parseFloat(measurement.vest_thigh)+ " Cm");
                    $("#final_pant_thigh").html(parseFloat(measurement.pant_thigh)+ " Cm");
                    $("#final_shirt_nape").html(parseFloat(measurement.shirt_nape_to_waist)+ " Cm");
                    $("#final_jacket_nape").html(parseFloat(measurement.jacket_nape_to_waist)+ " Cm");
                    $("#final_vest_nape").html(parseFloat(measurement.vest_nape_to_waist)+ " Cm");
                    $("#final_pant_nape").html(parseFloat(measurement.pant_nape_to_waist)+ " Cm");
                    $("#final_shirt_front_waist").html(parseFloat(measurement.shirt_front_waist_length)+ " Cm");
                    $("#final_jacket_front_waist").html(parseFloat(measurement.jacket_front_waist_length)+ " Cm");
                    $("#final_vest_front_waist").html(parseFloat(measurement.vest_front_waist_length)+ " Cm");
                    $("#final_pant_front_waist").html(parseFloat(measurement.pant_front_waist_length)+ " Cm");
                    $("#final_shirt_wrist").html(parseFloat(measurement.shirt_wrist)+ " Cm");
                    $("#final_jacket_wrist").html(parseFloat(measurement.jacket_wrist)+ " Cm");
                    $("#final_vest_wrist").html(parseFloat(measurement.vest_wrist)+ " Cm");
                    $("#final_pant_wrist").html(parseFloat(measurement.pant_wrist)+ " Cm");
                    $("#final_shirt_waist").html(parseFloat(measurement.shirt_waist)+ " Cm");
                    $("#final_jacket_waist").html(parseFloat(measurement.jacket_waist)+ " Cm");
                    $("#final_vest_waist").html(parseFloat(measurement.vest_waist)+ " Cm");
                    $("#final_pant_waist").html(parseFloat(measurement.pant_waist)+ " Cm");
                    $("#final_shirt_calf").html(parseFloat(measurement.shirt_calf)+ " Cm");
                    $("#final_jacket_calf").html(parseFloat(measurement.jacket_calf)+ " Cm");
                    $("#final_vest_calf").html(parseFloat(measurement.vest_calf)+ " Cm");
                    $("#final_pant_calf").html(parseFloat(measurement.pant_calf)+ " Cm");
                    $("#final_shirt_b_waist").html(parseFloat(measurement.shirt_back_waist_height)+ " Cm");
                    $("#final_jacket_b_waist").html(parseFloat(measurement.jacket_back_waist_height)+ " Cm");
                    $("#final_vest_b_waist").html(parseFloat(measurement.vest_back_waist_height)+ " Cm");
                    $("#final_pant_b_waist").html(parseFloat(measurement.pant_back_waist_height)+ " Cm");
                    $("#final_shirt_f_waist").html(parseFloat(measurement.shirt_front_waist_height)+ " Cm");
                    $("#final_jacket_f_waist").html(parseFloat(measurement.jacket_front_waist_height)+ " Cm");
                    $("#final_vest_f_waist").html(parseFloat(measurement.vest_front_waist_height)+ " Cm");
                    $("#final_pant_f_waist").html(parseFloat(measurement.pant_front_waist_height)+ " Cm");
                    $("#final_shirt_knee").html(parseFloat(measurement.shirt_knee)+ " Cm");
                    $("#final_jacket_knee").html(parseFloat(measurement.jacket_knee)+ " Cm");
                    $("#final_vest_knee").html(parseFloat(measurement.vest_knee)+ " Cm");
                    $("#final_pant_knee").html(parseFloat(measurement.pant_knee)+ " Cm");
                    $("#final_shirt_jacket").html(parseFloat(measurement.shirt_back_jacket_length)+ " Cm");
                    $("#final_jacket_back_jacket_length").html(parseFloat(measurement.jacket_back_jacket_length)+ " Cm");
                    $("#final_vest_jacket").html(parseFloat(measurement.vest_back_jacket_length)+ " Cm");
                    $("#final_pant_jacket").html(parseFloat(measurement.pant_back_jacket_length)+ " Cm");
                    $("#final_shirt_rise").html(parseFloat(measurement.shirt_u_rise)+ " Cm");
                    $("#final_jacket_rise").html(parseFloat(measurement.jacket_u_rise)+ " Cm");
                    $("#final_vest_rise").html(parseFloat(measurement.vest_u_rise)+ " Cm");
                    $("#final_pant_rise").html(parseFloat(measurement.pant_u_rise)+ " Cm");
                    $("#final_shirt_p_left").html(parseFloat(measurement.shirt_pant_left_outseam)+ " Cm");
                    $("#final_jacket_p_left").html(parseFloat(measurement.jacket_pant_left_outseam)+ " Cm");
                    $("#final_vest_p_left").html(parseFloat(measurement.vest_pant_left_outseam)+ " Cm");
                    $("#final_pant_p_left").html(parseFloat(measurement.pant_pant_left_outseam)+ " Cm");
                    $("#final_shirt_p_right").html(parseFloat(measurement.shirt_pant_right_outseam)+ " Cm");
                    $("#final_jacket_p_right").html(parseFloat(measurement.jacket_pant_right_outseam)+ " Cm");
                    $("#final_vest_p_right").html(parseFloat(measurement.vest_pant_right_outseam)+ " Cm");
                    $("#final_pant_p_right").html(parseFloat(measurement.pant_pant_right_outseam)+ " Cm");
                    $("#final_shirt_bottom").html(parseFloat(measurement.shirt_bottom)+ " Cm");
                    $("#final_jacket_bottom").html(parseFloat(measurement.jacket_bottom)+ " Cm");
                    $("#final_vest_bottom").html(parseFloat(measurement.vest_bottom)+ " Cm");
                    $("#final_pant_bottom").html(parseFloat(measurement.pant_bottom)+ " Cm");
                }
                $(".loaderdiv").removeClass("block");
                $(".loaderdiv").addClass("loaderhidden");
            }else{
                showMessage("Invalid Measurement Data","red");
            }
        });
    }
    getData();
</script>