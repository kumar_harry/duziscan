<?php
include("header2.php");
if(isset($_SESSION['duzi_user_id'])) {
}
else{
    header("Location:index");
}
?>
    <link rel="icon" type="image/icon" href="">
    <link rel='stylesheet prefetch' href='https://fonts.googleapis.com/css?family=Open+Sans:600'>
    <link rel="stylesheet" href="css/bootstrap.css">
    <link rel="stylesheet" href="css/dist/sidebar-menu.css">
    <link rel="stylesheet" href="css/readymade.css">
    <link rel="stylesheet" href="css/dist/custom.css">
    <style>
        html::-webkit-scrollbar-track {
            -webkit-box-shadow: inset 0 0 6px rgba(0, 0, 0, 0.3);
            border-radius: 10px;
            background-color: transparent;
        }
        html::-webkit-scrollbar {
            width: 12px;
            background-color: #F5F5F5;
        }
        html::-webkit-scrollbar-thumb {
            border-radius: 10px;
            -webkit-box-shadow: inset 0 0 6px rgba(0, 0, 0, .3);
            background-color: #8c939d ! important;
        }
        #avatar {
            position: absolute;
            top: -7px;
        }
        .shirt_fit_list {
            position: absolute
        }
        /*progress bar start here*/
        .progdiv {
            left: 40%;
            position: absolute;
            top: 50%;
            width: 30%;
            display: none;
            z-index: 9999999;
        }
        .pload {
            left: 40%;
            position: absolute;
            top: 46%;
            width: 30%;
            display: none;
            text-align: center;
        }
        .content_div{
            width: 100%;
        }
        .left_menu{
            width:11%;
            float: left;
            height: 100%;
        }
        .right_canvas{
            float: left;
            width:88%;
            height: 600px;
            margin-top:-100px;
        }
        .w3_breadcrumb
        {
            position: absolute;
            width: 100%;
        }
        .content_div{
            margin-top: 50px;
        }
        .breadcrumb-inner > ul > li > a{
            color: #111!important;
        }
        .breadcrumb-inner > ul > li {
            letter-spacing: 2px;
            font-size: 16px;
        }
        .modal{
            z-index: 99999999!important;
        }
        body
        {
            overflow-x: hidden;
        }


    </style>

    <div class="w3_breadcrumb">
        <div class="breadcrumb-inner">
            <ul>
                <li><a href="index">Home</a> <i> /</i></li>
                <li>Order Now</li>
            </ul>
        </div>
    </div>
<!--/*progress bar start here*/-->
<p class="pload">Please Wait ... Object Is Loading</p>
<div class="progress progdiv">
    <div id="prog" class="progress-bar progress-bar-striped active" role="progressbar" aria-valuenow="40"
         aria-valuemin="0" aria-valuemax="100" style="width:0%">
        0%
    </div>
</div>

    <input type="button" value="Add to Cart" onclick="addToCart()" class="cartbtn" style="display:none;">

<div class="content_div">

    <div class="left_menu">
        <ul class="sidebar-menu">
            <li class=" 1">
                    <a  id="fit" onclick="">
                        <img class="side_imge  img_style" src="images/style/Style01.jpg"/ >
                        <p class="side_img_font">Style</p>
                    </a>
                    <ul class="sidebar-submenu">
                        <li id="StyleOne" onclick="meshChange(this.id);" class="inner_li style active">
                            <i class="fa fa-check-circle-o styletick" id="StyleOnetick" ></i>
                            <a>
                                <img class="side_img " src="images/style/Style01.jpg"/>
                                <p class="side_img_font"> Style 1</p>
                            </a>
                        </li>
                        <li id="StyleTwo" onclick="meshChange(this.id);" class="inner_li style">
                            <i class="fa fa-check-circle-o styletick" id="StyleTwotick" style="display: none"></i>
                            <a>
                                <img class="side_img " src="images/style/Style02.jpg"/>
                                <p class="side_img_font"> Style 2</p>
                            </a>
                        </li>
                        <li id="StyleThree" onclick="meshChange(this.id);" class="inner_li style">
                            <i class="fa fa-check-circle-o styletick" id="StyleThreetick" style="display: none"></i>
                            <a>
                                <img class="side_img " src="images/style/Style03.jpg"/>
                                <p class="side_img_font"> Style 3</p>
                            </a>
                        </li>
                        <li id="StyleFour" onclick="meshChange(this.id);" class="inner_li style">
                            <i class="fa fa-check-circle-o styletick" id="StyleFourtick" style="display: none"></i>
                            <a>
                                <img class="side_img " src="images/style/Style04.jpg"/>
                                <p class="side_img_font"> Style 4</p>
                            </a>
                        </li>
                        <li id="StyleFive" onclick="meshChange(this.id);" class="inner_li style">
                            <i class="fa fa-check-circle-o styletick" id="StyleFivetick" style="display: none"></i>
                            <a>
                                <img class="side_img " src="images/style/Style05.jpg"/>
                                <p class="side_img_font">Style 5</p>
                            </a>
                        </li>
                        <li id="StyleSix" onclick="meshChange(this.id);" class="inner_li style">
                            <i class="fa fa-check-circle-o styletick" id="StyleSixtick" style="display: none"></i>
                            <a>
                                <img class="side_img " src="images/style/Style06.jpg"/>
                                <p class="side_img_font">Style 6</p>
                            </a>
                        </li>
                    </ul>
                </li>
            <li >
                    <a class=" 3"  id="fabric" onclick="">
                        <img class="side_imge  img_fabric" src="images/fabric/F1.jpg"/>
                        <p class="side_img_font">Fabric</p>
                    </a>
                    <ul class="sidebar-submenu">
                        <li id="0" onclick="textureChange(this.id);" class="inner_li fabric blue green check">
                            <i class="fa fa-check-circle-o fabrictick" id="fabrictick0" ></i>
                            <a>
                                <img class="side_img" src="images/fabric/F1.jpg"/>
                                <p class="side_img_font">Fabric 1</p>
                            </a>
                        </li>
                        <li id="1" onclick="textureChange(this.id);" class="inner_li fabric blue purple check">
                            <i class="fa fa-check-circle-o fabrictick" id="fabrictick1" style="display: none"></i>
                            <a>
                                <img class="side_img" src="images/fabric/F2.jpg"/>
                                <p class="side_img_font">Fabric 2</p>
                            </a>
                        </li>
                        <li id="2" onclick="textureChange(this.id);" class="inner_li fabric orange check">
                            <i class="fa fa-check-circle-o fabrictick" id="fabrictick2" style="display: none"></i>
                            <a>
                                <img class="side_img" src="images/fabric/F3.jpg"/>
                                <p class="side_img_font">Fabric 3</p>
                            </a>
                        </li>
                        <li id="3" onclick="textureChange(this.id);" class="inner_li fabric blue check">
                            <i class="fa fa-check-circle-o fabrictick" id="fabrictick3" style="display: none"></i>
                            <a>
                                <img class="side_img" src="images/fabric/F4.jpg"/>
                                <p class="side_img_font">Fabric 4</p>
                            </a>
                        </li>
                        <li id="4" onclick="textureChange(this.id);" class="inner_li fabric green purple strip" >
                            <i class="fa fa-check-circle-o fabrictick" id="fabrictick4" style="display: none"></i>
                            <a>
                                <img class="side_img" src="images/fabric/F5.jpg"/>
                                <p class="side_img_font">Fabric 5</p>
                            </a>
                        </li>
                        <li id="5" onclick="textureChange(this.id);" class="inner_li fabric red purple strip">
                            <i class="fa fa-check-circle-o fabrictick" id="fabrictick5" style="display: none"></i>
                            <a>
                                <img class="side_img" src="images/fabric/F6.jpg"/>
                                <p class="side_img_font">Fabric 6</p>
                            </a>
                        </li>
                        <li id="6" onclick="textureChange(this.id);" class="inner_li fabric pink check">
                            <i class="fa fa-check-circle-o fabrictick" id="fabrictick6" style="display: none"></i>
                            <a>
                                <img class="side_img" src="images/fabric/F7.jpg"/>
                                <p class="side_img_font">Fabric 7</p>
                            </a>
                        </li>
                        <li id="7" onclick="textureChange(this.id);" class="inner_li fabric blue check">
                            <i class="fa fa-check-circle-o fabrictick" id="fabrictick7" style="display: none"></i>
                            <a>
                                <img class="side_img" src="images/fabric/F8.jpg"/>
                                <p class="side_img_font">Fabric 8</p>
                            </a>
                        </li>
                        <li id="8" onclick="textureChange(this.id);" class="inner_li fabric white strip">
                            <i class="fa fa-check-circle-o fabrictick" id="fabrictick8" style="display: none"></i>
                            <a>
                                <img class="side_img" src="images/fabric/F9.jpg"/>
                                <p class="side_img_font">Fabric 9</p>
                            </a>
                        </li>
                        <li id="9" onclick="textureChange(this.id);" class="inner_li fabric white yellow strip">
                            <i class="fa fa-check-circle-o fabrictick" id="fabrictick9" style="display: none"></i>
                            <a>
                                <img class="side_img" src="images/fabric/F10.jpg"/>
                                <p class="side_img_font">Fabric 10</p>
                            </a>
                        </li>
                        <li id="10" onclick="textureChange(this.id);" class="inner_li fabric orange blue purple check">
                            <i class="fa fa-check-circle-o fabrictick" id="fabrictick10" style="display: none"></i>
                            <a>
                                <img class="side_img" src="images/fabric/F11.jpg"/>
                                <p class="side_img_font">Fabric 11</p>
                            </a>
                        </li>
                        <li id="11" onclick="textureChange(this.id);" class="inner_li fabric purple green blue check">
                            <i class="fa fa-check-circle-o fabrictick" id="fabrictick11" style="display: none"></i>
                            <a>
                                <img class="side_img" src="images/fabric/F12.jpg"/>
                                <p class="side_img_font">Fabric 12</p>
                            </a>
                        </li>
                        <li id="12" onclick="textureChange(this.id);" class="inner_li fabric orange red white check">
                            <i class="fa fa-check-circle-o fabrictick" id="fabrictick12" style="display: none"></i>
                            <a>
                                <img class="side_img" src="images/fabric/F13.jpg"/>
                                <p class="side_img_font">Fabric 13</p>
                            </a>
                        </li>
                        <li id="13" onclick="textureChange(this.id);" class="inner_li fabric purple green blue check">
                            <i class="fa fa-check-circle-o fabrictick" id="fabrictick13" style="display: none"></i>
                            <a>
                                <img class="side_img" src="images/fabric/F14.jpg"/>
                                <p class="side_img_font">Fabric 14</p>
                            </a>
                        </li>
                        <li id="14" onclick="textureChange(this.id);" class="inner_li fabric orange red white check">
                            <i class="fa fa-check-circle-o fabrictick" id="fabrictick14" style="display: none"></i>
                            <a>
                                <img class="side_img" src="images/fabric/F15.jpg"/>
                                <p class="side_img_font">Fabric 15</p>
                            </a>
                        </li>
                        <li id="15" onclick="textureChange(this.id);" class="inner_li fabric orange red white check">
                            <i class="fa fa-check-circle-o fabrictick" id="fabrictick15" style="display: none"></i>
                            <a>
                                <img class="side_img" src="images/fabric/F16.jpg"/>
                                <p class="side_img_font">Fabric 16</p>
                            </a>
                        </li>
                        <li id="16" onclick="textureChange(this.id);" class="inner_li fabric purple green blue check">
                            <i class="fa fa-check-circle-o fabrictick" id="fabrictick16" style="display: none"></i>
                            <a>
                                <img class="side_img" src="images/fabric/F17.jpg"/>
                                <p class="side_img_font">Fabric 17</p>
                            </a>
                        </li>
                        <li id="17" onclick="textureChange(this.id);" class="inner_li fabric orange red white check">
                            <i class="fa fa-check-circle-o fabrictick" id="fabrictick17" style="display: none"></i>
                            <a>
                                <img class="side_img" src="images/fabric/F18.jpg"/>
                                <p class="side_img_font">Fabric 18</p>
                            </a>
                        </li>
                    </ul>
                </li>
                <li class=" 5">
                    <a  id="mani" onclick="">
                        <img class="side_imge  img_manneqine" src="images/manneqine/M1.jpg"/>
                        <p class="side_img_font">mannequin</p>
                        <div class="manq">
                            <input id="visib" onclick="visi();" type="radio" name="checkbox" checked>
                                <i class="chck"></i>Yes
                            <input id="hidd" onclick="hide();" type="radio" style="margin-left: 26px;" name="checkbox">
                                <i class="chck"></i>No
                        </div>
                </li>
                <li id="mannequ" class="manneq">
                    <a  id="mani" onclick="">
                        <img class="side_imge  img_manneqine" src="images/manneqine/M1.jpg"/>
                        <p class="side_img_font">Skin color</p>

                    </a>
                    <ul class="sidebar-submenu">
                        <li id="2d2d2d" onclick="manneColor(this.id);" class="inner_li manneqine" title="manneqine|M1.jpg">
                            <i class="fa fa-check-circle-o skinstick" id="skinstick2d2d2d" ></i>
                            <a>
                                <img  class="side_img" src="images/manneqine/M1.jpg"/>
                                <p class="side_img_font"> Skin 1</p>
                            </a>
                        </li>
                        <li id="996e26" onclick="manneColor(this.id);" class="inner_li manneqine" title="manneqine|M4.jpg">
                            <i class="fa fa-check-circle-o skinstick" id="skinstick996e26" style="display: none"></i>
                            <a>
                                <img class="side_img" src="images/manneqine/M4.jpg"/>
                                <p class="side_img_font"> Skin 2</p>
                            </a>
                        </li>
                        <li id="d9d9b8" onclick="manneColor(this.id);" class="inner_li manneqine" title="manneqine|M3.jpg">
                            <i class="fa fa-check-circle-o skinstick" id="skinstickd9d9b8" style="display: none"></i>
                            <a>
                                <img class="side_img" src="images/manneqine/M3.jpg"/>
                                <p class="side_img_font"> Skin 3</p>
                            </a>
                        </li>
                        <li id="e6bc67" onclick="manneColor(this.id);" class="inner_li manneqine" title="manneqine|M2.jpg">
                            <i class="fa fa-check-circle-o skinstick" id="skinsticke6bc67" style="display: none"></i>
                            <a>
                                <img class="side_img" src="images/manneqine/M2.jpg"/>
                                <p class="side_img_font"> Skin 4</p>
                            </a>
                        </li>
                        <li id="997d45" onclick="manneColor(this.id);" class="inner_li manneqine" title="manneqine|M5.jpg">
                            <i class="fa fa-check-circle-o skinstick" id="skinstick997d45" style="display: none"></i>
                            <a>
                                <img class="side_img" src="images/manneqine/M5.jpg"/>
                                <p class="side_img_font"> Skin 5</p>
                            </a>
                        </li>
                    </ul>
                </li>
        </ul>
    </div>
    <div class="right_canvas" id="loadCanvas">

    </div>
</div>
<script src="js/jquery-2.1.4.min.js"></script>
<script src="css/dist/sidebar-menu.js"></script>
<script>
    $.sidebarMenu($('.sidebar-menu'))
</script>
<script>
    $('#hidd').click(function () {
        $('#mannequ').hide();
    });
    $('#visib').click(function () {
        $('#mannequ').show();
    });
</script>
<script src="js/config/libs/TweenMax.js"></script>
<script src="js/config/libs/three.min.js"></script>
<script src="js/config/libs/OrbitControls.js"></script>
<script src="js/config/libs/CTMLoader.js"></script>
<script src="js/config/swim_JSON.js"></script>
<script src="js/config/swim_Config.js"></script>
<div style="clear: both"></div>
<?php
    include("footer.php");
?>