<?php include("header2.php"); ?>
    <!-- breadcrumb -->
    <div class="w3_breadcrumb">
        <div class="breadcrumb-inner">
            <ul>
                <li><a href="index">Home</a> <i> /</i></li>
                <li>Privacy Policy</li>
            </ul>
        </div>
    </div>
    <!-- //breadcrumb -->
    <!--/content-inner-section-->
    <div class="w3_content_agilleinfo_inner">
        <div class="container">
            <div class="inner-agile-w3l-part-head">
                <h2 class="w3l-inner-h-title">Privacy Policy</h2>
            </div>
            <div class="ab-w3l-spa">
                <p>
                   This Privacy Policy governs the manner in which duziscan.com collects, uses, maintains and discloses information collected from users (each, a "User") of the duziscan.com website ("Site"). This privacy policy applies to the Site and all products and services offered by duziscan.com.</p>

<h3 class="founder-heading">Personal identification information</h3>
</br></br>
<p>We may collect personal identification information from Users in a variety of ways, including, but not limited to, when Users visit our site, register on the site, place an order, subscribe to the newsletter, and in connection with other activities, services, features or resources we make available on our Site. Users may be asked for, as appropriate, name, email address. We will collect personal identification information from Users only if they voluntarily submit such information to us. Users can always refuse to supply personally identification information, except that it may prevent them from engaging in certain Site related activities.</p>
</br></br>
<h3 class="founder-heading">Non-personal identification information</h3>
</br></br>
<p>We may collect non-personal identification information about Users whenever they interact with our Site. Non-personal identification information may include the browser name, the type of computer and technical information about Users means of connection to our Site, such as the operating system and the Internet service providers utilized and other similar information.</p>
</br></br>
<h3 class="founder-heading">Web browser cookies</h3>
</br></br>
<p>Our Site may use "cookies" to enhance User experience. User’s web browser places cookies on their hard drive for record-keeping purposes and sometimes to track information about them. User may choose to set their web browser to refuse cookies, or to alert you when cookies are being sent. If they do so, note that some parts of the Site may not function properly.</p>
</br></br>
<h3 class="founder-heading">How we use collected information</h3>
</br></br>
<p>duziscan.com may collect and use Users personal information for the following purposes:

– To improve customer service
Information you provide helps us respond to your customer service requests and support needs more efficiently.
– To personalize user experience
We may use information in the aggregate to understand how our Users as a group use the services and resources provided on our Site.
– To improve our Site
We may use feedback you provide to improve our products and services.
– To send periodic emails
We may use the email address to send User information and updates pertaining to their order. It may also be used to respond to their inquiries, questions, and/or other requests. If User decides to opt-in to our mailing list, they will receive emails that may include company news, updates, related product or service information, etc. If at any time the User would like to unsubscribe from receiving future emails, we include detailed unsubscribe instructions at the bottom of each email.</p>
</br></br>
<h3 class="founder-heading">How we protect your information</h3>
</br></br>
<p>We adopt appropriate data collection, storage and processing practices and security measures to protect against unauthorized access, alteration, disclosure or destruction of your personal information, username, password, transaction information and data stored on our Site.

Our Site is in compliance with PCI vulnerability standards in order to create as secure of an environment as possible for Users.</p>
</br></br>
<h3 class="founder-heading">Sharing your personal information</h3>
</br></br>
<p>We do not sell, trade, or rent Users personal identification information to others. We may share generic aggregated demographic information not linked to any personal identification information regarding visitors and users with our business partners, trusted affiliates and advertisers for the purposes outlined above.We may use third party service providers to help us operate our business and the Site or administer activities on our behalf, such as sending out newsletters or surveys. We may share your information with these third parties for those limited purposes provided that you have given us your permission.</p>
</br></br>
<h3 class="founder-heading">Advertising</h3>
</br></br>
<p>Ads appearing on our site may be delivered to Users by advertising partners, who may set cookies. These cookies allow the ad server to recognize your computer each time they send you an online advertisement to compile non personal identification information about you or others who use your computer. This information allows ad networks to, among other things, deliver targeted advertisements that they believe will be of most interest to you. This privacy policy does not cover the use of cookies by any advertisers.</p>

</br></br>
<h3 class="founder-heading">Changes to this privacy policy</h3>
</br></br>
<p>duziscan.com has the discretion to update this privacy policy at any time. When we do, we will revise the updated date at the bottom of this page and send you an email. We encourage Users to frequently check this page for any changes to stay informed about how we are helping to protect the personal information we collect. You acknowledge and agree that it is your responsibility to review this privacy policy periodically and become aware of modifications.</p>
</br></br>
<h3 class="founder-heading">Your acceptance of these terms</h3>
</br></br>
<p>By using this Site, you signify your acceptance of this policy and terms of service. If you do not agree to this policy, please do not use our Site. Your continued use of the Site following the posting of changes to this policy will be deemed your acceptance of those changes.</p>
			   
</br></br></br></br>
            </div>
        </div>
    </div>

    <div class="clearfix"></div>
    </div>
    </div>
    </div>
    <div class="forSpace"></div>
    <!--//content-inner-section-->

<?php include("footer.php"); ?>