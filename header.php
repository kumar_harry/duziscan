<?php
session_start();
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <title>DuziScan</title>
    <style>
        #logoSiteHeader {
            width: 110px;
        }
    </style>
    <!-- for-mobile-apps -->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <script type="application/x-javascript"> addEventListener("load", function () {
            setTimeout(hideURLbar, 0);
        }, false);

        function hideURLbar() {
            window.scrollTo(0, 1);
        } </script>
    <!-- //for-mobile-apps -->
    <link href="css/bootstrap.css" rel="stylesheet" type="text/css" media="all"/>
    <!-- pop-up -->
    <link href="css/popuo-box.css" rel="stylesheet" type="text/css" media="all"/>
    <!-- //pop-up -->
    <link rel="stylesheet" type="text/css" href="css/zoomslider.css"/>
    <link rel="stylesheet" type="text/css" href="css/style.css"/>
    <link rel="stylesheet" type="text/css" href="css/duzi-scan.css"/>
    <link href="css/font-awesome.css" rel="stylesheet">
    <!--/web-fonts-->
    <link href='//fonts.googleapis.com/css?family=Tangerine:400,700' rel='stylesheet' type='text/css'>
    <link href="//fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i" rel="stylesheet">
    <link href='//fonts.googleapis.com/css?family=Montserrat:400,700' rel='stylesheet' type='text/css'>
    <!-- for datepicker  -->
    <!-- <link rel="stylesheet" href="css/bootstrap-datetimepicker.min.css" />
    <script type="text/javascript" src="js/bootstrap-datetimepicker.js" charset="UTF-8"></script>
    <script type='text/javascript' src='js/location.js'></script> -->

    <!--//web-fonts-->
</head>
<body>
<div id="loadd" class="loader hideloader">
    <img class="loaderimg" src="images/762.gif">
    <p style="margin-left:49%;font-size:18px">Loading....</p>
</div>
<!--/main-header-->
<!--/banner-section-->
<div id="demo-1"
     data-zs-src='["images/web/shutterstock_86232880.jpg", "images/web/shutterstock_411057478.jpg", "images/web/shutterstock_519815515.jpg", "images/web/shutterstock_254911351.jpg"]'
     data-zs-overlay="dots">
    <div class="demo-inner-content">
        <!--/header-w3l-->
        <div class="header-w3-agileits" id="home">
            <div class="inner-header-agile">
                <nav class="navbar navbar-default">
                    <div class="navbar-header">
                        <button type="button" class="navbar-toggle" data-toggle="collapse"
                                data-target="#bs-example-navbar-collapse-1">
                            <span class="sr-only">Toggle navigation</span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                        </button>
                        <h1>
                            <img src="images/logo_duziscan.png" class="img-responsive" id="logoSiteHeader">
                        </h1>
                    </div>
                    <!-- navbar-header -->
                    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                        <ul class="nav navbar-nav">
                            <li class="home active"><a href="index">Home</a></li>
                            <li class="about"><a href="about">About Us</a></li>
                            <?php
                            if (isset($_SESSION['duzi_user_id'])) {
                                ?>
                                <li class="order"><a href="orders">Order Now</a></li>
                                <?php
                            } else {
                                ?>
                                <li class="order">
                                    <a href="#" onclick="reuestFor('orders')" data-toggle="modal" data-target="#loginModal">Order Now</a>
                                </li>
                                <?php
                            }
                            ?>
                            <li class="contact"><a href="contact">Contact Us</a></li>
                            <?php
                            if (isset($_SESSION['duzi_user_id'])) {
                                ?>
                                <li class="user_profile_li">
                                    <img class="user_profile_img" src="" alt="p">
                                </li>

                                <li class="dropdown " style="margin-left: 0px">
                                    <a href="#" class="dropdown-toggle hvr-bounce-to-bottom user_profile_ancor"
                                       data-toggle="dropdown" aria-expanded="false">
                                        <span class="top_user_name">Sign In</span>
                                        <b class="caret"></b>
                                    </a>
                                    <ul class="dropdown-menu">
                                        <li><a href="myaccount">My Account</a></li>
                                        <li><a href="myorders">My Orders</a></li>
                                        <li><a href="signout">Sign Out</a></li>
                                    </ul>
                                </li>
                                <li>
                                    <div id="sb-search" onclick="rediredTo('cart')">
                                        <span class="sb-icon-search">
                                            <span class="bagde">0</span>
                                        </span>
                                    </div>
                                </li>
                                <?php
                            } else {
                                ?>
                                <li class="signin">
                                    <a href="#" class="header-li-a" data-toggle="modal" data-target="#loginModal">
                                        Sign In
                                    </a>
                                </li>
                                <?php
                            }
                            ?>
                        </ul>


                    </div>
                    <div class="clearfix"></div>
                </nav>
            </div>
            <!--//header-w3l-->
            <!--/banner-info-->
            <div class="baner-info">
                <h3><span>You </span> Always <span> Have </span> A <span> Choice </span></h3>
                <h4>Make it Simple But Significant</h4>

            </div>
            <!--/banner-ingo-->

        </div>
    </div>
</div>
<!--/banner-section-->
<!--//main-header-->