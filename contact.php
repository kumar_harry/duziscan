<?php include("header2.php"); ?>
    <!-- breadcrumb -->
    <div class="w3_breadcrumb">
        <div class="breadcrumb-inner">
            <ul>
                <li><a href="index">Home</a> <i>//</i></li>

                <li>Contact</li>
            </ul>
        </div>
    </div>
    <!-- //breadcrumb -->
    <!--/content-inner-section-->
    <div class="w3_content_agilleinfo_inner">
        <div class="container">
            <div class="inner-agile-w3l-part-head">
                <h2 class="w3l-inner-h-title">Contact</h2>
            </div>
            <div class="w3_mail_grids">
                <form action="#" method="post">
                    <div class="col-md-6 w3_agile_mail_grid">
                        <input type="text" placeholder="Your Name" required="">
                        <input type="email" placeholder="Your Email" required="">
                        <input type="text" placeholder="Your Phone Number" required="">


                    </div>
                    <div class="col-md-6 w3_agile_mail_grid">
                        <textarea name="Message" placeholder="Your Message" required=""></textarea>
                        <input type="submit" value="Submit">
                    </div>
                    <div class="clearfix"></div>
                </form>
            </div>
        </div>
        <div class="map" id="map">
            <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3303.4953290561525!2d-84.14900478529061!3d34.10806682223469!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x88f59bd4709d9fc9%3A0x3c540c616f4a500a!2s3310+Ashton+Dr%2C+Suwanee%2C+GA+30024%2C+USA!5e0!3m2!1sen!2sin!4v1500995107267"></iframe>
        </div>
    </div>
    <!--//content-inner-section-->
    <div class="w3l_contact-bottom">
        <div class="container">

            <div class="w3ls_con_grids">

                <div class="w3ls_footer_grid">
                    <div class="col-md-4 con-ions-left">
                        <div class="con-ions-left-w3l">
                            <i class="fa fa-map-marker" aria-hidden="true"></i>
                        </div>
                        <div class="con-grid-w3l-leftr">
                            <h4>Location</h4>
                            <p>3030 New York, NY, USA</p>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                    <div class="col-md-4 con-ions-left">
                        <div class="con-ions-left-w3l">
                            <i class="fa fa-envelope" aria-hidden="true"></i>
                        </div>
                        <div class="con-grid-w3l-leftr">
                            <h4>Email</h4>
                            <a href="mailto:info@example.com">info@example.com</a>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                    <div class="col-md-4 con-ions-left">
                        <div class="con-ions-left-w3l">
                            <i class="fa fa-phone" aria-hidden="true"></i>
                        </div>
                        <div class="con-grid-w3l-leftr">
                            <h4>Call Us</h4>
                            <p>(+000) 003 003 0052</p>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                    <div class="clearfix"></div>
                </div>
            </div>
        </div>
    </div>


<?php include("footer.php"); ?>