<?php
include('header2.php');
$paypal_url = 'https://www.paypal.com/cgi-bin/webscr'; //Test PayPal API URL
$paypal_id = 'kapillikes@gmail.com'; //Business Email
//$paypal_id = 'sksh3527@gmail.com';
?>


<style>
.top-bar {
    background: rgba(0, 0, 0, 0.50) none repeat scroll 0 0;
}
.img {
    height: 150px;
    position: absolute;
    width:auto !important;
    margin-top:20px;
}
.nodata {
    color: #002856;
    font-size: 19px;
    font-weight: bold;
    text-align: center;
    text-shadow: 10px 9px 3px #ccc;
}
.active {
    border: 3px solid #000;
}
table {
    font-family: arial, sans-serif;
    border-collapse: collapse;
    width: 100%;
}
td{
    max-width: 100px;
    overflow-wrap: break-word;
}
td, th {
    border: 1px solid #dddddd;
    font-size: 12px;
    letter-spacing: 1.2px;
    padding: 1.4px 16px;
    text-align: left;
}
tr:nth-child(even) {
    background-color: #eee;
}
.clearcartbtn{
    margin-bottom: 24px;
    margin-top: 0px;
    text-transform: uppercase;
    margin-right: 15px;
    border-radius: 4px;
    padding: 8px 25px;
    background: rgb(212, 80, 79) none repeat scroll 0% 0%;
    border: 1px solid rgb(212, 80, 79); color: white;
    display:none;
}
.cartcaption {
    font-size: 64px;
    margin-top: 20px;
    text-align: center;
}
.optionnumbox {
    background: white none repeat scroll 0 0;
    border: 2px solid #202020;
    border-radius: 17px;
    line-height: 40px;
    padding: 2px 8px;
    color: white;
    position: relative;
    z-index: 99;
}

.clearcartbtn {
    background: #00ADEE;
    border: 1px solid rgb(212, 80, 79);
    border-radius: 4px;
    color: white;
    /*display: none;*/
    margin-bottom: 24px;
    margin-right: 15px;
    margin-top: 0;
    padding: 8px 25px;
    text-transform: uppercase;
    font-size: 16px;
    font-weight: 600;
}
.completecart {
    margin-bottom: 25px;
}
.productinfo {
    -moz-border-bottom-colors: none;
    -moz-border-left-colors: none;
    -moz-border-right-colors: none;
    -moz-border-top-colors: none;
    border-color: #202020 currentcolor #202020 #202020;
    border-image: none;
    border-style: solid none solid solid;
    border-width: 1px medium 1px 1px;
    padding: 10px 0;
    min-height:200px;
    max-height:200px;
}
.cartdelicon {
    font-size: 28px;
    margin-top: 126px;
    position: absolute;
}
.quantity {
    border: 1px solid #202020;
    color: #555;
    font-size: 18px;
    font-weight: normal;
    height: 172px;
    line-height: 200px;
    text-align: center;

    min-height:200px;
    max-height:200px;
}
.increment_quantity {
    height: 25px;
    margin-top: 91px;
    position: absolute;
    width: 24px;
}
.productpricing {
    -moz-border-bottom-colors: none;
    -moz-border-left-colors: none;
    -moz-border-right-colors: none;
    -moz-border-top-colors: none;
    border-color: #202020 #202020 #202020 currentcolor;
    border-image: none;
    border-style: solid solid solid none;
    border-width: 1px 1px 1px medium;
    color: red;
    font-size: 18px;
    font-weight: normal;
    height: 172px;
    line-height: 200px;
    text-align: right;
    min-height:200px;
    max-height:200px;
}
.finalpaymentcart {
    -moz-border-bottom-colors: none;
    -moz-border-left-colors: none;
    -moz-border-right-colors: none;
    -moz-border-top-colors: none;
    border-color: #202020 #202020 currentcolor;
    border-image: none;
    border-style: solid solid none;
    border-width: 1px 1px medium;
    height: 90px;
    margin-left: 1%;
    width: 98%;
}
.finalpaymentcart {
    -moz-border-bottom-colors: none;
    -moz-border-left-colors: none;
    -moz-border-right-colors: none;
    -moz-border-top-colors: none;
    border-color: #202020 #202020 currentcolor;
    border-image: none;
    border-style: solid solid none;
    border-width: 1px 1px medium;
    height: 90px;
    margin-left: 1%;
    width: 98%;
}
.finalpaymentcartlabel1 {
    float: right;
    font-size: 19px;
    margin-top: 10px;
}
.finalpaymentcartlabel2 {
    float: right;
    font-size: 19px;
    margin-top: 10px;
}
.finalpaymentcartlabel1 {
    float: right;
    font-size: 19px;
    margin-top: 10px;
}
.finalpaymentcartlabel2 {
    float: right;
    font-size: 19px;
    margin-top: 10px;
}
.totalpaymentcart {
    border: 1px solid #202020;
    height: 60px;
    margin-left: 1%;
    width: 98%;
}
.totalpaymentcartlabel1 {
    float: right;
    font-size: 19px;
    margin-top: 15px;
}
.totalpaymentcartlabel1 {
    float: right;
    font-size: 19px;
    margin-top: 15px;
}
.custombottomcartbtn {

    font-size: 22px;
    margin-top: 50px;
    padding: 12px 62px;
}
.custombottomcartbtn {
    background: #EB008B;
    border: medium none;
    color: white;
    font-size: 22px;
    margin-top: 50px;
    padding: 12px 62px;
}
.optionbox {
    color: #555;
    font-size: 19px;
    text-align: center;
}
.optionnumbox {
    background: white none repeat scroll 0 0;
    border: 2px solid #202020;
    border-radius: 17px;
    line-height: 40px;
    padding: 2px 8px;
}
.optionnumboxactive {

    color: white;
}

.custommpbtn {
    background: white none repeat scroll 0 0;
    border: 1px solid #ccc;
    border-radius: 5px;
    color: #000;
    font-size: 16px;
    font-weight: 600;
    margin-top: 30px;
    padding: 8px 60px;
}
.decrement_quantity {
    height: 25px;
    margin-left: -26px;
    margin-top: 91px;
    position: absolute;
    width: 24px;
}

.breakingline2 {
    border: 1px solid #202020;
    margin-left: 2%;
    margin-top: 47px;
    position: absolute;
    width: 94%;
}
.optionbox {
    color: #555;
    font-size: 16px;
    text-align: center;
}
.optionnumbox {
    background: white none repeat scroll 0 0!important;
    border: 2px solid #202020;
    border-radius: 17px;
    line-height: 48px;
    padding: 2px 8px;
    color: #000;
}
.optionnumboxactive {
    background: #002856 none repeat scroll 0 0!important;
    border: 3px solid #002856;
    color: white;
}
.productinfo_table{
    letter-spacing:1px;
    line-height:28px;
}
.cart_delete{
    cursor:pointer;
    margin-top:145px;
}

    .modalHeaderText{
        font-size: 18px!important;
    }
.clearcartbtn {
    background: rgb(212, 80, 79) none repeat scroll 0 0;
    border: 1px solid rgb(212, 80, 79);
    border-radius: 4px;
    color: white;
    display: none;
    margin-bottom: 24px;
    margin-right: 15px;
    margin-top: 0;
    padding: 8px 25px;
    text-transform: uppercase;
}
    #make_payment{
        display: none;
    }
</style>
<div class="about-bottom wthree-3">
<div class="col-md-12" style="margin-bottom: 100px">
    <div class="col-md-12">
        <p class="cartcaption">CHECKOUT CART</p>
        <div class="col-md-12">
            <div class="col-md-6 optionbox">CART<br>
                <span class="optionnumbox optionnumboxactive" style="cursor:pointer" onclick="backon_1()" id="optioncart">1</span>
            </div>
            <div class="col-md-6 optionbox">PAYMENT CONFIRMATION<br>
                <span class="optionnumbox" id="paymentcart">2</span>
            </div>
        </div>
        <div class="col-md-12 breakingline2"></div>
        <div class="clear"></div>
        <button class='custommpbtn pull-right clearcartbtn' onclick=clear_cart() id='clear_cart' >
            Clear Cart
        </button>
        <div id="cart"></div>
        <!--<div class='col-md-12 finalpaymentcart' style="padding: 27px;" id="applycoupondiv">
            <div class='col-md-9'>
                <input type="text" name="validcoupon" id="validcoupon" placeholder="Enter valid coupon code" style="height: 38px; width: 370px; padding-left: 19px;">
                <input type="button" onclick="applyCoupon()" name="applycoupon" id="applycoupon" value="Apply Coupon" style="margin-bottom: 24px; margin-top: 0px; text-transform: uppercase;
                 margin-right: 15px; padding: 8px 25px; background: rgb(212, 80, 79) none repeat scroll 0% 0%; border: 1px solid rgb(212, 80, 79); color: white;">
            </div>
            <div class='col-md-1'></div>
            <div class='col-md-2'></div>
        </div>-->
        <div class='col-md-12 finalpaymentcart' id="finalpaymentcart">
            <div class='col-md-9'>
                <label class="finalpaymentcartlabel1">Cart Subtotal (Without TAX) : </label></br></br>
                <div class="clear"></div>
                <label class="finalpaymentcartlabel2"><i class="fa fa-barcode"></i> Tax % = 7.25% : </label>
            </div>
            <div class='col-md-1'></div>
            <div class='col-md-2'>
                <input class=" finalpaymentcartlabel1" type="text" id="finalcartsubtotal"
                style="border:none;text-align:left" readonly/><br><br>
                <div class="clear"></div>
                <input class="finalpaymentcartlabel2" style="border:none;text-align:left" readonly />
            </div>
        </div>
        <div class='col-md-12 totalpaymentcart' id="totalpaymentcart">
            <div class='col-md-9'>
                <label class="totalpaymentcartlabel1">Order Total (With TAX) : </label>
            </div>
            <div class='col-md-1'></div>
            <div class='col-md-2'>
                <input class=" totalpaymentcartlabel1 " type="text" id="totalcartsubtotal"
                style="border:none;text-align:left" readonly/><br><br>
            </div>
        </div>
        <div class='col-md-12' id='stp1_buttons'>
            <input type="button" class="custombottomcartbtn" onclick="continue_shoping()"
            style="float: left; background: white none repeat scroll 0% 0%; color: black; border: 1px solid rgb(32, 32, 32);" value="CONTINUE SHOPPING"/>
            <input type="button" class="custombottomcartbtn" onclick="step_2_task()" id="placeorder" style="float:right"
            value="Shipping & Billing"/>
        </div>
        <!------------------------------step 2functionality start here------------------------------------------------>
        <div class="col-md-12" id="step2_div" style="display: none">
            <h2 class="addressdataheading">Ship to this address</h2>
            <hr>
            <div class="col-md-12" style="padding:20px">
                <div class="col-md-8" style="line-height:45px;border-right:1px solid #ccc">
                    <div class="col-md-6">
                        <label class="otherradio_label">First name(*)</label>
                        <input class="custom_feild " id="first_name" placeholder="first name" type="text">
                    </div>
                    <div class="col-md-6">
                        <label class="otherradio_label">Last name(*)</label>
                        <input class="custom_feild" id="last_name" placeholder="last name" type="text">
                    </div>
                    <div class="col-md-12">
                        <label class="otherradio_label ">Address line 1:(*)</label>
                        <textarea class="custom_feild" id="address"
                        placeholder="Enter Billing / Shipping Address"></textarea>
                    </div>
                    <div class="col-md-4">
                        <label class="otherradio_label">Country(*)</label>
                        <div id="">
                            <input type="hidden" class="ma_country_field">
                            <select class=" countries custom_feild" name="country" id="countryId"></select>
                        </div>
                    </div>
                    <div class="col-md-4 ">
                        <label class="otherradio_label">State(*)</label>
                        <div id="">
                            <input type="hidden" class="ma_state_field">
                            <select class=" states custom_feild" name="state" id="stateId"></select>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <label class="otherradio_label">City(*)</label>
                        <div id="cityfeild">
                            <input type="hidden" class="ma_city_field">
                            <select class=" cities" name="city" id="cityId"></select>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <label class="otherradio_label">Postal code(*)</label>
                        <input class="custom_feild only-numbers" id="postalcode" placeholder="postal code" type="text">
                    </div>
                    <div class="col-md-4">
                        <label class="otherradio_label">Phone number(*)</label>
                        <input class="custom_feild only-numbers" id="phonenumber" placeholder="contact number" type="text">
                    </div>
                </div>
                <div class="col-md-4" style="text-align:right">
                    <p class="payment_confirmation">Payment Confirmation</p>
                    <p class="total_am">Total Amount (Without Tax) = $<span id="total_am"></span></p>
                    <p class="tax">Tax : 7.25% = <span id="tax"></span></p>
                    <p class="grand_total">Grand Total (With Tax) = $<span id="grand_total"></span></p>
                    <form action="<?php echo $paypal_url; ?>" method="post">
                        <!-- Identify your business so that you can collect the payments. -->
                        <input name="business" value="<?php echo $paypal_id; ?>" type="hidden">
                        <!-- Specify a Buy Now button. -->
                        <input name="cmd" value="_xclick" type="hidden">
                        <!--    <input type="hidden" name="cmd" value="_cart">-->
                        <!-- Specify details about the item that buyers will purchase. -->
                        <input name="item_name" value="Orders" type="hidden">
                        <input name="item_number" id="pp_order_id" value="" type="hidden">
                        <input id="pp_order_amount" name="amount" value="" type="hidden">
                        <input name="currency_code" value="USD" type="hidden">
                        <!-- Specify URLs -->
                        <input type='hidden' name='cancel_return' value="" id="cancelReturnUrl"/>
                        <input name="return" value="" id="returnUrl" type="hidden"/>
                        <!-- Display the payment button. -->
                        <input style="display:none" name="submit" value="PayNow" id="paybtn" type="submit">
                    </form>
                </div>
            </div>
        </div>
        <div class="col-md-12">
            <input id="make_payment" value="Make Payment" onclick="update_shipping_address()"
            class="custommpbtn pull-right" type="button">
        </div>
        <!------------------------------payment confirmation div end here------------------------------------------------------------------>
    </div>
</div>
<input type="hidden" id="selectedMeasurement" />


<div class="clearfix"></div>
<?php
    include('footer.php');
?>
<script>
    var user_id = $("#user_id").val();
    var stock = 0;
    function getcart(){
        var url = "admin/api/orderProcess.php";
        stock=10;
        $(".nav a").removeClass("active");
        $(".loader").fadeIn();
        $.post(url, {"type":"getCart","user_id":user_id}, function (data) {
            var status = data.Status;
            var button = "";
            var cartshow = "";
            if (status == "Success") {
                $("#clear_cart").show();
                var items = data.cartData;
                if(items.length > 0) {
                    var cartTotalAmount = 0;
                    for (var i = 0; i < items.length; i++) {
                        var totalamount = items[i].quantity *   items[i].product_price;
                        var prod_name = items[i].product_name;
                        var product_type = items[i].product_type;
                        cartTotalAmount = totalamount +  cartTotalAmount ;
                        if (product_type == "Swimsuit") {
                            cartshow = cartshow + "<div class='col-md-12 completecart'><div class='col-md-9 productinfo'>" +
                            "<div class='col-md-3'><img alt='Custom Fabric' src='images/lady.png' class='img'/>" +
                            "</div><div class='col-md-8'><table class='productinfo_table' >" +
                                "<tr><th class=''>Product Name </th><td>&nbsp;&nbsp;:" + prod_name + "</td></tr>" +
                                "<tr><th class=''>Product Style </th><td>&nbsp;&nbsp;:" + items[i].style + "</td></tr>" +
                                "<tr><th class=''>Product Fabric </th><td>&nbsp;&nbsp;:" + items[i].fabric + "</td></tr>" +
                                "<tr><th class=''>Product Mannequin </th><td>&nbsp;&nbsp;:" + items[i].manq + "</td></tr>" +
                                "<tr><th class=''>Product Skin Color </th><td>&nbsp;&nbsp;:" + items[i].manneColor + "</td></tr></table>" +
                                " </div><div class='col-md-1' style='text-align:right;" +
                            "padding-right:26px'><span class='cart_delete' " +
                            "onclick=deletecart('"+items[i].cart_id+"') class='cartdelicon'><i class='fa fa-trash'></i></span>" +
                            "</div></div><div class='col-md-1 quantity'>" +
                             "<i class='fa fa-plus-circle increment_quantity' onclick=quantityProcess('"+items[i].user_id+"','quantity" + i + "','plus','" + i + "','" + items[i].cart_id + "',5,'"+items[i].product_price+"')></i>" +
                                "<i class='fa fa-minus-circle decrement_quantity' onclick=quantityProcess('"+items[i].user_id+"','quantity" + i + "','minus','" + i + "','" + items[i].cart_id + "',5,'"+items[i].product_price+"')>" +
                             "</i>" +
                                "<input type='text' id='limit_reach" + i + "' value='" + items[i].quantity +"' hidden  />" +
                            "<input type='text' class='form-control text-center' readonly style='margin-top:50px' " +
                            "id='quantity" + i + "' value='" + items[i].quantity + "'/></div><div class='col-md-2 productpricing'" +
                            "><input class='form-control' type='text' style='margin-top:50px;border:none' " +
                            "readonly value='" + totalamount + "' id='total" + i + "' /><input type='text'" +
                            "id='stock_value' value='" + stock + "' hidden  /><input type='text' hidden id='stock_value' " +
                            "value='" + stock + "' /></div></div>";
                        }
                    }
                    $("#cart").html(cartshow);
                    $("#finalcartsubtotal").val("$" + cartTotalAmount);
                    $(".finalpaymentcartlabel2").val("$"+((parseFloat(cartTotalAmount)*7.25)/100).toFixed(2));
                    $("#totalcartsubtotal").val("$" + (parseFloat(cartTotalAmount) + ((parseFloat(cartTotalAmount)*7.25)/100)).toFixed(2));
                    $("#paymentconfirm").html("$" + cartTotalAmount);
                    $("#totalpaymentconfirm").html("$" + (parseFloat(cartTotalAmount) + ((parseFloat(cartTotalAmount)*7.25)/100)).toFixed(2));
                    $(".loader").fadeOut();
                }
                else {
                    document.getElementById("finalpaymentcart").style.display = "none";
                    cartshow = cartshow+"<p class='nodata'>Cart Is Empty !!!!</p>";
                    document.getElementById("totalpaymentcart").style.display = "none";
                    document.getElementById("placeorder").style.display = "none";
                    $("#make_payment").hide();
                    $("#placeorder").attr("disabled", true);
                    $("#placeorder").css({"opacity": "0.5"});
                    $(".loader").fadeOut();
                    $("#cart").html(cartshow);
                }
            }
            else{
                var urlData = window.location.href;
                var urlData = urlData.split("stp=");
                if(urlData[1] == 2)
                {
                    window.location.href = "orders";
                }
                document.getElementById("finalpaymentcart").style.display = "none";
                cartshow = cartshow+"<p class='nodata'>Cart Is Empty !!!!</p>";
                document.getElementById("totalpaymentcart").style.display = "none";
                document.getElementById("placeorder").style.display = "none";
                $("#placeorder").attr("disabled", true);
                $("#placeorder").css({"opacity": "0.5"});
                $("#make_payment").hide();
                $(".loader").fadeOut();
                $("#cart").html(cartshow);
            }
        });
    }
    getcart();
    function backon_1() {
        var path = window.location.href;
        if (path.indexOf("?") >= 1) {
            path = path.split("?");
            var temp = path[1].split("=");
            if (temp[0] == "stp" && temp[1] == "2" || temp[0] == "stp" && temp[1] == "3") {
                window.location = "cart.php?stp=1";
            }
        }
    }
    function backon_2() {
        var path = window.location.href;
        if (path.indexOf("?") >= 2) {
            path = path.split("?");
            var temp = path[1].split("=");
            if (temp[0] == "stp" && temp[1] == "3") {
                window.location = "cart.php?stp=2";
            }
        }
    }
    function continue_shoping() {
        window.location = "orders";
    }
    function quantityProcess(user_id,cart_qid,opt,row_id,cart_id,stock,price) {
        var quantity = parseInt($("#" + cart_qid).val());
        if(opt == "plus") {
            quantity = quantity + 1;
            if (quantity > stock) {
                $(".modal-header").html("<button type='button' class='close' data-dismiss='modal'>&times;</button>" +
                    "<label style='color:green'>Quantity Limit Reach !!!</label><hr class='custom-hr'>" +
                    "<h5>We have only " + stock + " Items Available in Stock.</h5>");
                $("#modal").modal("show");
                return false;
            }
        }
        else if(opt == "minus")
        {
            if(quantity != 1)
            quantity = quantity - 1;
        }
        var url = "admin/api/orderProcess.php";
        $.post(url, {"type":"updateCart","quantity":quantity,"cart_id":cart_id,"user_id":user_id,"price":price}, function (datas) {
            var status = datas.Status;
            var cart_totalPrice = datas.cart_totalPrice;
            if (status == "Success") {
                $("#total" + row_id).val(quantity * price);
                $("#quantity"+row_id).val(quantity);
                $("#finalcartsubtotal").val("$" + cart_totalPrice);
                var currentTax = (parseFloat(cart_totalPrice) * parseFloat(7.25))/100;
                var finalwithtax = (parseFloat(cart_totalPrice) + parseFloat(currentTax)).toFixed(2);
                $(".finalpaymentcartlabel2").val("$" + currentTax.toFixed(2));
                $("#totalcartsubtotal").val("$" + finalwithtax);
            }
        });
    }
    function deletecart(cart_id) {
        $(".modal-header").html('<button type="button" class="close" data-dismiss="modal">&times;</button>' +
            ' <p clss="modalHeaderText" style="color: green;font-size:18px ">Confirm Status !!!</p><hr class="custom-hr"><form action="#" method="post">' +
            '<div class="row"><div class="col-md-12"><span>Are you sure want to delete this item.</span></div></div><hr class="custom-hr">' +
            '<input type="button" class="cart_modalbtn pull-right" value="Confirm" onclick=confirm_deletecart("'+encodeURI(cart_id)+'")></form></div>');
        $("#modal").modal("show");
    }
    function confirm_deletecart(cart_id) {
        $(".loader").fadeIn("slow");
        var url = "admin/api/orderProcess.php";
        $.post(url, {"type":"removeItem","cart_id":cart_id},function (data) {
            var status = data.Status;
            if (status == "Success") {
                $(".loader").fadeOut("slow");
                window.location = "cart?stp=1";
            }
            else {
                $(".loader").fadeOut("slow");
                $(".modal-title").html("<label style='color:red'>Error Occured</label>");
                $(".modal-body").html(data.Message);
                $("#modal").modal("show");
            }
        });
    }
    //clear all cart items
    function clear_cart() {
        var user_id = $("#user_id").val();
        $(".modal-header").html('<button type="button" class="close" data-dismiss="modal">&times;' +
            '</button> <p clss="modalHeaderText" style="color: green;font-size:18px ">Confirm Status !!!</p><hr class="custom-hr"><form action="#" method="post">' +
            '<div class="row"><div class="col-md-12"><span>Are you sure want to delete all items in the cart.</span></div></div><hr class="custom-hr">' +
            '<input type="button" class="cart_modalbtn " value="Confirm" onclick=confirm_clear_cart("'+encodeURI(user_id)+'")></form></div>');
        $("#modal").modal("show");
    }
    function confirm_clear_cart(user_id) {
        $(".loader").fadeIn("slow");
        var url = "admin/api/orderProcess.php";
        $.post(url, {"type":"clearCart","user_id":decodeURI(user_id)},function (data) {
            var status = data.Status;
            if (status == "Success") {
                $(".loader").fadeOut("slow");
                window.location = "cart?stp=1";
            }
            else {
                $(".loader").fadeOut("slow");
                $(".modal-title").html("<label style='color:red'>Error Occured</label>");
                $(".modal-body").html(json.message);
                $("#modal").modal("show");
            }
        });
    }
    function step_2_task(meas_id) {
        window.location = "cart.php?stp=2";
    }
    var path = window.location.href;
    if (path.indexOf("?") >= 0) {
        step = path.split("=");
        if(step[1] == 1)
        {
            $("#paymentcart").removeClass("optionnumboxactive");
            $("#optioncart").addClass("optionnumboxactive");
            $("#make_payment").css({"display": "none"});
        }
        if(step[1] == 2){
            $("#clear_cart").hide();
            $("#optioncart").removeClass("optionnumboxactive");
            $("#paymentcart").addClass("optionnumboxactive");
            $("#cart").css({"display": "none"});
            $("#applycoupondiv").css({"display": "none"});
            $("#finalpaymentcart").css({"display": "none"});
            $("#totalpaymentcart").css({"display": "none"});
            $("#stp1_buttons").css({"display": "none"});
            $("#step2_div").css({"display": "block"});
            $("#make_payment").css({"display": "block"});
            userPersonal();
        }
    }
    function userPersonal() {
        var user_id = $("#user_id").val();
        var url = "admin/api/registerUser.php";
        $.post(url,{"type":"getParticularUserData","user_id":user_id}, function (data) {
            var status = data.Status;
            var userData = data.userData;
            if (status == "Success") {
                var current_year = (new Date()).getFullYear();
                var name = userData.name;
                name = name.split(" ");
                var dob = userData.dob;
                dob = dob.split("/");
                $("#user_id").val(userData.user_id);
                $("#first_name").val(name[0]);
                $("#last_name").val(name[1]);
                $("#address").val(userData.address);
                $("#postalcode").val(userData.pincode);
                $("#phonenumber").val(userData.mobile);
                $(".ma_country_field").val(userData.b_country);
                $(".ma_state_field").val(userData.b_state);
                $(".ma_city_field").val(userData.b_city);
                loadScript("js/location1.js", function(){
                });
                setTimeout(function(){
                    $("#total_am").html(parseFloat($("#finalcartsubtotal").val().split("$")[1]));
                    $("#tax").html("$"+((parseFloat($("#finalcartsubtotal").val().split("$")[1]) *7.25)/100).toFixed(2));
                    $("#grand_total").html(parseFloat(parseFloat($("#finalcartsubtotal").val().split("$")[1])+ ((parseFloat($("#finalcartsubtotal").val().split("$")[1]) *7.25)/100)).toFixed(2));
                },500);
                $("#cityId").html("<option selected='selected'>" + item.city + "</option>")
            }
        });
    }
    //shipping and billing process end here
    //payment confirmation process start here
    //update_shipping_address();
    function update_shipping_address() {
        $(".loader").fadeIn("slow");
        var user_id = $("#user_id").val();
        var billing_fname = $("#first_name").val();
        var billing_lname = $("#last_name").val();
        var address = $("#address").val();
        var country = $("#countryId").val();
        var state = $("#stateId").val();
        var city = $("#cityId").val();
        var pincode = $("#postalcode").val();
        var phone = $("#phonenumber").val();
        var billing_address = address;

        if(city == "" || city == undefined){
            city = $(".cf").val();
        }
        if (billing_fname == "" || billing_lname == "" || address == "" || country == "" || state == ""
            || city == "" || pincode == "" || phone == "") {
            $(".loader").fadeOut("slow");
            $(".modal-header").html('<button type="button" class="close" data-dismiss="modal">&times;' +
                '</button> <p clss="modalHeaderText" style="color: green;font-size:18px ">Alert Message !!!</p><hr class="custom-hr">' +
                '<form action="#" method="post"><div class="row"><div class="col-md-12"><span>Parameters are missing.</span></div></div>' +
                '</form>');
            $("#modal").modal("show");
        }
        else {
            var url = "admin/api/orderProcess.php";
            $.post(url,{"type":"billing","user_id":user_id,"billing_fname":billing_fname,"billing_lname":billing_lname,
            "billing_address":billing_address,"country":country,"state":state,"city":city,"pincode":pincode,"phone":phone},
                function (data) {
                var status = data.Status;
                var cartData = data.cartData;
                if (status == "Success")
                {
                    var product = "";
                    var tam = 0;
                    for(var a=0; a< cartData.length; a++)
                    {
                        var product_type = cartData[a].product_type;
                        if(product_type == "Swimsuit"){
                            product_type = product_type;
                            var product_name = cartData[a].product_name;
                            var product_price = cartData[a].product_price;
                            var user_id = cartData[a].user_id;
                            var quantity = cartData[a].quantity;
                            var total_amount = cartData[a].total_amount;
                            var style = cartData[a].style;
                            var fabric = cartData[a].fabric;
                            var manq = cartData[a].manq;
                            var manneColor = cartData[a].manneColor;
                            tam = tam + parseFloat(total_amount);


                            var products = '{"product_type":"' + product_type + '","product_name":"' + product_name+ '",' +
                                '"product_quantity":"' + quantity + '","total_amount":"'+ total_amount + '","style":"' + style +'","product_price":"' +
                                product_price + '","fabric":"' + fabric + '","manq":"' + manq + '","manneColor":"' + manneColor + '"}';
                            product = product + products;
                            if (a != cartData.length - 1) {
                                product = product + ",";
                            }
                        }
                    }
                    var tax = ((tam * 7.25)/100).toFixed(2);
                    tam = parseFloat(tam) + parseFloat(tax);
                    order_detail(product,user_id, tam);
                }
            });
        }
    }
    function order_detail(product,user_id, total) {
        var json = '{"user_id":"' + user_id + '","total":"' + total + '","items":[' + product + ']}';
        var url = "admin/api/orderProcess.php";
        $.post(url,{"type":"orderDetail","json":json,"user_id":user_id}, function (data) {
            var status = data.Status;
            if (status == "Success") {
                var order_id = data.order_id;
                var totalAmount = total;
                $("#pp_order_id").val(order_id);
                $("#returnUrl").val("http://scan2fit.com/duziscan/admin/api/payment.php?payment_status=Success&order_id=" +
                    order_id + "&paid_amount=" + total);
                $("#cancelReturnUrl").val("http://scan2fit.com/duziscan/admin/api/payment.php?payment_status=Failed&order_id=" +
                    order_id + "&paid_amount=" + total);
                $("#pp_order_amount").val(totalAmount);
                $("#paybtn").click();
            }
            else {
                alert(data.Message);
            }
        });
    }
</script>

