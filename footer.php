<?php
$userId = "";
if(isset($_SESSION['duzi_user_id'])){
    $userId = $_SESSION['duzi_user_id'];
}
echo "<input type='hidden' value='".$userId."' id='user_id'>";
echo "<input type='hidden'  id='rqstFor'>";
echo "<input type='hidden'  id='cartCount'>";
    ?>
<div class="modal fade" id="modal" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content custom_model">
            <div class="modal-header book-form">
                <!--<button type="button" class="close" data-dismiss="modal">&times;</button><h4>Sign up Form</h4><form action="#" method="post"><div class="row"><div class="col-md-6"><input type="text" name="fname" placeholder="First Name" required=""/></div><div class="col-md-6"><input type="text" name="lname" class="lname" placeholder="Last Name" required=""/></div></div><input type="submit" value="Sign Up"></form></div>-->
            </div>
        </div>
    </div>

</div>


<!--<div class="modal fade" id="signupMOdal" role="dialog">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header book-form">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4>Sign up Form</h4>
				<form action="#" method="post">
					<div class="row">
						<div class="col-md-6">
							<input type="text" name="fname" placeholder="First Name" required=""/>
						</div>
						<div class="col-md-6">
							<input type="text" name="lname" class="lname" placeholder="Last Name" required=""/>
						</div>
					</div>
					<div class="row">
						<div class="col-md-6">
							
						</div>
						<div class="col-md-6">
							<input placeholder="Last Name" name="lname" class=" custom_signup_feild form-control" type="text">
						</div>
					</div>
						
						<input type="password" name="Password" class="password" placeholder="Password" required=""/>	
						<div class="check-box">
							<input name="chekbox" type="checkbox" id="brand" value="">
							<label for="brand"><span></span>Remember Me.</label>
						</div>
						<input type="submit" value="Sign Up">
					</form>
			</div>
		</div>
	</div>
</div>-->
<!-- //Modal1 -->

<!-- Modal1 login -->
<div class="modal fade" id="loginModal" role="dialog">
	<div class="modal-dialog">
	<!-- Modal content-->
		<div class="modal-content">
			<div class="modal-header book-form">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4>Sign In Form</h4>
                <label style="margin-left: 20px;color: red" id="message"></label>
					<p class="errorMessage">Error</p>
					<input type="text" name="Email" class="email" id="user_email" placeholder="Email" required=""/>
					<input type="password" name="Password" id="user_password" class="password" placeholder="Password" required=""/>
					<div class="check-box">
						<label  class="no-account" onclick="signupMOdal()"><span></span>Don't have an account ? 
						<span class="create-account" >Create new account !!</span> </label>
					</div>

					<input type="button" value="Sign In" class="modalButton" onclick="signin()">

			</div>
		</div>
	</div>
</div>
<!-- //Modal1 login-->
<!-- Footer -->
	<div class="w3l-footer">
		<div class="container">
         <div class="footer-info-agile">
				<div class="col-md-2 footer-info-grid links">
					<h4>Quick links</h4>
					<ul>
						       <li><a href="index">Home</a></li>
								<li><a href="about">About</a></li>
                                <?php
                                if(isset($_SESSION['duzi_user_id'])) {
                                    ?>
                                    <li><a href="orders" >Order Now</a></li>
                                    <?php
                                }
                                else{
                                    ?>
                                    <li class="order" >
                                            <a href="#" data-toggle="modal" data-target="#loginModal" onclick="reuestFor('orders')">Order Now</a>
                                    </li>
                                    <?php
                                }
                                ?>
								<li><a href="contact">Contact Us</a></li>
					</ul>
				</div>
				<div class="col-md-3 footer-info-grid address">
					<h4>Address</h4>
					<address>
						<ul>
							<li>3310 Ashton Drive</li>
							<li>Suwanee GA 30024</li>


							<li>Phone : 678-697-6431</li>
							<li>Email : <a class="mail" href="info@duziscan.com">info@duziscan.com</a></li>
						</ul>
					</address>
				</div>
				<div class="col-md-3 footer-grid">
				   <h4>Instagram</h4>
					<div class="footer-grid-instagram">
					<a href="#"><img src="images/web/shutterstock_572259820.jpg" alt=" " class="img-responsive"></a>
					</div>
					<div class="footer-grid-instagram">
					<a href="#"><img src="images/web/shutterstock_477370582.jpg" alt=" " class="img-responsive"></a>
					</div>
					<div class="footer-grid-instagram">
						<a href="#"><img src="images/web/shutterstock_290399939.jpg" alt=" " class="img-responsive"></a>
					</div>
					<div class="footer-grid-instagram">
					<a href="#"><img src="images/web/shutterstock_301675481.jpg" alt=" " class="img-responsive"></a>
					</div>
					<div class="clearfix"> </div>
				</div>
				<div class="col-md-4 footer-info-grid">
				<div class="connect-social">
					<h4>Connect with us</h4>
					<section class="social">
                        <ul>
							<li><a class="icon fb" href="#"><i class="fa fa-facebook"></i></a></li>
							<li><a class="icon tw" href="#"><i class="fa fa-twitter"></i></a></li>							
							<li><a class="icon pin" href="#"><i class="fa fa-pinterest"></i></a></li>
							<li><a class="icon db" href="#"><i class="fa fa-dribbble"></i></a></li>
							<li><a class="icon gp" href="#"><i class="fa fa-google-plus"></i></a></li>
						</ul>
					</section>
				</div>					
				</div>
				<div class="clearfix"></div>
			</div>
			<div class="connect-agileits newsletter">
				<h4>Newsletter</h4>
					<p>Subscribe to our newsletter and we will inform you about newest projects and promotions.
					</p>
					<form action="#" method="post" class="newsletter">
						<input class="email" type="email" placeholder="Your email address..." required="">
						<input type="submit" class="submit" value="Subscribe">
					</form>
			</div>
	   </div>
     </div>
			<div class="w3agile_footer_copy">
				    <p>© 2017 DUZISCAN. All rights reserved</p>
			</div>
		<a href="#home" id="toTop" class="scroll" style="display: block;"> <span id="toTopHover" style="opacity: 1;"> </span></a>
    <script>
        $(document).ready(function(){
            $(".dropdown").hover(
                function() {
                    $('.dropdown-menu', this).stop( true, true ).slideDown("fast");
                    $(this).toggleClass('open');
                },
                function() {
                    $('.dropdown-menu', this).stop( true, true ).slideUp("fast");
                    $(this).toggleClass('open');
                }
            );
        });
    </script>
<script src="js/jquery-2.1.4.min.js"></script>
<script type="text/javascript" src="js/modernizr-2.6.2.min.js"></script>
<script type="text/javascript" src="js/jquery.zoomslider.min.js"></script>
<script src="js/main.js"></script>
<script src="js/jquery.magnific-popup.js" type="text/javascript"></script>
<script type="text/javascript" src="js/move-top.js"></script>
<script type="text/javascript" src="js/easing.js"></script>
<script src="js/bootstrap.js"></script>
<script src="js/duzi-scan.js"></script>
 <script type="text/javascript">
     $(document).ready(function() {
         $('.w3_play_icon,.w3_play_icon1,.w3_play_icon2').magnificPopup({
             type: 'inline',
             fixedContentPos: false,
             fixedBgPos: true,
             overflowY: 'auto',
             closeBtnInside: true,
             preloader: false,
             midClick: true,
             removalDelay: 300,
             mainClass: 'my-mfp-zoom-in'
         });
     });

     jQuery(document).ready(function($) {
         $(".scroll").click(function(event){
             event.preventDefault();
             $('html,body').animate({scrollTop:$(this.hash).offset().top},900);
         });
     });

    $(document).ready(function() {
        $().UItoTop({ easingType: 'easeOutQuart' });
    });

     get_user_info();
     get_cart_count();
 </script>
<!--end-smooth-scrolling-->


<link rel="stylesheet" href="css/bootstrap-datetimepicker.min.css" />
<script type="text/javascript" src="js/bootstrap-datetimepicker.js" charset="UTF-8"></script>
<script type='text/javascript' src='js/location.js'></script>
</body>
</html>