$(document).ready(function () {
   $(".inner_li").click(function() {
	        var container_title = $(this).attr("title");
	        var title = container_title.split("|");
	        var container = title[0];
	        var image = title[1];
	        var src = "images/" + container + "/" + image;
	        $("." + container).removeClass("active");
	        $(".img_" + container).attr("src", src);
	        $(this).addClass("active");
	    });
});
