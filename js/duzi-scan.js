function rediredTo(page) {
    window.location=page;
}
/*active tab according to current page start here*/


/*common method start here*/
/*common method please use this method start here*/
$('.only-letters').bind('keyup blur',function(){
    var node = $(this);
    node.val(node.val().replace(/[^a-zA-Z]/g,'') ); }
);
$('.only-numbers').bind('keyup blur',function(){
    var node = $(this);
    node.val(node.val().replace(/[^0-9]/g,'') ); }
);
/*load library when click to update for select state and city start here*/
function toHumanFormat(unixTime){
    date = new Date(unixTime * 1000);
    var year    = date.getFullYear();
    var month   = date.getMonth();
    var months = ["Jan","Feb","Mar","Apr","May","Jun","Jul","Aug","Sep","Oct","Nov","Dec"];
    var day     = date.getDate();
    var hour    = date.getHours();
    var ampm = "AM";
    var minute  = date.getMinutes();
    var seconds = date.getSeconds();
    minute = parseInt(minute);
    seconds = parseInt(seconds);
    hour = parseInt(hour);
    if(hour<12){
        ampm = "AM";
    }else if(hour == 12){
        ampm = "PM";
    }else{
        hour = hour-12;
        ampm = "PM";
    }
    if(hour<10){
        hour = "0"+hour;
    }
    if(minute<10){
        minute = "0"+minute;
    }
    if(seconds<10){
        seconds = "0"+seconds;
    }
    //return(day+" "+months[month]+" "+year+" "+hour+":"+minute+":"+seconds+" "+ampm);
    return(day+" "+months[month]+" "+year);
}
function loadScript(url, callback){
    var script = document.createElement("script")
    script.type = "text/javascript";
    if (script.readyState){  //IE
        script.onreadystatechange = function(){
            if (script.readyState == "loaded" ||
                script.readyState == "complete"){
                script.onreadystatechange = null;
                callback();
            }
        };
    } else {  //Others
        script.onload = function(){
            callback();
        };
    }

    script.src = url;
    document.getElementsByTagName("head")[0].appendChild(script);
}
/*load library when click to update for select state and city end here*/

function validateEmail(email) {
    var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(email);
}
/*common method end here*/

function reuestFor(rqstfor) {
    $("#rqstFor").val(rqstfor);
}


function getUrl() {
    var currentPage = window.location.href;
    currentPage = currentPage.split("duziscan/");
    currentPage = currentPage[1];
    switch (currentPage) {
        case 'order.php':
            $(".order").addClass("active");
            break;
        case 'index.php':
            $(".home").addClass("active");
            break;
        case 'about.php':
            $(".about").addClass("active");
            break;
        case 'contact.php':
            $(".contact").addClass("active");
            break;
        case 'signup.php':
            $(".signin").addClass("active");
            break;
    }
}

getUrl();
/*active tab according to current page end here*/

/*signup form start here*/
function signupMOdal() {
    $("#loginModal").modal("hide");
    window.location = "signup.php";
}

/*signup form end here*/

/*for datepicker signup form start here*/
function setdate() {
    var CountryName = $(".countries").val();
    if (CountryName == "United States") {
        $('.form_date').datetimepicker({
            language: 'fr',
            weekStart: 1,
            autoclose: 1,
            todayHighlight: 1,
            startView: 2,
            minView: 2,
            format: "mm/dd/yyyy",
            forceParse: 0
        });
    }
    else {
        $('.form_date').datetimepicker({
            language: 'fr',
            weekStart: 1,
            autoclose: 1,
            todayHighlight: 1,
            startView: 2,
            minView: 2,
            format: "dd/mm/yyyy",
            forceParse: 0
        });
    }
}
/*for datepicker signup form end here*/
/*for height change signup form*/
function heightChange() {
    var heightParam = $("#heightparam").val();
    if (heightParam == "Feets") {
        $("#heightp").html("<div class='row'><input id='height0' class='custom_signup_feild' class='signupField' " +
        "type='text' placeholder='Feets' value='' style='float:left;margin-left:16px;width:43%' /><input type='text'" +
        "id='height1' class='custom_signup_feild' style='float:left;width:40%;border-left:none' " +
        "placeholder='Inches' class='' value='' /></div>");
    }
    else {
        $("#heightp").html("<input type='text' name='height' id='height' class='custom_signup_feild' placeholder='Enter Height'>");
    }
}
/*for height change signup form*/
/*sign in start here*/
function signin() {
    $("#message").html("");
    var user_email = $("#user_email").val();
    var user_password = $("#user_password").val();
    if (user_email == '' || user_password == '') {
        $("#message").html('Please enter all fields');
        return false;
    }
    else  if(!validateEmail(user_email)) {
            $("#message").html("Please fill valid email address.");
            return false;

    }
    var url = "admin/api/registerUser.php";
    $.post(url, {"type":"Login","email": user_email, "password": user_password}, function (data) {
        var status = data.Status;
        if (status == "Success") {
            $("#message").html(data.Message);
            $("#message").css('color', 'green');
            var rqstFor = $("#rqstFor").val();
            if( rqstFor != "")
            {
                window.location = rqstFor;
            }

            setTimeout(function () {
                window.location.reload();
            }, 500);
        }
        else  {
            $("#message").html(data.Message);
            $("#message").css('color', 'red');
        }

    }).fail(function () {
        $("#message").html("Server Error !!! Please Try After Some Time...");
    });
}
/*sign in end here*/
function registerUser() {
    $("#message").html("");
    var height = 0;
    var fname = $("#fname").val();
    var lname = $("#lname").val();
    var country = $("#countryId").val();
    var state = $("#stateId").val();
    var city = $("#cityId").val();
    var dob = $("#inputdob").val();
    var heightparam = $("#heightparam").val();
    var email = $("#email").val();
    var password = $("#password").val();
    var stylist = $("#stylist").val();
    var gender = $("#gender").val();
    var language = $("#language").val();
    var weightparam = $("#weightparam").val();
    var weight = $("#weight").val();
    var user_profile = $("#user_profile").val();
    var address = $("#address").val();
    var zipcode= $("#zipcode").val();
    var mobile = $("#mobile").val();
    if(city == "" || city == undefined)
    {
        city = $(".cf").val();
    }

    if(heightparam == "Feets"){
        var height0 = $("#height0").val();
        var height1 = $("#height1").val();
        height = (parseFloat(height0)*12)+parseFloat(height1);
    }else if(heightparam == "Centimeters"){
        height = parseFloat($("#height").val())*2.54;
    }
    else{
        height = $("#height").val();
    }
    if(weightparam == "Kilograms"){
        weight = (parseFloat(weight)*2.20462);
    }
    else if(weightparam == "Stones"){
        weight = (parseFloat(weight)*14);
    }
    var url = "admin/api/registerUser.php";

    if (fname == '' || lname == '' || country == '' || state == '' || city == '' || height == '' || email == '' || password == '' ||
        stylist == '' || gender == '' || language == '' || weight == '' || dob == '' || address == '' || zipcode == '' || mobile== '') {
        $("#signup_message").html("All Feilds Must Required Filled");
        return false;
    }
    if(!validateEmail(email))
    {
        $("#signup_message").html("Please fill valid email address.");
        return false;
    }


    var data = new FormData();
    if(user_profile != "") {
        var ext = user_profile.split(".");
        ext = ext[ext.length - 1];
        ext = ext.toLowerCase();
        if(ext == "jpg" || ext == "jpeg" || ext == "png" || ext == "gif"){
            var img = document.getElementById("user_profile");
            data.append("user_profile", img.files[0]);
        }else{
            $("#signup_message").html("Invalid Profile Picture Selected");
            return false;
        }
    }

    data.append("type", "Register");
    data.append("fname", fname);
    data.append("lname", lname);
    data.append("countryId", country);
    data.append("stateId", state);
    data.append("cityId", city);
    data.append("stylist", stylist);
    data.append("dob", dob);
    data.append("gender", gender);
    data.append("language", language);
    data.append("height", height);
    data.append("heightparam", heightparam);
    data.append("weight", weight);
    data.append("weightparam", weightparam);
    data.append("email", email);
    data.append("password", password);
    data.append("address", address);
    data.append("zipcode", zipcode);
    data.append("mobile", mobile);
    data.append("app_type", "Web");
    var xhttp = new XMLHttpRequest();
    xhttp.onreadystatechange = function () {
        if (this.readyState === 4 && this.status === 200) {
            var response = xhttp.responseText;
            response = JSON.parse(response);
            var status = response.Status;
            if (status == "Success") {
                $("#signup_message").html(response.Message);
                window.location = 'index.php';
            }
            else {
                $("#signup_message").html(response.Message);
            }
        }
    };
    xhttp.open("POST", url, true);
    xhttp.send(data);
}

function setdatePickerUs(){
    $('.form_date').datetimepicker({
        language: 'fr',
        weekStart: 1,
        autoclose: 1,
        todayHighlight: 1,
        startView: 2,
        minView: 2,
        format: "mm/dd/yyyy",
        forceParse: 0
    });
}
function setdatePickerOther()
{
    $('.form_date').datetimepicker({
        language: 'fr',
        weekStart: 1,
        autoclose: 1,
        todayHighlight: 1,
        startView: 2,
        minView: 2,
        format: "dd/mm/yyyy",
        forceParse: 0
    });
}
// values for cart/**/
var selectedstyle = "StyleOne";
var selectedfabric = "fabric0";
var selectedmanq = "Yes";
var selectedmanneColor = "2d2d2d";

function addToCart(){
    $(".loader").fadeIn("slow");
    var user_id = $("#user_id").val();
    var product_type = 'Swimsuit';
    var product_name = 'Swimsuit'+Math.floor(Math.random()*900) + 100;
    var product_price = 100;
    var quantity = 1;
    var total_amount = product_price * quantity;
    var url = "admin/api/orderProcess.php";
    $.post(url, {"type": "addToCart", "user_id": user_id,"selectedstyle":selectedstyle,"selectedfabric":selectedfabric,
    "selectedmanq":selectedmanq,"selectedmanneColor":selectedmanneColor,"product_name":product_name,"product_price":
        product_price,"product_type":product_type,"quantity":quantity,"total_amount":total_amount}, function (data) {
        var status = data.Status;
        if (status == "Success") {
            window.location="cart";
            $(".loader").fadeOut("slow");
        }
        else
        {
            $(".loader").fadeOut("slow");
            alert(data.Message);
        }
    });
}

/*in footer get user info and cart count start here*/
function get_user_info() {
    var user_id = $("#user_id").val();
    var url = "admin/api/registerUser.php";
    $.post(url, {"type": "getParticularUserData", "user_id": user_id}, function (data) {
        var status = data.Status;
        var userData = data.userData;
        if (status == "Success") {
            $(".top_user_name").html(userData.name);
            $(".user_profile_img").attr("src",userData.profile_pic);
        }
        else
        {

        }
    });
}
function get_cart_count() {
    var user_id = $("#user_id").val();
    var url = "admin/api/orderProcess.php";
    $.post(url, {"type": "getCartCount", "user_id": user_id}, function (data) {
        var status = data.Status;
        var count = data.count;
        if (status == "Success") {
            $(".bagde").html(count);
            $("#cartCount").val(count);
        }
        else
        {
            $("#cartCount").val(0);
        }
    });
}

/*in footer get user info end here*/
