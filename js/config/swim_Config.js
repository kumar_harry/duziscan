var camera, scene, s, controls, renderer;
var cameraControls, cube;
var clock = new THREE.Clock();
var loader = new THREE.CTMLoader();
var menTrouser_mesh, button_mesh;
var material;
var Lining_material, Piping_material;
var front_light;
var back_light;
var right_light;
var texArray = [];
var len, index = 0;
var forX = [];
var forY = [];
var height = $("#loadCanvas").height();
var width = $("#loadCanvas").width();
function swimLoad(geometry, s, material, x, y, z, ry, name, visi) {
    swim_mesh = new THREE.Mesh(geometry, material);
    swim_mesh.position.set(x, y, z);
    swim_mesh.scale.set(s, s, s);
    swim_mesh.rotation.y = ry;
    swim_mesh.name = name;
    swim_mesh.visible = false;
    scene.add(swim_mesh);
    //console.log(name);
    console.log("geometry = "+geometry + "s="+s+ "material = "+material+ "x = "+x+"  y = "+ y+" z = "+ z+" ry = "+ ry+" name = "+ name+" visi = "+ visi);
}
function init() {
    // SCENE
    scene = new THREE.Scene();
    // LIGHTS
    var ambientLight = new THREE.AmbientLight(0x858585);//858585
    scene.add(ambientLight);
    front_light = new THREE.DirectionalLight(0xffffff);
    front_light.position.set(-10, 20, 50);
    front_light.shadow.camera.far = 500.5;
    front_light.shadow.camera.left = -300;
    front_light.shadow.camera.right = 300;
    front_light.shadow.camera.top = 300;
    front_light.shadow.camera.bottom = -300;
    front_light.intensity = .35;
    scene.add(front_light);
    back_light = new THREE.DirectionalLight(0xffffff);
    back_light.position.set(0, 10, -50);
    back_light.shadow.camera.far = 100.5;
    back_light.shadow.camera.left = -300;
    back_light.shadow.camera.right = 300;
    back_light.shadow.camera.top = 300;
    back_light.shadow.camera.bottom = -300;
    back_light.intensity = .35;
    scene.add(back_light);
    right_light = new THREE.DirectionalLight(0xffffff);
    right_light.position.set(20, 10, -10);
    right_light.castShadow = true;
    right_light.shadowDarkness = 2;
    right_light.shadow.camera.far = 100.5;
    right_light.shadow.camera.left = -300;
    right_light.shadow.camera.right = 300;
    right_light.shadow.camera.top = 300;
    right_light.shadow.camera.bottom = -300;
    right_light.intensity = .35;
    scene.add(right_light);
    //LIGHTS
    // RENDERER
    renderer = new THREE.WebGLRenderer({antialias: true});
    renderer.setSize((width), (height));
    renderer.setPixelRatio(window.devicePixelRatio);
    renderer.setClearColor(0xffffff);
    renderer.shadowMapEnabled = true;
    renderer.shadowMapSoft = true;
    renderer.shadowMapType = THREE.PCFSoftShadowMap;
    renderer.gammaInput = true;
    renderer.gammaOutput = true;
    renderer.physicallyBasedShading = true;
    document.getElementById("loadCanvas").appendChild(renderer.domElement);
    window.addEventListener('resize', onWindowResize, false);
    //CAMERA
    camera = new THREE.PerspectiveCamera(32, (width) / (height), 1, 20000);
    camera.position.z = 30;
    camera.position.x = -20;
    camera.position.y = 0;
    camera.lookAt(new THREE.Vector3(0, 0, 0));
    controls = new THREE.OrbitControls(camera, renderer.domElement);
    controls.maxPolarAngle = Math.PI /2 * 115 / 120;
    cube = new THREE.Mesh(new THREE.BoxGeometry(0, 0, 0), new THREE.MeshNormalMaterial());
    cube.position.y = 0;
    scene.add(cube);
    cube.add(camera);
    texLoad();
    codLoad();
    defaultLoad();
    onWindowResize();
}
function texLoad() {
    for (var i = 0; i < texture.length; i++) {
        var mat = THREE.ImageUtils.loadTexture("tex/" + texture[i]);
        texArray.push(mat);
    }
}
function codLoad() {
    for (var i = 0; i < xCod.length; i++) {
        forX.push(xCod[i]);
    }
    for (var i = 0; i < yCod.length; i++) {
        forY.push(yCod[i]);
    }
}
Piping_material = new THREE.MeshPhongMaterial({color: 0xffffff, combine: THREE.MixOperation, side: THREE.DoubleSide});
Lining_material = new THREE.MeshPhongMaterial({color: 0x585859, combine: THREE.MixOperation, side: THREE.DoubleSide});
avatar_material = new THREE.MeshPhongMaterial({
    color: 0x2d2d2d,
    specular: 0xBABABA,
    shininess: 4,
    combine: THREE.MixOperation,
    side: THREE.DoubleSide
});
var aa = 0;
function defaultLoad() {
    len = sctm.length;
    (function () {
        function load() {
            //console.log(sctm)
            if (index < len) {
                aa++;
                loaderwait(aa);
                var name = sctm[index].substring(0, sctm[index].lastIndexOf('.'));
                loader.load("ctm/" + sctm[index], function (geometry) {
                    var c = name.split("_");
                    if (c[c.length - 1] == "Li") {
                        swimLoad(geometry, .028, Lining_material, 0, -33, 0, 0, name, false);
                    }
                    else if (c[c.length - 1] == "Pi") {
                        swimLoad(geometry, .028, Piping_material, 0, -33, 0, 0, name, false);
                    }
                    else if (c[0] == "Avatar") {
                        swimLoad(geometry, .028, avatar_material, 0, -33, 0, 0, name, false);
                    }
                    else {
                        material = new THREE.MeshPhongMaterial({
                            map: texArray[0],
                            combine: THREE.MixOperation,
                            specular: 0x050505,
                            shininess: 15,
                            side: THREE.DoubleSide,
                        });
                        material.map.wrapS = material.map.wrapT = THREE.RepeatWrapping;
                        material.map.repeat.set(forX[0], forY[0]);
                        swimLoad(geometry, .028, material, 0, -33, 0, 0, name, false);
                    }
                    ++index;
                    load(); // for looping the function
                    if (index == len) {
                        scene.traverse(function (swim_mesh) {
                            if (swim_mesh instanceof THREE.Mesh) {
                                if (swim_mesh.name == "StyleOne" || swim_mesh.name == "StyleOne_Li"
                                    || swim_mesh.name == "StyleOne_Pi" || swim_mesh.name == "Avatar") {
                                    swim_mesh.visible = true;
                                }
                            }
                        });
                        var x = camera.position.x;
                        var z = camera.position.z;
                        var y = camera.position.y;
                        TweenMax.to(camera.position, 2, {
                            x:0, z: 120, y: 10, onUpdate: function () {
                                camera.updateProjectionMatrix();
                                camera.lookAt(scene.position);
                            }
                        });
                    }
                }, {useWorker: true});
            }
        }
        load();
    })();
}
function loaderwait(value) {
    $(".progdiv").css("display", "block");
    $(".pload").css("display", "block");
    var progval = Math.floor(6.68 * value);
    $("#prog").css({"width": progval + "%"});
    $("#prog").text(progval + "%");
    if (progval == 100) {
        $(".progdiv").css("display", "none");
        $(".pload").css("display", "none");
        $(".cartbtn").show();
    }
}
function meshChange(getId) {
    $(".styletick").css("display","none");
    $("#"+getId+"tick").css("display","block");
    var lining = getId + "_Li";
    var piping = getId + "_Pi";
    scene.traverse(function (swim_mesh) {
        if (swim_mesh instanceof THREE.Mesh) {
            if (swim_mesh.visible) {
                var n = swim_mesh.name.split('_');
                if (n[0] == "Avatar") {
                    swim_mesh.visible = true;
                }
                else {
                    swim_mesh.visible = false;
                }
            }
            if (getId == swim_mesh.name || lining == swim_mesh.name || piping == swim_mesh.name) {
                swim_mesh.visible = true;
            }
        }
    });
}
function visi() {
    scene.traverse(function (swim_mesh) {
        if (swim_mesh instanceof THREE.Mesh) {
            var n = swim_mesh.name.split('_');
            if (n[0] == "Avatar") {
                swim_mesh.visible = true;
            }
        }
    });
}
function hide() {
    scene.traverse(function (swim_mesh) {
        if (swim_mesh instanceof THREE.Mesh) {
            if (swim_mesh.visible) {
                var n = swim_mesh.name.split('_');
                if (n[0] == "Avatar") {
                    swim_mesh.visible = false;
                }
            }
        }
    });
}
//for TEXTURE change
function textureChange(getColor) {
    $(".fabrictick").css("display","none");
    $("#fabrictick"+getColor).css("display","block");
    var b = getColor;
    scene.traverse(function (swim_mesh) {
        if (swim_mesh instanceof THREE.Mesh) {
            var s = swim_mesh.name.split("_");
            if (s[s.length - 1] == "Li" || s[s.length - 1] == "Pi" || s[0] == "Avatar") {
            }
            else {
                swim_mesh.material.map = texArray[b];
                swim_mesh.material.map.wrapS = swim_mesh.material.map.wrapT = THREE.RepeatWrapping;
                swim_mesh.material.map.repeat.set(forX[b], forY[b]);
                swim_mesh.material.needsUpdate = true;
            }
        }
    });
}
function manneColor(getId) {
    $(".skinstick").css("display","none");
    $("#skinstick"+getId).css("display","block");
    var Color = "0x" + getId;
    avatar_material.color.setHex(Color);
    avatar_material.needsUpdate = true;
}
function onWindowResize() {
    //windowHalfX = height;
    //windowHalfY = width;
    camera.aspect = width / height;
    camera.updateProjectionMatrix();
    renderer.setSize(width, height);
}
function animate() {
    requestAnimationFrame(animate);
    controls.update();
    render();
}
function render() {
    renderer.render(scene, camera);
}
init();
animate();