<?php
include("header2.php");
?>
<style>
.custom_feild{
    border: none;
}
    #profilefeild
    {
        display: none;
    }
    .nogutter{
        padding: 0px;
    }
    .custom_feild
    {
        cursor: default;
        padding: 0.6em 0.2em;
    }
input[type="text"]:disabled{background-color:#fff;}
    #message
    {
        margin-top: 10px;
        margin-bottom: 10px;
    }

</style>

<!-- breadcrumb -->
<div class="w3_breadcrumb">
    <div class="breadcrumb-inner">
        <ul>
            <li><a href="index.php">Home</a> <i> /</i></li>
            <li>User Profile</li>
        </ul>
    </div>
</div>
<!-- //breadcrumb -->

<div class="container">
    <div class="row myaccount-main-box">
        <div class="col-md-1"></div>
        <div class="col-md-10" style="box-shadow:0 0 5px;border-radius:5px;padding:20px">
            <h3 class="text-center w3l-inner-h-title">
                User Details
            </h3>
            <hr>
            <form action="webservice/update_data.php" enctype="multipart/form-data" method="Post">
                <p id="message" ></p>
                    <div class="col-md-12">
                        <div class="col-md-8 nogutter">
                            <div class="col-md-12 nogutter">
                                <div class="col-md-6 ">
                                    <div class="form-group">
                                            <label>First Name</label>
                                            <input  class="custom_feild ma_firstaname" required="" id="fname" name="fname" readonly
                                                    placeholder=" First Name" type="text">
                                    </div>
                                </div>
                                <div class="col-md-6 ">
                                    <div class="form-group">
                                        <label>Last Name</label>
                                        <input  class=" custom_feild ma_lastname" required="" id="lname" name="lname" readonly
                                               placeholder=" Last Name"  type="text">
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-12 nogutter">
                                <div class="col-md-6 ">
                                    <div class="form-group">
                                        <label>Gender</label>
                                        <select name="gender" class="custom_feild selectfeild ma_gender" id="gender" readonly="">
                                            <option selected="selected" value="Male"> Male</option>
                                            <option value=""> Select Gender </option>
                                            <option value="Female"> Female</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-6 ">
                                    <div class="form-group">
                                        <label>Country</label>
                                        <input  class="custom_feild ma_country_field textfeild" required="" readonly
                                                placeholder=" First Name" type="text">
                                        <select name="" class="ma_country countries  custom_feild selectfeild" id="countryId"
                                                 readonly="" hidden>
                                            <option value="">Select Country</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="col-md-4">
                            <img src=""  id="img" class="thumbnail pull-right user_profile" style="height:130px;margin-top:15px;width:58%">
                        </div>
                    </div>
                <div class="col-md-12 ">
                    <div class="col-md-4 ">
                        <div class="form-group">
                            <label>State</label>
                            <input  class="custom_feild ma_state_field textfeild" required=""  name="fname" readonly
                                    placeholder=" enter state" type="text">
                            <select name="" class="ma_state selectfeild states custom_feild" id="stateId" readonly="" hidden>
                                <option value="">Select State</option>
                            </select>
                        </div>
                    </div>
                    <div class="col-md-4 ">
                        <div class="form-group">
                            <label>City</label>
                            <div id="cityfeild">
                                <input  class="custom_feild ma_city_field textfeild" required="" readonly
                                        placeholder="enter state" type="text">
                                <select name="" class="ma_city selectfeild cities custom_feild" id="cityId" readonly="" hidden>
                                    <option value="">Select City</option>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4 ">
                        <div class="form-group">
                            <label>Date of Birth</label>
                            <input type="text" id="inputdob" placeholder="Date of birth" readonly class="ma_age signupField form_date
                             custom_feild">
                        </div>
                    </div>
                </div>
                <div class="col-md-12">
                    <div class="col-md-4 ">
                        <label>Height</label>
                        <div class="row nogutter">
                            <div class="col-md-4 ">
                                <select class="ma_height_param custom_feild" id="heightparam" readonly
                                        onchange="heightChange()">
                                    <option value="">Parameter</option>
                                    <option value="Inches">Inches</option>
                                    <option value="Centimeters">Centimeters</option>
                                    <option value="Feets">Feets</option>
                                </select>
                            </div>
                            <div class="col-md-8 " id="heightp">
                                <input type="text" readonly id="height" placeholder="Height" class="ma_height custom_feild only-numbers">
                            </div>
                        </div>

                    </div>
                    <div class="col-md-4 ">
                        <label>Stylist</label>
                        <input readonly type="text" id="stylist" placeholder="Enter stylist"   class="ma_stylist custom_feild">
                    </div>
                    <div class="col-md-4 ">
                        <label>Language</label>
                        <select class="custom_feild ma_language" id="language" readonly>
                            <option value="">Select Language</option>
                            <option value="English(US)">English(US)</option>
                            <option value="English(UK)">English(UK)</option>
                            <option value="FRENCE">FRENCE</option>
                        </select>
                    </div>
                </div>
                <div class="col-md-12">
                    <div class="col-md-4 ">
                        <label>Weight</label>
                        <div class="row nogutter">
                            <div class="col-md-4 ">
                                <select id="weightparam" class="ma_weight_param custom_feild" readonly>
                                    <option value="">Parameter </option>
                                    <option value="Kilograms">Kilograms</option>
                                    <option value="Pounds">Pounds</option>
                                    <option value="Stones">Stones</option>
                                </select>
                            </div>
                            <div class="col-md-8 " >
                                <input type="text" id="weight" placeholder="Weight" class="ma_weight custom_feild only-numbers" readonly>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4 ">
                        <div class="form-group">
                            <label>Age</label>
                            <input id="age" class="ma_age_no custom_feild" placeholder="Age" readonly
                                   disabled="disabled" type="text">
                        </div>
                    </div>
                    <div class="col-md-4 ">
                        <div class="form-group">
                            <label>Zip Code</label>
                            <input id="zipcode" class="ma_zipcode custom_feild" placeholder="zipcode" type="text">
                        </div>
                    </div>
                </div>
                <div class="col-md-12">
                    <div class="col-md-4 ">
                        <div class="form-group">
                            <label>Address</label>
                            <textarea  rows="3" id="address" class="ma_address custom_feild" placeholder="address"
                                       readonly></textarea>
                        </div>
                    </div>
                    <div class="col-md-4 ">
                        <div class="form-group"  style="">
                            <label>Mobile</label>
                            <input class="custom_feild ma_mobile"   type="text" id="mobile">
                        </div>
                    </div>
                    <div class="col-md-4 ">
                        <div class="form-group" id="profilefeild" style="">
                            <label>Profile Picture</label>
                            <input class="custom_feild" name="file0"  type="file" id="user_profile">
                        </div>
                    </div>
                </div>
                <div class="col-md-12">
                    <input id="edit" class=" pull-right custom_button"
                           value="Edit" type="button">
                    <input class="  pull-right custom_button" value="Update" type="button"
                           style="display: none;" id="update_profile">
                </div>
            </form>
        </div>
        <div class="col-md-1"></div>
    </div>
</div>
<?php
include("footer.php");
?>
<script>
    $(document).ready(function()
    {
        $("#edit").click(function()
        {
            $("#profilefeild").show();
            $(".form-control").attr("readonly",false);
            $(".form-control").attr("disabled",false);
            $(".ma_age_field").attr("disabled",true);
            $(".custom_feild").css({"border":"1px solid #ccc"});
            $(".custom_feild").attr("readonly",false);
            $(".textfeild").hide();
            $(".selectfeild").show();
            $("#edit").hide();
            $("#update_profile").show();
            loadScript("js/location1.js", function(){
                $('script[src="js/location.js"]').remove();
            });
        });
    });

    function ma_get_userdata(){
        $(".loader").fadeIn("slow");
        $('script[src="js/location.js"]').remove();
        var user_id = $("#user_id").val();
        var url = "admin/api/registerUser.php";
        $.post(url, {"type": "getParticularUserData", "user_id": user_id}, function (data) {
            var status = data.Status;
            var userData = data.userData;
            if (status == "Success") {
                var current_year = (new Date()).getFullYear();
                var name = userData.name;
                name = name.split(" ");
                var dob = userData.dob;
                dob = dob.split("/");

                var dateObj = new Date();
                var month = dateObj.getUTCMonth() + 1; //months from 1-12
                var day = dateObj.getUTCDate();
                var year = dateObj.getUTCFullYear();
                var age = year - dob[2];
                    if(dob[1] > month)
                    {
                        age = age - 1;
                    }
                    else{
                    }
                    var height = userData.height;
                    height  =  height.split(" ");
                    var weight = userData.weight;
                    weight  =  weight.split(" ");
                $(".ma_firstaname").val(name[0]);
                $(".ma_lastname").val(name[1]);
                $(".ma_gender").val(userData.gender);
                $(".ma_country_field").val(userData.b_country);
                $(".ma_state_field").val(userData.b_state);
                $(".ma_city_field").val(userData.b_city);
                $(".ma_age").val(userData.dob);
                $(".ma_age_no").val(age);
                $(".user_profile").attr("src",userData.profile_pic);
                $(".ma_stylist").val(userData.stylist);
                $(".ma_language").val(userData.lang);
                $(".ma_height").val(height[0]);
                $(".ma_height_param").val(height[1]);
                $(".ma_weight").val(weight[0]);
                $(".ma_weight_param").val(weight[1]);
                $(".ma_mobile").val(userData.mobile);
                $(".ma_address").val(userData.address);
                $(".ma_zipcode").val(userData.pincode);
                $(".loader").fadeOut("slow");
            }
            else
            {

            }
        });
    }
    $("#update_profile").click(function(){
        $(".loader").fadeIn("slow");
            $("#message").html("");
            var user_id = $("#user_id").val();
            var fname = $("#fname").val();
            var lname = $("#lname").val();
            var country = $("#countryId").val();
            var state = $("#stateId").val();
            var city = $("#cityId").val();
            var dob = $("#inputdob").val();
            var heightparam = $("#heightparam").val();
            var email = $("#email").val();
            var password = $("#password").val();
            var stylist = $("#stylist").val();
            var gender = $("#gender").val();
            var language = $("#language").val();
            var weightparam = $("#weightparam").val();
            var weight = $("#weight").val();
            var user_profile = $("#user_profile").val();
            var zipcode = $("#zipcode").val();
            var mobile = $("#mobile").val();
            var address = $("#address").val();
            if(city == "" || city == undefined)
            {
                city = $(".cf").val();
            }

            if(heightparam == "Feets"){
                var height0 = $("#height0").val();
                var height1 = $("#height1").val();
                height = (parseFloat(height0)*12)+parseFloat(height1);
            }else if(heightparam == "Centimeters"){
                height = parseFloat($("#height").val())*2.54;
            }
            else{
                height = $("#height").val();
            }
            if(weightparam == "Kilograms"){
                weight = (parseFloat(weight)*2.20462);
            }
            else if(weightparam == "Stones"){
                weight = (parseFloat(weight)*14);
            }
        if (fname == '' || lname == '' || country == '' || state == '' || city == '' || height == '' || email == '' || password == '' ||
            stylist == '' || gender == '' || language == '' || weight == '' || dob == '' || address == '' || zipcode == '' || mobile== '') {
            $("#message").html("All Feilds Must Required Filled");
            $("#message").css('color', 'red');
            return false;
        }
        var url = "admin/api/registerUser.php";
            var data = new FormData();
            if(user_profile != "") {
                var ext = user_profile.split(".");
                ext = ext[ext.length - 1];
                ext = ext.toLowerCase();
                if(ext == "jpg" || ext == "jpeg" || ext == "png" || ext == "gif"){
                    var img = document.getElementById("user_profile");
                    data.append("user_profile", img.files[0]);
                }else{
                    $("#message").html("Invalid Profile Picture Selected");
                    $("#message").css('color', 'red');
                    return false;
                }
            }
            data.append("type", "updateProfile");
            data.append("fname", fname);
            data.append("lname", lname);
            data.append("countryId", country);
            data.append("stateId", state);
            data.append("cityId", city);
            data.append("stylist", stylist);
            data.append("dob", dob);
            data.append("gender", gender);
            data.append("language", language);
            data.append("height", height);
            data.append("heightparam", heightparam);
            data.append("weight", weight);
            data.append("weightparam", weightparam);
            data.append("mobile", mobile);
            data.append("zipcode", zipcode);
            data.append("address", address);
            data.append("user_id", user_id);
            var xhttp = new XMLHttpRequest();
            xhttp.onreadystatechange = function () {
                if (this.readyState === 4 && this.status === 200) {
                    var response = xhttp.responseText;
                    response = JSON.parse(response);
                    var status = response.Status;
                    if (status == "Success") {
                        //$("#message").html(response.Message);
                        $(".loader").fadeOut("slow");
                        location.reload();
                    }
                    else {
                        $(".loader").fadeOut("slow");
                        $("#message").html(response.Message);
                        $("#message").css('color', 'red');
                    }
                }
            };
            xhttp.open("POST", url, true);
            xhttp.send(data);

    });
    ma_get_userdata();

</script>
