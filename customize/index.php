<?php
$user_id = "";
if(isset($_REQUEST['user_id']))
{
    $user_id = $_REQUEST['user_id'];
}
echo "<input type='hidden' id='user_id' value=".$user_id.">";
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title>DuziScan</title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="icon" type="image/icon" href="">
	<link rel='stylesheet prefetch' href='https://fonts.googleapis.com/css?family=Open+Sans:600'>
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <link rel="stylesheet" href="css/dist/sidebar-menu.css">
    <link rel="stylesheet" href="css/bootstrap.css">
    <link rel="stylesheet" href="css/font-awesome.css">
    <!-- <link rel="stylesheet" href="css/style.css"> -->
    <!-- <link rel="stylesheet" href="css/bootstrap.min.css"> -->
    <link rel="stylesheet" href="css/readymade.css">
	<link rel="stylesheet" href="css/login.css">
</head>
<style>
    /*progress bar start here*/
    .progdiv {
        left: 40%;
        position: absolute;
        top: 50%;
        width: 30%;
        display: none;
    }
    .pload {
        left: 40%;
        position: absolute;
        top: 46%;
        width: 30%;
        display: none;
        text-align: center;
    }
       /*html {
            overflow-y: hidden !important;
			height:100%;
			width:100%;
        }
        body {overflow:hidden !important}
        */


        /*html::-webkit-scrollbar-track {
            -webkit-box-shadow: inset 0 0 6px rgba(0, 0, 0, 0.3);
            border-radius: 10px;
            background-color: transparent;
        }

        html::-webkit-scrollbar {
            width: 12px;
            background-color: #F5F5F5;
        }*/

        /*html::-webkit-scrollbar-thumb {
            border-radius: 10px;
            -webkit-box-shadow: inset 0 0 6px rgba(0, 0, 0, .3);
            background-color: #8c939d ! important;
        }*/
		#avatar {position:absolute;top: -7px;}
		.shirt_fit_list {position:absolute}
    .cartbtn {
        border: medium none;
        letter-spacing: 1.5px;
        padding: 10px;
        position: fixed;
        right: 10px;
        top: 300px;
        width: 110px;
        z-index: 999999;
    }
    .loader {
        background: #fff none repeat scroll 0 0;
        height: 900px;
        position: fixed;
        width: 100%;
        z-index: 999;
        display: none;
    }
    .loaderimg {
        background-attachment: fixed;
        height: 50px;
        margin-left: 49%;
        margin-top: 20%;
        width: 50px;
    }
    .styletick {
        color: green;
        font-size: 14px;
        margin-top: 0;
        position: absolute;
    }
    .fabrictick {
        color: green;
        font-size: 14px;
        margin-top: 0;
        position: absolute;
    }
    .skinstick {
        color: green;
        font-size: 14px;
        margin-top: 0;
        position: absolute;
    }
</style>

<body>
<div id="loadd" class="loader hideloader">
    <img class="loaderimg" src="../images/762.gif">
    <p style="margin-left:49%;font-size:18px">Loading....</p>
</div>
    <!-- End Header Section -->
	<!-- Start Body OF Avatar-->
	<!-- <div id="avatar"> -->
	
	<!-- </div> -->
	<!-- <!--End Body OF Avatar-->
    <p class="pload">Please Wait ... Object Is Loading</p>
    <div class="progress progdiv">
        <div id="prog" class="progress-bar progress-bar-striped active" role="progressbar" aria-valuenow="40"
             aria-valuemin="0" aria-valuemax="100" style="width:0%">
            0%
        </div>
    </div>
    <input class="cartbtn" value="Add to Cart" onclick="addToCart()" style="display: none" type="button">
    <div class="container-fluid">
     <section >
    <ul class="sidebar-menu " style="height: 130px">
        <li style="display: flex;flex-direction: column;text-align: center">
            <img src="images/logo.png" style="width: 70px;height: 70px;border-bottom: 1px solid lightgray;margin: 5px"/>
            <span style="cursor: pointer"><i class="fa fa-arrow-left"></i> Back</span>
        </li>
            <li class=" 1">

                <a  id="fit" onclick="">
                    <img class="side_imge  img_style" src="images/style/Style01.jpg"/><span class="side_img_font">Style</span>
               </a>
                <ul class="sidebar-submenu">
                    <li id="StyleOne" onclick="meshChange(this.id);" class="inner_li style active" title="style|Style01.jpg">
                        <i class="fa fa-check-circle-o styletick" id="StyleOnetick" ></i>
                        <a>
                        <img class="side_img " src="images/style/Style01.jpg"/>
                        <span class="side_img_font"> Style 1</span></a>
                    </li>
                    <li id="StyleTwo" onclick="meshChange(this.id);" class="inner_li style" title="style|Style02.jpg">
                        <i class="fa fa-check-circle-o styletick" id="StyleTwotick" style="display: none"></i>
                        <a>
                        <img class="side_img " src="images/style/Style02.jpg"/>
                        <span class="side_img_font"> Style 2</span>
                    </a>
                    </li>
                    <li id="StyleThree" onclick="meshChange(this.id);" class="inner_li style" title="style|Style03.jpg">
                        <i class="fa fa-check-circle-o styletick" id="StyleThreetick" style="display: none"></i>
                        <a>
                        <img class="side_img " src="images/style/Style03.jpg"/>
                        <span class="side_img_font"> Style 3</span>
                    </a>
                    </li>
                    <li id="StyleFour" onclick="meshChange(this.id);" class="inner_li style" title="style|Style04.jpg">
                        <i class="fa fa-check-circle-o styletick" id="StyleFourtick" style="display: none"></i>
                        <a>
                        <img class="side_img " src="images/style/Style04.jpg"/>
                        <span class="side_img_font"> Style 4</span>
                    </a>
                    </li>
                    <li id="StyleFive" onclick="meshChange(this.id);" class="inner_li style" title="style|Style05.jpg">
                        <i class="fa fa-check-circle-o styletick" id="StyleFivetick" style="display: none"></i>
                        <a>
                        <img class="side_img " src="images/style/Style05.jpg"/>
                        <span class="side_img_font">Style 5</span>
                    </a>
                    </li>
                    <li id="StyleSix" onclick="meshChange(this.id);" class="inner_li style" title="style|Style06.jpg">
                        <i class="fa fa-check-circle-o styletick" id="StyleSixtick" style="display: none"></i>
                        <a>
                        <img class="side_img " src="images/style/Style06.jpg"/>
                        <span class="side_img_font">Style 6</span>
                    </a>
                    </li>
                </ul>
            </li>
            <li >
                <a class=" 3"  id="fabric" onclick="">
          <img class="side_imge  img_fabric" src="images/fabric/F1.jpg"/><span class="side_img_font">Fabric</span>
          </a>
			
				 <!-- style="overflow-y:scroll;height: 700px;width: 220px;" -->
                <ul class="sidebar-submenu">
					<li id="0" onclick="textureChange(this.id);" class="inner_li fabric blue green check" title="fabric|F1.jpg">
                        <i class="fa fa-check-circle-o fabrictick" id="fabrictick0" ></i>
                        <a>
                            <img class="side_img" src="images/fabric/F1.jpg"/>
						    <span class="side_img_font">Fabric 1</span></a>
                    </li>
                    <li id="1" onclick="textureChange(this.id);" class="inner_li fabric blue purple check" title="fabric|F2.jpg">
                        <i class="fa fa-check-circle-o fabrictick" id="fabrictick1" style="display: none"></i>
                        <a>
                            <img class="side_img" src="images/fabric/F2.jpg"/>
						    <span class="side_img_font">Fabric 2</span></a>
                    </li>
                    <li id="2" onclick="textureChange(this.id);" class="inner_li fabric orange check" title="fabric|F3.jpg">
                        <i class="fa fa-check-circle-o fabrictick" id="fabrictick2" style="display: none"></i>
                        <a>
                            <img class="side_img" src="images/fabric/F3.jpg"/>
						    <span class="side_img_font">Fabric 3</span></a>
                    </li>
                    <li id="3" onclick="textureChange(this.id);" class="inner_li fabric blue check" title="fabric|F4.jpg">
                        <i class="fa fa-check-circle-o fabrictick" id="fabrictick3" style="display: none"></i>
                        <a>
                            <img class="side_img" src="images/fabric/F4.jpg"/>
						    <span class="side_img_font">Fabric 4</span></a>
                    </li>
                    <li id="4" onclick="textureChange(this.id);" class="inner_li fabric green purple strip" title="fabric|F5.jpg">
                        <i class="fa fa-check-circle-o fabrictick" id="fabrictick4" style="display: none"></i>
                        <a>
                            <img class="side_img" src="images/fabric/F5.jpg"/>
						    <span class="side_img_font">Fabric 5</span></a>
                    </li>
                    <li id="5" onclick="textureChange(this.id);" class="inner_li fabric red purple strip" title="fabric|F6.jpg">
                        <i class="fa fa-check-circle-o fabrictick" id="fabrictick5" style="display: none"></i>
                        <a>
                            <img class="side_img" src="images/fabric/F6.jpg"/>
						    <span class="side_img_font">Fabric 6</span></a>
                    </li>
                    <li id="6" onclick="textureChange(this.id);" class="inner_li fabric pink check" title="fabric|F7.jpg">
                        <i class="fa fa-check-circle-o fabrictick" id="fabrictick6" style="display: none"></i>
                        <a>
                            <img class="side_img" src="images/fabric/F7.jpg"/>
						    <span class="side_img_font">Fabric 7</span></a>
                    </li>
                    <li id="7" onclick="textureChange(this.id);" class="inner_li fabric blue check" title="fabric|F8.jpg">
                        <i class="fa fa-check-circle-o fabrictick" id="fabrictick7" style="display: none"></i>
                        <a>
                            <img class="side_img" src="images/fabric/F8.jpg"/>
						    <span class="side_img_font">Fabric 8</span></a>
                    </li>
					<li id="8" onclick="textureChange(this.id);" class="inner_li fabric white strip" title="fabric|F9.jpg">
                        <i class="fa fa-check-circle-o fabrictick" id="fabrictick8" style="display: none"></i>
                        <a>
                            <img class="side_img" src="images/fabric/F9.jpg"/>
						    <span class="side_img_font">Fabric 9</span></a>
                    </li>
                    <li id="9" onclick="textureChange(this.id);" class="inner_li fabric white yellow strip" title="fabric|F10.jpg">
                        <i class="fa fa-check-circle-o fabrictick" id="fabrictick9" style="display: none"></i>
                        <a>
                            <img class="side_img" src="images/fabric/F10.jpg"/>
						    <span class="side_img_font">Fabric 10</span>
                        </a>
                    </li>
                    <li id="10" onclick="textureChange(this.id);" class="inner_li fabric orange blue purple check" title="fabric|F11.jpg">
                        <i class="fa fa-check-circle-o fabrictick" id="fabrictick10" style="display: none"></i>
                        <a>
                            <img class="side_img" src="images/fabric/F11.jpg"/>
						    <span class="side_img_font">Fabric 11</span>
                        </a></li>
                    <li id="11" onclick="textureChange(this.id);" class="inner_li fabric purple green blue check" title="fabric|F12.jpg">
                        <i class="fa fa-check-circle-o fabrictick" id="fabrictick11" style="display: none"></i>
                        <a>
                            <img class="side_img" src="images/fabric/F12.jpg"/>
						    <span class="side_img_font">Fabric 12</span>
                        </a>
                    </li>
                    <li id="12" onclick="textureChange(this.id);" class="inner_li fabric orange red white check" title="fabric|F13.jpg">
                        <i class="fa fa-check-circle-o fabrictick" id="fabrictick12" style="display: none"></i>
                        <a>
                            <img class="side_img" src="images/fabric/F13.jpg"/>
						    <span class="side_img_font">Fabric 13</span>
                        </a>
                    </li>
					<li id="13" onclick="textureChange(this.id);" class="inner_li fabric purple green blue check" title="fabric|F14.jpg">
                        <i class="fa fa-check-circle-o fabrictick" id="fabrictick13" style="display: none"></i>
                        <a>
                            <img class="side_img" src="images/fabric/F14.jpg"/>
						    <span class="side_img_font">Fabric 14</span>
                        </a>
                    </li>
                    <li id="14" onclick="textureChange(this.id);" class="inner_li fabric orange red white check" title="fabric|F15.jpg">
                        <i class="fa fa-check-circle-o fabrictick" id="fabrictick14" style="display: none"></i>
                        <a>
                            <img class="side_img" src="images/fabric/F15.jpg"/>
						<span class="side_img_font">Fabric 15</span>
                        </a></li>
					<li id="15" onclick="textureChange(this.id);" class="inner_li fabric orange red white check" title="fabric|F16.jpg">
                        <i class="fa fa-check-circle-o fabrictick" id="fabrictick15" style="display: none"></i>
                        <a>
                            <img class="side_img" src="images/fabric/F16.jpg"/>
						    <span class="side_img_font">Fabric 16</span>
                        </a>
                    </li>
					<li id="16" onclick="textureChange(this.id);" class="inner_li fabric purple green blue check" title="fabric|F17.jpg">
                        <i class="fa fa-check-circle-o fabrictick" id="fabrictick16" style="display: none"></i>
                        <a>
                            <img class="side_img" src="images/fabric/F17.jpg"/>
						    <span class="side_img_font">Fabric 17</span>
                        </a>
                    </li>
                    <li id="17" onclick="textureChange(this.id);" class="inner_li fabric orange red white check" title="fabric|F18.jpg">
                        <i class="fa fa-check-circle-o fabrictick" id="fabrictick17" style="display: none"></i>
                        <a>
                            <img class="side_img" src="images/fabric/F18.jpg"/>
						    <span class="side_img_font">Fabric 18</span>
                        </a>
                    </li>
                </ul>
            </li>
			<li class=" 5">

                <a  id="manii" onclick="" style="display: flex;flex-direction: column;">
                    <div style="display: flex;flex-direction: row;">
                        <img class="side_imge  img_manneqine" src="images/manneqine/M1.jpg"/>
                        <span class="side_img_font">mannequin</span>
                    </div>

		            <div class="manq">
                      <input id="visib" onclick="visi();" type="radio" name="checkbox" checked><i class="chck"></i>Yes
                      <input id="hidd" onclick="hide();" type="radio" style="margin-left: 26px;" name="checkbox"><i class="chck"></i>No
                    </div>
                </a>
		  <!-- <i class="fa fa-angle-left side_img_txt"></i> -->
				<!-- </a> -->
                <!-- <ul class="sidebar-submenu"> -->
					<!-- <li id="2d2d2d" onclick="maniColor(this.id);" class="inner_li hidd manneqine" title="manneqine|M1.jpg"><a> -->
					<!-- <img  class="side_img" src="images/manneqine/M1.jpg"/><span class="side_img_font"> No</span></a></li> -->
                    <!-- <li id="2d2d2d" onclick="maniColor(this.id);" class="inner_li visib manneqine" title="manneqine|M1.jpg"><a> -->
					<!-- <img  class="side_img" src="images/manneqine/M1.jpg"/><span class="side_img_font">Yes</span></a></li> -->
            	<!-- </ul> -->
            </li>
			<li id="mannequ" class="manneq">
                <a  id="mani" onclick="">
          <img class="side_imge  img_manneqine" src="images/manneqine/M1.jpg"/><span class="side_img_font">Skin color</span>
          <i class="fa fa-angle-left side_img_txt"></i>
				</a>
                <ul class="sidebar-submenu">
					<li id="2d2d2d" onclick="manneColor(this.id);" class="inner_li manneqine" title="manneqine|M1.jpg">
                        <i class="fa fa-check-circle-o skinstick" id="skinstick2d2d2d" ></i>
                        <a>
					        <img  class="side_img" src="images/manneqine/M1.jpg"/><span class="side_img_font"> Skin 1</span>
                        </a>
                    </li>
                    <li id="996e26" onclick="manneColor(this.id);" class="inner_li manneqine" title="manneqine|M4.jpg">
                        <i class="fa fa-check-circle-o skinstick" id="skinstick996e26" style="display: none"></i>
                        <a>
					        <img class="side_img" src="images/manneqine/M4.jpg"/><span class="side_img_font"> Skin 2</span>
                        </a>
                    </li>
					 <li id="d9d9b8" onclick="manneColor(this.id);" class="inner_li manneqine" title="manneqine|M3.jpg">
                         <i class="fa fa-check-circle-o skinstick" id="skinstickd9d9b8" style="display: none"></i>
                         <a>
					        <img class="side_img" src="images/manneqine/M3.jpg"/><span class="side_img_font"> Skin 3</span>
                         </a>
                     </li>
                    <li id="e6bc67" onclick="manneColor(this.id);" class="inner_li manneqine" title="manneqine|M2.jpg">
                        <i class="fa fa-check-circle-o skinstick" id="skinsticke6bc67" style="display: none"></i>
                        <a>
					        <img class="side_img" src="images/manneqine/M2.jpg"/><span class="side_img_font"> Skin 4</span>
                        </a>
                    </li>
					<li id="997d45" onclick="manneColor(this.id);" class="inner_li manneqine" title="manneqine|M5.jpg">
                        <i class="fa fa-check-circle-o skinstick" id="skinstick997d45" style="display: none"></i>
                        <a>
					        <img class="side_img" src="images/manneqine/M5.jpg"/><span class="side_img_font"> Skin 5</span>
                        </a>
                    </li>
				</ul>
            </li>

        </ul>
    </section>
    </div>

	<script type="text/javascript" src="js/duzi-script.js"></script>
	<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js" type="text/javascript"></script>
    <script src="https://code.jquery.com/jquery-3.0.0.min.js"></script>
    <script src="css/dist/sidebar-menu.js"></script>
    <script>
        $.sidebarMenu($('.sidebar-menu'));
    </script>
	<script>
	$('#hidd').click(function() {
				document.getElementById('mannequ').style.display = 'none';
      });
	  $('#visib').click(function() {
//	            alert('item clicked');
				document.getElementById('mannequ').style.display = 'flex';
      });
	</script>
	</body>
	<script src="js/config/libs/uiblock.js"></script>
	<script src="js/config/libs/TweenMax.js"></script>
	<script src="js/config/libs/three.min.js"></script>
	<script src="js/config/libs/OrbitControls.js"></script>
	<script src="js/config/libs/lzma.js"></script>
	<script src="js/config/libs/ctm.js"></script>
	<script src="js/config/libs/CTMLoader.js"></script>
	<script src="js/config/swim_JSON.js"></script>
	<!-- <script src="js/config/swim_RemLoad.js"></script> -->
	<script src="js/config/swim_Config.js"></script>
	<script src="js/config/swim_Cam.js"></script>
	<script src="js/config/swim_Cart.js"></script>

<script>
    var selectedstyle = "StyleOne";
    var selectedfabric = "fabric0";
    var selectedmanq = "Yes";
    var selectedmanneColor = "2d2d2d";

    function addToCart(){
        $(".loader").fadeIn("slow");
        var user_id = $("#user_id").val();
        var product_type = 'Swimsuit';
        var product_name = 'Swimsuit'+Math.floor(Math.random()*900) + 100;
        var product_price = 100;
        var quantity = 1;
        var total_amount = product_price * quantity;
        if(user_id == "")
        {
            alert("User id should not be blank");
            $(".loader").fadeOut("slow");
            return false;
        }
        var url = "../admin/api/orderProcess.php";
        $.post(url, {"type": "addToCart", "user_id": user_id,"selectedstyle":selectedstyle,"selectedfabric":selectedfabric,
            "selectedmanq":selectedmanq,"selectedmanneColor":selectedmanneColor,"product_name":product_name,"product_price":
            product_price,"product_type":product_type,"quantity":quantity,"total_amount":total_amount}, function (data) {
            var status = data.Status;
            $(".loader").fadeOut("slow");
            if (status == "Success") {
                window.close();
                $(".loader").fadeOut("slow");
            }
            else
            {
                $(".loader").fadeOut("slow");
                alert(data.Message);
            }
        });
    }
</script>
</html>