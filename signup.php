<?php include("header2.php"); ?>
    <!-- breadcrumb -->
    <div class="w3_breadcrumb">
        <div class="breadcrumb-inner">
            <ul>
                <li><a href="index">Home</a> <i> /</i></li>
                <li>Sign Up</li>
            </ul>
        </div>
    </div>
    <!-- //breadcrumb -->
    <!--/content-inner-section-->
    <div class="w3_content_agilleinfo_inner">
        <div class="container">
            <div class="inner-agile-w3l-part-head">
                <h2 class="w3l-inner-h-title">Sign Up</h2>
            </div>
            <div class="ab-w3l-spa">
                <div class="row">
                    <!-- signin form end here -->
                    <div class="col-md-5">
                        <img class="img-responsive loginImg" src="images/web/shutterstock_294251069Form.png"/>
                    </div>
                    <!-- signin form end here -->
                    <!-- signup form start here -->
                    <div class="col-md-1"></div>
                    <div class="col-md-7 login-box">
                        <p class="errorMessage">Error msg !!</p>
                        <div class="row">
                            <div class="col-md-6">
                                <input type="text" id="fname" placeholder="Enter First Name"
                                       class="custom_signup_feild only-letters">
                            </div>
                            <div class="col-md-6">
                                <input type="text" id="lname" placeholder="Enter Last Name" class="custom_signup_feild only-letters">
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-6">
                                <select name="" class="selectBox-signup countries  custom_signup_feild" id="countryId"
                                        onchange="setdate()">
                                    <option value="">Select Country</option>
                                </select>
                            </div>
                            <div class="col-md-6">
                                <select name="" class="selectBox-signup states custom_signup_feild" id="stateId">
                                    <option value="">Select State</option>
                                </select>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-6" id="cityfeild">
                                <select name="" class="selectBox-signup cities custom_signup_feild" id="cityId">
                                    <option value="">Select City</option>
                                </select>
                            </div>
                            <div class="col-md-6">
                                <input type="text" id="inputdob" placeholder="Date of birth"
                                       class=" signupField form_date custom_signup_feild">
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-6">
                                <input type="file" id="user_profile" class=" signupField custom_signup_feild">
                            </div>
                            <div class="col-md-6">

                                <div class="col-md-4" style="padding: 0px;">
                                    <select class=" selectBox-signup custom_signup_feild" id="heightparam"
                                            onchange="heightChange()">
                                        <option value="">Parameter</option>
                                        <option value="Inches">Inches</option>
                                        <option value="Centimeters">Centimeters</option>
                                        <option value="Feets">Feets</option>
                                    </select>
                                </div>
                                <div class="col-md-8" style="padding: 0px;" id="heightp">
                                    <input type="text" id="height" placeholder="Height" class="signupField custom_signup_feild only-numbers">
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-6">
                                <input type="text" id="email" placeholder="Enter email"
                                       class=" signupField custom_signup_feild">
                            </div>
                            <div class="col-md-6">
                                <input type="password" id="password" placeholder="Enter password"
                                       class=" signupField custom_signup_feild">
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <input type="text" id="stylist" placeholder="Enter stylist"
                                       class=" signupField custom_signup_feild">
                            </div>
                            <div class="col-md-6">
                                <select class=" selectBox-signup custom_signup_feild" id="gender">
                                    <option value="">Select Gender</option>
                                    <option value="Male">Male</option>
                                    <option value="Female">Female</option>
                                </select>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-6">
                                <select class=" selectBox-signup custom_signup_feild" id="language">
                                    <option value="">Select Language</option>
                                    <option value="English(US)">English(US)</option>
                                    <option value="English(UK)">English(UK)</option>
                                    <option value="FRENCE">FRENCE</option>
                                </select>
                            </div>
                            <div class="col-md-6">
                                <div class="col-md-4" style="padding: 0px;">
                                    <select id="weightparam" class="selectBox-signup custom_signup_feild">
                                        <option value="Kilograms">Kilograms</option>
                                        <option value="Pounds">Pounds</option>
                                        <option value="Stones">Stones</option>
                                    </select>
                                </div>
                                <div class="col-md-8" style="padding: 0px;">
                                    <input type="text" id="weight" placeholder="Weight" class=" signupField custom_signup_feild only-numbers">
                                </div>

                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <input type="text" id="mobile" placeholder="Enter Mobile"
                                       class="signupField custom_signup_feild only-numbers">
                            </div>
                            <div class="col-md-6">
                                <input type="text" id="zipcode" placeholder="Enter Zipcode"
                                       class="signupField custom_signup_feild only-numbers">
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <textarea  id="address" placeholder="Enter Address"
                                           class="signupField custom_signup_feild "></textarea>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-12">
                                <label style="margin-top: 3%;color: red" id="signup_message"></label>
                                <input type="button" class="signInBtn pull-right " value="Sign Up" id="signUpbtn"
                                       onclick="registerUser()">
                            </div>
                            <div class="col-md-12">
                                <a class="ancor-textDeco" data-toggle="modal" data-target="#loginModal"><label
                                            class="no-account"><span></span>Have an account ?
                                        <span class="create-account">Sign In !!</span> </label>
                                </a>
                            </div>
                        </div>
                    </div>
                    <!-- signup form end here -->


                </div>
            </div>
        </div>
    </div>
    <div class="clearfix"></div>
    <div class="forSpace"></div>
    <!--//content-inner-section-->

<?php include("footer.php"); ?>