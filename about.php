<?php include("header2.php"); ?>
    <!-- breadcrumb -->
    <div class="w3_breadcrumb">
        <div class="breadcrumb-inner">
            <ul>
                <li><a href="index">Home</a> <i> /</i></li>
                <li>About Us</li>
            </ul>
        </div>
    </div>
    <!-- //breadcrumb -->
    <!--/content-inner-section-->
    <div class="w3_content_agilleinfo_inner">
        <div class="container">
            <div class="inner-agile-w3l-part-head">
                <h2 class="w3l-inner-h-title">About Us</h2>
            </div>
            <div class="ab-w3l-spa">
                <img src="images/web/header2black.png" alt="" class="img-responsive" style="width: 100%;"/>
                <p>
                    We offer affordable, superbly made and fit clothing options to any woman who wants a more modest
                    twist on fashions that fit and look appropriate  with an all inclusive goal for women with disabilities. Let’s take fashion
                    to the next level for everyone!
                    <br>
                    <br>
                    DuziScan was developed by its three co-founders whose young family member has a neurological
                    disability and wants to dress hip but needs more modest and customized designs so she can be independent
                    when dressing. Our idea to make clothing with superb fit and style for women who have difficulty
                    dressing themselves due to neurological or physical disabilities has since expanded.
                    <br>
                    <br>
                    Out of this idea we learned most women of any age, with and without disabilities, want stylish
                    clothing with more modest designs. So now, we are excited to announce we have two phases of rollout.
                    <br>
                    <br>
                    After learning about and purchasing a state-of-the-art 3D Body Scanner, DuziScan's Phase I will
                    include off-the-rack AND bespoke swimwear, tunic tops and foundation garments all designed by our
                    in-house professional designer and co-founder, Judi Aultman, and MADE IN THE USA.
                    <br>
                    <br>
                    Phase I will also offer third party services with our Custom MOBILE 3D Body Scanner at an incredibly
                    affordable cost and most industry-accurate scans to uniform companies, tailors, costumers, etc. to
                    reduce/eliminate multiple fittings and alterations. Our clients will be able to offer perfect fit
                    clothing, uniforms and costumes.
                    <br>
                    <br>
                    Close on the heels of Phase I, DuziScan will launch Phase II, rolling out custom designs
                    specifically for women with neurological and physical disabilities who will now have more
                    independence dressing themselves. These designs will feature front and side closures with cutting
                    edge materials and self closing technology.
                    <br>
                    <br>
                    Phase II will include national expansion of Custom Mobile 3D Body Scanners and broader commercial
                    use to offer bespoke clothing to the general public through collaborations with designers and
                    clothing manufacturers.
                    <br>
                    <br>
                <h3 class="founder-heading">
                    The Founders of DuziScan
                </h3>
                <h4 class="founder-name">
                    Jennifer Deaton, CEO
                </h4>
                <p class="fouder-desc">
                    Jennifer has a background in economics and worked in the investment industry for a short time. Her
                    Life Experience came staying home to raise 2 children, one of whom has severe Autism Spectrum
                    Disorder. She is the inspiration for our design business! Jennifer ran a home business while caring
                    and providing therapy for her special needs daughter, Hannah. As Hannah became a teenager, clothes
                    were more difficult to find and nothing was available so Hannah could have the dignity of dressing
                    herself. This is where our story really begins and we are so excited to bring this vision to life!
                </p>
                <h4 class="founder-name">
                    Judi Aultman, Lead Designer
                </h4>
                <p class="fouder-desc">
                    Judi ran a bookkeeping business but always with a goal to put her education and gifts as a designer
                    into business. As the grandmother of Hannah, Judi could not only see the difficulties in finding
                    great clothing for Hannah but, has the expertise and contacts to make the vision a reality. Judi’s
                    experience in the small business world of art and design makes her uniquely qualified to lead our
                    Design Department and her love as a grandmother gives her the passion to achieve our Mission!

                </p>
                <h4 class="founder-name">
                    Kathy Anderson, Vice President & Customer Service Diva
                </h4>
                <p class="fouder-desc">
                    Kathy has a varied background in organization and administration of business both for profit and
                    non-profit. Most recently, Kathy runs her own home-based Virtual Assistant business,  with clients as far
                    as France. As great-aunt of Hannah, Kathy has been an integral part of developing our business plan,
                    keeping us organized and on target with goals. With her own frustrations in finding stylish clothing
                    with a perfect fit, Kathy helped us understand our market was bigger than we originally planned. She
                    is thrilled to be part of bringing women freedom in choosing and wearing clothes that fit just
                    right!

                </p>
                <h4 class="founder-name">
                    Jeffrey Deaton, CFO
                </h4>
                <p class="fouder-desc">
                    Jeff is an actuary and reinsurance broker. As a well-respected innovator in his own industry, we
                    knew Jeff was perfect as CFO. He has a detailed, refined critical eye with current business trend
                    knowledge as well as the Risk Management side to run scenarios; he isn’t afraid to ask tough
                    questions and push for excellence. He’s the guy every investor loves! Jeff is not only Hannah’s
                    fabulous dad but he truly supports the Mission of DuziScan with an energy second to none!

                </p>

                </p>

            </div>
        </div>
    </div>
    <div class="team-section">
        <div class="container">
            <h3 class="tittle">Our Team</h3>
            <div class="team-row">
                <div class="col-md-3 team-grids">
                    <div class="team-img">
                        <img class="img-responsive" src="images/web/no-user-img.png" alt="">
                        <div class="captn">
                            <ul class="top-links">
                                <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                                <li><a href="#"><i class="fa fa-twitter"></i></a></li>

                                <li><a href="#"><i class="fa fa-google-plus"></i></a></li>
                            </ul>

                        </div>
                    </div>
                    <div class="team-agile">
                        <h4>Jennifer Deaton</h4>

                    </div>
                </div>
                <div class="col-md-3 team-grids">
                    <div class="team-img">
                        <img class="img-responsive" src="images/web/no-user-img.png" alt="">
                        <div class="captn">
                            <ul class="top-links">
                                <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                                <li><a href="#"><i class="fa fa-twitter"></i></a></li>

                                <li><a href="#"><i class="fa fa-google-plus"></i></a></li>
                            </ul>

                        </div>
                    </div>
                    <div class="team-agile">
                        <h4>Judi Aultman</h4>

                    </div>
                </div>
                <div class="col-md-3 team-grids">
                    <div class="team-img">
                        <img class="img-responsive" src="images/web/no-user-img.png" alt="">
                        <div class="captn">
                            <ul class="top-links">
                                <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                                <li><a href="#"><i class="fa fa-twitter"></i></a></li>

                                <li><a href="#"><i class="fa fa-google-plus"></i></a></li>
                            </ul>

                        </div>
                    </div>
                    <div class="team-agile">
                        <h4>Kathy Anderson</h4>

                    </div>
                </div>
                <div class="col-md-3 team-grids">
                    <div class="team-img">
                        <img class="img-responsive" src="images/web/no-user-img.png" alt="">
                        <div class="captn">
                            <ul class="top-links">
                                <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                                <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                                <li><a href="#"><i class="fa fa-google-plus"></i></a></li>
                            </ul>

                        </div>
                    </div>
                    <div class="team-agile">
                        <h4>Jeffrey Deaton</h4>
                    </div>
                </div>
                <div class="clearfix"></div>
            </div>
        </div>
    </div>
    <div class="clearfix"></div>
    </div>
    </div>
    </div>
    <div class="forSpace"></div>
    <!--//content-inner-section-->

<?php include("footer.php"); ?>